$(document).ready(function(){

	"use strict";
	
  $('#toggle,#mb-toggle').click(function(){
    $('#mobile-menu').toggleClass('slide-left');
    $(this).toggleClass('active');
  });

	// Login
	$('#login-btn,#mb-login-btn').click(function(){
		$('.signup-box').slideUp();
		$('.login-box').stop().slideToggle('slow');
	});
	$('#signup-btn,#mb-signup-btn').click(function(){
		$('.login-box').slideUp();
		$('.signup-box').stop().slideToggle('slow');
	});
	
	
	
	//File Upload slider
$('.chooseFile').bind('change', function () {
	
  var filename = $(this).val();
	
  if (/^\s*$/.test(filename)) {
    $(this).parents(".file-uploads").removeClass('active');
     $(this).siblings(".placeholder").text("No file chosen..."); 
  }
  else {
    $(this).parents(".file-uploads").addClass('active');
    $(this).siblings(".placeholder").text(filename.replace("C:\\fakepath\\", "")); 
  }
	
});
	
	//----- OPEN
	$('[data-popup-open]').on('click', function(e) {
		var targeted_popup_class = jQuery(this).attr('data-popup-open');
		$('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

		e.preventDefault();
	});

	//----- CLOSE
	$('[data-popup-close]').on('click', function(e) {
		var targeted_popup_class = jQuery(this).attr('data-popup-close');
		$('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

		e.preventDefault();
	});
	
	//----User Top Menu
	$('#driver-mb-menu').click(function(){
		$('.cb-nav').slideToggle();
	});
	

	
	
});

$(window).resize(function(){
	
	'use strict';
	
	if ( $(window).width() > 739) {
	
		$('.cb-nav').removeAttr('style');

}
	
});


//wow animation
new WOW().init();




