<?php
// FID DETAIL
// ################################################################################## //
// 01 - LOGIN 
// 02 - Register 
// 03 - Prefiled profile
// 04 - Edit Profile
// 05 - Driver register
// 06 - Receving Driver lat long
// 07 - Subscription
// 08 - 
// 09 -
// 10 -
// 11 - Feature and Driveway
// 12 - Booking for truck
// 13 - GET BOOKING HISTORY IN DRIVER ACCOUNT
// 14 - SHOW BOOKING DETAIL IN DRIVER ACCOUNT
// 15 - USE PROMO CODE AT THE TIME OF ORDER
// 16 - NOTIFICATION / OFFER LIST FOR DRIVE
// 17 - SHOWING DETAIL OF BOOKING TO DRIVER
// 18 - BOOKING ACCECT & REJECT BY DRIVER
// 19 - NOTIFICATION
// 20 - BOOKING SUCCESS
// 21 - SHOW BOOKING DETAIL IN USER ACCOUNT
// 22 - SHOWING BOOKING DETAIL IN DRIVER ACCOUNT WITH BOOKING STATUS
// 23 - UPDATE BOOKING STATUS
// 24 - USER BOOKING LIST
// 25 - USER BOOKING DETAIL
// 26 - PAYMENT SUCCESSFULLY AND SEND JOB CLOSED OTP
// 27 - BOOKING COMPLETED BY CUSTOMER AFTER FILLING OTP
// 28 - DRYFT ELITE LIST
// 29 - DRYFT ELITE SUBSCRIPTION PAYEMENT
// 30 - SUBSCRIPTION PAYMENT STATUS
// 31 - TRACKING DETAIL IN USER END
// 32 - 
// 33 - UPDATE BOOKING STATUS
// 34 - ACCOUNT DELETE BY USER
// 35 - 
// 36 - CONTENT (ABOUT US, TERMS & CONDITIONS)
// 37 - RATING & REVIEW
// 38 - CHECK DRIVER EXIST
// 39 - DRIVER REGISTRATION
// 40 - ORDER CANCEL BY USER

// ################################################################################## //  
session_start();
//error_reporting(-1);
ob_start();
$baseURL = "http://".$_SERVER['HTTP_HOST'];
if($baseURL == "http://localhost"){
	$baseURL = 'http://localhost/dryftnow.com';
} else if($baseURL == "http://192.168.1.12"){
	$baseURL = 'http://192.168.1.12/dryftnow.com';
} else {
	$baseURL = "http://".$_SERVER['HTTP_HOST'] . '/dryft';
}
include '../lib/Connection.php';
include '../lib/Fileupload.php';
include '../lib/DBQuery.php';
include '../lib/Helpers.php';
include '../lib/ReturnMsg.php';
include '../lib/SentEmailSMS.php';
include '../lib/_appFunction_.php';

$connection = new Connection();
$helper = new Helpers();
$FileUpload = new Fileupload();
$dbAccess = new DBQuery();
$LoadMsg = new ReturnMsg();
$sentEmailSMS = new SentEmailSMS();
$appFunctions = new appFunction();

require '../twilio-php-master/Twilio/autoload.php';
use Twilio\Rest\Client;

require_once 'Googleapi/vendor/autoload.php';

// FCM API KEY
// ===========
$fcmApiKey = "AAAAHjs3cGE:APA91bGx3gtE9yBUSO266Q-ohjHmX0CwvKujZqemLuwX5r-_-1_VO2HMSZsUMoAPh9HW4ga3Y4FtXLAnjj6R4NfhvbMIAx4-2lT2-kICzbKK1G3eX36RK8lW9IrVor-8Bapqu5MA9KBg";
// ===========
		
date_default_timezone_set("America/Chicago");
$nowtime = date("Y-m-d H:i:s");
$hourMin = date("H:i");
$entryDate = date("Y-m-d H:i:s");
$currDate = date("Y-m-d");
$numDate = date("YmdHis");
$intDate = date("Ymd");

ini_set('max_execution_time','300'); 

$fid = '';
$dryftphone = '';
$infomail = '';
$twilio_account_sid = '';
$twilio_account_token = '';
$twilio_phone_number = '';

$errCode = '';

// read JSon input
$postData = json_decode(file_get_contents('php://input'));
// var_dump($postData);die;
 
// set json string to php variables
if(!empty($postData->{"fid"})){
	$fid = $postData->{"fid"};
}
//$fid = $appFunctions->validHTML($_POST['fid']);
//echo $fid;
//exit;
// FID = 1
if(empty($fid) AND !empty($_POST['fid'])){
	$fid = $appFunctions->validHTML($_POST['fid']);
}

//if (empty($_POST['fid'])){
if(empty($fid)){
	$tempStr = array('code' => "203" , 'success' => false, 'message' => "Function Id not recieved");
	echo json_encode($tempStr);
	exit;
// } else {
//	$fid = $appFunctions->validHTML($_POST['fid']);
}

$mySQL = "";
$mySQL = "SELECT `phone`, `infomail`, `twilio_account_sid`, `twilio_account_token`, `twilio_phone_number` FROM `config` WHERE isactive = '1'";
//echo $mySQL;
$rsTemp = $dbAccess->selectSingleStmt($mySQL);
if(!empty($rsTemp)){
	$dryftphone = $rsTemp['phone'];
	$infomail = $rsTemp['infomail'];
	$twilio_account_sid = $rsTemp['twilio_account_sid'];
	$twilio_account_token = $rsTemp['twilio_account_token'];
	$twilio_phone_number = $rsTemp['twilio_phone_number'];
}

if ($fid == '1'){
	//$userName = $appFunctions->validHTML(stripslashes(trim($postData->{'name'})));
	$usertype  = 'CUSTOMER';		//$appFunctions->validHTML(stripslashes(trim($postData->{'usertype'})));	
	$socialtoken = $appFunctions->validHTML(stripslashes(trim($postData->{'socialtoken'})));	
	$logintype  = $appFunctions->validHTML(stripslashes(trim($postData->{'logintype'})));
	$fcmtoken = $appFunctions->validHTML(stripslashes(trim($postData->{'fcmtoken'})));	
	$userMobile = $appFunctions->validHTML(stripslashes(trim($postData->{'mobile'})));
	$userEmail = $appFunctions->validHTML(stripslashes(trim($postData->{'userEmail'})));
	$userOTP = $appFunctions->validHTML(trim($postData->{'otp'}));

	if($logintype == 'MOBILE'){
		if(empty($userMobile)){
			$tempStr = array('code' => "203", "success" => false, 'message' => "Please enter your phone number.");
		} else {
			$mySQL = "";
			$mySQL = "SELECT COUNT(0) AS isExist FROM user WHERE isactive = '1' AND isdeleted = '0' AND (mobile = '".$appFunctions->validSQL($userMobile,"")."'";
			if(!empty($userEmail)){
				$mySQL .= " OR email = '".$appFunctions->validSQL($userEmail,"")."'";
			}
			$mySQL .= ")";	
			// echo $mySQL;
			$rsTempcnt = $dbAccess->selectSingleStmt($mySQL);
			if($rsTempcnt['isExist'] > '0'){
				// $otp = 9999;
				// $mySQL = "";
				// $mySQL = "UPDATE user SET otp = '".$appFunctions->validSQL($otp,"")."' WHERE mobile = '".$appFunctions->validSQL($userMobile,"")."'";
				// $dbAccess->queryExec($mySQL);
				
				if(empty($userOTP)){
    				$otp = rand(1009, 9999);
    				$mySQL = "";
    				$mySQL = "INSERT INTO otp (email, otp, date) VALUES ('".$appFunctions->validSQL($userMobile,"")."', '".$appFunctions->validSQL($otp,"")."', '".$nowtime."')";
    				//echo $mySQL;
    				$dbAccess->queryExec($mySQL);
    				
    				// ###############################################################
    				//`twilio_account_sid`, `twilio_account_token`, `twilio_phone_number`
    				$sid    = $twilio_account_sid;
    				$token  = $twilio_account_token;
    				$fromPhone = $twilio_phone_number;
    				// $toPhone = '+91'.$userMobile;		// TEST FOR INDIAN PHONE
					$toPhone = '+1'.$userMobile;
    				
					// $toPhone = '+918800329793';			// TEST FOR INDIAN PHONE
    				// $toPhone = '+18479096973';			// TEST FOR INDIAN PHONE
    				
    				$body = "Dear customer, Your one time password is " . $otp . " Do not share your OTP with anybody its confidential. From DRYFT"; 
    				try {			
	    				$twilio = new Client($sid, $token);
	    				$message = $twilio->messages
	    								//->create("+19133538609",
	    								->create($toPhone,
	    										array(
	    											'body' => $body,
	    											'from' => $fromPhone
	    										)
	    								);
	    			} catch (Exception $e) { 
				        // echo '<pre>';
				        // print_r($e);
				        $errCode = $e->getCode();
				    	$errMsg = $e->getMessage();
				    }
					// ############################################################### textnow
					if($errCode != ''){
				    	$tempStr = array('code' => "203", "success" => false, 'message' => $errMsg);
				    } else {
						$tempStr = array('code' => "200", "success" => true, 'message' => "OTP SENT...");
					}
				}
				
				if(!empty($userOTP)){
					$mySQL = "";
					// $mySQL = "SELECT mobile, otp FROM user WHERE mobile = '".$appFunctions->validSQL($userMobile,"")."' AND otp = '".$appFunctions->validSQL($userOTP,"")."'";
					$mySQL = "SELECT `email`, `otp` FROM `otp` WHERE `email` = '".$appFunctions->validSQL($userMobile,"")."' AND `otp` = '".$appFunctions->validSQL($userOTP,"")."'";
					$rsTemp = $dbAccess->selectSingleStmt($mySQL);
					if(empty($rsTemp)){
						$tempStr = array('code' => "203", "success" => false, 'message' => "Please enter valid OTP");
					} else {
						$mySQL = "";
						$mySQL = "SELECT `iduser`
							, `fullname`
							, `mobile`
							, `email`
							, (CASE WHEN profileimage <> '' THEN CONCAT('$baseURL', '/userphoto/', profileimage) ELSE '' END) AS `profileimage`
							, usertype
							, isactive
							, `isnotification`
							, `profileprivacy`
						FROM `user` WHERE isactive = '1' AND isdeleted = '0' AND (mobile = '".$appFunctions->validSQL($userMobile,"")."' ";
						if(!empty($userEmail)){
							$mySQL .= " OR email = '".$appFunctions->validSQL($userEmail,"")."'";
						}
						$mySQL .= ")";	
						//echo $mySQL;
						$rsTemp = $dbAccess->selectSingleStmt($mySQL);
						
						// UPDATE FCM TOKEN
						$mySQL = "";
						$mySQL = "UPDATE user SET fcmtoken = '".$appFunctions->validSQL($fcmtoken,"")."' WHERE iduser = '".$rsTemp['iduser']."'";
						$dbAccess->queryExec($mySQL);						
						
						$status = array('code' => "200", "success" => true, 'message' => "Login Successfully");
						$data = array("data"=>$rsTemp);
						$tempStr = array_merge($status, $data);
						
						$mySQL = "";
						$mySQL = "DELETE FROM `otp` WHERE email = '".$appFunctions->validSQL($userMobile,"")."'";
						//echo $mySQL;
						$dbAccess->queryExec($mySQL);
					}
				}
			} else { 
				$tempStr = array('code' => "203", "success" => false, 'message' => "Your phone number is not registered with us");
			}
		}
	} else {
		
		$errorcode = '';
		
		if(strtoupper($logintype) == 'FACEBOOK'){
			// $socialtoken = 'EAAIO2BZCLNN4BAHsRDIpijIZBtzVGoZCsJY8q1MrfjZCzuQkFuXbCuswqSSycrhdLBnPmFUE4piu1bU6hN9Ule8Nczg4AE9ASUXtS24jQCO0Y8ZCSOuUABdYWKavw7z7QZCDmiou81q2usbVjFdS5KNFBXXUZA5bJTvBS3CMD72xsbbdKlTIfkwR0v3XCZAZBXXMCxDhDAUXMxAOdcPZAqdUcbaHIYZAZCQ3sTUZD';
			
			$graph_url = "https://graph.facebook.com/me/";
			$postData = "fields=id,first_name,last_name,name,name_format,picture,email"
				. "&access_token=" .$socialtoken;

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $graph_url);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

			$output = curl_exec($ch);
			curl_close($ch);
			// var_dump($output);
			// echo '<br>';
			// exit;
			
			$payload = json_decode($output, JSON_PRETTY_PRINT);
		
			$email = $payload['email'];
			$name = $payload['name'];
			$errorcode = $payload['error']['code'];
		} else if(strtoupper($logintype) == 'GOOGLE'){
			//$socialtoken = "";
			$client = new Google_Client(['client_id' => $CLIENT_ID]);  // Specify the CLIENT_ID of the app that accesses the backend
			$payload = $client->verifyIdToken($socialtoken);
			
			if ($payload){
				$email = $payload['email'];
				//$userID = $payload['sub'];
				$name = $payload['name'];
				$Image =  $payload['picture'];
			} else {
				$errorcode = '203';
			}
		} 
		
		if ($payload AND ($errorcode != '190' AND $errorcode != '203')){
			$mySQL = "";
			$mySQL = "SELECT COUNT(0) AS isExist FROM user WHERE isactive = '1'"; 
			//$mySQL .= " AND (mobile = '".$appFunctions->validSQL($userMobile,"")."'";
			//if(!empty($email)){
				//$mySQL .= " OR ";
				$mySQL .= " AND email = '".$appFunctions->validSQL($email,"")."'";
			//}
			//$mySQL .= ")";	
			//echo $mySQL;
			$rsTempcnt = $dbAccess->selectSingleStmt($mySQL);
			if($rsTempcnt['isExist'] > 0){
				$mySQL = "";
				$mySQL = "UPDATE user SET fcmtoken = '".$appFunctions->validSQL($fcmtoken,"")."' WHERE email = '".$appFunctions->validSQL($email,"")."'";
				$dbAccess->queryExec($mySQL);
						
				$mySQL = "";
				$mySQL = "SELECT `iduser`
					, `fullname`
					, `mobile`
					, `email`
					, (CASE WHEN profileimage <> '' THEN CONCAT('$baseURL', '/userphoto/', profileimage) ELSE '' END) AS `profileimage`
					, usertype
					, `isnotification`
					, `profileprivacy`
					, isactive
				FROM `user` WHERE isactive = '1'";		// AND (mobile = '".$appFunctions->validSQL($userMobile,"")."'";
				//if(!empty($email)){
					//$mySQL .= " OR ";
					$mySQL .= " AND email = '".$appFunctions->validSQL($email,"")."'";
				//}
				//$mySQL .= ")";	
				//echo $mySQL;
				$rsTemp = $dbAccess->selectSingleStmt($mySQL);
				$status = array('code' => "200", "success" => true, 'message' => "Login Successfully");
				$data = array("data"=>$rsTemp);
				$tempStr = array_merge($status, $data);
			} else {
				$mySQL = "";
				$mySQL = "INSERT INTO user (";
				$mySQL .= "usertype";
				$mySQL .= ",fullname";			
				$mySQL .= ", email";
				$mySQL .= ", mobile";			
				$mySQL .= ", fcmtoken";
				$mySQL .= ", isactive";
				$mySQL .= " ) VALUES (";
				$mySQL .= "'".$appFunctions->validSQL($usertype,"")."'";
				$mySQL .= ",'".$appFunctions->validSQL($name,"")."'";
				$mySQL .= ", '".$appFunctions->validSQL($email,"")."'";
				$mySQL .= ", '".$appFunctions->validSQL($userMobile,"")."'";
				$mySQL .= ", '".$appFunctions->validSQL($fcmtoken,"")."'";
				$mySQL .= ", '1'";
				$mySQL .= ")";
				//echo $mySQL;
				$dbAccess->queryExec($mySQL);

				$mySQL = "";
				$mySQL = "SELECT `iduser`
					, `fullname`
					, `mobile`
					, `email`
					, (CASE WHEN profileimage <> '' THEN CONCAT('$baseURL', '/userphoto/', profileimage) ELSE '' END) AS `profileimage`
					, usertype
					, `isnotification`
					, `profileprivacy`
					, isactive
				FROM `user` WHERE isactive = '1'";		// AND (mobile = '".$appFunctions->validSQL($userMobile,"")."' ";
				//if(!empty($email)){
					//$mySQL .= " OR ";
					$mySQL .= " AND email = '".$appFunctions->validSQL($email,"")."'";
				//}
				//$mySQL .= ")";	
				//echo $mySQL;
				$rsTemp = $dbAccess->selectSingleStmt($mySQL);
				$status = array('code' => "200", "success" => true, 'message' => "Login Successfully");
				$data = array("data"=>$rsTemp);
				$tempStr = array_merge($status, $data);
			}
		} else {	
			$tempStr = array('code' => "203", "success" => false, 'message' => "invalid token"); 
		}
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '2'){
	$userName = $appFunctions->validHTML(stripslashes(trim($postData->{'name'})));
	$usertype  = $appFunctions->validHTML(stripslashes(trim($postData->{'usertype'})));	
	//$socialtoken = $appFunctions->validHTML(stripslashes(trim($postData->{'socialtoken'})));	
	//$logintype  = $appFunctions->validHTML(stripslashes(trim($postData->{'logintype'})));
	$fcmtoken = $appFunctions->validHTML(stripslashes(trim($postData->{'fcmtoken'})));	
	$userMobile = $appFunctions->validHTML(stripslashes(trim($postData->{'mobile'})));
	$userEmail = $appFunctions->validHTML(stripslashes(trim($postData->{'userEmail'})));
	$userOTP = $appFunctions->validHTML(trim($postData->{'otp'}));
	
	$mySQL = "";
	//$mySQL = "SELECT COUNT(0) AS isExist FROM user WHERE isactive = '1' AND (mobile = '".$appFunctions->validSQL($userMobile,"")."' ";
	$mySQL = "SELECT mobile, email FROM user WHERE isactive = '1' AND isdeleted = '0' AND (mobile = '".$appFunctions->validSQL($userMobile,"")."'";
	if(!empty($userEmail)){
		$mySQL .= " OR email = '".$appFunctions->validSQL($userEmail,"")."'";
	}
	$mySQL .= ")";	
	// echo $mySQL;
	$rsTempcnt = $dbAccess->selectSingleStmt($mySQL);
	if($rsTempcnt['mobile'] == $userMobile AND $rsTempcnt['email'] == $userEmail){
		$tempStr = array("code"=>"203","success"=>"false","message"=>"Your mobile & email already registered!!!");
	} else if($rsTempcnt['mobile'] == $userMobile){
		$tempStr = array("code"=>"203","success"=>"false","message"=>"Your mobile already registered!!!");
	} else if($rsTempcnt['email'] == $userEmail){
		$tempStr = array("code"=>"203","success"=>"false","message"=>"Your email already registered!!!");
	} else {
		if(empty($userOTP)){
			// $otp = 9999;
			$otp = rand(1009, 9999);
			$mySQL = "";
			$mySQL = "INSERT INTO otp (email, otp, date) VALUES ('".$appFunctions->validSQL($userMobile,"")."', '".$appFunctions->validSQL($otp,"")."', '".$nowtime."')";
			//echo $mySQL;
			$dbAccess->queryExec($mySQL);
			
			// ###############################################################
			//`twilio_account_sid`, `twilio_account_token`, `twilio_phone_number`
			$sid    	= $twilio_account_sid;
			$token  	= $twilio_account_token;
			$fromPhone 	= $twilio_phone_number;
			// $toPhone = '+91'.$userMobile;		// TEST FOR INDIAN PHONE
			//$toPhone = '+918800329793';				// TEST FOR INDIAN PHONE
			// $toPhone = '+18479096973';				// TEST FOR INDIAN PHONE
			$toPhone 	= '+1'.$userMobile;
			
			$body = "Dear customer, Your one time password is " . $otp . " Do not share your OTP with anybody its confidential. From DRYFT"; 
						
			/*$twilio = new Client($sid, $token);
			$message = $twilio->messages
							//->create("+19133538609",
							->create($toPhone,
									array(
										'body' => $body,
										'from' => $fromPhone
									)
							);
			//print_r($twilio);
			// ###############################################################*/
			try {			
				$twilio = new Client($sid, $token);
				$message = $twilio->messages
								//->create("+19133538609",
								->create($toPhone,
										array(
											'body' => $body,
											'from' => $fromPhone
										)
								);
			} catch (Exception $e) {
		        // echo '<pre>';
		        // print_r($e);
		        $errCode = $e->getCode();
		    	$errMsg = $e->getMessage();
		    }
			// ###############################################################
			
			if($errCode != ''){
		    	$tempStr = array('code' => "203", "success" => false, 'message' => $errMsg);
		    } else {
				$tempStr = array('code' => "200", "success" => true, 'message' => "OTP SENT...");
			}
		} else if(!empty($userOTP)){
			$mySQL = "";
			$mySQL = "SELECT email, otp FROM otp WHERE email = '".$appFunctions->validSQL($userMobile,"")."' AND otp = '".$appFunctions->validSQL($userOTP,"")."'";
			//echo $mySQL;
			$rsTemp = $dbAccess->selectSingleStmt($mySQL);
			if(empty($rsTemp)){
				$tempStr = array('code' => "203", "success" => false, 'message' => "Please enter valid OTP");
			} else {
				$mySQL = "";
				$mySQL = "INSERT INTO user (";
				$mySQL .= " usertype";
				$mySQL .= ",fullname";			
				$mySQL .= ", email";
				$mySQL .= ", mobile";			
				$mySQL .= ", fcmtoken";
				$mySQL .= ", isactive";
				$mySQL .= " ) VALUES (";
				$mySQL .= "'customer'";
				$mySQL .= ",'".$appFunctions->validSQL($userName,"")."'";
				$mySQL .= ", '".$appFunctions->validSQL($userEmail,"")."'";
				$mySQL .= ", '".$appFunctions->validSQL($userMobile,"")."'";
				$mySQL .= ", '".$appFunctions->validSQL($fcmtoken,"")."'";
				$mySQL .= ", '1'";
				$mySQL .= ")";
				//echo $mySQL .'<br>';
				$dbAccess->queryExec($mySQL);

				$mySQL = "";
				$mySQL = "SELECT `iduser`
					, `fullname`
					, `mobile`
					, `email`
					, (CASE WHEN profileimage <> '' THEN CONCAT('$baseURL', '/userphoto/', profileimage) ELSE '' END) AS `profileimage`
					, usertype
					, `isnotification`
					, `profileprivacy`
					, isactive
				FROM `user` WHERE isactive = '1' AND isdeleted = '0' AND (mobile = '".$appFunctions->validSQL($userMobile,"")."'";
				if(!empty($userEmail)){
					$mySQL .= " OR email = '".$appFunctions->validSQL($userEmail,"")."'";
				}
				$mySQL .= ")";	
				//echo $mySQL .'<br>';
				$rsTemp = $dbAccess->selectSingleStmt($mySQL);
				$status = array('code' => "200", "success" => true, 'message' => "Registered Successfully");
				$data = array("data"=>$rsTemp);
				$tempStr = array_merge($status, $data);
				//$tempStr = array('code' => "200", "success" => true, 'message' => "acoount created successfully");
				
				$mySQL = "";
				$mySQL = "DELETE FROM `otp` WHERE email = '".$appFunctions->validSQL($userMobile,"")."'";
				//echo $mySQL;
				$dbAccess->queryExec($mySQL);
				
				// ###############################################################
				//`twilio_account_sid`, `twilio_account_token`, `twilio_phone_number`
				$sid    	= $twilio_account_sid;
				$token  	= $twilio_account_token;
				$fromPhone 	= $twilio_phone_number;
				// $toPhone = '+91'.$userMobile;		// TEST FOR INDIAN PHONE
				//$toPhone = '+918800329793';				// TEST FOR INDIAN PHONE
				// $toPhone = '+18479096973';				// TEST FOR INDIAN PHONE
				$toPhone 	= '+1'.$userMobile;
				
				$body = "Dear " . $userName .", Thanks for choosing DRYFT services"; 
							
				/*$twilio = new Client($sid, $token);
				$message = $twilio->messages
								//->create("+19133538609",
								->create($toPhone,
										array(
											'body' => $body,
											'from' => $fromPhone
										)
								);
				//print_r($twilio);
				// ###############################################################*/
				try {			
					$twilio = new Client($sid, $token);
					$message = $twilio->messages
									//->create("+19133538609",
									->create($toPhone,
											array(
												'body' => $body,
												'from' => $fromPhone
											)
									);
				} catch (Exception $e) {
		        	// echo '<pre>';
			        // print_r($e);
			        $errCode = $e->getCode();
			    	$errMsg = $e->getMessage();
			    }
				// ###############################################################
				if($errCode != ''){
			    	$tempStr = array('code' => "203", "success" => false, 'message' => $errMsg);
			    }
			}	
		} 
	}	
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '3'){ 
	$iduser = $appFunctions->validHTML(trim($postData->{'iduser'}));
	
	$mySQL = "";
	$mySQL = "SELECT `iduser`
		, (CASE WHEN `fullname` <> '' THEN `fullname` ELSE '' END) AS fullname
		, (CASE WHEN `address` <> '' THEN `address` ELSE '' END) AS address
		, (CASE WHEN `mobile` <> '' THEN `mobile` ELSE '' END) AS mobile
		, (CASE WHEN `email` <> '' THEN `email` ELSE '' END) AS email
		, (CASE WHEN profileimage <> '' THEN CONCAT('$baseURL', '/userphoto/', profileimage) ELSE '' END) AS `profileimage`
		, isnotification
		, profileprivacy
		FROM `user` U WHERE iduser = '".$appFunctions->validSQL($iduser,"")."'";
	//echo $mySQL;
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	if(!empty($rsTemp)){
		$status = array('code' => "200", "success" => true, 'message' => "Data Found");
		$data = array("data"=>$rsTemp);
		$tempStr = array_merge($status, $data);
	} else {
		$tempStr = array('code' => "203", "success" => false, 'message' => "Record not found");
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if($fid == '4'){
	$iduser = $appFunctions->validHTML(trim($_POST['iduser']));
	$fullname = $appFunctions->validHTML(trim($_POST['fullname']));
	$email = $appFunctions->validHTML(stripslashes(trim($_POST['email'])));
	$mobile = $appFunctions->validHTML(stripslashes(trim($_POST['mobile'])));
	$address = $appFunctions->validHTML(stripslashes(trim($_POST['address'])));
	$mySQL = "";
	$mySQL = "SELECT COUNT(0) AS isExist FROM user WHERE (mobile='".$mobile."' OR email='".$email."')";
//	echo $mySQL;die();
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	
	if ($rsTemp['isExist']>0)
	{
		$target_dir = "../userphoto/";
		$targetProfileimage = basename($_FILES["profileimage"]["name"]);
		//$targetProfilebackground = basename($_FILES["profilebackground"]["name"]);

		if(!empty($targetProfileimage)){
			move_uploaded_file($_FILES["profileimage"]["tmp_name"], $target_dir . $iduser . '_'. str_replace('.php','',$targetProfileimage));
		}
		
		$mySQL = "";
		$mySQL = "UPDATE user SET 
			fullname = '".$appFunctions->validSQL($fullname,"")."'
			, email = '".$appFunctions->validSQL($email,"")."'
			, mobile = '".$appFunctions->validSQL($mobile,"")."'
			, address = '".$appFunctions->validSQL($address,"")."'";
		if(!empty($targetProfileimage)){
			$mySQL .= ", profileimage = '".$appFunctions->validSQL($iduser . '_'. $targetProfileimage,"")."'";
		}
		
		$mySQL .= " WHERE iduser = '".$appFunctions->validSQL($iduser,"")."'";
		//echo $mySQL;
		$dbAccess->queryExec($mySQL);
			
		
		$tempStr = array('code' => "200", "success" => true, 'message' => "Profile updated");
	} else {
		$tempStr = array('code'=>"200",'success'=>false,'message'=>"mobile or email already exit");
	}
	
	echo json_encode($tempStr, JSON_PRETTY_PRINT); 
} else if($fid == '6'){
	$lat = $appFunctions->validHTML(stripslashes(trim($postData->{'lat'})));
	$long = $appFunctions->validHTML(stripslashes(trim($postData->{'long'})));
	$iduser = $appFunctions->validHTML(stripslashes(trim($postData->{'iduser'})));

	$mySQL = "";
	$mySQL = "UPDATE user SET 
		lat = '".$appFunctions->validSQL($lat,"")."'
		, lng = '".$appFunctions->validSQL($long,"")."'";	
	$mySQL .= " WHERE iduser = '".$appFunctions->validSQL($iduser,"")."'";
	//echo $mySQL;
	$dbAccess->queryExec($mySQL);
	$tempStr = array('code' => "200", "success" => true, 'message' => "lat long updatetd");
	echo json_encode($tempStr, JSON_PRETTY_PRINT); 
} else if($fid == '7'){

	$iduser = $appFunctions->validHTML(stripslashes(trim($postData->{'iduser'})));
	$userEmail = $appFunctions->validHTML(stripslashes(trim($postData->{'userEmail'})));
	$subscribe = $appFunctions->validHTML(stripslashes(trim($postData->{'subscribe'})));

 	if(empty($userEmail)) {

		$tempStr = array('code' => "203", "success" => false, 'message' => "Missing email address");

	} else {
		$mySQL = "";
		$mySQL = "SELECT count(0) AS iduserexit From user WHERE email = '".$appFunctions->validSQL($userEmail,"")."' ";
		//echo $mySQL; 
		$rsTemp = $dbAccess->selectSingleStmt($mySQL);		
		if($rsTemp['iduserexit']==0){
			$tempStr = array('code' => "200", "success" => true, 'message' => "Wrong email provided");
		}else {
			$mySQL = "";
			$mySQL = "SELECT subscribe From user WHERE email = '".$appFunctions->validSQL($userEmail,"")."' ";
			//echo $mySQL;
			$rsTemp = $dbAccess->selectSingleStmt($mySQL);
			
			if(!is_null($rsTemp['subscribe'])){
				$tempStr = array('code' => "200", "success" => true, 'message' => "Already subscribed");
			}else{
					$mySQL = "";
					$mySQL = "UPDATE user SET 
					subscribe = '".$appFunctions->validSQL($subscribe,"")."'";
					$mySQL .= " WHERE email = '".$appFunctions->validSQL($userEmail,"")."'";
					//echo $mySQL;
					$Exexcut = $dbAccess->queryExec($mySQL);
					$to = $userEmail;
					$subject = "Dryft notifications";
					$headers = "MIME-Version: 1.0" . "\r\n";
					$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
					$headers .= 'From: '. $infomail . "\r\n";
					$message = "Name: You have successfully subscribed dryft notifications";

			if(@mail($to,$subject,$message,$headers)){
					$tempStr = array('code' => "200", "success" => true, 'message' => "Thank you for Subscription");
				} 
			}
		}
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT); 
} else if($fid == '8'){
	$mySQL = "";
	$mySQL = "SELECT city FROM user WHERE usertype = 'DRIVER' GROUP BY city";
	$rsTemp = $dbAccess->selectData($mySQL);
	$status = array('code' => "200", "success" => true, 'message' => "Data Found");
	$data = array("data"=>$rsTemp);
	$tempStr = array_merge($status,$data);
	echo json_encode($tempStr, JSON_PRETTY_PRINT); 
} else if($fid == '9'){
	$lat = $appFunctions->validHTML(stripslashes(trim($postData->{'lat'})));
	$long = $appFunctions->validHTML(stripslashes(trim($postData->{'long'})));
	$iduser = $appFunctions->validHTML(stripslashes(trim($postData->{'iduser'})));
	
	$mySQL = "";
	$mySQL = "SELECT city From user WHERE usertype='driver' group by city";
	$rsTemp = $dbAccess->selectData($mySQL);
	$status = array('code' => "200", "success" => true, 'message' => "Data Found");
	$data = array("data"=>$rsTemp);
	$tempStr = array_merge($status,$data);
	echo json_encode($tempStr, JSON_PRETTY_PRINT); 
} else if($fid == '10'){
	$center_lat = $appFunctions->validHTML(stripslashes(trim($postData->{'lat'})));
	$center_lng = $appFunctions->validHTML(stripslashes(trim($postData->{'lng'})));
	//$iduser = $appFunctions->validHTML(stripslashes(trim($postData->{'iduser'})));
	$radius = 100;
	$mySQL = sprintf("SELECT  iduser,lat, lng, ( 3959 * acos( cos( radians('%s') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( lat ) ) ) ) AS distance 
	FROM user WHERE usertype = 'DRIVER' HAVING distance < '%s' ORDER BY distance LIMIT 0 , 20",
		($center_lat),
		($center_lng),
		($center_lat),
		($radius));
	//echo $mySQL; 
	$rsTemp = $dbAccess->selectData($mySQL);
	if(!empty($rsTemp)){
		$status = array('code' => "200", "success" => true, 'message' => "Truck Found");
		$data = array("data"=>$rsTemp);
		$tempStr = array_merge($status,$data);
	} else {
		$tempStr = array('code' => "203", "success" => false, 'message' => "No Truck Found");
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT); 
} else if ($fid == '11'){
	$mySQL = "";
	$mySQL = "SELECT idfeatures, name, (CASE WHEN iscall = 'Call Now' THEN 1 ELSE 0 END) AS iscall FROM features WHERE isdefault = '0' AND isactive = '1'";
	$rsTemp = $dbAccess->selectData($mySQL);
	$feature = array("feature" => $rsTemp);	
		
	$mySQL = "";
	$mySQL = "SELECT iddrivewaytype, name FROM drivewaytype WHERE isactive = '1'";
	$rsTemp = $dbAccess->selectData($mySQL);
	$driveway = array("driveway" => $rsTemp);

	$mySQL = "";
	$mySQL = "SELECT 
		(CASE WHEN areafrom > '50000' THEN 'Call now' ELSE CONCAT(areafrom, '-', areato) END) AS area FROM featuresareaprice WHERE isactive = '1'";
	$rsTemp = $dbAccess->selectData($mySQL);
	$areaSquareFeet = array("area" => $rsTemp);
	
	$mySQL = "";
	$mySQL = "SELECT 
		(CASE WHEN snowdepthfrom > '8' THEN 'Call now' ELSE CONCAT(snowdepthfrom, '-', snowdepthto) END) AS depth FROM featuresdepthprice WHERE isactive = '1'";
	$rsTemp = $dbAccess->selectData($mySQL);
	$areaDepth = array("depth" => $rsTemp);
		
	$mySQL = "";
	$mySQL = "SELECT 
		(CASE WHEN areafrom > '50000' THEN 'Call now' ELSE CONCAT(areafrom, '-', areato) END) AS area  FROM featuresareaprice WHERE isactive = '1'";
	$rsTemp = $dbAccess->selectData($mySQL);
	$areaSquareFeet = array("area" => $rsTemp);

	$mySQL = "";
	$mySQL = "SELECT idextracharge, CONCAT(name, ' ', amount) AS nameamount FROM featuresextracharge WHERE isactive = '1'";
	$rsTemp = $dbAccess->selectData($mySQL);
	
	$additional = array();
	if(!empty($rsTemp)){
		$additional = array("additional" => $rsTemp);
	}
	
	$dryftphone = array("dryftphone" => $dryftphone);
	
	//$featureadditional = array_merge($feature,$additional);
	//$featuredriveway = array_merge($featureadditional,$driveway);
	//$data = array("data"=>$featuredriveway);
	
	$data = array("data" => array_merge($feature, $driveway, $areaSquareFeet, $areaDepth, $additional, $dryftphone));
	$status = array('code' => "200", "success" => true, 'message' => "Services");
	$tempStr = array_merge($status, $data);

	echo json_encode($tempStr, JSON_PRETTY_PRINT); 
} else if ($fid == '12'){
	$iduser = $appFunctions->validHTML(trim($postData->{'iduser'}));
	$bookingdate = $appFunctions->validHTML(trim($postData->{'bookingdate'}));
	$starttime = $appFunctions->validHTML(trim($postData->{'starttime'}));
	$idfeatures = $appFunctions->validHTML(trim($postData->{'idfeatures'}));
	$iddrivewaytype = $appFunctions->validHTML(trim($postData->{'iddrivewaytype'}));
	$idextracharge = $appFunctions->validHTML(trim($postData->{'idextracharge'}));
	$snowdepth = $appFunctions->validHTML(trim($postData->{'snowdepth'}));
	$areasize = $appFunctions->validHTML(trim($postData->{'areasize'}));
	$destinationlat = $appFunctions->validHTML(trim($postData->{'destinationlat'}));
	$destinationlng = $appFunctions->validHTML(trim($postData->{'destinationlng'}));
	$destinationaddress = $appFunctions->validHTML(trim($postData->{'destinationaddress'}));
	
	$radius = 100;
	$mySQL = sprintf("SELECT iduser, lat, lng, ( 3959 * acos( cos( radians('%s') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( lat ) ) ) ) AS distance 
	FROM user HAVING distance < '%s' ORDER BY distance LIMIT 0 , 20",
		($destinationlat),
		($destinationlng),
		($destinationlat),
		($radius));
	//echo $mySQL; 
	$rsTemp = $dbAccess->selectData($mySQL);
	if(empty($rsTemp)){
		$tempStr = array('code' => "203", "success" => false, 'message' => "Trucks not available");
	} else {
		$resultArr = array();
		$totalPrice = 0;
		
		$arrdestinationaddress = array("address" => $destinationaddress);
		
		$mySQL = "";
		$mySQL .= "SELECT price";
		$mySQL .= " FROM pricedetail WHERE '".$appFunctions->validSQL($snowdepth,"")."' BETWEEN snowdepthfrom";
		$mySQL .= " AND snowdepthto AND CONCAT(areafrom, '-', areato) = '".$appFunctions->validSQL($areasize,"")."'";
		//echo $mySQL;
		$rsTemp = $dbAccess->selectSingleStmt($mySQL);
		$sPrice = $rsTemp['price'];
		$serviceprice = array("price" => $sPrice);	
		$totalPrice = $totalPrice + $rsTemp['price'];
		
		$featuresextracharge = '';
		$amount = '0';
		$amounttype = '';
		$extracharge = array("additional" => array("nameamount" => '', "amount" => '', "amounttype" => ''));	
		
		if(!empty($idextracharge)){
			$mySQL = "";
			$mySQL .= "SELECT idextracharge, name, amount, amounttype";
			$mySQL .= " FROM featuresextracharge WHERE isactive = '1' AND idextracharge = '".$appFunctions->validSQL($idextracharge,"")."'";
			//echo $mySQL;
			$rsTempExPrice = $dbAccess->selectSingleStmt($mySQL);
			
			if(!empty($rsTempExPrice)){
				if (strtoupper($rsTempExPrice['amounttype']) == 'PERCENT'){
					$nameamount = $rsTempExPrice['name'] . ' ' .$rsTempExPrice['amount']. '%';
				} else {
					$nameamount = '$' . $rsTempExPrice['name'];
				}
				$featuresextracharge = $rsTempExPrice['name'];
				$amount = $rsTempExPrice['amount'];
				$amounttype = $rsTempExPrice['amounttype'];
				$extracharge = array("additional" => array("nameamount" => $nameamount, "amount" => $amount, "amounttype" => $amounttype));	
			} else {
				$amount = '0';
			}
		}
		
		$featuresextrachargeamt = $amount;
		
		// DEFAULT FEATURES
		$mySQL = "";
		$mySQL = "SELECT idfeatures, name";
		$mySQL .= " FROM features WHERE isdefault = '1' AND isactive = '1'";
		//echo $mySQL;
		$rsTemp = $dbAccess->selectData($mySQL);
		
		foreach($rsTemp AS $rsTempVal){
			//$idfeatures2 = $rsTempVal['idfeatures'];
			$name = $rsTempVal['name'];
			$resultArr['feature'][$rsTempVal['idfeatures']] = array("idfeatures" => $rsTempVal['idfeatures'], "name" => $name, "featureprice" => '');
		}
		
		// SELECT PAID FREATURES
		if(!empty($idfeatures)){
			$mySQL = "";
			$mySQL = "SELECT idfeatures, name";
			$mySQL .= " FROM features WHERE isdefault = '0' AND isactive = '1'";
			$mySQL .= " AND idfeatures IN(".$appFunctions->validSQL($idfeatures,"").")";
			//echo $mySQL;
			$rsTemp = $dbAccess->selectData($mySQL);
			
			foreach($rsTemp AS $rsTempVal){
				$idfeatures1 = $rsTempVal['idfeatures'];
				$name = $rsTempVal['name'];
				$resultArr['feature'][$idfeatures1] = array("idfeatures" => $idfeatures1, "name" => $name);
				if(!empty($snowdepth) AND strtoupper($name) == 'STEPS IN BACK'){
					$mySQL = "";
					$mySQL = "SELECT price FROM featuresdepthprice WHERE 
						idfeatures = '".$appFunctions->validSQL($idfeatures1,"")."' 
						AND '".$appFunctions->validSQL($snowdepth,"")."' 
						BETWEEN snowdepthfrom AND snowdepthto";
					//echo $mySQL;
					$rsTempDepthPrice = $dbAccess->selectSingleStmt($mySQL);
					if(!empty($rsTempDepthPrice)){
						$resultArr['feature'][$idfeatures1]['featureprice'] = $rsTempDepthPrice['price'];
						$totalPrice = $totalPrice + $rsTempDepthPrice['price'];
					} else {
						$resultArr['feature'][$idfeatures1]['featureprice'] = '';
					}
				} else if(!empty($areasize) AND strtoupper($rsTempVal['name']) == 'SALT DRIVEWAY'){
					$mySQL = "";
					$mySQL = "SELECT price FROM featuresareaprice WHERE idfeatures = '".$appFunctions->validSQL($idfeatures1,"")."' AND CONCAT(areafrom, '-', areato) = '".$appFunctions->validSQL($areasize,"")."'";
					$rsTempAreaPrice = $dbAccess->selectSingleStmt($mySQL);
					if(!empty($rsTempAreaPrice)){
						$resultArr['feature'][$idfeatures1]['featureprice'] = $rsTempAreaPrice['price'];
						$totalPrice = $totalPrice + $rsTempAreaPrice['price'];
						if(!empty($amount)){
							if(strtoupper($amounttype) == 'PERCENT'){
								$totalPrice = $totalPrice + ($rsTempAreaPrice['price']*$amount)/100; 
							} else {
								$totalPrice = $totalPrice + $amount;
							}
						}
					} else {
						$resultArr['feature'][$idfeatures1]['featureprice'] = '';
					}
				} else {
					$resultArr['feature'][$idfeatures1]['featureprice'] = '';
				}
			}
		}
		
		$totalamount = $totalPrice;
		$arrtotalPrice = array("totalprice" => number_format($totalamount,2));
		
		$resultArr['feature'] = array_values($resultArr['feature']);
		
		// #############################
		// INSERT DATA IN BOOKING TABLES
		// #############################
		$mySQL = "";
		$mySQL = "SELECT fullname, email, mobile FROM user WHERE iduser = '".$appFunctions->validSQL($iduser,"")."'";
		$rsTemp = $dbAccess->selectSingleStmt($mySQL);
		
		$fullname = $rsTemp['fullname'];
		$email = $rsTemp['email'];
		$mobile = $rsTemp['mobile'];
				
		$t = explode(" ",microtime());
		$bookingno = date($numDate,$t[1]) . substr((string)$t[0],-4,3);
		
		//echo '=== ' . $featuresextrachargeamt;
		
		$mySQL = "";
		$mySQL = "INSERT INTO `booking` SET";
		$mySQL .= " iduser = '".$appFunctions->validSQL($iduser,"")."'";
		$mySQL .= ", bookingno = '".$appFunctions->validSQL($bookingno,"")."'";
		$mySQL .= ", bookingdate = '".$appFunctions->validSQL($bookingdate,"")."'";
		$mySQL .= ", starttime = '".$appFunctions->validSQL($starttime,"")."'";
		$mySQL .= ", fullname = '".$appFunctions->validSQL($fullname,"")."'";
		$mySQL .= ", mobile = '".$appFunctions->validSQL($mobile,"")."'";
		$mySQL .= ", email = '".$appFunctions->validSQL($email,"")."'";
		$mySQL .= ", destinationaddress = '".$appFunctions->validSQL($destinationaddress,"")."'";
		//$mySQL .= ", destinationcity = '".$appFunctions->validSQL($destinationaddress,"")."'";
		//$mySQL .= ", destinationstate = '".$appFunctions->validSQL($destinationaddress,"")."'";
		//$mySQL .= ", destinationzipcode = '".$appFunctions->validSQL($destinationaddress,"")."'";
		$mySQL .= ", destinationlatlng = '".$appFunctions->validSQL($destinationlat.','.$destinationlng,"")."'";
		$mySQL .= ", area = '".$appFunctions->validSQL($areasize,"")."'";
		$mySQL .= ", price = '".$appFunctions->validSQL($sPrice,"")."'";
		$mySQL .= ", totalamount = '".$appFunctions->validSQL($totalamount,"")."'";
		$mySQL .= ", featuresextracharge = '".$appFunctions->validSQL($featuresextracharge,"")."'";
		$mySQL .= ", featuresextrachargeamt = '".$featuresextrachargeamt."'";
		$mySQL .= ", featuresextrachargetype = '".$appFunctions->validSQL($amounttype,"")."'";
		$mySQL .= ", bookedbyuser = '1'";
		$mySQL .= ", isactive = '0'";
		//echo $mySQL;
		//exit;
		$dbAccess->queryExec($mySQL);
		$idbooking = $dbAccess->LastId();
		
		$arrbooking = array("idbooking" => $idbooking, "bookingno" => $bookingno);
		
		// INSERT IN BOOKING DETAIL TABLE
		
		// DEFAULT FEATURES
		$mySQL = "";
		$mySQL = "SELECT idfeatures, name";
		$mySQL .= " FROM features WHERE isdefault = '1' AND isactive = '1'";
		//echo $mySQL;
		$rsTemp1 = $dbAccess->selectData($mySQL);
		
		foreach($rsTemp1 AS $rsTempVal1){
			$idfeatures1 = $rsTempVal1['idfeatures'];
			$name1 = $rsTempVal1['name'];
			
			$mySQLBookingDetail1 = "";
			$mySQLBookingDetail1 = "INSERT INTO bookingdetail SET";
			$mySQLBookingDetail1 .= " idbooking = '".$appFunctions->validSQL($idbooking,"")."'";
			$mySQLBookingDetail1 .= ", idfeatures = '".$appFunctions->validSQL($idfeatures1,"")."'";
			$mySQLBookingDetail1 .= ", featurename = '".$appFunctions->validSQL($name1,"")."';";
			//echo $mySQLBookingDetail1;
			$dbAccess->queryExec($mySQLBookingDetail1);
		}
		
		// PAID FEATURES
		if(!empty($idfeatures)){
			$mySQL = "";
			$mySQL = "SELECT idfeatures, name";
			$mySQL .= " FROM features WHERE isdefault = '0' AND isactive = '1' AND idfeatures IN(".$appFunctions->validSQL($idfeatures,"").")";
			//echo $mySQL .'<hr>';
			$rsTemp1 = $dbAccess->selectData($mySQL);
			
			foreach($rsTemp1 AS $rsTempVal1){
				$idfeatures = $rsTempVal1['idfeatures'];
				$name = $rsTempVal1['name'];
				
				$mySQLBookingDetail = "";
				$mySQLBookingDetail = "INSERT INTO bookingdetail SET";
				$mySQLBookingDetail .= " idbooking = '".$appFunctions->validSQL($idbooking,"")."'";
						
				$mySQLBookingDetail .= ", idfeatures = '".$appFunctions->validSQL($idfeatures,"")."'";
				$mySQLBookingDetail .= ", featurename = '".$appFunctions->validSQL($name,"")."'";
				
				if(!empty($snowdepth) AND strtoupper($name) == 'STEPS IN BACK'){
					$mySQL = "";
					$mySQL = "SELECT snowdepthfrom, snowdepthto, price FROM featuresdepthprice WHERE 
					idfeatures = '".$appFunctions->validSQL($idfeatures,"")."' 
					AND '".$appFunctions->validSQL($snowdepth,"")."' BETWEEN snowdepthfrom AND snowdepthto";
					//echo $mySQL;
					$rsTempDepthPrice = $dbAccess->selectSingleStmt($mySQL);
					if(!empty($rsTempDepthPrice)){
						$mySQLBookingDetail .= ", featuredepthfrom = '".$rsTempDepthPrice['snowdepthfrom']."'";
						$mySQLBookingDetail .= ", featuredepthto = '".$appFunctions->validSQL($rsTempDepthPrice['snowdepthto'],"")."'";
						$mySQLBookingDetail .= ", featuredepthprice = '".$appFunctions->validSQL($rsTempDepthPrice['price'],"")."'";
					}
				}
				
				if(!empty($areasize) AND strtoupper($rsTempVal1['name']) == 'SALT DRIVEWAY'){
					$mySQL = "";
					$mySQL = "SELECT areafrom, areato, price FROM featuresareaprice WHERE idfeatures = '".$appFunctions->validSQL($idfeatures,"")."' AND CONCAT(areafrom, '-', areato) = '".$appFunctions->validSQL($areasize,"")."'";
					$rsTempAreaPrice = $dbAccess->selectSingleStmt($mySQL);
					if(!empty($rsTempAreaPrice)){
						$totalPrice = $totalPrice + $rsTempAreaPrice['price'];
						$mySQLBookingDetail .= ", featureareafrom = '".$appFunctions->validSQL($rsTempAreaPrice['areafrom'],"")."'";
						$mySQLBookingDetail .= ", featureareato = '".$appFunctions->validSQL($rsTempAreaPrice['areato'],"")."'";
						$mySQLBookingDetail .= ", featureprice = '".$appFunctions->validSQL($rsTempAreaPrice['price'],"")."'";
					}
				}
				
				//echo $mySQLBookingDetail . '<br>';
				$dbAccess->queryExec($mySQLBookingDetail);
			}
		}	
		if(!empty($iddrivewaytype)){
			$mySQL = "";
			$mySQL = "SELECT iddrivewaytype, name";
			$mySQL .= " FROM drivewaytype WHERE isactive = '1' AND iddrivewaytype = '".$appFunctions->validSQL($iddrivewaytype,"")."'";
			// echo $mySQL;
			$rsTemp = $dbAccess->selectSingleStmt($mySQL);
			$driveway = array("driveway" => $rsTemp);
			
			$mySQLBookingDetail = "";
			$mySQLBookingDetail = "INSERT INTO bookingdetail SET";
			$mySQLBookingDetail .= " idbooking = '".$appFunctions->validSQL($idbooking,"")."'";
			$mySQLBookingDetail .= ", iddrivewaytype = '".$appFunctions->validSQL($rsTemp['iddrivewaytype'],"")."'";
			$mySQLBookingDetail .= ", drivewaytypename = '".$appFunctions->validSQL($rsTemp['name'],"")."'";
			//echo $mySQLBookingDetail . '<br>';
			$dbAccess->queryExec($mySQLBookingDetail);
		} else {
			$driveway = array("driveway" => array("iddrivewaytype" => "", "name" => ""));
		}
			
			
		//} else {
		//	$driveway = array("driveway" => array("iddrivewaytype" => "", "name" => ""));
		//}
		
		$mySQL = "";
		$mySQL = "SELECT 
			(CASE WHEN areafrom > '50000' THEN 'Call now' ELSE CONCAT(areafrom, '-', areato) END) AS area FROM featuresareaprice WHERE isactive = '1'";
		$rsTemp = $dbAccess->selectData($mySQL);
		$areaSquareFeet = array("area" => $rsTemp);
		
		$mySQL = "";
		$mySQL = "SELECT 
			(CASE WHEN snowdepthfrom > '8' THEN 'Call now' ELSE CONCAT(snowdepthfrom, '-', snowdepthto) END) AS depth FROM featuresdepthprice WHERE isactive = '1'";
		$rsTemp = $dbAccess->selectData($mySQL);
		$areaDepth = array("depth" => $rsTemp);
		
		$mySQL = "";
		$mySQL = "SELECT 
			(CASE WHEN areafrom > '50000' THEN 'Call now' ELSE CONCAT(areafrom, '-', areato) END) AS area  FROM featuresareaprice WHERE isactive = '1'";
		$rsTemp = $dbAccess->selectData($mySQL);
		$areaSquareFeet = array("area" => $rsTemp);

		/* $mySQL = "";
		$mySQL = "SELECT idextracharge, CONCAT(name, ' ', amount) AS nameamount FROM featuresextracharge WHERE isactive = '1' AND idextracharge = '".$appFunctions->validSQL($idextracharge,"")."'";
		$rsTemp = $dbAccess->selectSingleStmt($mySQL);
		
		$additional = array();
		if(!empty($rsTemp)){
			$additional = array("additional" => $rsTemp);
		} */
		
		// var_dump ($arrbooking);
		// var_dump ($arrdestinationaddress);
		// var_dump ($serviceprice);
		// var_dump ($extracharge);
		// var_dump ($arrtotalPrice);
		// var_dump ($resultArr);
		// var_dump ($driveway);
		// var_dump ($areaSquareFeet);
		// var_dump ($areaDepth);
		
		//$data = array("data" => array_merge($arrbooking, $arrdestinationaddress, $serviceprice, $extracharge, $arrtotalPrice, $resultArr, $driveway, $areaSquareFeet, $areaDepth, $additional));
		$data = array("data" => array_merge($arrbooking, $arrdestinationaddress, $serviceprice, $extracharge, $arrtotalPrice, $resultArr, $driveway, $areaSquareFeet, $areaDepth));
		$status = array('code' => "200", "success" => true, 'message' => "Services");
		$tempStr = array_merge($status, $data);
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT); 
	
} else if ($fid == '13'){
	$iddriver = $appFunctions->validHTML(trim($postData->{'iddriver'}));
	
	$mySQL = "";
	$mySQL = "SELECT `idbooking`
		, `iduser`
		, `bookingno`
		, `bookingdate`
		, `starttime`
		, `fullname`
		, `originlatlng`
		, `destinationlatlng`
		, `totalamount`
		, CONCAT('$baseURL', '/workingareaphoto/') AS workingareaphoto
		, (CASE WHEN `beforeimage` <> '' THEN `beforeimage` ELSE '' END) AS `beforeimage`
		, (CASE WHEN `afterimage` <> '' THEN `afterimage` ELSE '' END) AS `afterimage`
		, `bookedbyuser`
		, `createdon` AS `bookedbyuseron`
		, `acceptbydriver`
		, (CASE WHEN `acceptbydriveron` THEN `acceptbydriveron` ELSE '' END) AS `acceptbydriveron`
		, `rejectbydriver`
		, (CASE WHEN `rejectbydriveron` THEN `rejectbydriveron` ELSE '' END) AS `rejectbydriveron`
		, `cancelbyuser`
		, (CASE WHEN `cancelbyuseron` THEN `cancelbyuseron` ELSE '' END) AS `cancelbyuseron`
		, `cancelbydriver`
		, (CASE WHEN `cancelbydriveron` THEN `cancelbydriveron` ELSE '' END) AS `cancelbydriveron`";
	$mySQL .= ", bookingstatus AS bookingcode";
	$mySQL .= ", (CASE 	WHEN bookingstatus = '1' THEN 'On The Way'
					WHEN bookingstatus = '2' THEN 'Job Started'
					WHEN bookingstatus = '3' THEN 'Job Complete'
			 ELSE 'Job Not Started' END) AS bookingstatus1";
			 
	$mySQL .= ", (CASE 	WHEN bookingstatus = '1' AND bookingcomplete = '0' THEN 'Job Not Started, Start Now'
						WHEN bookingstatus = '2' AND bookingcomplete = '0' THEN 'End Job Now'
						WHEN bookingstatus = '3' AND bookingcomplete = '1' THEN 'Job Complete'
				 ELSE 'Drive To Job Location' END) AS bookingstatus";
	$mySQL .= " 
	FROM `booking` WHERE `bookedbyuser` = '1' AND `acceptbydriver` = '1' AND `bookingcomplete` = '1' AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
	$mySQL .= " ORDER BY `bookingdate` DESC";
	$rsTemp = $dbAccess->selectData($mySQL);
	
	if(!empty($rsTemp)){
		$status = array('code' => "200", "success" => true, 'message' => "Boooking Found");
		$data = array("data"=>$rsTemp);
		$tempStr = array_merge($status, $data);
	} else {
		$tempStr = array('code' => "203", "success" => false, 'message' => "Booking Not Found");
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT); 
} else if ($fid == '14'){
	$idbooking = $appFunctions->validHTML(trim($postData->{'idbooking'}));
	$iddriver = $appFunctions->validHTML(trim($postData->{'iddriver'}));
	
	$resultArr = array();
	
	$mySQL = "";
	$mySQL = "SELECT `idbooking`
		, `iduser`
		, `bookingno`
		, `bookingdate`
		, `starttime`
		, (CASE WHEN `endtime` <> '' THEN `endtime` ELSE '' END) AS `endtime`
		, `fullname`
		
		, (CASE WHEN `originaddress` <> '' THEN `originaddress` ELSE '' END) AS `originaddress`
		, (CASE WHEN `origincity` <> '' THEN `origincity` ELSE '' END) AS `origincity`
		, (CASE WHEN `originstate` <> '' THEN `originstate` ELSE '' END) AS `originstate`
		, (CASE WHEN `originzipcode` <> '' THEN `originzipcode` ELSE '' END) AS `originzipcode`
		, (CASE WHEN `originlatlng` <> '' THEN `originlatlng` ELSE '' END) AS `originlatlng`
		
		, (CASE WHEN `destinationaddress` <> '' THEN `destinationaddress` ELSE '' END) AS `destinationaddress`
		, (CASE WHEN `destinationcity` <> '' THEN `destinationcity` ELSE '' END) AS `destinationcity`
		, (CASE WHEN `destinationstate` <> '' THEN `destinationstate` ELSE '' END) AS `destinationstate`
		, (CASE WHEN `destinationzipcode` <> '' THEN `destinationzipcode` ELSE '' END) AS `destinationzipcode`
		, (CASE WHEN `destinationlatlng` <> '' THEN `destinationlatlng` ELSE '' END) AS `destinationlatlng`
		
		, 'Service Area' AS `areatitle`
		, `area`
		, CONCAT('$baseURL', '/workingareaphoto/') AS workingareaphoto
		, (CASE WHEN `beforeimage` <> '' THEN `beforeimage` ELSE '' END) AS `beforeimage`
		, (CASE WHEN `afterimage` <> '' THEN `afterimage` ELSE '' END) AS `afterimage`
		, `price`
		, `totalamount`
		, `featuresextracharge`
		, `featuresextrachargeamt`
		, `featuresextrachargetype`
	FROM `booking` WHERE `bookedbyuser` = '1' 
	AND `acceptbydriver` = '1' 
	AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' 
	AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	$bookingDetail = $rsTemp;
	//$resultArr['data'] = array("data" =>$rsTemp);
	
	// FEATURES
	$mySQL = "";
	$mySQL = "SELECT";
	$mySQL .= " `idfeatures`";
	$mySQL .= ", `featurename`";
	//$mySQL .= ", `featureareafrom`";
	//$mySQL .= ", `featureareato`";
	$mySQL .= ", (CASE WHEN `featureprice` > 0 THEN featureprice ELSE '' END) AS featureprice";
	//$mySQL .= ", (CASE WHEN `featuredepthfrom` <> '' THEN featuredepthfrom ELSE '' END) AS featuredepthfrom";
	//$mySQL .= ", (CASE WHEN `featuredepthto` > 0 THEN featuredepthto ELSE '' END) AS featuredepthto";
	$mySQL .= ", (CASE WHEN `featuredepthprice` > 0 THEN featuredepthprice ELSE '' END) AS featuredepthprice";
	$mySQL .= " FROM `bookingdetail` WHERE `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
	$rsTempFeatures = $dbAccess->selectData($mySQL);
	$arrFeatures = array();
	if(!empty($rsTempFeatures)){
		$arrFeatures = array("features" => $rsTempFeatures);
		//$resultArr['data'][] = array("features" => $rsTempFeatures);
	} else {
		$arrFeatures = array("features" => []);
		//$resultArr['data'][] = array("features" => '');
	}
	// DRIVEWAY
	$mySQL = "";
	$mySQL = "SELECT 
		`iddrivewaytype`
		, `drivewaytypename`
	FROM `bookingdetail` WHERE iddrivewaytype <> '' AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
	$rsTempDriveway = $dbAccess->selectData($mySQL);
	$arrDriveway = array();
	if(!empty($rsTempDriveway)){
		$arrDriveway = array("driveway" => $rsTempDriveway);
		//$resultArr['data'][] = array("driveway" => $rsTempDriveway);
	} else {
		$arrDriveway = array("driveway" => '');
		//$resultArr['data'][] = array("driveway" => '');
	}
	
	if(!empty($rsTemp)){
		$status = array('code' => "200", "success" => true, 'message' => "Boooking Found");
		//$resultArr['data'] = array_values($resultArr['data']);
		$data = array("data" => array_merge($bookingDetail, $arrFeatures, $arrDriveway));
		$tempStr = array_merge($status, $data);
	} else {
		$tempStr = array('code' => "203", "success" => false, 'message' => "Booking Not Found");
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT); 
} else if ($fid == '15'){
	$idbooking = $appFunctions->validHTML(trim($postData->{'idbooking'}));
	$promocode = $appFunctions->validHTML(trim($postData->{'promocode'}));
	
	$errMsg = '';
	$idpromo = '';
	$promovalue = ''; 
	$promotype = '';
	if(!empty(trim($promocode))){
		$mySQL =  "";
		$mySQL = "SELECT idpromo, promovalue, promotype FROM promos 
			WHERE (isactive = '1' OR endtime >= '".$appFunctions->validSQL($currDate,"")."')
			AND promocode = '".$appFunctions->validSQL($promocode,"")."'";
		$rsTemp = $dbAccess->selectSingleStmt($mySQL);
		
		if(empty($rsTemp)){
			$errMsg = '1';
			$tempStr = array('code' => "203", "success" => false, 'message' => "Promo code is invalid / expired");
		} else {
			$idpromo = $rsTemp['idpromo'];
			$promovalue = $rsTemp['promovalue'];
			$promotype = $rsTemp['promotype'];
		}
	}
	if(empty($errMsg)){
		// UPDATE PROMO CODE IN BOOKING TABLE
		$mySQL = "";
		$mySQL = "UPDATE booking SET";
		$mySQL .= " promocode = '".$appFunctions->validSQL($promocode,"")."'";
		$mySQL .= ", promovalue = '".$appFunctions->validSQL($promovalue,"")."'";
		$mySQL .= ", promotype = '".$appFunctions->validSQL($promotype,"")."'";
		$mySQL .= " WHERE idbooking = '".$appFunctions->validSQL($idbooking,"")."'";
		//echo $mySQL;
		$dbAccess->queryExec($mySQL);
		
		$resultArr = array();
	
		$mySQL = "";
		$mySQL = "SELECT `idbooking`
			, `bookingno`
			, `bookingdate`
			, `destinationaddress`
			, `price`
			, `totalamount`
			
			, `featuresextracharge`
			, `featuresextrachargeamt`
			, (CASE WHEN `featuresextrachargetype` <> '' THEN featuresextrachargetype ELSE '' END) AS featuresextrachargetype
			
			, `promocode`
			, `promovalue`
			, `promotype`
			
		FROM `booking` WHERE `bookedbyuser` = '1' 
		AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."'"; 
		$rsTemp = $dbAccess->selectSingleStmt($mySQL);
		
		$totalamount = $rsTemp['totalamount'];
		
		if(strtoupper($rsTemp['promotype']) == 'PERCENT'){
			$totalamount = $rsTemp['totalamount'] - ($rsTemp['totalamount']*$rsTemp['promovalue'])/100; 
		} else if(strtoupper($rsTemp['promotype']) == 'USD'){
			$totalamount = $totalamount - $rsTemp['promovalue'];
		}		
		
		$bookingDetail = array(
								"idbooking" => $rsTemp['idbooking']
								, "bookingno" => $rsTemp['bookingno']
								, "address" => $rsTemp['destinationaddress']
								, "price" => $rsTemp['price']
								, "totalamount" => number_format($rsTemp['totalamount'],2)
								, "totalprice" => number_format($totalamount,2)
								, "promocode" => $rsTemp['promocode']
								, "promovalue" => $rsTemp['promovalue']
								, "promotype" => $rsTemp['promotype']
							);
		
		// FEATURES
		$mySQL = "";
		$mySQL = "SELECT";
		$mySQL .= " (CASE WHEN `featureprice` > 0 THEN featureprice
						WHEN `featuredepthprice` > 0 THEN featuredepthprice
						ELSE ''
						END) AS fPrice";
		$mySQL .= " FROM `bookingdetail` WHERE featurename = 'Salt Driveway' AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."'";
		$rsTempSD = $dbAccess->selectSingleStmt($mySQL);
		
		$fPrice = 0;
		if(!empty($rsTempSD)){
			$fPrice = $rsTempSD['fPrice'];
		}
		
		if (strtoupper($rsTemp['featuresextrachargetype']) == 'PERCENT'){
			$nameamount = $rsTemp['featuresextracharge'] . ' ' .$rsTemp['featuresextrachargeamt']. '% Extra Charge';
			if($fPrice > '0'){
				//$amount = ($fPrice * $rsTemp['featuresextrachargeamt'])/100;
				$amount = $rsTemp['featuresextrachargeamt'];
			} else {
				$amount = '';
			}
		} else {
			$nameamount = $rsTemp['featuresextracharge'] . ' $' .$rsTemp['featuresextrachargeamt']. ' Extra Charge';
			if($fPrice > '0'){
				// $amount = $fPrice - $rsTemp['featuresextrachargeamt'];
				$amount = $rsTemp['featuresextrachargeamt'];
			} else {
				$amount = '0';
			}
		}
		$featuresextracharge = $rsTemp['featuresextracharge'];
		
		$amounttype = $rsTemp['featuresextrachargetype'];
		$extracharge = array("additional" => array("nameamount" => $nameamount, "amount" => $amount, "amounttype" => $amounttype));	
		
		// FEATURES
		$mySQL = "";
		$mySQL = "SELECT";
		$mySQL .= " `idfeatures`";
		$mySQL .= ", `featurename` AS name";
		//$mySQL .= ", `featureareafrom`";
		//$mySQL .= ", `featureareato`";
		$mySQL .= ", (CASE WHEN `featureprice` > 0 THEN featureprice
						WHEN `featuredepthprice` > 0 THEN featuredepthprice
						ELSE ''
						END) AS featureprice";
		$mySQL .= " FROM `bookingdetail` WHERE `idfeatures` <> '' AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."'";
		// echo $mySQL;
		$rsTempFeatures = $dbAccess->selectData($mySQL);
		$arrFeatures = array();
		if(!empty($rsTempFeatures)){
			$arrFeatures = array("feature" => $rsTempFeatures);
		} else {
			$arrFeatures = array("feature" => '');
		}
		
		// DRIVEWAY
		$mySQL = "";
		$mySQL = "SELECT 
			`iddrivewaytype`
			, `drivewaytypename` AS name
		FROM `bookingdetail` WHERE iddrivewaytype <> '' AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."'";
		$rsTempDriveway = $dbAccess->selectSingleStmt($mySQL);
		$arrDriveway = array();
		if(!empty($rsTempDriveway)){
			$arrDriveway = array("driveway" => $rsTempDriveway);
		} else {
			$arrDriveway = array("driveway" => '');
		}
		
		if(!empty($rsTemp)){
			$status = array('code' => "200", "success" => true, 'message' => "Boooking Found");
			//$data = array("data" => array_merge($arrbooking, $arrdestinationaddress, $serviceprice, $extracharge, $arrtotalPrice, $resultArr, $driveway, $areaSquareFeet, $areaDepth));
			$data = array("data" => array_merge($bookingDetail, $extracharge, $arrFeatures, $arrDriveway));
			$tempStr = array_merge($status, $data);
		} else {
			$tempStr = array('code' => "203", "success" => false, 'message' => "Booking Not Found");
		}
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT); 
} else if ($fid == '16'){
	//$idbooking = $appFunctions->validHTML(trim($postData->{'idbooking'}));
	$iddriver = $appFunctions->validHTML(trim($postData->{'iddriver'}));
	//$serviceresp = $appFunctions->validHTML(trim($postData->{'serviceresp'}));
	
	$mySQL = "";
	$mySQL = "SELECT `idbooking`
		, `iduser`
		, `bookingno`
		, `bookingdate`
		, `starttime`
		, (CASE WHEN `endtime` <> '' THEN `endtime` ELSE '' END) AS `endtime`
		, `fullname`
		
		, (CASE WHEN `originaddress` <> '' THEN `originaddress` ELSE '' END) AS `originaddress`
		, (CASE WHEN `origincity` <> '' THEN `origincity` ELSE '' END) AS `origincity`
		, (CASE WHEN `originstate` <> '' THEN `originstate` ELSE '' END) AS `originstate`
		, (CASE WHEN `originzipcode` <> '' THEN `originzipcode` ELSE '' END) AS `originzipcode`
		, `originlatlng`
		
		, (CASE WHEN `destinationaddress` <> '' THEN `destinationaddress` ELSE '' END) AS `destinationaddress`
		, (CASE WHEN `destinationcity` <> '' THEN `destinationcity` ELSE '' END) AS `destinationcity`
		, (CASE WHEN `destinationstate` <> '' THEN `destinationstate` ELSE '' END) AS `destinationstate`
		, (CASE WHEN `destinationzipcode` <> '' THEN `destinationzipcode` ELSE '' END) AS `destinationzipcode`
		, `destinationlatlng`
		
		, 'Service Area' AS `areatitle`
		, `area`
		, `price`
		, `totalamount`
		, `featuresextracharge`
		, `featuresextrachargeamt`
		, `featuresextrachargetype`
	FROM `booking` WHERE `bookedbyuser` = '1' AND `bookingcomplete` = '0' AND `acceptbydriver` = '0' AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
	$mySQL .= " ORDER BY `bookingdate` DESC";
	$rsTemp = $dbAccess->selectData($mySQL);
	
	if(!empty($rsTemp)){
		$booking = array('booking' => $rsTemp);
	} else {
		$booking = array('booking' => []);
	}
	
	$mySQL = "";
	$mySQL = "SELECT `title`,`image`,`description`,`startdate`,`offertype`, CONCAT('$baseURL', '/offer-list/') AS `siteofferlink`";
	$mySQL .= ", (CASE WHEN couponcode <> '' THEN couponcode ELSE '' END ) AS couponcode";
	$mySQL .= ", (CASE WHEN offervalue <> '' THEN offervalue ELSE '' END ) AS offervalue ";
	$mySQL .= ", (CASE WHEN offervaluetype <> '' THEN offervaluetype ELSE '' END ) AS offervaluetype ";
	$mySQL .= ", (CASE WHEN enddate <> '' THEN enddate ELSE '' END ) AS enddate";
	$mySQL .= " FROM offers WHERE isactive = '1' AND (enddate >= '".$currDate."' OR enddate IS NULL)";
	//echo $mySQL1; die;
	$rsTemp1 = $dbAccess->selectData($mySQL);
	
	$offers = array();
	if(!empty($rsTemp1)){
		$offers = array('offers' => $rsTemp1);
	}
	
	//if(!empty($rsTemp)){
		$status = array('code' => "200", "success" => true, 'message' => "Record Found");
		$data = array("data" => array_merge($booking, $offers));
		$tempStr = array_merge($status, $data);
	//} else {
	//	$status = array('code' => "200", "success" => true, 'message' => "Record not found");
	//	$data = array("data" => $offers);
	//	$tempStr = array_merge($status, $data);
	//}
	echo json_encode($tempStr, JSON_PRETTY_PRINT); 	
} else if ($fid == '17'){
	$idbooking = $appFunctions->validHTML(trim($postData->{'idbooking'}));
	$iddriver = $appFunctions->validHTML(trim($postData->{'iddriver'}));
	
	$resultArr = array();
	
	$mySQL = "";
	$mySQL = "SELECT `idbooking`
		, `iduser`
		, `bookingno`
		, `bookingdate`
		, `starttime`
		, (CASE WHEN `endtime` <> '' THEN `endtime` ELSE '' END) AS `endtime`
		, `fullname`
		
		, (CASE WHEN `originaddress` <> '' THEN `originaddress` ELSE '' END) AS `originaddress`
		, (CASE WHEN `origincity` <> '' THEN `origincity` ELSE '' END) AS `origincity`
		, (CASE WHEN `originstate` <> '' THEN `originstate` ELSE '' END) AS `originstate`
		, (CASE WHEN `originzipcode` <> '' THEN `originzipcode` ELSE '' END) AS `originzipcode`
		, `originlatlng`
		
		, (CASE WHEN `destinationaddress` <> '' THEN `destinationaddress` ELSE '' END) AS `destinationaddress`
		, (CASE WHEN `destinationcity` <> '' THEN `destinationcity` ELSE '' END) AS `destinationcity`
		, (CASE WHEN `destinationstate` <> '' THEN `destinationstate` ELSE '' END) AS `destinationstate`
		, (CASE WHEN `destinationzipcode` <> '' THEN `destinationzipcode` ELSE '' END) AS `destinationzipcode`
		, `destinationlatlng`
		
		, 'Service Area' AS `areatitle`
		, `area`
		, `price`
		, `totalamount`
		, `featuresextracharge`
		, `featuresextrachargeamt`
		, `featuresextrachargetype`
	FROM `booking` WHERE `bookedbyuser` = '1' 
	AND `bookingcomplete` = '0' 
	AND `acceptbydriver` = '0' 
	AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'
	AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."'";
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	$bookingDetail = $rsTemp;
	//$resultArr['data'] = array("data" =>$rsTemp);
	
	// FEATURES
	$mySQL = "";
	$mySQL = "SELECT";
	$mySQL .= " `idfeatures`";
	$mySQL .= ", `featurename`";
	//$mySQL .= ", `featureareafrom`";
	//$mySQL .= ", `featureareato`";
	$mySQL .= ", (CASE WHEN `featureprice` > 0 THEN featureprice ELSE '' END) AS featureprice";
	//$mySQL .= ", (CASE WHEN `featuredepthfrom` <> '' THEN featuredepthfrom ELSE '' END) AS featuredepthfrom";
	//$mySQL .= ", (CASE WHEN `featuredepthto` > 0 THEN featuredepthto ELSE '' END) AS featuredepthto";
	$mySQL .= ", (CASE WHEN `featuredepthprice` > 0 THEN featuredepthprice ELSE '' END) AS featuredepthprice";
	$mySQL .= " FROM `bookingdetail` WHERE `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
	$rsTempFeatures = $dbAccess->selectData($mySQL);
	$arrFeatures = array();
	if(!empty($rsTempFeatures)){
		$arrFeatures = array("features" => $rsTempFeatures);
		//$resultArr['data'][] = array("features" => $rsTempFeatures);
	} else {
		$arrFeatures = array("features" => '');
		//$resultArr['data'][] = array("features" => '');
	}
	// DRIVEWAY
	$mySQL = "";
	$mySQL = "SELECT 
		`iddrivewaytype`
		, `drivewaytypename`
	FROM `bookingdetail` WHERE iddrivewaytype <> '' AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
	$rsTempDriveway = $dbAccess->selectData($mySQL);
	$arrDriveway = array();
	if(!empty($rsTempDriveway)){
		$arrDriveway = array("driveway" => $rsTempDriveway);
		//$resultArr['data'][] = array("driveway" => $rsTempDriveway);
	} else {
		$arrDriveway = array("driveway" => '');
		//$resultArr['data'][] = array("driveway" => '');
	}
	
	if(!empty($rsTemp)){
		$status = array('code' => "200", "success" => true, 'message' => "Boooking Found");
		//$resultArr['data'] = array_values($resultArr['data']);
		$data = array("data" => array_merge($bookingDetail, $arrFeatures, $arrDriveway));
		$tempStr = array_merge($status, $data);
	} else {
		$tempStr = array('code' => "203", "success" => false, 'message' => "Booking Not Found");
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT); 
} else if ($fid == '18'){
	$iddriver = $appFunctions->validHTML(trim($postData->{'iddriver'}));
	$idbooking = $appFunctions->validHTML(trim($postData->{'idbooking'}));
	$acceptbydriver = $appFunctions->validHTML(trim($postData->{'acceptbydriver'}));
	//$iduser = $appFunctions->validHTML(trim($postData->{'iduser'}));
	$rejectbydriver = $appFunctions->validHTML(trim($postData->{'rejectbydriver'}));
	
	$bookingStatus = '';
	
	$mySQL = "";
	$mySQL = "UPDATE booking SET";
	if(!empty($acceptbydriver)){
		$mySQL .=" `acceptbydriver` = '".$appFunctions->validSQL($acceptbydriver,"")."'";
		$mySQL .=", `acceptbydriveron` = '".$appFunctions->validSQL($nowtime,"")."'";
		$bookingStatus = 'accepted';
		$notificationcode = '1';
	}
	if(!empty($rejectbydriver)){
		$mySQL .=" `rejectbydriver` = '".$appFunctions->validSQL($rejectbydriver,"")."'";
		$mySQL .=", `rejectbydriveron` = '".$appFunctions->validSQL($nowtime,"")."'";
		$bookingStatus = 'rejected';
		$notificationcode = '0';
	}
	$mySQL .= " WHERE `bookingcomplete` = '0' AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' 
		AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
	//echo $mySQL;
	$rsTemp = $dbAccess->queryExec($mySQL);

	if ($bookingStatus == 'rejected'){
		$mySQL = "";
		$mySQL = "INSERT INTO `bookingaccepted` SET
			`idbooking` = '".$appFunctions->validSQL($idbooking,"")."'
			, `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'
			, `isnotaccepted` = '1'
			, `createdon` = '".$appFunctions->validSQL($entryDate,"")."'"; 
		$dbAccess->queryExec($mySQL);
	}
	
	if($rsTemp){
		// #################################
		$mySQL = "";
		$mySQL = "SELECT iduser
			, bookingno
			, (SELECT fcmtoken FROM user U WHERE U.iduser = B.iduser LIMIT 1) AS fcmtoken
		FROM booking B WHERE `idbooking` = '".$appFunctions->validSQL($idbooking,"")."'";
		// echo $mySQL;
		$rsTemp1 = $dbAccess->selectSingleStmt($mySQL);
		
		$iduser = $rsTemp1['iduser'];
		$bookingno = $rsTemp1['bookingno'];
		$fcmtoken = $rsTemp1['fcmtoken'];
		
		$mySQL = "";
		$mySQL = "SELECT 
			`isnotification`
			, `profileprivacy`
		FROM `user` WHERE `isnotification` = '1' AND iduser = '".$appFunctions->validSQL($iduser,"")."'";
		// echo $mySQL;
		$rsTemp = $dbAccess->selectSingleStmt($mySQL);
		
		if(!empty($rsTemp)){
			$body = 'Your booking '. $bookingStatus.' by driver! booking no. : ' .$bookingno;
			$message = $body;
			$toTokenId = $fcmtoken;
			
			$mySQL = "";
			$mySQL = "INSERT INTO `notification` (`idsender`, `idreceiver`, `idsource`, `notificationtype`, `notificatiomode`, `sort`, `description`, `isactive`, `isviewed`) 
				VALUES ('".$appFunctions->validSQL($iduser,"")."', '".$appFunctions->validSQL($iddriver,"")."', '".$appFunctions->validSQL($idbooking,"")."', 'New Booking', 'TEXT', '2', '".$body."', '1', '0')";
			$dbAccess->queryExec($mySQL);
			
			$notificationTitle = "Booking ". $bookingStatus;
			$notificationDataTitle = "Request ". $bookingStatus;
			
			$payload = array(
				//'to'=>'/topics/my-app',
				//'to'=>'fqnkZbkucDw:APA91bFQwkDe4SUqqKwfXVG1ANWiLAqUqcEC_GjkjoNmia5JcAoHtqvC6KwIGrI97lM-stF7pq9cSG1xnAfxTr-HxmCivp0OWZ3CtH7lMYIqWuRzDCMVrdS36LlqL-4HPUWO_xwQB3KU',
				"to"=>$toTokenId,
				"priority"=>"high",
				"mutable_content"=>true,
				"notification"=>array("title" => $notificationTitle, "body" => $body, "sound" => "default"),
				"data"=> array("response" => array("usertype" => "CUSTOMER", "iddriver" => $iddriver, "title" => $notificationDataTitle, "message" => $message, "timestamp" => $nowtime, "image" => "", "notificationcode" => $notificationcode, "notificationtype" => $notificationDataTitle, "idbooking" => $idbooking, "bookingno" => $bookingno, "iduser" => $iduser))
			);
			
			// echo json_encode($payload, JSON_PRETTY_PRINT);
			// exit;
			
			$fcmApiKey = "AAAAHjs3cGE:APA91bGx3gtE9yBUSO266Q-ohjHmX0CwvKujZqemLuwX5r-_-1_VO2HMSZsUMoAPh9HW4ga3Y4FtXLAnjj6R4NfhvbMIAx4-2lT2-kICzbKK1G3eX36RK8lW9IrVor-8Bapqu5MA9KBg";
			$headers = array('Authorization: key='.$fcmApiKey, 'Content-Type: application/json');

			$ch = curl_init();
			//curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
			curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode($payload) );
			$result = curl_exec($ch );
			curl_close( $ch );
			// echo '<pre>';
			// var_dump($result);
			// exit;
		}
		// #################################
		
		$mySQL = "";
		$mySQL = "SELECT `idbooking`
			, `iduser`
			, `bookingno`
			, `bookingdate`
			, `starttime`
			, `fullname`
			, `originlatlng`
			, `destinationlatlng`
			, `totalamount`
		FROM `booking` WHERE `bookedbyuser` = '1' AND `bookingcomplete` = '0' AND `acceptbydriver` = '0' AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
		$rsTemp = $dbAccess->selectData($mySQL);
		$booking = array();
		if(!empty($rsTemp)){
			$booking = array('booking' => $rsTemp);
		}
		
		$mySQL = "";
		$mySQL = "SELECT `title`,`image`,`description`,`startdate`,`offertype`";
		$mySQL .= ", (CASE WHEN couponcode <> '' THEN couponcode ELSE '' END ) AS couponcode";
		$mySQL .= ", (CASE WHEN offervalue <> '' THEN offervalue ELSE '' END ) AS offervalue ";
		$mySQL .= ", (CASE WHEN offervaluetype <> '' THEN offervaluetype ELSE '' END ) AS offervaluetype ";
		$mySQL .= ", (CASE WHEN enddate <> '' THEN enddate ELSE '' END ) AS enddate";
		$mySQL .= " FROM offers WHERE isactive = '1' AND (enddate >= '".$currDate."' OR enddate IS NULL)";
		//echo $mySQL1; die;
		$rsTemp1 = $dbAccess->selectData($mySQL);
		
		$offers = array();
		if(!empty($rsTemp1)){
			$offers = array('offers' => $rsTemp1);
		} else {
			$offers = array('offers' => []);
		}
		
		//if(!empty($rsTemp)){
			$status = array('code' => "200", "success" => true, 'message' => "Booking $bookingStatus");
			$data = array("data" => array_merge($booking, $offers));
			$tempStr = array_merge($status, $data);
		//} else {
		//	$status = array('code' => "200", "success" => true, 'message' => "Record not found");
		//	$data = array("data" => $offers);
		//	$tempStr = array_merge($status, $data);
		//}
	} else {
		$tempStr = array('code' => "203", "success" => false, 'message' => "Record not found");
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '19'){
	$idreceiver = $appFunctions->validHTML(trim($postData->{'iduser'}));
	
	$mySQL =  "";
	$mySQL = "SELECT idnotification, idsender, idreceiver, notificationtype, description, createdon FROM notification 
		WHERE isactive = '1' AND isviewed = '0' AND idreceiver = '".$appFunctions->validSQL($idreceiver,"")."'";
	//$mySQL .= "GROUP BY notificationtype";
	$mySQL .= "ORDER BY sort LIMIT 50";
	$rsTemp = $dbAccess->selectData($mySQL);
	if(empty($rsTemp)){
		$errMsg = '1';
		$tempStr = array('code' => "203", "success" => false, 'message' => "You have 0 notification");
	} else {
		$status = array('code' => "200", "success" => true, 'message' => "Data Found");
		
		$resultArr = array();
		foreach($rsTemp AS $rsTempVal){
			$idnotification = $rsTempVal['idnotification'];
			$idreceiver = $rsTempVal['idreceiver'];
			$notificationtype = $rsTempVal['notificationtype'];
			$description = $rsTempVal['description'];
			$createdon = $rsTempVal['createdon'];
			
			$resultArr['data'][$idnotification] = array('idnotification' => $idnotification, 'notificationtype' => $notificationtype, 'description' => $description, 'createdon' => $createdon);
			
			if(strtoupper($notificationtype) == 'FRIEND REQUEST'){
				$mySQL =  "";
				$mySQL = "SELECT idsender
					, (SELECT fullname FROM user WHERE iduser = notification.idsender LIMIT 1) AS sendername 
					FROM notification 
					WHERE isactive = '1' AND isviewed = '0' AND idreceiver = '".$appFunctions->validSQL($idreceiver,"")."'
					AND notificationtype = 'Friend Request'
					ORDER BY sort LIMIT 50";
				$rsTempNotification = $dbAccess->selectData($mySQL);
				foreach($rsTempNotification AS $rsTempNotificationVal){
					//echo '==== ' . $rsTempNotificationVal['sendername'];
					
					if(!empty($rsTempNotificationVal['sendername'])){
						$sendername = $rsTempNotificationVal['sendername'];
					} else {
						$sendername = '';
					}
					$resultArr['data'][$idnotification]['request'][] = array('idsender' => $rsTempNotificationVal['idsender'], 'sendername' => $sendername);				
				}
			} else {
				$resultArr['data'][$idnotification]['request'] = array();				
			}
		}
		$resultArr['data'] = array_values($resultArr['data']);
		//$data = array("data"=>$rsTemp);
		$tempStr = array_merge($status, $resultArr);
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '20'){
	$idbooking = $appFunctions->validHTML(trim($postData->{'idbooking'}));
		
	$mySQL = "UPDATE booking SET isactive = '1' WHERE idbooking = '".$appFunctions->validSQL($idbooking,"")."'";
	$dbAccess->queryExec($mySQL);
	$tempStr = array('code' => "200", "success" => true, 'message' => "Booking successfully placed.");
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '21'){
	$iddriver = $appFunctions->validHTML(trim($postData->{'iddriver'}));
	//$serviceresp = $appFunctions->validHTML(trim($postData->{'serviceresp'}));
	
	$mySQL = "";
	$mySQL = "SELECT `idbooking`
		, `iduser`
		, `bookingno`
		, `bookingdate`
		, `starttime`
		, (CASE WHEN `endtime` <> '' THEN `endtime` ELSE '' END) AS `endtime`
		, `fullname`
		, (SELECT (CASE WHEN fullname <> '' THEN fullname ELSE '' END) AS `fullname` FROM user U WHERE U.iduser = B.iddriver LIMIT 1) AS drivername
		, (SELECT (CASE WHEN profileimage <> '' THEN CONCAT('$baseURL', '/userphoto/', profileimage) ELSE '' END) AS `profileimage` FROM user U WHERE U.iduser = B.iddriver LIMIT 1) AS driverimage
		
		, (CASE WHEN `originaddress` <> '' THEN `originaddress` ELSE '' END) AS `originaddress`
		, (CASE WHEN `origincity` <> '' THEN `origincity` ELSE '' END) AS `origincity`
		, (CASE WHEN `originstate` <> '' THEN `originstate` ELSE '' END) AS `originstate`
		, (CASE WHEN `originzipcode` <> '' THEN `originzipcode` ELSE '' END) AS `originzipcode`
		, `originlatlng`
		
		, (CASE WHEN `destinationaddress` <> '' THEN `destinationaddress` ELSE '' END) AS `destinationaddress`
		, (CASE WHEN `destinationcity` <> '' THEN `destinationcity` ELSE '' END) AS `destinationcity`
		, (CASE WHEN `destinationstate` <> '' THEN `destinationstate` ELSE '' END) AS `destinationstate`
		, (CASE WHEN `destinationzipcode` <> '' THEN `destinationzipcode` ELSE '' END) AS `destinationzipcode`
		, `destinationlatlng`
		
		, 'Service Area' AS `areatitle`
		, `area`
		, `price`
		, `totalamount`
		, `featuresextracharge`
		, `featuresextrachargeamt`
		, `featuresextrachargetype`
		, `cancelbyuser`
		, `cancelbyuseron`
	FROM `booking` B WHERE `bookedbyuser` = '1' AND `bookingcomplete` = '0' AND `acceptbydriver` = '1' AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
	$mySQL .= " ORDER BY `bookingdate` DESC";
	$rsTemp = $dbAccess->selectData($mySQL);
	
	/*$booking = array();
	if(!empty($rsTemp)){
		$booking = array('booking' => $rsTemp);
	}*/

	if(!empty($rsTemp)){
		$status = array('code' => "200", "success" => true, 'message' => "Record Found");
		$data = array("data" => $rsTemp);
		$tempStr = array_merge($status, $data);
	} else {
		$status = array('code' => "203", "success" => false, 'message' => "No Record Found");
		$data = array("data" => $rsTemp);
		$tempStr = array_merge($status);
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '22'){
	$idbooking = $appFunctions->validHTML(trim($postData->{'idbooking'}));
	$iddriver = $appFunctions->validHTML(trim($postData->{'iddriver'}));
	//$iddriver = $appFunctions->validHTML(trim($postData->{'iddriver'}));
	
	$resultArr = array();
	
	$mySQL = "";
	$mySQL = "SELECT `idbooking`
		, `iduser`
		, `bookingno`
		, `bookingdate`
		, `starttime`
		, (CASE WHEN `endtime` <> '' THEN `endtime` ELSE '' END) AS `endtime`
		, `fullname`
		, (SELECT (CASE WHEN fullname <> '' THEN fullname ELSE '' END) AS `fullname` FROM user U WHERE U.iduser = B.iddriver LIMIT 1) AS drivername
		, (SELECT (CASE WHEN profileimage <> '' THEN CONCAT('$baseURL', '/userphoto/', profileimage) ELSE '' END) AS `profileimage` FROM user U WHERE U.iduser = B.iddriver LIMIT 1) AS driverimage
		
		, (CASE WHEN `originaddress` <> '' THEN `originaddress` ELSE '' END) AS `originaddress`
		, (CASE WHEN `origincity` <> '' THEN `origincity` ELSE '' END) AS `origincity`
		, (CASE WHEN `originstate` <> '' THEN `originstate` ELSE '' END) AS `originstate`
		, (CASE WHEN `originzipcode` <> '' THEN `originzipcode` ELSE '' END) AS `originzipcode`
		, `originlatlng`
		
		, (CASE WHEN `destinationaddress` <> '' THEN `destinationaddress` ELSE '' END) AS `destinationaddress`
		, (CASE WHEN `destinationcity` <> '' THEN `destinationcity` ELSE '' END) AS `destinationcity`
		, (CASE WHEN `destinationstate` <> '' THEN `destinationstate` ELSE '' END) AS `destinationstate`
		, (CASE WHEN `destinationzipcode` <> '' THEN `destinationzipcode` ELSE '' END) AS `destinationzipcode`
		, `destinationlatlng`
		
		, 'Service Area' AS `areatitle`
		, `area`
		, `price`
		, `totalamount`
		, `featuresextracharge`
		, `featuresextrachargeamt`
		, `featuresextrachargetype`
		, `featuresextrachargetype`
		, `cancelbyuser`
		, `cancelbyuseron`";	
	$mySQL .= ", bookingstatus AS bookingcode";
	$mySQL .= ", (CASE 	WHEN bookingstatus = '1' THEN 'On The Way'
						WHEN bookingstatus = '2' THEN 'Job Started'
						WHEN bookingstatus = '3' THEN 'Job Complete'
				 ELSE 'Job Not Started, Start Now' END) AS bookingstatus_NOT_AN_USE";
	
	$mySQL .= ", (CASE 	WHEN bookingstatus = '1' THEN 'Job Not Started, Start Now'
						WHEN bookingstatus = '2' THEN 'End Job Now'
						WHEN bookingstatus = '3' THEN 'Job Complete, Enter OTP'
				 ELSE 'Drive To Job Location' END) AS bookingstatus";
	$mySQL .= " FROM `booking` B WHERE `bookedbyuser` = '1'";
	$mySQL .= " AND `bookingcomplete` = '0'";
	$mySQL .= " AND `acceptbydriver` = '1' 
	AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."'
	AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
	$mySQL .= " ORDER BY `bookingdate` DESC";
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	$bookingDetail = $rsTemp;
	//$resultArr['data'] = array("data" =>$rsTemp);
	
	//echo $mySQL;
	// FEATURES
	$mySQL = "";
	$mySQL = "SELECT";
	$mySQL .= " `idfeatures`";
	$mySQL .= ", `featurename`";
	//$mySQL .= ", `featureareafrom`";
	//$mySQL .= ", `featureareato`";
	$mySQL .= ", (CASE WHEN `featureprice` > 0 THEN featureprice ELSE '' END) AS featureprice";
	//$mySQL .= ", (CASE WHEN `featuredepthfrom` <> '' THEN featuredepthfrom ELSE '' END) AS featuredepthfrom";
	//$mySQL .= ", (CASE WHEN `featuredepthto` > 0 THEN featuredepthto ELSE '' END) AS featuredepthto";
	$mySQL .= ", (CASE WHEN `featuredepthprice` > 0 THEN featuredepthprice ELSE '' END) AS featuredepthprice";
	$mySQL .= " FROM `bookingdetail` WHERE `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' 
	AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
	$rsTempFeatures = $dbAccess->selectData($mySQL);
	$arrFeatures = array();
	if(!empty($rsTempFeatures)){
		$arrFeatures = array("features" => $rsTempFeatures);
		//$resultArr['data'][] = array("features" => $rsTempFeatures);
	} else {
		$arrFeatures = array("features" => []);
		//$resultArr['data'][] = array("features" => '');
	}
	// DRIVEWAY
	$mySQL = "";
	$mySQL = "SELECT 
		`iddrivewaytype`
		, `drivewaytypename`
	FROM `bookingdetail` WHERE iddrivewaytype <> '' AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
	$rsTempDriveway = $dbAccess->selectData($mySQL);
	$arrDriveway = array();
	if(!empty($rsTempDriveway)){
		$arrDriveway = array("driveway" => $rsTempDriveway);
		//$resultArr['data'][] = array("driveway" => $rsTempDriveway);
	} else {
		$arrDriveway = array("driveway" => '');
		//$resultArr['data'][] = array("driveway" => '');
	}
	
	if(!empty($rsTemp)){
		$status = array('code' => "200", "success" => true, 'message' => "Boooking Found");
		//$resultArr['data'] = array_values($resultArr['data']);
		$data = array("data" => array_merge($bookingDetail, $arrFeatures, $arrDriveway));
		$tempStr = array_merge($status, $data);
	} else {
		$tempStr = array('code' => "203", "success" => false, 'message' => "Booking Not Found");
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT);  
} else if ($fid == '23'){
	$idbooking = $appFunctions->validHTML(trim($postData->{'idbooking'}));
	$iddriver = $appFunctions->validHTML(trim($postData->{'iddriver'}));
	$bookingstatus = $appFunctions->validHTML(trim($postData->{'bookingstatus'}));
	
	$mySQL = "";
	$mySQL = "UPDATE booking SET bookingstatus = '".$appFunctions->validSQL($bookingstatus,"")."'"; 
	$mySQL .= " WHERE idbooking = '".$appFunctions->validSQL($idbooking,"")."'";
	$mySQL .= " AND iddriver = '".$appFunctions->validSQL($iddriver,"")."'";
	//echo $mySQL;
	$dbAccess->queryExec($mySQL);

	// #################################
	$mySQL = "";
	$mySQL = "SELECT iduser
		, bookingno
		, (SELECT fcmtoken FROM user U WHERE U.iduser = B.iduser LIMIT 1) AS fcmtoken
	FROM booking B WHERE `idbooking` = '".$appFunctions->validSQL($idbooking,"")."'";
	// echo $mySQL;
	$rsTemp1 = $dbAccess->selectSingleStmt($mySQL);
	
	$iduser = $rsTemp1['iduser'];
	$bookingno = $rsTemp1['bookingno'];
	$fcmtoken = $rsTemp1['fcmtoken'];
	
	if($bookingstatus == '1'){
		$bookingStatus = 'driver on the way';
		$notificationcode = '-1';
	}
	$mySQL = "";
	$mySQL = "SELECT 
		`isnotification`
		, `profileprivacy`
	FROM `user` WHERE `isnotification` = '1' AND iduser = '".$appFunctions->validSQL($iduser,"")."'";
	// echo $mySQL;
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	
	if(!empty($rsTemp)){
		$body = "Driver on the way! You don''t go any where";
		$message = $body;
		$toTokenId = $fcmtoken;
		
		$mySQL = "";
		$mySQL = "INSERT INTO `notification` (`idsender`, `idreceiver`, `idsource`, `notificationtype`, `notificatiomode`, `sort`, `description`, `isactive`, `isviewed`) 
			VALUES ('".$appFunctions->validSQL($iddriver,"")."', '".$appFunctions->validSQL($iduser,"")."', '".$appFunctions->validSQL($idbooking,"")."', 'New Booking', 'TEXT', '2', '".$body."', '1', '0')";
		$dbAccess->queryExec($mySQL);
		
		$notificationTitle = "Booking ". $bookingStatus;
		$notificationDataTitle = "Request ". $bookingStatus;
		
		$payload = array(
			//'to'=>'/topics/my-app',
			//'to'=>'fqnkZbkucDw:APA91bFQwkDe4SUqqKwfXVG1ANWiLAqUqcEC_GjkjoNmia5JcAoHtqvC6KwIGrI97lM-stF7pq9cSG1xnAfxTr-HxmCivp0OWZ3CtH7lMYIqWuRzDCMVrdS36LlqL-4HPUWO_xwQB3KU',
			"to"=>$toTokenId,
			"priority"=>"high",
			"mutable_content"=>true,
			"notification"=>array("title" => $notificationTitle, "body" => $body, "sound" => "default"),
			"data"=> array("response" => array("usertype" => "CUSTOMER", "iddriver" => $iddriver, "title" => $notificationDataTitle, "message" => $message, "timestamp" => $nowtime, "image" => "", "notificationcode" => $notificationcode, "notificationtype" => $notificationDataTitle, "idbooking" => $idbooking, "bookingno" => $bookingno, "iduser" => $iduser))
		);
		
		// echo json_encode($payload, JSON_PRETTY_PRINT);
		// exit;
		
		$fcmApiKey = "AAAAHjs3cGE:APA91bGx3gtE9yBUSO266Q-ohjHmX0CwvKujZqemLuwX5r-_-1_VO2HMSZsUMoAPh9HW4ga3Y4FtXLAnjj6R4NfhvbMIAx4-2lT2-kICzbKK1G3eX36RK8lW9IrVor-8Bapqu5MA9KBg";
		$headers = array('Authorization: key='.$fcmApiKey, 'Content-Type: application/json');

		$ch = curl_init();
		//curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode($payload) );
		$result = curl_exec($ch );
		curl_close( $ch );
		// echo '<pre>';
		// var_dump($result);
		// exit;
	}
	// #################################
	
	$resultArr = array();
	
	$mySQL = "";
	$mySQL = "SELECT `idbooking`
		, `iduser`
		, `bookingno`
		, `bookingdate`
		, `starttime`
		, (CASE WHEN `endtime` <> '' THEN `endtime` ELSE '' END) AS `endtime`
		, `fullname`
		, (SELECT (CASE WHEN fullname <> '' THEN fullname ELSE '' END) AS `fullname` FROM user U WHERE U.iduser = B.iddriver LIMIT 1) AS drivername
		, (SELECT (CASE WHEN profileimage <> '' THEN CONCAT('$baseURL', '/userphoto/', profileimage) ELSE '' END) AS `profileimage` FROM user U WHERE U.iduser = B.iddriver LIMIT 1) AS driverimage
		
		, (CASE WHEN `originaddress` <> '' THEN `originaddress` ELSE '' END) AS `originaddress`
		, (CASE WHEN `origincity` <> '' THEN `origincity` ELSE '' END) AS `origincity`
		, (CASE WHEN `originstate` <> '' THEN `originstate` ELSE '' END) AS `originstate`
		, (CASE WHEN `originzipcode` <> '' THEN `originzipcode` ELSE '' END) AS `originzipcode`
		, `originlatlng`
		
		, (CASE WHEN `destinationaddress` <> '' THEN `destinationaddress` ELSE '' END) AS `destinationaddress`
		, (CASE WHEN `destinationcity` <> '' THEN `destinationcity` ELSE '' END) AS `destinationcity`
		, (CASE WHEN `destinationstate` <> '' THEN `destinationstate` ELSE '' END) AS `destinationstate`
		, (CASE WHEN `destinationzipcode` <> '' THEN `destinationzipcode` ELSE '' END) AS `destinationzipcode`
		, `destinationlatlng`
		
		, 'Service Area' AS `areatitle`
		, `area`
		, `price`
		, `totalamount`
		, `featuresextracharge`
		, `featuresextrachargeamt`
		, `featuresextrachargetype`
		, `featuresextrachargetype`";	
	$mySQL .= ", bookingstatus AS bookingcode";
	$mySQL .= ", (CASE 	WHEN bookingstatus = '1' THEN 'On The Way'
						WHEN bookingstatus = '2' THEN 'Job Started'
						WHEN bookingstatus = '3' THEN 'Job Complete'
				 ELSE 'Job Not Started, Start Now' END) AS bookingstatus_NOT_AN_USE";
				 
	$mySQL .= ", (CASE 	WHEN bookingstatus = '1' THEN 'Job Not Started, Start Now'
						WHEN bookingstatus = '2' THEN 'End Job Now'
						WHEN bookingstatus = '3' THEN 'Job Complete, Enter OTP'
				 ELSE 'Drive To Job Location' END) AS bookingstatus";
	$mySQL .= " FROM `booking` B WHERE `bookedbyuser` = '1'";
	$mySQL .= " AND `bookingcomplete` = '0'";
	$mySQL .= " AND `acceptbydriver` = '1'";
	$mySQL .= " AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."'";
	$mySQL .= " AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
	$mySQL .= " ORDER BY `bookingdate` DESC";
	// echo $mySQL;
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	$bookingDetail = $rsTemp;
	//$resultArr['data'] = array("data" =>$rsTemp);
	
	//echo $mySQL;
	// FEATURES
	$mySQL = "";
	$mySQL = "SELECT";
	$mySQL .= " `idfeatures`";
	$mySQL .= ", `featurename`";
	$mySQL .= ", (CASE WHEN `featureprice` > 0 THEN featureprice ELSE '' END) AS featureprice";
	$mySQL .= ", (CASE WHEN `featuredepthprice` > 0 THEN featuredepthprice ELSE '' END) AS featuredepthprice";
	$mySQL .= " FROM `bookingdetail` WHERE `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' 
	AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
	$rsTempFeatures = $dbAccess->selectData($mySQL);
	$arrFeatures = array();
	if(!empty($rsTempFeatures)){
		$arrFeatures = array("features" => $rsTempFeatures);
	} else {
		$arrFeatures = array("features" => []);
	}
	// DRIVEWAY
	$mySQL = "";
	$mySQL = "SELECT 
		`iddrivewaytype`
		, `drivewaytypename`
	FROM `bookingdetail` WHERE iddrivewaytype <> '' AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
	$rsTempDriveway = $dbAccess->selectData($mySQL);
	$arrDriveway = array();
	if(!empty($rsTempDriveway)){
		$arrDriveway = array("driveway" => $rsTempDriveway);
	} else {
		$arrDriveway = array("driveway" => '');
	}
	
	if(!empty($rsTemp)){
		$status = array('code' => "200", "success" => true, 'message' => "Boooking Found");
		$data = array("data" => array_merge($bookingDetail, $arrFeatures, $arrDriveway));
		$tempStr = array_merge($status, $data);
	} else {
		$tempStr = array('code' => "203", "success" => false, 'message' => "Booking Not Found");
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT); 	
} else if ($fid == '24'){
	$iduser = $appFunctions->validHTML(trim($postData->{'iduser'}));
	
	$mySQL = "";
	$mySQL = "SELECT `idbooking`
		, `iduser`
		, (CASE WHEN `iddriver` <> '' THEN iddriver ELSE '' END) AS iddriver
		, `bookingno`
		, `bookingdate`
		, `starttime`
		, `fullname`
		, (SELECT (CASE WHEN fullname <> '' THEN fullname ELSE '' END) AS `fullname` FROM user U WHERE U.iduser = B.iddriver LIMIT 1) AS drivername
		, (SELECT (CASE WHEN profileimage <> '' THEN CONCAT('$baseURL', '/userphoto/', profileimage) ELSE '' END) AS `profileimage` FROM user U WHERE U.iduser = B.iddriver LIMIT 1) AS driverimage
		
		, (CASE WHEN `originlatlng` <> '' THEN originlatlng ELSE '' END) AS originlatlng
		, `destinationlatlng`
		, `totalamount`
		, (CASE WHEN `promotype` = 'PERCENT' AND `promovalue` <> '' THEN (`totalamount` - (`totalamount`*`promovalue`)/100)
				WHEN `promotype` = 'USD' AND `promovalue` <> '' THEN (`totalamount` - `promovalue`)
				ELSE totalamount END) AS `totalprice`

		, `bookedbyuser`
		, `createdon` AS `bookedbyuseron`
		, `acceptbydriver`
		, (CASE WHEN `acceptbydriveron` THEN `acceptbydriveron` ELSE '' END) AS `acceptbydriveron`
		, `rejectbydriver`
		, (CASE WHEN `rejectbydriveron` THEN `rejectbydriveron` ELSE '' END) AS `rejectbydriveron`
		, `cancelbyuser`
		, (CASE WHEN `cancelbyuseron` THEN `cancelbyuseron` ELSE '' END) AS `cancelbyuseron`
		, `cancelbydriver`
		, (CASE WHEN `cancelbydriveron` THEN `cancelbydriveron` ELSE '' END) AS `cancelbydriveron`";
	$mySQL .= ", bookingstatus AS bookingcode";
	$mySQL .= ", (CASE 	WHEN bookingstatus = '1' THEN 'On The Way'
					WHEN bookingstatus = '2' THEN 'Job Started'
					WHEN bookingstatus = '3' THEN 'Job Complete'
			 ELSE 'Job Not Started' END) AS bookingstatus";
	$mySQL .= " FROM `booking` B WHERE isactive = '1' AND `bookedbyuser` = '1' AND `iduser` = '".$appFunctions->validSQL($iduser,"")."'";
	$mySQL .= " ORDER BY `bookingdate` DESC";
	// echo $mySQL;
	
	$rsTemp = $dbAccess->selectData($mySQL);
	
	if(!empty($rsTemp)){
		$status = array('code' => "200", "success" => true, 'message' => "Boooking Found");
		$data = array("data"=>$rsTemp);
		$tempStr = array_merge($status, $data);
	} else {
		$tempStr = array('code' => "203", "success" => false, 'message' => "Booking Not Found");
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT); 
} else if ($fid == '25'){
	$iduser = $appFunctions->validHTML(trim($postData->{'iduser'}));
	$idbooking = $appFunctions->validHTML(trim($postData->{'idbooking'}));
			
	// BOOKING DETAIL
	$resultArr = array();

	$mySQL = "";
	$mySQL = "SELECT `idbooking`
		, `iduser`
		, `bookingno`
		, `bookingdate`
		, `starttime`
		, (CASE WHEN `endtime` <> '' THEN endtime ELSE '' END) AS endtime
		, `fullname`
		, `iddriver`
		, (SELECT (CASE WHEN fullname <> '' THEN fullname ELSE '' END) AS `fullname` FROM user U WHERE U.iduser = B.iddriver LIMIT 1) AS drivername
		, (SELECT (CASE WHEN profileimage <> '' THEN CONCAT('$baseURL', '/userphoto/', profileimage) ELSE '' END) AS `profileimage` FROM user U WHERE U.iduser = B.iddriver LIMIT 1) AS driverimage
		
		, (SELECT (CASE WHEN (SUM(rating) / COUNT(iduser)) > 0 THEN ROUND((SUM(rating) / COUNT(iduser)),1) ELSE '' END) AS rating FROM driver_rating DR WHERE DR.iddriver = B.iddriver LIMIT 1) AS driverrating
		
		, (CASE WHEN `paymentmode` <> '' THEN paymentmode ELSE '' END) AS paymentmode
		, (CASE WHEN `paymentstatus` <> '' THEN paymentstatus ELSE '' END) AS paymentstatus
		, (CASE WHEN `transactionid` <> '' THEN transactionid ELSE '' END) AS transactionid
		
		, (CASE WHEN `originaddress` <> '' THEN originaddress ELSE '' END) AS originaddress
		, (CASE WHEN `origincity` <> '' THEN origincity ELSE '' END) AS origincity
		, (CASE WHEN `originstate` <> '' THEN originstate ELSE '' END) AS originstate
		, (CASE WHEN `originzipcode` <> '' THEN originzipcode ELSE '' END) AS originzipcode
		, (CASE WHEN `originlatlng` <> '' THEN originlatlng ELSE '' END) AS originlatlng
		
		, (CASE WHEN `destinationaddress` <> '' THEN destinationaddress ELSE '' END) AS destinationaddress
		, (CASE WHEN `destinationcity` <> '' THEN destinationcity ELSE '' END) AS destinationcity
		, (CASE WHEN `destinationstate` <> '' THEN destinationstate ELSE '' END) AS destinationstate
		, (CASE WHEN `destinationzipcode` <> '' THEN destinationzipcode ELSE '' END) AS destinationzipcode
		, (CASE WHEN `destinationlatlng` <> '' THEN destinationlatlng ELSE '' END) AS destinationlatlng
		
		, 'Service Area' AS `areatitle`
		, `area`
		, `price`
		, `totalamount`
		, `featuresextracharge`
		, `featuresextrachargeamt`
		, `featuresextrachargetype`
		
		, `bookedbyuser`
		, `createdon` AS `bookedbyuseron`
		, `acceptbydriver`
		, (CASE WHEN `acceptbydriveron` THEN `acceptbydriveron` ELSE '' END) AS `acceptbydriveron`
		, `rejectbydriver`
		, (CASE WHEN `rejectbydriveron` THEN `rejectbydriveron` ELSE '' END) AS `rejectbydriveron`
		, `cancelbyuser`
		, (CASE WHEN `cancelbyuseron` THEN `cancelbyuseron` ELSE '' END) AS `cancelbyuseron`
		, `cancelbydriver`
		, (CASE WHEN `cancelbydriveron` THEN `cancelbydriveron` ELSE '' END) AS `cancelbydriveron`";
	$mySQL .= ", bookingstatus AS bookingcode";
	$mySQL .= ", (CASE 	WHEN bookingstatus = '1' THEN 'On The Way'
					WHEN bookingstatus = '2' THEN 'Job Started'
					WHEN bookingstatus = '3' THEN 'Job Complete'
			 ELSE 'Job Not Started' END) AS bookingstatus";
	$mySQL .= ", `bookingcomplete`
		, (CASE WHEN `bookingcompleteon` THEN `bookingcompleteon` ELSE '' END) AS `bookingcompleteon`
		, (CASE WHEN `bookingclosedopt` <> '' THEN bookingclosedopt ELSE '' END) AS bookingclosedopt
		
		, (CASE WHEN `promocode` <> '' THEN promocode ELSE '' END) AS promocode
		, (CASE WHEN `promovalue` <> '' THEN promovalue ELSE '' END) AS promovalue
		, (CASE WHEN `promotype` <> '' THEN promotype ELSE '' END) AS promotype
	FROM `booking` B WHERE `bookedbyuser` = '1' 
	AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' 
	AND `iduser` = '".$appFunctions->validSQL($iduser,"")."'";
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	//$bookingDetail = $rsTemp;
	$totalamount = $rsTemp['totalamount'];

	if(strtoupper($rsTemp['promotype']) == 'PERCENT'){
		$totalamount = $rsTemp['totalamount'] - ($rsTemp['totalamount']*$rsTemp['promovalue'])/100; 
	} else if(strtoupper($rsTemp['promotype']) == 'USD'){
		$totalamount = $totalamount - $rsTemp['promovalue'];
	}		
	
	if(!empty($rsTemp['driverimage'])){
		$driverimage = $rsTemp['driverimage'];
	} else {
		$driverimage = '';
	}
	$bookingDetail = array(
							"idbooking" => $rsTemp['idbooking']
							, "iduser" => $rsTemp['iduser']
							, "bookingno" => $rsTemp['bookingno']
							, "bookingdate" => $rsTemp['bookingdate']
							, "starttime" => $rsTemp['starttime']
							, "endtime" => $rsTemp['endtime']
							, "fullname" => $rsTemp['fullname']
							, "iddriver" => $rsTemp['iddriver']
							, "drivername" => $rsTemp['drivername']
							, "driverimage" => $driverimage
							, "driverrating" => $rsTemp['driverrating']
									
							, "paymentmode" => $rsTemp['paymentmode']
							, "paymentstatus" => $rsTemp['paymentstatus']
							, "transactionid" => $rsTemp['transactionid']
							
							, "originaddress" => $rsTemp['originaddress']
							, "origincity" => $rsTemp['origincity']
							, "originstate" => $rsTemp['originstate']
							, "originzipcode" => $rsTemp['originzipcode']
							, "originlatlng" => $rsTemp['originlatlng']
							
							, "destinationaddress" => $rsTemp['destinationaddress']
							, "destinationcity" => $rsTemp['destinationcity']
							, "destinationstate" => $rsTemp['destinationstate']
							, "destinationzipcode" => $rsTemp['destinationzipcode']
							, "destinationlatlng" => $rsTemp['destinationlatlng']
							
							, "areatitle" => $rsTemp['areatitle']
							, "area" => $rsTemp['area']
							, "price" => $rsTemp['price']
							, "totalamount" => number_format($rsTemp['totalamount'],2)
							, "totalprice" => number_format($totalamount,2)
							
							, "bookedbyuser" => $rsTemp['bookedbyuser']
							, "bookedbyuseron" => $rsTemp['bookedbyuseron']
							, "bookedbyusermsg" => $LoadMsg->bookingStatus($rsTemp['bookedbyuser'], 'bookedbyuser', $rsTemp['bookedbyuseron'])
							, "acceptbydriver" => $rsTemp['acceptbydriver']
							, "acceptbydriveron" => $rsTemp['acceptbydriveron']
							, "acceptbydrivermsg" => $LoadMsg->bookingStatus($rsTemp['acceptbydriver'], 'acceptbydriver', $rsTemp['acceptbydriveron'])
							, "rejectbydriver" => $rsTemp['rejectbydriver']
							, "rejectbydriveron" => $rsTemp['rejectbydriveron']
							, "rejectbydrivermsg" => $LoadMsg->bookingStatus($rsTemp['rejectbydriver'], 'rejectbydriver', $rsTemp['rejectbydriveron'])
							, "cancelbyuser" => $rsTemp['cancelbyuser']
							, "cancelbyuseron" => $rsTemp['cancelbyuseron']
							, "cancelbyusermsg" => $LoadMsg->bookingStatus($rsTemp['cancelbyuser'], 'cancelbyuser', $rsTemp['cancelbyuseron'])
							//, "cancelbydriver" => $rsTemp['cancelbydriver']
							//, "cancelbydriveron" => $rsTemp['cancelbydriveron']
							, "bookingcode" => $rsTemp['bookingcode']
							, "bookingstatus" => $rsTemp['bookingstatus']
							, "bookingstatusmsg" => $LoadMsg->bookingStatus($rsTemp['bookingcode'], 'bookingstatus', $rsTemp['bookingcompleteon'])
							
							, "bookingcomplete" => $rsTemp['bookingcomplete']
							, "bookingcompleteon" => $rsTemp['bookingcompleteon']
							, "bookingcompletemsg" => $LoadMsg->bookingStatus($rsTemp['bookingcomplete'], 'bookingcomplete', $rsTemp['bookingcompleteon'])
							, "bookingclosedopt" => $rsTemp['bookingclosedopt']
							
							, "promocode" => $rsTemp['promocode']
							, "promovalue" => $rsTemp['promovalue']
							, "promotype" => $rsTemp['promotype']
							//, "bookingclosedopt" => $rsTemp['bookingclosedopt']
						);
	// FEATURES
	$mySQL = "";
	$mySQL = "SELECT";
	$mySQL .= " (CASE WHEN `featureprice` > 0 THEN featureprice
					WHEN `featuredepthprice` > 0 THEN featuredepthprice
					ELSE ''
					END) AS fPrice";
	$mySQL .= " FROM `bookingdetail` WHERE featurename = 'Salt Driveway' AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."'";
	$rsTempSD = $dbAccess->selectSingleStmt($mySQL);
	
	$fPrice = 0;
	if(!empty($rsTempSD)){
		$fPrice = $rsTempSD['fPrice'];
	}
	
	if (strtoupper($rsTemp['featuresextrachargetype']) == 'PERCENT'){
		$nameamount = $rsTemp['featuresextracharge'] . ' ' .$rsTemp['featuresextrachargeamt']. '% Extra Charge';
		if($fPrice > '0'){
			// $amount = ($fPrice * $rsTemp['featuresextrachargeamt'])/100;
			$amount = $rsTemp['featuresextrachargeamt'];
		} else {
			$amount = '';
		}
	} else {
		$nameamount = $rsTemp['featuresextracharge'] . ' ' .$rsTemp['featuresextrachargeamt']. ' Extra Charge';
		if($fPrice > '0'){
			// $amount = $fPrice - $rsTemp['featuresextrachargeamt']; 
			$amount = $rsTemp['featuresextrachargeamt']; 
		} else {
			$amount = '';
		}
	}
	$featuresextracharge = $rsTemp['featuresextracharge'];
	
	$amounttype = $rsTemp['featuresextrachargetype'];
	$extracharge = array("additional" => array("nameamount" => $nameamount, "amount" => $amount, "amounttype" => $amounttype));	
		
	// FEATURES
	$mySQL = "";
	$mySQL = "SELECT";
	$mySQL .= " `idfeatures`";
	$mySQL .= ", `featurename`";
	$mySQL .= ", (CASE WHEN `featureprice` > 0 THEN featureprice";
	$mySQL .= "   WHEN `featuredepthprice` > 0 THEN featuredepthprice ELSE '' END) AS featureprice";
	$mySQL .= " FROM `bookingdetail` WHERE idfeatures <> '' AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' 
	AND idbooking IN (SELECT idbooking FROM booking WHERE `iduser` = '".$appFunctions->validSQL($iduser,"")."')";
	$rsTempFeatures = $dbAccess->selectData($mySQL);
	$arrFeatures = array();
	if(!empty($rsTempFeatures)){
		$arrFeatures = array("features" => $rsTempFeatures);
	} else {
		$arrFeatures = array("features" => '');
	}
	// DRIVEWAY
	$mySQL = "";
	$mySQL = "SELECT 
		`iddrivewaytype`
		, `drivewaytypename`
	FROM `bookingdetail` WHERE iddrivewaytype <> '' AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' 
	AND idbooking IN (SELECT idbooking FROM booking WHERE `iduser` = '".$appFunctions->validSQL($iduser,"")."')";
	$rsTempDriveway = $dbAccess->selectData($mySQL);
	$arrDriveway = array();
	if(!empty($rsTempDriveway)){
		$arrDriveway = array("driveway" => $rsTempDriveway);
	} else {
		$arrDriveway = array("driveway" => '');
	}
	
	if(!empty($rsTemp)){
		$status = array('code' => "200", "success" => true, 'message' => "Boooking Found");
		$data = array("data" => array_merge($bookingDetail, $extracharge, $arrFeatures, $arrDriveway));
		$tempStr = array_merge($status, $data);
	} else {
		$tempStr = array('code' => "203", "success" => false, 'message' => "Booking Not Found");
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '26'){
	$iduser = $appFunctions->validHTML(trim($postData->{'iduser'}));
	$idbooking = $appFunctions->validHTML(trim($postData->{'idbooking'}));
	$bookingno = $appFunctions->validHTML(trim($postData->{'bookingno'}));
	$paymentmode = $appFunctions->validHTML(trim($postData->{'paymentmode'}));
	$paymentstatus = $appFunctions->validHTML(trim($postData->{'paymentstatus'}));
	$paymentstatuscode = $appFunctions->validHTML(trim($postData->{'paymentstatuscode'}));
	$transactionid = $appFunctions->validHTML(trim($postData->{'transactionid'}));
	
	//if(strtoupper($paymentstatus) == 'SUCCESSFUL'){
	if(strtoupper($paymentstatuscode) == 'I00001'){
		if(!empty($idbooking) AND !empty($bookingno)){
			// ########
			// SEND OTP
			// ########
			// GET USER FCM TOKEN
			$mySQL = "";
			$mySQL = "SELECT fcmtoken FROM user WHERE iduser = '".$appFunctions->validSQL($iduser,"")."'";
			//echo $mySQL;
			$rsTemp = $dbAccess->SelectSingleStmt($mySQL);
			$fcmtoken = $rsTemp['fcmtoken'];
			//echo $fcmtoken;
			
			$otp = RAND(1009, 9999);
			
			$mySQL = "";
			$mySQL = "SELECT 
				`isnotification`
				, `profileprivacy`
			FROM `user` WHERE `isnotification` = '1' AND iduser = '".$appFunctions->validSQL($iduser,"")."'";
			// echo $mySQL;
			$rsTemp = $dbAccess->selectSingleStmt($mySQL);
			
			if(!empty($rsTemp)){
				// $body = 'You have recieved job completed OTP ' . $otp .' for booking no. : ' .$bookingno;
				$body = 'Thank you for payment! Your booking successfully saved and booking no. : ' .$bookingno;
				$toTokenId = $fcmtoken;
				
				$mySQL = "";
				$mySQL = "INSERT INTO `notification` (`idsender`, `idreceiver`, `idsource`, `notificationtype`, `notificatiomode`, `sort`, `description`, `isactive`, `isviewed`) 
					VALUES ('".$appFunctions->validSQL($iduser,"")."', '".$appFunctions->validSQL($iduser,"")."', '".$appFunctions->validSQL($idbooking,"")."', 'Job Completed OTP', 'TEXT', '6', '".$body."', '1', '0')";
				$dbAccess->queryExec($mySQL);
				
				$payload = array(
					//'to'=>'/topics/my-app',
					//'to'=>'fqnkZbkucDw:APA91bFQwkDe4SUqqKwfXVG1ANWiLAqUqcEC_GjkjoNmia5JcAoHtqvC6KwIGrI97lM-stF7pq9cSG1xnAfxTr-HxmCivp0OWZ3CtH7lMYIqWuRzDCMVrdS36LlqL-4HPUWO_xwQB3KU',
					'to'=>$toTokenId,
					'priority'=>'high',
					"mutable_content"=>true,
					"notification"=>array("title"=> 'Booked new appointment',"body"=> $body)
					//'data'=>array("idchat" => $idchat, "message" => $message, "createdAt" => $entryDate, "sender" => array("idsender" => $iduser, "name" => $fullname, "email" => $email))
				);
				
				//$fcmApiKey = "AAAAQNE260M:APA91bGpqtB_0AQqlfOAUDqtGstsVNuhy62ZtpQimyiz3TMk2-FlzDf36zdQkItPaVLcXQhvT2TyQIx3CIQdnIfMihEDQTEr4FaTduwT9FBTjhOZM3hMHZipSwepFjMTDPDeu3FtRovh";
				$headers = array('Authorization: key='.$fcmApiKey, 'Content-Type: application/json');

				$ch = curl_init();
				//curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
				curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $payload ) );
				$result = curl_exec($ch );
				curl_close( $ch );
				//echo '<pre>';
				//var_dump($result);
				//exit;	
			}
			// #################################
			
			// UPDATE BOKING TABLE WITH BOOKING CLOSED OTP
			$mySQL = "";
			$mySQL = "UPDATE booking SET isactive = '1'";
			// $mySQL .= ", bookingclosedopt = '".$appFunctions->validSQL($otp,"")."'";
			$mySQL .= ", paymentmode = '".$appFunctions->validSQL($paymentmode,"")."'";
			$mySQL .= ", paymentstatus = '".$appFunctions->validSQL($paymentstatus,"")."'";
			$mySQL .= ", transactionid = '".$appFunctions->validSQL($transactionid,"")."'";
			$mySQL .= " WHERE iduser = '".$appFunctions->validSQL($iduser,"")."' AND idbooking = '".$appFunctions->validSQL($idbooking,"")."'";
			//echo $mySQL;
			$dbAccess->queryExec($mySQL);
			
			// BOOKING DETAIL
			$resultArr = array();
	
			$mySQL = "";
			$mySQL = "SELECT `idbooking`
				, `iduser`
				, `bookingno`
				, `bookingdate`
				, `starttime`
				, (CASE WHEN `endtime` <> '' THEN `endtime` ELSE '' END) AS `endtime`
				, `fullname`
				
				, (CASE WHEN `originaddress` <> '' THEN `originaddress` ELSE '' END) AS `originaddress`
				, (CASE WHEN `origincity` <> '' THEN `origincity` ELSE '' END) AS `origincity`
				, (CASE WHEN `originstate` <> '' THEN `originstate` ELSE '' END) AS `originstate`
				, (CASE WHEN `originzipcode` <> '' THEN `originzipcode` ELSE '' END) AS `originzipcode`
				, `originlatlng`
				
				, (CASE WHEN `destinationaddress` <> '' THEN `destinationaddress` ELSE '' END) AS `destinationaddress`
				, (CASE WHEN `destinationcity` <> '' THEN `destinationcity` ELSE '' END) AS `destinationcity`
				, (CASE WHEN `destinationstate` <> '' THEN `destinationstate` ELSE '' END) AS `destinationstate`
				, (CASE WHEN `destinationzipcode` <> '' THEN `destinationzipcode` ELSE '' END) AS `destinationzipcode`
				, `destinationlatlng`
				
				, 'Service Area' AS `areatitle`
				, `area`
				, `price`
				, `totalamount`
				, `featuresextracharge`
				, `featuresextrachargeamt`
				, `featuresextrachargetype`
				, `bookingclosedopt`
				
				, (CASE WHEN `promocode` <> '' THEN promocode ELSE '' END) AS promocode
				, (CASE WHEN `promovalue` <> '' THEN promovalue ELSE '' END) AS promovalue
				, (CASE WHEN `promotype` <> '' THEN promotype ELSE '' END) AS promotype
			FROM `booking` B WHERE `bookedbyuser` = '1' 
			AND `acceptbydriver` = '1'"; 
			$mySQL .= "	AND `bookingstatus` = '0'"; 
			$mySQL .= "	AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' 
			AND `iduser` = '".$appFunctions->validSQL($iduser,"")."'";
			// echo $mySQL;
			$rsTemp = $dbAccess->selectSingleStmt($mySQL);
			//$bookingDetail = $rsTemp;
			$totalamount = $rsTemp['totalamount'];
		
			if(strtoupper($rsTemp['promotype']) == 'PERCENT'){
				$totalamount = $rsTemp['totalamount'] - ($rsTemp['totalamount']*$rsTemp['promovalue'])/100; 
			} else if(strtoupper($rsTemp['promotype']) == 'USD'){
				$totalamount = $totalamount - $rsTemp['promovalue'];
			}		
			
			$bookingDetail = array(
				"idbooking" => $rsTemp['idbooking']
				, "iduser" => $rsTemp['iduser']
				, "bookingno" => $rsTemp['bookingno']
				, "bookingdate" => $rsTemp['bookingdate']
				, "starttime" => $rsTemp['starttime']
				, "endtime" => $rsTemp['endtime']
				, "fullname" => $rsTemp['fullname']
				
				, "originaddress" => $rsTemp['originaddress']
				, "origincity" => $rsTemp['origincity']
				, "originstate" => $rsTemp['originstate']
				, "originzipcode" => $rsTemp['originzipcode']
				, "originlatlng" => $rsTemp['originlatlng']
				
				, "destinationaddress" => $rsTemp['destinationaddress']
				, "destinationcity" => $rsTemp['destinationcity']
				, "destinationstate" => $rsTemp['destinationstate']
				, "destinationzipcode" => $rsTemp['destinationzipcode']
				, "destinationlatlng" => $rsTemp['destinationlatlng']
				
				, "areatitle" => $rsTemp['areatitle']
				, "area" => $rsTemp['area']
				, "price" => $rsTemp['price']
				, "totalamount" => $rsTemp['totalamount']
				, "totalprice" => $totalamount
				, "promocode" => $rsTemp['promocode']
				, "promovalue" => $rsTemp['promovalue']
				, "promotype" => $rsTemp['promotype']
				, "bookingclosedopt" => $rsTemp['bookingclosedopt']
			);
			// FEATURES
			$mySQL = "";
			$mySQL = "SELECT";
			$mySQL .= " (CASE WHEN `featureprice` > 0 THEN featureprice
							WHEN `featuredepthprice` > 0 THEN featuredepthprice
							ELSE ''
							END) AS fPrice";
			$mySQL .= " FROM `bookingdetail` WHERE featurename = 'Salt Driveway' AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."'";
			$rsTempSD = $dbAccess->selectSingleStmt($mySQL);
			
			$fPrice = 0;
			if(!empty($rsTempSD)){
				$fPrice = $rsTempSD['fPrice'];
			}
			
			if (strtoupper($rsTemp['featuresextrachargetype']) == 'PERCENT'){
				$nameamount = $rsTemp['featuresextracharge'] . ' ' .$rsTemp['featuresextrachargeamt']. '% Extra Charge';
				if($fPrice > '0'){
					$amount = ($fPrice * $rsTemp['featuresextrachargeamt'])/100;
				} else {
					$amount = '';
				}
			} else {
				$nameamount = $rsTemp['featuresextracharge'] . ' $' .$rsTemp['featuresextrachargeamt']. ' Extra Charge';
				if($fPrice > '0'){
					$amount = $fPrice - $rsTemp['featuresextrachargeamt'];
				} else {
					$amount = '';
				}
			}
			$featuresextracharge = $rsTemp['featuresextracharge'];
			
			$amounttype = $rsTemp['featuresextrachargetype'];
			$extracharge = array("additional" => array("nameamount" => $nameamount, "amount" => $amount, "amounttype" => $amounttype));	
			
			
			// FEATURES
			$mySQL = "";
			$mySQL = "SELECT";
			$mySQL .= " `idfeatures`";
			$mySQL .= ", `featurename`";
			$mySQL .= ", (CASE WHEN `featureprice` > 0 THEN featureprice";
			$mySQL .= "   WHEN `featuredepthprice` > 0 THEN featuredepthprice ELSE '' END) AS featureprice";
			$mySQL .= " FROM `bookingdetail` WHERE `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' 
			AND idbooking IN (SELECT idbooking FROM booking WHERE `iduser` = '".$appFunctions->validSQL($iduser,"")."')";
			$rsTempFeatures = $dbAccess->selectData($mySQL);
			$arrFeatures = array();
			if(!empty($rsTempFeatures)){
				$arrFeatures = array("features" => $rsTempFeatures);
			} else {
				$arrFeatures = array("features" => '');
			}
			// DRIVEWAY
			$mySQL = "";
			$mySQL = "SELECT 
				`iddrivewaytype`
				, `drivewaytypename`
			FROM `bookingdetail` WHERE iddrivewaytype <> '' AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' 
			AND idbooking IN (SELECT idbooking FROM booking WHERE `iduser` = '".$appFunctions->validSQL($iduser,"")."')";
			$rsTempDriveway = $dbAccess->selectData($mySQL);
			$arrDriveway = array();
			if(!empty($rsTempDriveway)){
				$arrDriveway = array("driveway" => $rsTempDriveway);
			} else {
				$arrDriveway = array("driveway" => '');
			}
			
			if(!empty($rsTemp)){
				// $status = array('code' => "200", "success" => true, 'message' => "Boooking Completed OTP send on your phone");
				$status = array('code' => "200", "success" => true, 'message' => "Thank you for payment! Your booking successfully saved");
				$data = array("data" => array_merge($bookingDetail, $extracharge, $arrFeatures, $arrDriveway));
				$tempStr = array_merge($status, $data);
			} else {
				$tempStr = array('code' => "203", "success" => false, 'message' => "Booking Not Found");
			}
		} else {
			$tempStr = array('code' => "203", "success" => false, 'message' => "Payment transaction failed");
		}
	} else {
		$mySQL = "";
		$mySQL = "UPDATE booking SET isactive = '0'";
		$mySQL .= ", paymentmode = '".$appFunctions->validSQL($paymentmode,"")."'";
		$mySQL .= ", paymentstatus = '".$appFunctions->validSQL($paymentstatus,"")."'";
		$mySQL .= ", transactionid = '".$appFunctions->validSQL($transactionid,"")."'";
		$mySQL .= " WHERE iduser = '".$appFunctions->validSQL($iduser,"")."' AND idbooking = '".$appFunctions->validSQL($idbooking,"")."'";
		//echo $mySQL;
		$dbAccess->queryExec($mySQL);
		
		$tempStr = array('code' => "203", "success" => false, 'message' => "Payment transaction failed");
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '27'){
	$iddriver = $appFunctions->validHTML(trim($postData->{'iddriver'}));
	$idbooking = $appFunctions->validHTML(trim($postData->{'idbooking'}));
	$bookingclosedopt = $appFunctions->validHTML(trim($postData->{'bookingclosedopt'}));
	
	if(empty($bookingclosedopt)){
		$tempStr = array('code' => "203", "success" => false, 'message' => "Please enter booking closed OTP");
	} else {
		$mySQL = "";
		$mySQL = "SELECT COUNT(0) AS isExist FROM booking WHERE 
			iddriver = '".$appFunctions->validSQL($iddriver,"")."' 
			AND idbooking = '".$appFunctions->validSQL($idbooking,"")."'
			AND bookingclosedopt = '".$appFunctions->validSQL($bookingclosedopt,"")."'";
		$rsTemp = $dbAccess->selectSingleStmt($mySQL);
		
		if($rsTemp['isExist'] == 0){
			$tempStr = array('code' => "203", "success" => false, 'message' => "Please enter valid booking closed OTP");
		} else {
			$mySQL = "";
			$mySQL = "UPDATE booking SET isactive = '1'";
			$mySQL .= ", bookingcomplete = '1'";
			$mySQL .= ", bookingclosedopt = ''";
			$mySQL .= " WHERE iddriver = '".$appFunctions->validSQL($iddriver,"")."' 
				AND idbooking = '".$appFunctions->validSQL($idbooking,"")."'
				AND bookingclosedopt = '".$appFunctions->validSQL($bookingclosedopt,"")."'";
			//echo $mySQL;
			$dbAccess->queryExec($mySQL);
			
			// #################################
			if(!empty($idbooking)){
				$mySQL = "";
				$mySQL = "SELECT iduser, bookingno FROM booking";
				$mySQL .= " WHERE iddriver = '".$appFunctions->validSQL($iddriver,"")."' 
					AND idbooking = '".$appFunctions->validSQL($idbooking,"")."'";
				// echo $mySQL;
				$rsTemp = $dbAccess->SelectSingleStmt($mySQL);
				$iduser = $rsTemp['iduser'];
				$bookingno = $rsTemp['bookingno'];
				
				// ########
				// SEND OTP
				// ########
				// GET STYLISH FCM TOKEN
				$mySQL = "";
				$mySQL = "SELECT fcmtoken FROM user WHERE iduser = '".$appFunctions->validSQL($iduser,"")."'";
				//echo $mySQL;
				$rsTemp = $dbAccess->SelectSingleStmt($mySQL);
				$fcmtoken = $rsTemp['fcmtoken'];
				//echo $fcmtoken;
				
				$notificationcode = '6';
				$notificationDataTitle = 'Job completed by driver';
				$body = 'Job completed by driver, booking no. : ' .$bookingno;
				$message = $body;
				$toTokenId = $fcmtoken;
				
				$mySQL = "";
				$mySQL = "INSERT INTO `notification` (`idsender`, `idreceiver`, `idsource`, `notificationtype`, `notificatiomode`, `sort`, `description`, `isactive`, `isviewed`) 
					VALUES ('".$appFunctions->validSQL($iddriver,"")."', '".$appFunctions->validSQL($iduser,"")."', '".$appFunctions->validSQL($idbooking,"")."', 'Job Completed', 'TEXT', '6', '".$body."', '1', '0')";
				$dbAccess->queryExec($mySQL);
				
				$payload = array(
					//'to'=>'/topics/my-app',
					//'to'=>'fqnkZbkucDw:APA91bFQwkDe4SUqqKwfXVG1ANWiLAqUqcEC_GjkjoNmia5JcAoHtqvC6KwIGrI97lM-stF7pq9cSG1xnAfxTr-HxmCivp0OWZ3CtH7lMYIqWuRzDCMVrdS36LlqL-4HPUWO_xwQB3KU',
					'to'=>$toTokenId,
					'priority'=>'high',
					"mutable_content"=>true,
					"notification"=>array("title"=> $notificationDataTitle,"body"=> $body),
					"data"=> array("response" => array("usertype" => "CUSTOMER", "iddriver" => $iddriver, "title" => $notificationDataTitle, "message" => $message, "timestamp" => $nowtime, "image" => "", "notificationcode" => $notificationcode, "notificationtype" => $notificationDataTitle, "idbooking" => $idbooking, "bookingno" => $bookingno, "iduser" => $iduser))
				);
				
				//$fcmApiKey = "AAAAQNE260M:APA91bGpqtB_0AQqlfOAUDqtGstsVNuhy62ZtpQimyiz3TMk2-FlzDf36zdQkItPaVLcXQhvT2TyQIx3CIQdnIfMihEDQTEr4FaTduwT9FBTjhOZM3hMHZipSwepFjMTDPDeu3FtRovh";
				$headers = array('Authorization: key='.$fcmApiKey, 'Content-Type: application/json');

				$ch = curl_init();
				//curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
				curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $payload ) );
				$result = curl_exec($ch );
				curl_close( $ch );
				//echo '<pre>';
				//var_dump($result);
				//exit;	
			}
			// #################################
			
			
			$tempStr = array('code' => "200", "success" => true, 'message' => "Job completed by driver.");
		}
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '28'){
	$iduser = $appFunctions->validHTML(trim($postData->{'iduser'}));
	
	$mySQL = "";
	$mySQL = "SELECT `idusersubscription`"; 
	$mySQL .= " , `iduser`"; 
	$mySQL .= " , `idservice`"; 
	//$mySQL .= " , `name`"; 
	//$mySQL .= " , `price`"; 
	$mySQL .= " , (CASE WHEN `startdate` <> '' THEN startdate ELSE '' END) AS startdate"; 
	$mySQL .= " , (CASE WHEN `enddate` <> '' THEN enddate ELSE '' END) AS enddate"; 
	$mySQL .= " FROM `usersubscription` WHERE `isactive` = '1'";
	$mySQL .= " AND `iduser` = '".$appFunctions->validSQL($iduser,"")."'";
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	
	$idservice = '2';
	$isSubscribe = '0';
	
	if (!empty($rsTemp)){
		$idservice = $rsTemp['idservice'];
		$isSubscribe = '1';
		$userdetail = array("userdetail" => $rsTemp);
	} else {
		$userdetail = array("userdetail" => '');
	}
	
	$mySQL = "";
	$mySQL = "SELECT idservice
		, name
		, (CASE WHEN `subtitle` <> '' THEN subtitle ELSE '' END) AS subtitle
		, mrp AS price";
	//$mySQL .= " , (CASE WHEN discount > 0 THEN discount ELSE '' END) AS discount";
	$mySQL .= " FROM service WHERE 
	isactive = '1'";
	//$mySQL .= "AND `iduser` = '".$appFunctions->validSQL($iduser,"")."'"; 
	$mySQL .= " AND `idservice` = '".$appFunctions->validSQL($idservice,"")."'";
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	$service =  array();	
	if(!empty($rsTemp)){
		$service = array("subscriptionprice" => $rsTemp);
	}
	
	// Service Feature 
	$mySQL = "";
	$mySQL = "SELECT idservicefeature
		, idservice
		, title
		, image
	FROM servicefeature WHERE isactive='1' AND `idservice` = '".$rsTemp['idservice']."'";
	$rsTemp = $dbAccess->selectData($mySQL);
	$servicefeature = array();
	$servicefeature = array("servicefeature" => $rsTemp);
	$status = array('code' => "200", "success" => true, 'message' => "Subscriber list");
	$data = array("data" => array_merge(array("isSubscribe" => $isSubscribe), $userdetail, $service, $servicefeature));
	$tempStr = array_merge($status,$data);
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '29'){
	$iduser = $appFunctions->validHTML(trim($postData->{'iduser'}));
	$idservice = $appFunctions->validHTML(trim($postData->{'idservice'}));
	$name = $dbAccess->getDetail('service', 'name', 'idservice', $idservice);
	$subtitle = $dbAccess->getDetail('service', 'subtitle', 'idservice', $idservice);
	$price = $appFunctions->validHTML(trim($postData->{'price'}));
	
	$t = explode(" ",microtime());
	$subscriptioncode = date($numDate,$t[1]) . substr((string)$t[0],-4,3);
		 
	$mySQL = "";
	$mySQL = "INSERT INTO usersubscription SET";
	$mySQL .= " iduser = '".$appFunctions->validSQL($iduser,"")."'";
	$mySQL .= ", subscriptioncode = '".$appFunctions->validSQL($subscriptioncode,"")."'";
	$mySQL .= ", idservice = '".$appFunctions->validSQL($idservice,"")."'";
	$mySQL .= ", name = '".$appFunctions->validSQL($name,"")."'";
	$mySQL .= ", subtitle = '".$appFunctions->validSQL($subtitle,"")."'";
	$mySQL .= ", price = '".$appFunctions->validSQL($price,"")."'";
	$mySQL .= ", isactive = '0'";
	$dbAccess->queryExec($mySQL);
	$idusersubscription = $dbAccess->LastId();
	
	$mySQL = "";
	$mySQL = "INSERT INTO usersubscriptionfeature (";
	$mySQL .= " idusersubscription";
	$mySQL .= ", idservice";
	$mySQL .= ", iduser";
	$mySQL .= ", title";
	$mySQL .= ", image";
	$mySQL .= ", isactive";
	$mySQL .= ")";
	$mySQL .= "SELECT";
	$mySQL .= " '".$appFunctions->validSQL($idusersubscription,"")."'";
	$mySQL .= ", idservice";
	$mySQL .= ", '".$appFunctions->validSQL($iduser,"")."'";
	$mySQL .= ", title";
	$mySQL .= ", image";
	$mySQL .= ", '1'";
	$mySQL .= " FROM servicefeature";
	$mySQL .= " WHERE isactive = '1' AND idservice = '".$appFunctions->validSQL($idservice,"")."'";
	$dbAccess->queryExec($mySQL);
		
	$mySQL = "";
	$mySQL = "SELECT idpayment, paymentmethod FROM `payment` WHERE isactive = '1'";
	$rsTemp = $dbAccess->selectData($mySQL);
	
	$status = array('code' => "200", "success" => true, 'message' => "Make payment & subscriber now");
	$data = array("data" => array_merge(array("iduser" => $iduser, "idusersubscription" => $idusersubscription, "subscriptioncode" => $subscriptioncode, "price" => $price), array("paymentmethod" => $rsTemp)));
	$tempStr = array_merge($status,$data);
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '30'){
	$iduser = $appFunctions->validHTML(trim($postData->{'iduser'}));
	$idusersubscription = $appFunctions->validHTML(trim($postData->{'idbooking'}));
	$subscriptioncode = $appFunctions->validHTML(trim($postData->{'bookingno'}));
	$paymentmode = $appFunctions->validHTML(trim($postData->{'paymentmode'}));
	$paymentstatus = $appFunctions->validHTML(trim($postData->{'paymentstatus'}));
	$paymentstatuscode = $appFunctions->validHTML(trim($postData->{'paymentstatuscode'}));
	$transactionid = $appFunctions->validHTML(trim($postData->{'transactionid'}));
	
	//if(strtoupper($paymentstatus) == 'SUCCESSFUL'){
	if(strtoupper($paymentstatuscode) == 'I00001'){
		if(!empty($idusersubscription) AND !empty($subscriptioncode)){
			/* // ########
			// SEND OTP
			// ########
			// GET STYLISH FCM TOKEN
			$mySQL = "";
			$mySQL = "SELECT fcmtoken FROM user WHERE iduser = '".$appFunctions->validSQL($iduser,"")."'";
			//echo $mySQL;
			$rsTemp = $dbAccess->SelectSingleStmt($mySQL);
			$fcmtoken = $rsTemp['fcmtoken'];
			//echo $fcmtoken;
			
			$otp = RAND(1009, 9999);
			
			$body = 'You have recieved job completed OTP ' . $otp .' for booking no. : ' .$bookingno;
			$toTokenId = $fcmtoken;
			
			$mySQL = "";
			$mySQL = "INSERT INTO `notification` (`idsender`, `idreceiver`, `idsource`, `notificationtype`, `notificatiomode`, `sort`, `description`, `isactive`, `isviewed`) 
				VALUES ('".$appFunctions->validSQL($iduser,"")."', '".$appFunctions->validSQL($iduser,"")."', '".$appFunctions->validSQL($idbooking,"")."', 'Job Completed OTP', 'TEXT', '6', '".$body."', '1', '0')";
			$dbAccess->queryExec($mySQL);
			
			$payload = array(
				//'to'=>'/topics/my-app',
				//'to'=>'fqnkZbkucDw:APA91bFQwkDe4SUqqKwfXVG1ANWiLAqUqcEC_GjkjoNmia5JcAoHtqvC6KwIGrI97lM-stF7pq9cSG1xnAfxTr-HxmCivp0OWZ3CtH7lMYIqWuRzDCMVrdS36LlqL-4HPUWO_xwQB3KU',
				'to'=>$toTokenId,
				'priority'=>'high',
				"mutable_content"=>true,
				"notification"=>array("title"=> 'Booked new appointment',"body"=> $body)
				//'data'=>array("idchat" => $idchat, "message" => $message, "createdAt" => $entryDate, "sender" => array("idsender" => $iduser, "name" => $fullname, "email" => $email))
			);
			
			//$fcmApiKey = "AAAAQNE260M:APA91bGpqtB_0AQqlfOAUDqtGstsVNuhy62ZtpQimyiz3TMk2-FlzDf36zdQkItPaVLcXQhvT2TyQIx3CIQdnIfMihEDQTEr4FaTduwT9FBTjhOZM3hMHZipSwepFjMTDPDeu3FtRovh";
			$headers = array('Authorization: key='.$fcmApiKey, 'Content-Type: application/json');

			$ch = curl_init();
			//curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
			curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $payload ) );
			$result = curl_exec($ch );
			curl_close( $ch );
			//echo '<pre>';
			//var_dump($result);
			//exit;	
			// ################################# */
			$startdate = $currDate;
			$enddate = date('Y-m-d', strtotime('+30 days',strtotime($currDate)));
			
			// UPDATE BOKING TABLE WITH BOOKING CLOSED OTP
			$mySQL = "";
			$mySQL = "UPDATE usersubscription SET isactive = '1'";
			$mySQL .= ", startdate = '".$appFunctions->validSQL($startdate,"")."'";
			$mySQL .= ", enddate = '".$appFunctions->validSQL($enddate,"")."'";
			$mySQL .= ", paymentmode = '".$appFunctions->validSQL($paymentmode,"")."'";
			$mySQL .= ", paymentstatus = '".$appFunctions->validSQL($paymentstatus,"")."'";
			$mySQL .= ", transactionid = '".$appFunctions->validSQL($transactionid,"")."'";
			$mySQL .= " WHERE iduser = '".$appFunctions->validSQL($iduser,"")."' AND idusersubscription = '".$appFunctions->validSQL($idusersubscription,"")."'";
			//echo $mySQL;
			$dbAccess->queryExec($mySQL);
			
			$tempStr = array('code' => "200", "success" => true, 'message' => "Subscription Completed");
		} else {
			$tempStr = array('code' => "203", "success" => false, 'message' => "Payment transaction failed");
		}
	} else {
		$mySQL = "";
		$mySQL = "UPDATE usersubscription SET isactive = '0'";
		$mySQL .= ", paymentmode = '".$appFunctions->validSQL($paymentmode,"")."'";
		$mySQL .= ", paymentstatus = '".$appFunctions->validSQL($paymentstatus,"")."'";
		$mySQL .= ", transactionid = '".$appFunctions->validSQL($transactionid,"")."'";
		$mySQL .= " WHERE iduser = '".$appFunctions->validSQL($iduser,"")."' AND idusersubscription = '".$appFunctions->validSQL($idusersubscription,"")."'";
		//echo $mySQL;
		$dbAccess->queryExec($mySQL);
		
		$tempStr = array('code' => "203", "success" => false, 'message' => "Payment transaction failed");
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '31'){
	$iduser = $appFunctions->validHTML(trim($postData->{'iduser'}));
	$idbooking = $appFunctions->validHTML(trim($postData->{'idbooking'}));
			
	// BOOKING DETAIL
	$resultArr = array();

	$mySQL = "";
	$mySQL = "SELECT `idbooking`
		, `iduser`
		, `bookingno`
		, `bookingdate`
		, `starttime`
		, (CASE WHEN `endtime` <> '' THEN endtime ELSE '' END) AS endtime
		, `fullname`
		, iddriver
		, (SELECT (CASE WHEN fullname <> '' THEN fullname ELSE '' END) AS `fullname` FROM user U WHERE U.iduser = B.iddriver LIMIT 1) AS drivername
		, (SELECT (CASE WHEN profileimage <> '' THEN CONCAT('$baseURL', '/userphoto/', profileimage) ELSE '' END) AS `profileimage` FROM user U WHERE U.iduser = B.iddriver LIMIT 1) AS driverimage
		, (SELECT (CASE WHEN (lat <> '' AND lat IS NOT NULL) AND (lng <> '' AND lng IS NOT NULL) THEN CONCAT(lat, ',', lng) ELSE '' END) AS driverlatlng FROM user U WHERE U.iduser = B.iddriver LIMIT 1) AS driverlatlng
		, (SELECT (CASE WHEN mobile <> '' THEN mobile ELSE '' END) AS `mobile` FROM user U WHERE U.iduser = B.iddriver LIMIT 1) AS driverphone
		, (SELECT (CASE WHEN (SUM(rating) / COUNT(iduser)) > 0 THEN ROUND((SUM(rating) / COUNT(iduser)),1) ELSE '' END) AS rating FROM driver_rating DR WHERE DR.iddriver = B.iddriver LIMIT 1) AS driverrating";
	// $mySQL .= ", (CASE WHEN `originaddress` <> '' THEN originaddress ELSE '' END) AS originaddress";
	// $mySQL .= ", (CASE WHEN `origincity` <> '' THEN origincity ELSE '' END) AS origincity";
	// $mySQL .= ", (CASE WHEN `originstate` <> '' THEN originstate ELSE '' END) AS originstate";
	// $mySQL .= ", (CASE WHEN `originzipcode` <> '' THEN originzipcode ELSE '' END) AS originzipcode";
	// $mySQL .= ", (CASE WHEN `originlatlng` <> '' THEN originlatlng ELSE '' END) AS originlatlng";
	$mySQL .= ", (CASE WHEN `destinationaddress` <> '' THEN destinationaddress ELSE '' END) AS destinationaddress";
	$mySQL .= ", (CASE WHEN `destinationcity` <> '' THEN destinationcity ELSE '' END) AS destinationcity
		, (CASE WHEN `destinationstate` <> '' THEN destinationstate ELSE '' END) AS destinationstate
		, (CASE WHEN `destinationzipcode` <> '' THEN destinationzipcode ELSE '' END) AS destinationzipcode
		, (CASE WHEN `destinationlatlng` <> '' THEN destinationlatlng ELSE '' END) AS destinationlatlng";
	$mySQL .= " FROM `booking` B WHERE `bookedbyuser` = '1' 
	AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' 
	AND `iduser` = '".$appFunctions->validSQL($iduser,"")."'";
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	//$bookingDetail = $rsTemp;
	
	if(!empty($rsTemp['driverimage'])){
		$driverimage = $rsTemp['driverimage'];
	} else {
		$driverimage = '';
	}
	$bookingDetail = array(
							"idbooking" => $rsTemp['idbooking']
							, "iduser" => $rsTemp['iduser']
							, "bookingno" => $rsTemp['bookingno']
							, "bookingdate" => $rsTemp['bookingdate']
							, "starttime" => $rsTemp['starttime']
							, "endtime" => $rsTemp['endtime']
							, "fullname" => $rsTemp['fullname']
							, "iddriver" => $rsTemp['iddriver']
							, "drivername" => $rsTemp['drivername']
							, "driverimage" => $driverimage
							, "driverlatlng" => $rsTemp['driverlatlng']
							, "driverphone" => $rsTemp['driverphone']
							, "driverrating" => $rsTemp['driverrating']
									
							//, "originaddress" => $rsTemp['originaddress']
							//, "origincity" => $rsTemp['origincity']
							//, "originstate" => $rsTemp['originstate']
							//, "originzipcode" => $rsTemp['originzipcode']
							//, "originlatlng" => $rsTemp['originlatlng']
							
							, "destinationaddress" => $rsTemp['destinationaddress']
							, "destinationcity" => $rsTemp['destinationcity']
							, "destinationstate" => $rsTemp['destinationstate']
							, "destinationzipcode" => $rsTemp['destinationzipcode']
							, "destinationlatlng" => $rsTemp['destinationlatlng']
						);
						
	if(!empty($rsTemp)){
		$status = array('code' => "200", "success" => true, 'message' => "Boooking Found");
		$data = array("data" => $bookingDetail);
		$tempStr = array_merge($status, $data);
	} else {
		$tempStr = array('code' => "203", "success" => false, 'message' => "Booking Not Found");
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} elseif ($fid=='32') {
	
	$iduser = $appFunctions->validHTML(trim($postData->{'iduser'}));
	$isactive = $appFunctions->validHTML(trim($postData->{'isactive'}));
	$mySQL = "";
	$mySQL = "SELECT COUNT(0) AS isExist FROM user WHERE iduser='$iduser'";
	//echo $mySQL;die();
	$rsTemp = $dbAccess-selectSingleStmt($mySQL);
	//print_r($rsTemp);

	if ($rsTemp['isExist']>0) 
	{

		$mySQL = "";
		$mySQL = "UPDATE user SET isactive='".$isactive."' WHERE iduser='".$iduser."'";
		$queryExec = $dbAccess->queryExec($mySQL);

		if ($queryExec) {

			$mySQL = "";
			$mySQL = "SELECT iduser,isactive FROM user WHERE iduser='$iduser'";
			$rsTemp = $dbAccess-selectSingleStmt($mySQL);
			$data = array("data"=>$rsTemp);
			$status = array('code' => "200", "success" => true, 'message' => "notification deactivated");
			$tempStr = array_merge($data,$status);

		}
		else
		{
			$status = array('code' => "203", "success" => true, 'message' => "Server error found");

		}
	}
	else
	{
		$status = array('code' => "203", "success" => true, 'message' => "User not found");

	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '33'){
	$idbooking = $appFunctions->validHTML(trim($_POST['idbooking']));
	$iddriver = $appFunctions->validHTML(trim($_POST['iddriver']));
	$bookingstatus = $appFunctions->validHTML(trim($_POST['bookingstatus']));
	
	$target_dir = "../workingareaphoto/";
	$targetBeforeImage = basename($_FILES["beforeimage"]["name"]);
	$targetAfterImage = basename($_FILES["afterimage"]["name"]);
	
	if(!empty($targetBeforeImage)){
		move_uploaded_file($_FILES["beforeimage"]["tmp_name"], $target_dir . $idbooking . '_'. str_replace('.php','',$targetBeforeImage));
	}
	
	if(!empty($targetAfterImage)){
		move_uploaded_file($_FILES["afterimage"]["tmp_name"], $target_dir . $idbooking . '_'. str_replace('.php','',$targetAfterImage));
	}
	
	$mySQL = "";
	$mySQL = "UPDATE `booking` SET `bookingstatus` = '".$appFunctions->validSQL($bookingstatus,"")."'";
	if (!empty($targetBeforeImage)){
		$mySQL .= ", `beforeimage` = '".$appFunctions->validSQL($idbooking . '_'. $targetBeforeImage,"")."'"; 
	} 
	if (!empty($targetAfterImage)){
		$mySQL .= ", `afterimage` = '".$appFunctions->validSQL($idbooking . '_'. $targetAfterImage,"")."'"; 
		$mySQL .= ", `bookingcomplete` = '1'"; 
	} 
	
	$mySQL .= " WHERE idbooking = '".$appFunctions->validSQL($idbooking,"")."'";
	$mySQL .= " AND iddriver = '".$appFunctions->validSQL($iddriver,"")."'";
	//echo $mySQL;
	$dbAccess->queryExec($mySQL);

	// #################################
	$mySQL = "";
	$mySQL = "SELECT iduser
		, bookingno
		, (SELECT fcmtoken FROM user U WHERE U.iduser = B.iduser LIMIT 1) AS fcmtoken
	FROM booking B WHERE `idbooking` = '".$appFunctions->validSQL($idbooking,"")."'";
	// echo $mySQL;
	$rsTemp1 = $dbAccess->selectSingleStmt($mySQL);
	
	$iduser = $rsTemp1['iduser'];
	$bookingno = $rsTemp1['bookingno'];
	$fcmtoken = $rsTemp1['fcmtoken'];
	
	if($bookingstatus == '2'){
		$bookingStatus = ' work started';
		$notificationcode = '2';
	} else if($bookingstatus == '3'){
		$bookingStatus = ' work finished';
		$notificationcode = '3';
	}
	$mySQL = "";
	$mySQL = "SELECT 
		`isnotification`
		, `profileprivacy`
	FROM `user` WHERE `isnotification` = '1' AND iduser = '".$appFunctions->validSQL($iduser,"")."'";
	// echo $mySQL;
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	
	if(!empty($rsTemp)){
		$body = 'Your booking '. $bookingStatus.' by driver! booking no. : ' .$bookingno;
		$message = $body;
		$toTokenId = $fcmtoken;
		
		$mySQL = "";
		$mySQL = "INSERT INTO `notification` (`idsender`, `idreceiver`, `idsource`, `notificationtype`, `notificatiomode`, `sort`, `description`, `isactive`, `isviewed`) 
			VALUES ('".$appFunctions->validSQL($iddriver,"")."', '".$appFunctions->validSQL($iduser,"")."', '".$appFunctions->validSQL($idbooking,"")."', 'New Booking', 'TEXT', '2', '".$body."', '1', '0')";
		$dbAccess->queryExec($mySQL);
		
		$notificationTitle = "Booking ". $bookingStatus;
		$notificationDataTitle = "Request ". $bookingStatus;
		
		$payload = array(
			//'to'=>'/topics/my-app',
			//'to'=>'fqnkZbkucDw:APA91bFQwkDe4SUqqKwfXVG1ANWiLAqUqcEC_GjkjoNmia5JcAoHtqvC6KwIGrI97lM-stF7pq9cSG1xnAfxTr-HxmCivp0OWZ3CtH7lMYIqWuRzDCMVrdS36LlqL-4HPUWO_xwQB3KU',
			"to"=>$toTokenId,
			"priority"=>"high",
			"mutable_content"=>true,
			"notification"=>array("title" => $notificationTitle, "body" => $body, "sound" => "default"),
			"data"=> array("response" => array("usertype" => "CUSTOMER", "iddriver" => $iddriver, "title" => $notificationDataTitle, "message" => $message, "timestamp" => $nowtime, "image" => "", "notificationcode" => $notificationcode, "notificationtype" => $notificationDataTitle, "idbooking" => $idbooking, "bookingno" => $bookingno, "iduser" => $iduser))
		);
		
		// echo json_encode($payload, JSON_PRETTY_PRINT);
		// exit;
		
		$fcmApiKey = "AAAAHjs3cGE:APA91bGx3gtE9yBUSO266Q-ohjHmX0CwvKujZqemLuwX5r-_-1_VO2HMSZsUMoAPh9HW4ga3Y4FtXLAnjj6R4NfhvbMIAx4-2lT2-kICzbKK1G3eX36RK8lW9IrVor-8Bapqu5MA9KBg";
		$headers = array('Authorization: key='.$fcmApiKey, 'Content-Type: application/json');

		$ch = curl_init();
		//curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode($payload) );
		$result = curl_exec($ch );
		curl_close( $ch );
		// echo '<pre>';
		// var_dump($result);
		// exit;
	}
	// #################################
	
	$resultArr = array();
	
	$mySQL = "";
	$mySQL = "SELECT `idbooking`
		, `iduser`
		, `bookingno`
		, `bookingdate`
		, `starttime`
		, (CASE WHEN `endtime` <> '' THEN `endtime` ELSE '' END) AS `endtime`
		, `fullname`
		
		, (CASE WHEN `originaddress` <> '' THEN `originaddress` ELSE '' END) AS `originaddress`
		, (CASE WHEN `origincity` <> '' THEN `origincity` ELSE '' END) AS `origincity`
		, (CASE WHEN `originstate` <> '' THEN `originstate` ELSE '' END) AS `originstate`
		, (CASE WHEN `originzipcode` <> '' THEN `originzipcode` ELSE '' END) AS `originzipcode`
		, `originlatlng`
		
		, (CASE WHEN `destinationaddress` <> '' THEN `destinationaddress` ELSE '' END) AS `destinationaddress`
		, (CASE WHEN `destinationcity` <> '' THEN `destinationcity` ELSE '' END) AS `destinationcity`
		, (CASE WHEN `destinationstate` <> '' THEN `destinationstate` ELSE '' END) AS `destinationstate`
		, (CASE WHEN `destinationzipcode` <> '' THEN `destinationzipcode` ELSE '' END) AS `destinationzipcode`
		, `destinationlatlng`
		
		, 'Service Area' AS `areatitle`
		, `area`
		, CONCAT('$baseURL', '/workingareaphoto/') AS workingareaphoto
		, (CASE WHEN `beforeimage` <> '' THEN `beforeimage` ELSE '' END) AS `beforeimage`
		, (CASE WHEN `afterimage` <> '' THEN `afterimage` ELSE '' END) AS `afterimage`
		, `price`
		, `totalamount`
		, `featuresextracharge`
		, `featuresextrachargeamt`
		, `featuresextrachargetype`
		, `featuresextrachargetype`";	
	$mySQL .= ", bookingstatus AS bookingcode";
	$mySQL .= ", (CASE 	WHEN bookingstatus = '1' THEN 'On The Way'
						WHEN bookingstatus = '2' THEN 'Job Started'
						WHEN bookingstatus = '3' THEN 'Job Complete'
				 ELSE 'Job Not Started, Start Now' END) AS bookingstatus_NOT_AN_USE";
				 
	$mySQL .= ", (CASE 	WHEN bookingstatus = '1' THEN 'Job Not Started, Start Now'
						WHEN bookingstatus = '2' THEN 'End Job Now'
						WHEN bookingstatus = '3' THEN 'Job Complete, Enter OTP'
				 ELSE 'Drive To Job Location' END) AS bookingstatus";
	$mySQL .= " FROM `booking` WHERE `bookedbyuser` = '1' 
	AND `bookingcomplete` = '1' 
	AND `acceptbydriver` = '1' 
	AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."'
	AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	$bookingDetail = $rsTemp;
	//$resultArr['data'] = array("data" =>$rsTemp);
	
	//echo $mySQL;
	// FEATURES
	$mySQL = "";
	$mySQL = "SELECT";
	$mySQL .= " `idfeatures`";
	$mySQL .= ", `featurename`";
	$mySQL .= ", (CASE WHEN `featureprice` > 0 THEN featureprice ELSE '' END) AS featureprice";
	$mySQL .= ", (CASE WHEN `featuredepthprice` > 0 THEN featuredepthprice ELSE '' END) AS featuredepthprice";
	$mySQL .= " FROM `bookingdetail` WHERE `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' 
	AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
	$rsTempFeatures = $dbAccess->selectData($mySQL);
	$arrFeatures = array();
	if(!empty($rsTempFeatures)){
		$arrFeatures = array("features" => $rsTempFeatures);
	} else {
		$arrFeatures = array("features" => []);
	}
	// DRIVEWAY
	$mySQL = "";
	$mySQL = "SELECT 
		`iddrivewaytype`
		, `drivewaytypename`
	FROM `bookingdetail` WHERE iddrivewaytype <> '' AND `idbooking` = '".$appFunctions->validSQL($idbooking,"")."' AND `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'";
	$rsTempDriveway = $dbAccess->selectData($mySQL);
	$arrDriveway = array();
	if(!empty($rsTempDriveway)){
		$arrDriveway = array("driveway" => $rsTempDriveway);
	} else {
		$arrDriveway = array("driveway" => '');
	}
	
	if(!empty($rsTemp)){
		$status = array('code' => "200", "success" => true, 'message' => "Boooking Found");
		$data = array("data" => array_merge($bookingDetail, $arrFeatures, $arrDriveway));
		$tempStr = array_merge($status, $data);
	} else {
		$tempStr = array('code' => "203", "success" => false, 'message' => "Booking Not Found");
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT); 	
} else if ($fid == '34'){
	$iduser = $appFunctions->validHTML(trim($postData->{'iduser'}));
	
	$mySQL = "";
	$mySQL = "UPDATE `user` SET 
		`isactive` = '0'
		, `isdeleted` = '1'
		, `deletedon` = '".$appFunctions->validSQL($entryDate,"")."'";
	$mySQL .= " WHERE iduser = '".$appFunctions->validSQL($iduser,"")."'";
	// echo $mySQL;
	$dbAccess->queryExec($mySQL);
	
	$tempStr = array('code' => "200", "success" => true, 'message' => "Account Deleted");
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '35'){
	$iduser = $appFunctions->validHTML(trim($postData->{'iduser'}));
	$isnotification = $appFunctions->validHTML(trim($postData->{'isnotification'}));
	$profileprivacy = $appFunctions->validHTML(trim($postData->{'profileprivacy'}));
	
	if($isnotification != ''){
		$mySQL = "";
		$mySQL = "UPDATE `user` SET"; 
		$mySQL .= " `isnotification` = '".$isnotification."'";
		if($profileprivacy != '') $mySQL .= ", `profileprivacy` = '".$profileprivacy."'";
		$mySQL .= " WHERE iduser = '".$appFunctions->validSQL($iduser,"")."'";
		// echo $mySQL;
		$dbAccess->queryExec($mySQL);
	}
	
	$mySQL = "";
	$mySQL = "SELECT 
		`isnotification`
		, `profileprivacy`
	FROM `user` WHERE iduser = '".$appFunctions->validSQL($iduser,"")."'";
	// echo $mySQL;
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	
	$status = array('code' => "200", "success" => true, 'message' => "Profile updated");
	$data = array("data" => $rsTemp);
	$tempStr = array_merge($status, $data);
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '36'){
	$idcontent = $appFunctions->validHTML(trim($postData->{'idcontent'}));	
	
	$mySQL = "";
	$mySQL = "SELECT `idcontent`, `title`, `detail` FROM `vcontent` WHERE `isactive` = '1'";
	if(!empty($idcontent)){
		$mySQL .= " AND `idcontent` = '".$appFunctions->validSQL($idcontent,"")."'";
	}
	// echo $mySQL;
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	// echo 'DETAIL: ' . $rsTemp['detail'];
	if(empty($rsTemp)){
		$tempStr = array('code' => "203", "success" => false, 'message' => "Record not found");
	} else {
		$data = array("data" => $rsTemp);
		$status = array('code' => "200", "success" => true, 'message' => "Record found");
		$tempStr = array_merge($status, $data);
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '37'){
	$iduser = $appFunctions->validHTML(trim($postData->{'iduser'}));
	$iddriver = $appFunctions->validHTML(trim($postData->{'iddriver'}));
	$rating = $appFunctions->validHTML(trim($postData->{'rating'}));
	$review = $appFunctions->validHTML(trim($postData->{'review'}));
	$idbooking = $appFunctions->validHTML(trim($postData->{'idbooking'}));

	$mySQL = "";
	$mySQL = "INSERT INTO `driver_rating` SET"; 
	$mySQL .= " `iduser` = '".$appFunctions->validSQL($iduser,"")."'"; 
	$mySQL .= ", `iddriver` = '".$appFunctions->validSQL($iddriver,"")."'"; 
	$mySQL .= ", `rating` = '".$appFunctions->validSQL($rating,"")."'"; 
	$mySQL .= ", `review` = '".$appFunctions->validSQL($review,"")."'"; 
	$mySQL .= ", `idbooking` = '".$appFunctions->validSQL($idbooking,"")."'";
	$dbAccess->queryExec($mySQL);

	$tempStr = array('code' => "200", "success" => true, 'message' => "Your rating submitted");
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '38'){
	$phone = trim($appFunctions->validHTML($postData->{'phone'}));
	$email = trim($appFunctions->validHTML($postData->{'email'}));

	$mySQL = "";
	$mySQL = "SELECT `email`, `mobile` FROM `user` WHERE `usertype` = 'DRIVER' AND (`email` = '".$appFunctions->validSQL($email,"")."' OR `mobile` = '".$appFunctions->validSQL($phone,"")."')";
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	if(!empty($rsTemp)){
		if($rsTemp['email'] == $email AND $rsTemp['mobile'] == $phone){
			$errorMsg = 'Email & Phone already registered';
		} else if($rsTemp['email'] == $email){
			$errorMsg = 'Email already registered';
		} else if($rsTemp['mobile'] == $phone){
			$errorMsg = 'Phone already registered';
		}

		$tempStr = array('code' => "200", "success" => true, 'message' => $errorMsg);
	} else {
		$tempStr = array('code' => "203", "success" => false, 'message' => "Record not found");
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '39'){
	$usertype = 'DRIVER';
	$isactive = '0';
	$password = randomPassword();
	
	$fullname = trim($appFunctions->validHTML($_POST['fullname']));
	$address = trim($appFunctions->validHTML($_POST['address']));
	$city = trim($appFunctions->validHTML($_POST['city']));
	$state = trim($appFunctions->validHTML($_POST['state']));
	$zipcode = trim($appFunctions->validHTML($_POST['zipcode']));
	$phone = trim($appFunctions->validHTML($_POST['phone']));
	$email = trim($appFunctions->validHTML($_POST['email']));
	
	$experience = trim($appFunctions->validHTML($_POST['experience']));
	$areaofwork = trim($appFunctions->validHTML($_POST['areaofwork']));
	$drivinglicense = trim($appFunctions->validHTML($_POST['drivinglicense']));
	$driverinsuranceno = trim($appFunctions->validHTML($_POST['driverinsuranceno']));
	$ssnumber = trim($appFunctions->validHTML($_POST['ssnumber']));
	
	$truckname = trim($appFunctions->validHTML($_POST['truckname']));
	$vehiclenumber = trim($appFunctions->validHTML($_POST['vehiclenumber']));
	$truckinsuranceno = trim($appFunctions->validHTML($_POST['truckinsuranceno']));
	
	$idplowtype = trim($appFunctions->validHTML($_POST['idplowtype']));
	$idplowsize = trim($appFunctions->validHTML($_POST['idplowsize']));
	
	if(!empty($_POST['saltspreader'])){
		$saltspreader = $appFunctions->validHTML($_POST['saltspreader']);
	}
	if(!empty($_POST['rubberblade'])){
		$rubberblade = $appFunctions->validHTML($_POST['rubberblade']);
	}
	if(!empty($_POST['bobcat'])){
		$bobcat = $appFunctions->validHTML($_POST['bobcat']);
	}
	if(!empty($_POST['snowblower'])){
		$snowblower = $appFunctions->validHTML($_POST['snowblower']);
	}
	if(!empty($_POST['shovellers'])){
		$shovellers = $appFunctions->validHTML($_POST['shovellers']);
	}
	
	if(!empty($_POST['driveragreement'])){
		$driveragreement = $_POST['driveragreement'];
	}
	
	// CHECK IMAGE EXT
	$uploadImagePIName = basename($_FILES['profileimage']['name']);
	$uploadImagePISize = basename($_FILES['profileimage']['size']);
	$uploadImagePIExt = pathinfo($uploadImagePIName,PATHINFO_EXTENSION);
	
	$uploadImageDLName = basename($_FILES['drivinglicenseimage']['name']);
	$uploadImageDLSize = basename($_FILES['drivinglicenseimage']['size']);
	$uploadImageDLExt = pathinfo($uploadImageDLName,PATHINFO_EXTENSION);
	
	$uploadImageDIName = basename($_FILES['driverinsurancenoimage']['name']);
	$uploadImageDISize = basename($_FILES['driverinsurancenoimage']['size']);
	$uploadImageDIExt = pathinfo($uploadImageDIName,PATHINFO_EXTENSION);
	
	$uploadImageSSNName = basename($_FILES['ssnumberimage']['name']);
	$uploadImageSSNSize = basename($_FILES['ssnumberimage']['size']);
	$uploadImageSSNExt = pathinfo($uploadImageSSNName,PATHINFO_EXTENSION);
	
	$uploadImageTIName = basename($_FILES['truckimage']['name']);
	$uploadImageTISize = basename($_FILES['truckimage']['size']);
	$uploadImageTIExt = pathinfo($uploadImageTIName,PATHINFO_EXTENSION);
	
	$uploadImageRCName = basename($_FILES['registrationcertificateimage']['name']);
	$uploadImageRCSize = basename($_FILES['registrationcertificateimage']['size']);
	$uploadImageRCExt = pathinfo($uploadImageRCName,PATHINFO_EXTENSION);
	
	$uploadImageTINName = basename($_FILES['truckinsuranceimage']['name']);
	$uploadImageTINSize = basename($_FILES['truckinsuranceimage']['size']);
	$uploadImageTINExt = pathinfo($uploadImageTINName,PATHINFO_EXTENSION);
	
	$friendlyURL = strtolower($fullname);
	$friendlyURL = $appFunctions->friendlyURL($friendlyURL);
	
	$mySQL = "";
	$mySQL = "SELECT `email`, `mobile` FROM `user` WHERE `usertype` = 'DRIVER' AND (`email` = '".$appFunctions->validSQL($email,"")."' OR `mobile` = '".$appFunctions->validSQL($phone,"")."')";
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	if(!empty($rsTemp)){
		if($rsTemp['email'] == $email AND $rsTemp['mobile'] == $phone){
			$errorMsg = 'Email & Phone already registered';
		} else if($rsTemp['email'] == $email){
			$errorMsg = 'Email already registered';
		} else if($rsTemp['mobile'] == $phone){
			$errorMsg = 'Phone already registered';
		}

		$tempStr = array('code' => "200", "success" => true, 'message' => $errorMsg);
	}

	$fileDir = '../driverimage/';
	if(empty($errorMsg)){
		// PROFILE IMAGE
		if(!empty($uploadImagePIName)) {
			$uploadImagePIName = time() . '_' . str_replace('.php', '', $uploadImagePIName);
			move_uploaded_file($_FILES['profileimage']['tmp_name'], $fileDir.$uploadImagePIName);
			//copy($_FILES['profileimage']['tmp_name'], $fileDir.$uploadImagePIName);
		}else{
			$uploadImagePIName = $uploadPIImage_Old;
		}
		
		// DL IMAGE
		if(!empty($uploadImageDLName)) {
			$uploadImageDLName = time() . '_' . str_replace('.php', '', $uploadImageDLName);
			move_uploaded_file($_FILES['drivinglicenseimage']['tmp_name'], $fileDir.$uploadImageDLName);
			//copy($_FILES['drivinglicenseimage']['tmp_name'], $fileDir.$uploadImageDLName);
		}else{
			$drivinglicenseimage = $uploadDLImage_Old;
		}
		
		// DI IMAGE
		if(!empty($uploadImageDIName)) {
			$uploadImageDIName = time() . '_' . str_replace('.php', '', $uploadImageDIName);
			move_uploaded_file($_FILES['driverinsurancenoimage']['tmp_name'], $fileDir.$uploadImageDIName);
			//copy($_FILES['driverinsurancenoimage']['tmp_name'], $fileDir.$uploadImageDIName);
		}else{
			$uploadImageDIName = $uploadDIImage_Old;
		}
		
		// DI IMAGE
		if(!empty($uploadImageSSNName)) {
			$uploadImageSSNName = time() . '_' . str_replace('.php', '', $uploadImageSSNName);
			move_uploaded_file($_FILES['ssnumberimage']['tmp_name'], $fileDir.$uploadImageSSNName);
			//copy($_FILES['ssnumberimage']['tmp_name'], $fileDir.$uploadImageSSNName);
		}else{
			$uploadImageSSNName = $uploadSSNImage_Old;
		}
		
		// RC IMAGE
		if(!empty($uploadImageRCName)) {
			$uploadImageRCName = time() . '_' . str_replace('.php', '', $uploadImageRCName);
			move_uploaded_file($_FILES['registrationcertificateimage']['tmp_name'], $fileDir.$uploadImageRCName);
			//copy($_FILES['registrationcertificateimage']['tmp_name'], $fileDir.$uploadImageRCName);
		}else{
			$uploadImageRCName = $uploadRCImage_Old;
		}
		
		// TI IMAGE
		if(!empty($uploadImageTINName)) {
			$uploadImageTINName = time() . '_' . str_replace('.php', '', $uploadImageTINName);
			move_uploaded_file($_FILES['truckinsuranceimage']['tmp_name'], $fileDir.$uploadImageTINName);
			//copy($_FILES['truckinsuranceimage']['tmp_name'], $fileDir.$uploadImageTINName);
		}else{
			$uploadImageTINName = $uploadTIImage_Old;
		}
		
		// TRUCK IMAGE
		if(!empty($uploadImageTIName)) {
			$uploadImageTIName = time() . '_' . str_replace('.php', '', $uploadImageTIName);
			move_uploaded_file($_FILES['truckimage']['tmp_name'], $fileDir.$uploadImageTIName);
			//copy($_FILES['truckimage']['tmp_name'], $fileDir.$uploadImageTIName);
		}else{
			$uploadImageTIName = $uploadTIImage_Old;
		}
		
		if(empty($iduser)){
			$mySQL = "";
			$mySQL .= "INSERT INTO `user` SET";
			$mySQL .= "  fullname = '". $appFunctions->validSQL($fullname,"")."'";
			$mySQL .= ", usertype = '". $appFunctions->validSQL($usertype,"")."'";
			$mySQL .= ", password = '". $appFunctions->validSQL($password,"")."'";
			$mySQL .= ", address = '". $appFunctions->validSQL($address,"")."'";
			$mySQL .= ", city = '". $appFunctions->validSQL($city,"")."'";
			$mySQL .= ", state = '". $appFunctions->validSQL($state,"")."'";
			$mySQL .= ", zipcode = '". $appFunctions->validSQL($zipcode,"")."'";
			$mySQL .= ", mobile = '". $appFunctions->validSQL($phone,"")."'";
			$mySQL .= ", email = '". $appFunctions->validSQL($email,"")."'";
			$mySQL .= ", experience = '". $appFunctions->validSQL($experience,"")."'";
			$mySQL .= ", areaofwork = '". $appFunctions->validSQL($areaofwork,"")."'";
			$mySQL .= ", drivinglicense = '". $appFunctions->validSQL($drivinglicense,"")."'";
			$mySQL .= ", drivinglicenseimage = '". $appFunctions->validSQL($uploadImageDLName,"")."'";
			$mySQL .= ", driverinsuranceno = '". $appFunctions->validSQL($driverinsuranceno,"")."'";
			$mySQL .= ", driverinsurancenoimage = '". $appFunctions->validSQL($uploadImageDIName,"")."'";
			$mySQL .= ", ssnumber = '". $appFunctions->validSQL($ssnumber,"")."'";
			$mySQL .= ", ssnumberimage = '". $appFunctions->validSQL($uploadImageSSNName,"")."'";
			$mySQL .= ", profileimage = '". $appFunctions->validSQL($uploadImagePIName,"")."'";
			//$mySQL .= ", isactive = '". $appFunctions->validSQL($isactive,"")."'";
			//echo $mySQL .'<br>';			
			//exit;
			$dbAccess->queryExec($mySQL);
			$iduser = $dbAccess->LastId();
			
			$mySQL = "";
			$mySQL .= "UPDATE `user` SET";
			$mySQL .= " `friendlyURL` = '". $appFunctions->validSQL($iduser . '-' . trim($friendlyURL),"") ."'";
			$mySQL .= " WHERE `iduser` = '".$appFunctions->validSQL($iduser,"")."';";
			//echo $mySQL .'<br>';			
			//exit;
			$dbAccess->queryExec($mySQL);
			
			$plowtype = $dbAccess->getDetail('plowtype','plowtype','idplowtype',$idplowtype);
			$plowsize = $dbAccess->getDetail('plowsize','plowsize','idplowsize',$idplowsize);
			
			$mySQL = "";
			$mySQL = "INSERT INTO `usertruck` SET";
			$mySQL .= " `iduser` = '".$appFunctions->validSQL($iduser,"")."'";
			$mySQL .= ", `idplowtype` = '".$appFunctions->validSQL($idplowtype,"")."'";
			$mySQL .= ", `plowtype` = '".$appFunctions->validSQL($plowtype,"")."'";
			$mySQL .= ", `truckname` = '".$appFunctions->validSQL($truckname,"")."'";
			$mySQL .= ", `vehiclenumber` = '".$appFunctions->validSQL($vehiclenumber,"")."'";
			$mySQL .= ", `registrationcertificateimage` = '".$appFunctions->validSQL($uploadImageRCName,"")."'";
			$mySQL .= ", `truckinsuranceno` = '".$appFunctions->validSQL($truckinsuranceno,"")."'";
			$mySQL .= ", `truckinsuranceimage` = '".$appFunctions->validSQL($uploadImageTINName,"")."'";
			$mySQL .= ", `truckimage` = '".$appFunctions->validSQL($uploadImageTIName,"")."'";
			$mySQL .= ", `plowsize` = '".$appFunctions->validSQL($plowsize,"")."'";
			$mySQL .= ", `rubberblade` = '".$appFunctions->validSQL($rubberblade,"")."'";
			$mySQL .= ", `snowblower` = '".$appFunctions->validSQL($snowblower,"")."'";
			$mySQL .= ", `saltspreader` = '".$appFunctions->validSQL($saltspreader,"")."'";
			$mySQL .= ", `bobcat` = '".$appFunctions->validSQL($bobcat,"")."'";
			$mySQL .= ", `shovellers` = '".$appFunctions->validSQL($shovellers,"")."'";
			//$mySQL .= ", `misc` = '".$appFunctions->validSQL($misc,"")."'";
			//$mySQL .= ", `isactive` = '".$appFunctions->validSQL($iduser,"")."'";
			//$mySQL .= ", `createdon` = '".$appFunctions->validSQL($iduser,"")."'";
			//echo $mySQL .'<br>';			
			//exit;
			$dbAccess->queryExec($mySQL);
		}
		$tempStr = array('code' => "200", "success" => true, 'message' => "Thank You! Your application is successfully submitted. Please wait for DRYFT admin to approve your application.");
	}

	echo json_encode($tempStr, JSON_PRETTY_PRINT);
} else if ($fid == '40'){
	$idbooking = $appFunctions->validHTML(trim($postData->{'idbooking'}));
	$iduser = $appFunctions->validHTML(trim($postData->{'iduser'}));
	
	$mySQL = "";
	$mySQL = "CALL sp_OrderStatusUpdate (
		  '".$appFunctions->validSQL($idbooking,"")."'
		, '".$appFunctions->validSQL($iduser,"")."'
		, @result)"; 
	// echo $mySQL;
	$rsTemp = $dbAccess->queryExec($mySQL);
	
	$mySQL = "";
	$mySQL = "SELECT @result AS result"; 
	//echo $mySQL;
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	//echo $rsTemp['result'] . '<hr>';
	$result = $rsTemp['result'];

	if($result == '1'){
		$mySQL = "";
		$mySQL = "SELECT `idbooking`
			, `iduser`
			, (CASE WHEN `iddriver` <> '' THEN iddriver ELSE '' END) AS iddriver
			, `bookingno`
			, `bookingdate`
			, `starttime`
			, `fullname`
			, (SELECT (CASE WHEN fullname <> '' THEN fullname ELSE '' END) AS `fullname` FROM user U WHERE U.iduser = B.iddriver LIMIT 1) AS drivername
			, (SELECT (CASE WHEN profileimage <> '' THEN CONCAT('$baseURL', '/userphoto/', profileimage) ELSE '' END) AS `profileimage` FROM user U WHERE U.iduser = B.iddriver LIMIT 1) AS driverimage
			
			, (CASE WHEN `originlatlng` <> '' THEN originlatlng ELSE '' END) AS originlatlng
			, `destinationlatlng`
			, `totalamount`
			, (CASE WHEN `promotype` = 'PERCENT' AND `promovalue` <> '' THEN (`totalamount` - (`totalamount`*`promovalue`)/100)
					WHEN `promotype` = 'USD' AND `promovalue` <> '' THEN (`totalamount` - `promovalue`)
					ELSE totalamount END) AS `totalprice`

			, `bookedbyuser`
			, `createdon` AS `bookedbyuseron`
			, `acceptbydriver`
			, (CASE WHEN `acceptbydriveron` THEN `acceptbydriveron` ELSE '' END) AS `acceptbydriveron`
			, `rejectbydriver`
			, (CASE WHEN `rejectbydriveron` THEN `rejectbydriveron` ELSE '' END) AS `rejectbydriveron`
			, `cancelbyuser`
			, (CASE WHEN `cancelbyuseron` THEN `cancelbyuseron` ELSE '' END) AS `cancelbyuseron`
			, `cancelbydriver`
			, (CASE WHEN `cancelbydriveron` THEN `cancelbydriveron` ELSE '' END) AS `cancelbydriveron`";
		$mySQL .= ", bookingstatus AS bookingcode";
		$mySQL .= ", (CASE 	WHEN bookingstatus = '1' THEN 'On The Way'
						WHEN bookingstatus = '2' THEN 'Job Started'
						WHEN bookingstatus = '3' THEN 'Job Complete'
				 ELSE 'Job Not Started' END) AS bookingstatus";
		$mySQL .= " FROM `booking` B WHERE isactive = '1' AND `bookedbyuser` = '1' AND `iduser` = '".$appFunctions->validSQL($iduser,"")."'";
		$mySQL .= " ORDER BY `bookingdate` DESC";
		// echo $mySQL;
		
		$rsTemp = $dbAccess->selectData($mySQL);
		
		if(!empty($rsTemp)){
			$status = array('code' => "200", "success" => true, 'message' => "Boooking Cancelled");
			$data = array("data"=>$rsTemp);
			$tempStr = array_merge($status, $data);
		} else {
			$tempStr = array('code' => "203", "success" => false, 'message' => "Booking Not Found");
		}
	} else {
		$tempStr = array('code' => "203", "success" => false, 'message' => "Booking Not Cancelled");
	}
	echo json_encode($tempStr, JSON_PRETTY_PRINT);
}
?>