<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>D R Y F T</title>
	<!-- Meta Section -->
	<meta name="viewport"
		  content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="description" content="">
	<meta name="Keywords" content="">
	<!-- CSS Library -->
	<link href="<?php echo base_url(); ?>assets/css/all.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/animate.css" type="text/css" rel="stylesheet" media="all">
	<link href="<?php echo base_url(); ?>assets/css/style.css" type="text/css" rel="stylesheet" media="all">

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<![endif]-->
	<!-- -->
