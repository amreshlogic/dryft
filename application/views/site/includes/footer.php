<footer class="wow fadeInDown">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="ft-right"> <img src="<?php echo base_url(); ?>assets/images/icons/dryft_logo_white.png" class="img-fluid" alt="Logo">
					<p>1-800-489-8128 – 24/7 Support</p>
					<p>Support: hello@dryft.com</p>
					<p><a href="#">FAQ</a> | <a href="#">Privacy Policy</a></p>
				</div>
			</div>
			<div class="col-md-8">
				<h3>Our Current Service Area</h3>
				<div class="row">
					<div class="col-md-3 col-6">
						<ul>
							<li>Atlanta, GA</li>
							<li>Akron, OH</li>
							<li>Austin, TX</li>
							<li>Boston, MA</li>
							<li>Charlotte, NC</li>
							<li>Chicago, IL</li>
						</ul>
					</div>
					<div class="col-md-3 col-6">
						<ul>
							<li>Cleveland, OH</li>
							<li>Columbus, OH</li>
							<li>Dallas, TX</li>
							<li>Durham, NC</li>
							<li>Grand Rapids, MI</li>
							<li>Indianapolis, IN</li>
						</ul>
					</div>
					<div class="col-md-3 col-6">
						<ul>
							<li>Jacksonville, FL</li>
							<li>Milwaukee, WI</li>
							<li>Minneapolis, MN</li>
							<li>Nashville, TN</li>
							<li>Orlando, FL</li>
							<li>Pittsburgh, PA</li>
						</ul>
					</div>
					<div class="col-md-3 col-6">
						<ul>
							<li>Raleigh, NC</li>
							<li>Rochester, NY</li>
							<li>St. Paul, MN</li>
							<li>Sterling Heights, MI</li>
							<li>Syracuse, NY</li>
							<li>Tampa, FL</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-9"> <i class="fa fa-copyright"></i> 2018 Dryft. All rights reserved | Design &amp; Developed by Ogrelogic </div>
				<div class="col-md-3">
					<div class="ft-social">
						<ul>
							<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- JS custom -->

</body>
</html>
