<!-- Slider -->
<section class="main-slider mb-hidden">
	<div class="caption-box">
		<input type="search" placeholder="Enter your location" class="form-control">
		<button class="btn btn-theme btn-large">Book Now</button>
	</div>
	<div class="carousel slide" data-ride="carousel">
		<!-- The slideshow -->
		<div class="carousel-inner">
			<div class="carousel-item active"><img src="<?php echo base_url(); ?>assets/images/slider/banner_1.jpg"
												   alt="Los Angeles" class="img-fluid"></div>
			<div class="carousel-item"><img src="<?php echo base_url(); ?>assets/images/slider/banner_1_1.jpg"
											alt="Los Angeles" class="img-fluid"></div>
			<div class="carousel-item"><img src="<?php echo base_url(); ?>assets/images/slider/banner_1_2.jpg"
											alt="Los Angeles" class="img-fluid"></div>
			<div class="carousel-item"><img src="<?php echo base_url(); ?>assets/images/slider/banner_1_3.jpg"
											alt="Los Angeles" class="img-fluid"></div>
		</div>
	</div>
</section>

<!-- Mobile Slider -->
<section class="main-slider desktop-hide">
	<div class="caption-box">
		<input type="search" placeholder="Enter your location" class="form-control">
		<button class="btn btn-theme btn-large">Book Now</button>
	</div>
	<div class="carousel slide" data-ride="carousel">
		<!-- The slideshow -->
		<div class="carousel-inner">
			<div class="carousel-item active"><img
					src="<?php echo base_url(); ?>assets/images/slider/mb/dryft_banner.png" style="height: 65vh;"
					alt="Los Angeles" class="img-fluid"></div>
			<div class="carousel-item"><img src="<?php echo base_url(); ?>assets/images/slider/mb/dryft_banner_1_1.png"
											style="height: 65vh;" alt="Los Angeles" class="img-fluid"></div>
			<div class="carousel-item"><img src="<?php echo base_url(); ?>assets/images/slider/mb/dryft_banner_1_2.png"
											style="height: 65vh;" alt="Los Angeles" class="img-fluid"></div>
			<div class="carousel-item"><img src="<?php echo base_url(); ?>assets/images/slider/mb/dryft_banner_1_3.png"
											style="height: 65vh;" alt="Los Angeles" class="img-fluid"></div>
		</div>
	</div>
</section>
