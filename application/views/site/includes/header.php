</head>
<body>
<!--mobile-menu-->
<ul class="sidenav" id="mobile-menu">
	<li class="active"><a href="index.html">Home</a></li>
	<li class="menu-item-has-children"><a href="javascript:;">About us</a>
		<ul class="sub-menu">
			<li><a href="user/schedule-services.html">One Time Service</a></li>
			<li><a href="javascript:;">Blog</a></li>
			<li><a href="javascript:;">Screen Drivers</a></li>
			<li><a href="user/schedule-services.html">One Time Service</a></li>
			<li><a href="javascript:;">Insurance info</a></li>
		</ul>
	</li>
	<li><a href="javascript:;">Plans</a></li>
	<li><a href="user/schedule-services.html">One Time Service</a></li>
	<li class="elite-yellow"><a href="user/dryft-payment-details.html"><img
					src="assets/images/icons/dryft_elite_yellow.png" class="img-fluid" alt=""></a></li>
	<li><a href="javascript:;">Commercial Services</a></li>
	<li><a href="javascript:;">Faq</a></li>
	<li><a href="javascript:;">Blog</a></li>
	<li><a href="javascript:;">Contact us</a></li>
</ul>
<!--mobile-menu end-->

<!-- [///////========== Mobile Header Start Here  \\\\\\\\==========]-->
<header class="desktop-hide">
	<div class="top-head">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="login-area">
						<ul>
							<li id="mb-login-btn">Sign In</li>
							<li id="mb-signup-btn">Sign Up</li>
						</ul>
						<div class="login-box">
							<h4>Sign In</h4>
							<form>
								<p>
									<input type="tel" class="form-control" placeholder="Mobile Number">
								</p>
								<p>
									<input type="text" class="form-control" placeholder="Name">
								</p>
								<p>
									<input type="email" class="form-control" placeholder="Email">
								</p>
								<button class="btn btn-theme btn-block">Login</button>
							</form>
							<p class="for-signup">Don’t have an account? Sign Up</p>
						</div>

						<!-- Signup box-->
						<div class="signup-box">
							<h4>Sign Up</h4>
							<form>
								<p class="facebook"><a href="#"><i class="fab fa-facebook-f"></i> CONTINUE WITH FACEBOOK</a>
								</p>
								<p class="facebook"><a href="#"><i class="fab fa-google-plus-g"></i> CONTINUE WITH
										GOOGLE</a></p>
								<p class="facebook"><a href="#"><i class="fa fa-mobile-alt"></i> CONTINUE WITH MOBILE
										NUMBER</a></p>
							</form>
							<p><a href="<?php echo base_url(); ?>driver">DRIVER’S REGISTER HERE</a></p>
							<p class="for-signup">Already have an account? Sign In</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="menu-area"><img src="<?php echo base_url(); ?>assets/images/dryft_logo_icon.png" class="img-fluid logo" alt="Logo"> <a href="#"
																											  class="sidenav-trigger"
																											  id="mb-toggle">
			<span></span> <span></span> <span></span> </a></div>
</header>

<!-- Header -->
<header class="mb-hidden">
	<div class="top-head">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-9 col-md-8 col-sm-8 col-12">
					<ul class="weathday-list">
						<li><strong>Expected Snowfall</strong></li>
						<li>Wed 1"</li>
						<li>Thu 1"</li>
						<li>Fri 2"</li>
						<li>Sat 4"</li>
						<li>Sun 4"</li>
						<li>Mon 5"</li>
						<li>Tue 6"</li>
					</ul>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-4  col-12">
					<div class="login-area">
						<ul>
							<li id="login-btn">Sign In</li>
							<li id="signup-btn">Sign Up</li>
						</ul>
						<div class="login-box">
							<h4>Sign In</h4>
							<form>
								<p>
									<input type="tel" class="form-control" placeholder="Mobile Number">
								</p>
								<p>
									<input type="text" class="form-control" placeholder="Name">
								</p>
								<p>
									<input type="email" class="form-control" placeholder="Email">
								</p>
								<button class="btn btn-theme btn-block">Login</button>
							</form>
							<p class="for-signup">Don’t have an account? Sign Up</p>
						</div>

						<!-- Signup box-->
						<div class="signup-box">
							<h4>Sign Up</h4>
							<form>
								<p class="facebook"><a href="#"><i class="fab fa-facebook-f"></i> CONTINUE WITH FACEBOOK</a>
								</p>
								<p class="facebook"><a href="#"><i class="fab fa-google-plus-g"></i> CONTINUE WITH
										GOOGLE</a></p>
								<p class="facebook"><a href="#"><i class="fa fa-mobile-alt"></i> CONTINUE WITH MOBILE
										NUMBER</a></p>
							</form>
							<p><a href="<?php echo base_url('driver-registration'); ?>">DRIVER’S REGISTER HERE</a></p>
							<p class="for-signup">Already have an account? Sign In</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Logo Section -->
	<div class="mid-head">
		<div class="container">
			<div class="row">
				<div class="col-md-2 col-sm-3"><img src="<?php echo base_url(); ?>assets/images/dryft_logo.png" class="img-fluid logo"
													alt="Logo"></div>
				<div class="col-md-10 col-sm-9 mb-hidden"><a class="weatherwidget-io"
															 href="https://forecast7.com/en/41d88n87d63/chicago/?unit=us"
															 data-label_1="CHICAGO" data-label_2="WEATHER"
															 data-icons="Climacons Animated" data-theme="marine">CHICAGO
						WEATHER</a>
					<script>
						!function (d, s, id) {
							var js, fjs = d.getElementsByTagName(s)[0];
							if (!d.getElementById(id)) {
								js = d.createElement(s);
								js.id = id;
								js.src = 'https://weatherwidget.io/js/widget.min.js';
								fjs.parentNode.insertBefore(js, fjs);
							}
						}(document, 'script', 'weatherwidget-io-js');
					</script>
				</div>
			</div>
		</div>
	</div>
	<div class="menu-area">
		<div class="container">
			<div class="row">
				<ul class="desktop-menu">
					<li class="active"><a href="index.html">Home</a></li>
					<li class="menu-item-has-children"><a href="javascript:;">About us</a>
						<ul class="sub-menu">
							<li><a href="user/schedule-services.html">One Time Service</a></li>
							<li><a href="javascript:;">Blog</a></li>
							<li><a href="javascript:;">Screen Drivers</a></li>
							<li><a href="user/schedule-services.html">One Time Service</a></li>
							<li><a href="javascript:;">Insurance info</a></li>
						</ul>
					</li>
					<li><a href="javascript:;">Plans</a></li>
					<li><a href="user/schedule-services.html">One Time Service</a></li>
					<li class="elite-yellow"><a href="user/dryft-payment-details.html"><img
									src="<?php echo base_url(); ?>assets/images/icons/dryft_elite_yellow.png" class="img-fluid" alt=""></a></li>
					<li><a href="javascript:;">Commercial Services</a></li>
					<li><a href="javascript:;">Faq</a></li>
					<li><a href="javascript:;">Blog</a></li>
					<li><a href="javascript:;">Contact us</a></li>
				</ul>
				<a href="#" class="sidenav-trigger" id="toggle"> <span></span> <span></span> <span></span> </a></div>
		</div>
	</div>
</header>
