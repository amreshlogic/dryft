

<!-- CSS Library -->
<link href="<?php echo base_url('/assets/css/all.css'); ?>" type="text/css" rel="stylesheet">

<link href="<?php echo base_url('/assets/css/bootstrap.min.css'); ?>" type="text/css" rel="stylesheet">
<link href="<?php echo base_url('/assets/css/animate.css'); ?>" type="text/css" rel="stylesheet" media="all" >
<link href="<?php echo base_url('/assets/css/style.css'); ?>" type="text/css" rel="stylesheet" media="all" >

<script src="<?php echo base_url('/js/_Application_.js')?>"></script>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<!-- <h2 align="center">Driver’s Registration</h2> -->
			<h3 align="center">Work With DRYFT</h3>

		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<?php echo $this->session->flashdata('message'); ?>

	<form role="form" action="<?php echo base_url('registration'); ?>" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3>Personal Information</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<label>Full Name</label>
								<input type="text" name="fullname" id="fullname" value="" maxlength="250" autocomplete="off" class="form-control" placeholder="Full Name" autofocus onkeypress="return isAlpha(event)" required />
							</div>
							<div class="col-md-6">
								<label>Driver Photo</label>
								<input type="file" name="profileimage" id="profileimage" class="form-control" placeholder="Driving Photo" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Address</label>
								<input type="text" name="address" id="address" value="" maxlength="250" autocomplete="off" class="form-control" placeholder="Address" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>City</label>
								<input type="text" name="city" id="city" value="" maxlength="100" autocomplete="off" class="form-control" placeholder="City" onkeypress="return isAlpha(event)" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>State</label>
								<input type="text" name="state" id="state" value="" maxlength="100" autocomplete="off" class="form-control" placeholder="State" onkeypress="return isAlpha(event)" required />
							</div>
							<div class="col-md-6">
								<label>Zipcode</label>
								<input type="text" name="zipcode" id="zipcode" value="" maxlength="10" autocomplete="off" class="form-control" placeholder="Zipcode" onkeypress="return isNumber(event)" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Phone</label>
								<input type="text" name="phone" id="phone" value="" maxlength="10" autocomplete="off" class="form-control" placeholder="Phone" required onkeyup="removeErrClass('mobile');removeHide('mobile', 'errMobile');" pattern="\d{10}" onkeypress="return isNumber(event)" />
							</div>
							<div class="col-md-6">
								<label>Email</label>
								<input type="email" name="email" id="email" value="" maxlength="150" autocomplete="off" class="form-control" placeholder="Email" required onkeyup="removeErrClass('email');removeHide('email', 'errEmail');" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" />
							</div>
						</div>
					</div>
					<!-- /.panel-body -->

					<div class="panel-heading">
						<h3>Other Information</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<label>Experience</label>
								<select name="experience" id="experience" class="form-control" autocomplete="off" placeholder="Driving Experience" required />
								<option value="">Select Experience</option>
								<?php
									$experience = '';
									$selected = '';
									for($i=1;$i<70;$i++){
										if(!empty($experience)){
											if($i == $experience){
												$selected = 'SELECTED';
											} 
										}
									?>
										<option value="<?=$i;?>" <?=$selected;?>><?=$i;?></option>
									<?php
									}
									?>
								
								</select>
							</div>
							<div class="col-md-6">
								<label>Area of Work</label>
								<input type="text" name="areaofwork" id="areaofwork" value="" maxlength="255" class="form-control" placeholder="Area of Wwork" autocomplete="off" onkeypress="return isAlpha(event)" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Driving License No.</label>
								<input type="text" name="drivinglicense" id="drivinglicense" value="" maxlength="50" class="form-control" placeholder="Driving License No." autocomplete="off" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>Driving License Image</label>
								<input type="file" name="drivinglicenseimage" id="drivinglicenseimage" class="form-control" placeholder="Driving License Image" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Insurance No.</label>
								<input type="text" name="driverinsuranceno" id="driverinsuranceno" value="" maxlength="50" class="form-control" placeholder="Insurance No." autocomplete="off" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>Insurance Image</label>
								<input type="file" name="driverinsurancenoimage" id="driverinsurancenoimage" class="form-control" placeholder="Driver Personal Insurance" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Social Security No.</label>
								<input type="text" name="ssnumber" id="ssnumber" value="" maxlength="50" class="form-control" placeholder="Social Security No." autocomplete="off" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>SSN Image</label>
								<input type="file" name="ssnumberimage" id="ssnumberimage" class="form-control" placeholder="SSN Image" autocomplete="off" required />
							</div>
						</div>
					</div>
					<!-- /.panel-body -->

					<div class="panel-heading">
						<h3>Truck / Equipment</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<label>Truck Type</label>
								<input type="text" name="truckname" id="truckname" value="" maxlength="255" class="form-control" placeholder="Truck Type" autocomplete="off" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>Truck Image</label>
								<input type="file" name="truckimage" id="truckimage" class="form-control" placeholder="Truck Image" autocomplete="off" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Vehicle Number</label>
								<input type="text" name="vehiclenumber" id="vehiclenumber" value="" maxlength="25" class="form-control" placeholder="Vehicle Number" autocomplete="off" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>Registration Certificate</label>
								<input type="file" name="registrationcertificateimage" id="registrationcertificateimage" class="form-control" placeholder="Registration Certificate" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Insurance Number</label>
								<input type="text" name="truckinsuranceno" id="truckinsuranceno" value="" maxlength="25" class="form-control" placeholder="Insurance Number" autocomplete="off" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>Truck Insurance</label>
								<input type="file" name="truckinsuranceimage" id="truckinsuranceimage" class="form-control" placeholder="Truck Insurance" autocomplete="off" required />
							</div>
						</div>

					</div>
					<div class="panel-heading">
						<h3>Truck Other Detail</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								
								<label>Plow Type</label>
								
								<select name="idplowtype" id="idplowtype" class="form-control" required>
									<option value="">Select</option>
									
									<?php    foreach ($plowtype as  $type) { ?>
										<option value="<?= $type->idplowtype?>"> <?= $type->plowtype?> </option>
										
									<?php } ?>
									
								
								</select>
							</div>
							<div class="col-md-6">
								<label>Plow Size</label>
								
								<select name="idplowsize" id="idplowsize" class="form-control" required>
									<option value="">Select</option>
									
									<?php    foreach ($plowsize as  $size) { ?>
										<option value="<?= $size->idplowsize?>"> <?= $size->plowsize?> </option>
										
									<?php } ?>
									
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="checkbox">
									<label><input type="checkbox" name="saltspreader" id="saltspreader" value="CHECKED"> Salt Spreader</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" name="rubberblade" id="rubberblade" value="CHECKED"> Rubber Blade</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="checkbox">
									<label><input type="checkbox" name="bobcat" id="bobcat" value="CHECKED"> Bobcat</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" name="snowblower" id="snowblower" value="CHECKED"> Snow Blower</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="checkbox">
									<label><input type="checkbox" name="shovellers" id="shovellers" value="CHECKED"> Shovellers</label>
								</div>
							</div>
						</div>
					</div>
					<!-- /.panel-body -->
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group text-center">
									<div>
										<input type="checkbox" name="driveragreement" id="driveragreement" checked value="CHECKED" required>
										Accept Agreement. <a href="<?php echo base_url('/driver-agreement.pdf');?> target="_blank">Download Here</a>
									</div>
									<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
									<div class="text-center text-danger">
										<?php
										//if (!empty($_GET['result']) AND $_GET['result'] == '1') {
										//	echo 'Thank You! Your application is successfully submitted. Please wait for DRYFT admin to approve your application.';
										//}

										//if (in_array('DRIVER EMAIL & PHONE EXIST', $errorMsg)){
										//	echo 'Driver email & phone no. already registered with us.';
										//} else if(in_array('DRIVER EMAIL EXIST', $errorMsg)){
										//	echo 'Driver email already registered with us.';
										//} else if(in_array('DRIVER PHONE EXIST', $errorMsg)){
										//	echo 'Driver phone no. already registered with us.';
										//} else if(in_array('ANY ONE SELECT', $errorMsg)){
										//	echo 'Please select any one of the option from the following (Salt Spreader, Bobcat, Shovellers, Rubber Blade, Snow Blower)';
										//}
										?>
									</div>
								</div>
								<!-- /.col-lg-6 (nested) -->
							</div>
						</div>
					</div>
					<!-- /.row (nested) -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</form>
</div>

<?php
// include '../include/_footer_.php';
?>

<!-- JS Libraries -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url('/assets/js/all.js');?>"></script>
<script src="<?php echo base_url('/assets/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('/assets/js/popper.min.js');?>"></script>
<script src="<?php echo base_url('/assets/js/wow.min.js');?>"></script>
<script src="<?php echo base_url('/assets/js/custom.js');?>"></script>
