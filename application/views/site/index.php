<?php include_once __DIR__ . "/includes/head.php"; ?>
<?php include_once __DIR__ . "/includes/header.php"; ?>
<!--SLIDER-->
<?php include_once __DIR__ . "/includes/slider.php"; ?>
<!-- First Section -->
<section class="top-section wow fadeInDown">
	<h1><img src="<?php echo base_url(); ?>assets/images/icons/dryft_elite_blue.png"
			 class="img-flui center-block elite_secnd" alt=""></h1>
	<h3 align="center">Go VIP. Sit Back &amp; Stay Warm</h3>
</section>

<!-- Second Section -->
<section class="second-section wow fadeInDown">
	<div class="container">
		<div class="row">
			<ul class="box">
				<li class="wow fadeInLeft" data-wow-delay=".2s">
					<figure><img src="<?php echo base_url(); ?>assets/images/icons/deploys-01.png"
								 class="img-fluid center-block" alt=""></figure>
					Dryft Elite Deploys Every 2"
				</li>
				<li class="wow fadeInLeft" data-wow-delay=".8s">
					<figure><img src="<?php echo base_url(); ?>assets/images/icons/priorityPlacement-01.png"
								 class="img-fluid center-block" alt=""></figure>
					Priority Placement
				</li>
				<li class="wow fadeInLeft" data-wow-delay="1.2s">
					<figure><img src="<?php echo base_url(); ?>assets/images/icons/support-01.png"
								 class="img-fluid center-block" alt=""></figure>
					24/7 Support Number
				</li>
				<li class="wow fadeInLeft" data-wow-delay="1.6s">
					<figure><img src="<?php echo base_url(); ?>assets/images/icons/5starDriver-01.png"
								 class="img-fluid center-block" alt=""></figure>
					Only 5 Star Driver
				</li>
				<li class="wow fadeInLeft" data-wow-delay="2s">
					<figure><img src="<?php echo base_url(); ?>assets/images/icons/realtimeWeatherAlert-01.png"
								 class="img-fluid center-block" alt=""></figure>
					Real Time Weather Alerts
				</li>
			</ul>
		</div>
	</div>
</section>

<!-- Plans -->
<section class="default-section plans wow fadeInDown">
	<div class="container">
		<div class="row">
			<div class="col-md-12"> <span class="heading-title">
        <h2>PLANS</h2>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </span></div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="plan-box wow fadeInLeft" data-wow-delay=".4s">
					<h3>ONE TIME SERVICE</h3>
					<div class="btm-content">
						<p class="price">$ 88.00</p>
						<p>PER SERVICE</p>
						<p>Lorem Impsum doler sit</p>
						<a href="javascript:;" class="btn btn-theme btn-large">Book Now</a></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="plan-box wow fadeInUp" data-wow-delay="1s">
					<h3><img src="<?php echo base_url(); ?>assets/images/icons/dryft_elite_blue.png"
							 class="img-fluid center-block" alt=""></h3>
					<div class="btm-content">
						<p class="price">$ 88.00</p>
						<p>PER MONTH</p>
						<p>Lorem Impsum doler sit</p>
						<a href="javascript:;" class="btn btn-theme btn-large">Subscribe Now</a></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="plan-box wow fadeInRight" data-wow-delay="1.6s">
					<h3>COMMERCIAL SERVICES</h3>
					<div class="btm-content">
						<p class="price">$ 88.00</p>
						<p>PER MONTH</p>
						<p>Lorem Impsum doler sit</p>
						<a href="javascript:;" class="btn btn-theme btn-large">Subscribe Now</a></div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Download -->
<section class="download-sect wow fadeInDown">
	<div class="container">
		<div class="row">
			<div class="col-md-4 order-2 order-md-1">
				<figure><img src="<?php echo base_url(); ?>assets/images/mobile_phones.png" alt=""
							 class="img-fluid wow slideInLeft"></figure>
			</div>
			<div class="col-md-8 order-1 order-md-2">
				<div class="download-box wow fadeInRight" data-wow-delay="1s">
					<figure><img src="<?php echo base_url(); ?>assets/images/icons/dryft_logo_white.png"
								 class="img-fluid" alt="Logo"></figure>
					<h2>DOWNLOAD NOW!</h2>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
						It has survived not only five centuries</p>
					<a href="javascript:;"><img src="<?php echo base_url(); ?>assets/images/icons/play_store.png"
												alt="Playstore"></a> <a href="javascript:;"><img
								src="<?php echo base_url(); ?>assets/images/icons/apple_store.png" alt="Playstore"></a>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Testimonials -->
<section class="testimonial-section testimonials wow fadeInDown">
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2 col-10 offset-1 mt-5">
				<h2 align="center">Testimonials</h2>
				<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner mt-4">
						<div class="carousel-item text-center active">
							<p class="m-0 pt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem
								tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper
								malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non
								aliquet.</p>
							<h3>Martha M. Home Owner, Chicago</h3>
						</div>
						<div class="carousel-item text-center">
							<p class="m-0 pt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem
								tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper
								malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non
								aliquet.</p>
							<h3>Martha M. Home Owner, Chicago</h3>
						</div>
						<div class="carousel-item text-center">
							<p class="m-0 pt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem
								tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper
								malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non
								aliquet.</p>
							<h3>Martha M. Home Owner, Chicago</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Footer -->
<?php include_once __DIR__ . "/includes/foot.php"; ?>
<?php include_once __DIR__ . "/includes/footer.php"; ?>
