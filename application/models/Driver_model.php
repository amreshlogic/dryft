<?php

class Driver_model extends CI_Model{

	public function getRow($tbl,$clms='*',$con){

		$this->db->select($clms);
		$this->db->from($tbl);
		if($con)
			$this->db->where($con);
		$q=	$this->db->get();
		//echo $this->db->last_query();

		return $q->num_rows()?$q->row():'';
	}
	public function getMultipleRows($tbl,$clms='*',$con, $order_by){
		$this->db->select($clms);
		$this->db->from($tbl);
		if($con)
			$this->db->where($con);

		// $qry = "SELECT `UP`.`iduser` FROM `vgetuserplan` `UP` WHERE `UP`.`success` ='1'";
		// $this->db->where_in ('U.iduser', $qry, false);

		if($order_by)
			$this->db->order_by($order_by['col_name'], $order_by['order']);
		$q=	$this->db->get();

		return $q->num_rows()?$q->result():'';
	}
	public function getMultipleRowsEmail($tbl,$clms='*',$con, $order_by){
		$this->db->select($clms);
		$this->db->from($tbl);
		if($con)
			$this->db->where($con);

		$qry = "SELECT `UP`.`iduser` FROM `vgetuserplan` `UP` WHERE `UP`.`success` ='1'";
		$this->db->where_in ('U.iduser', $qry, false);

		if($order_by)
			$this->db->order_by($order_by['col_name'], $order_by['order']);
		$q=	$this->db->get();

		return $q->num_rows()?$q->result():'';
	}
	public function insertData($procedure){
		$this->db->query($procedure);
		$query = 'SELECT @result as result';
		$response = $this->db->query($query);
		return $response->result();
	}
	public function updateData($procedure){
		$this->db->query($procedure);
		$query = 'SELECT @result as result';
		$response = $this->db->query($query);
		return $response->result();
	}
	public function getSingleRow($tbl,$clms='*',$con,$order_by,$limit){

		$this->db->select($clms);
		$this->db->from($tbl);
		if($con)
			$this->db->where($con);
		if($order_by)
			$this->db->order_by($order_by['col_name'], $order_by['order']);
		if($limit)
			$this->db->limit($limit);
		$q=	$this->db->get();
		//echo $this->db->last_query();

		return $q->num_rows()?$q->row():'';
	}
	public function getMultipleRowsJoin($tbl1,$tbl2,$on_con,$clms='*',$con,$group_by, $order_by){
		$this->db->select($clms);
		$this->db->from($tbl1);
		$this->db->join($tbl2, $on_con);
		if($con)
			$this->db->where($con);
		if($group_by)
			$this->db->group_by($group_by);
		if($order_by)
			$this->db->order_by($order_by['col_name'], $order_by['order']);
		$q=	$this->db->get();
		echo $this->db->last_query();
		return $q->num_rows()?$q->result():'';
	}
	public function deleteData($tbl,$con){
		$this->db->where($con);
		$del = $this->db->delete($tbl);
		return $del;
	}
	public function updateRec($tbl,$data,$con){
		$this->db->WHERE($con);
		$result = $this->db->update($tbl,$data);
		if($result == true){
			return 1;
		}
	}
	public function getCount($tbl, $clms = '*', $con) {
		$this->db->select($clms);
		$this->db->from($tbl);
		if($con)
			$this->db->where($con);
		// echo $this->db->last_query();
		// return $this->db->count_all($table);
		$q = $this->db->get();
		//return $q->num_rows();
		return $q->num_rows();
	}
	public function num_rows($tbl,$clms='*',$con){
		$this->db->select($clms);
		$this->db->from($tbl);
		if($con)
			$this->db->where($con);
		$q=	$this->db->get();
		return $q->num_rows();
	}
	public function getRowPagination($tbl,$clms='*',$con, $order_by,$limit,$offset){

		$this->db->select($clms);
		$this->db->from($tbl);
		if($con)
			$this->db->where($con);
		$this->db->limit($limit,$offset);
		if($order_by)
			$this->db->order_by($order_by['col_name'], $order_by['order']);
		$q=	$this->db->get();

		return $q->num_rows()?$q->result():'';
	}
	public function getMultipleRowsGroupBy($tbl,$clms='*',$con, $order_by,$groupby){
		$this->db->select($clms);
		$this->db->from($tbl);
		if($con)
			$this->db->where($con);
		if($order_by)
			$this->db->order_by($order_by['col_name'], $order_by['order']);
		if($groupby)
			$this->db->group_by($groupby);
		$q=	$this->db->get();

		return $q->num_rows()?$q->result():'';
	}
	public function num_rows_in($tbl,$clms='*',$con){
		$this->db->select($clms);
		$this->db->from($tbl);
		if($con)
			$this->db->where_in('idcategory', $con);
		$q=	$this->db->get();
		return $q->num_rows();
	}
	public function getRowPaginationIn($tbl,$clms='*',$con, $order_by,$limit,$offset){

		$this->db->select($clms);
		$this->db->from($tbl);
		if($con)
			$this->db->where_in('idcategory', $con);
		$this->db->limit($limit,$offset);
		if($order_by)
			$this->db->order_by($order_by['col_name'], $order_by['order']);
		$q=	$this->db->get();

		return $q->num_rows()?$q->result():'';
	}
	public function getMultipleRowsGroupByIn($tbl,$clms='*',$con,$incon, $order_by,$groupby){
		$this->db->select($clms);
		$this->db->from($tbl);
		if($con)
			$this->db->where($con);
		if($incon)
			$this->db->where_in('idcategory', $incon,false);
		if($order_by)
			$this->db->order_by($order_by['col_name'], $order_by['order']);
		if($groupby)
			$this->db->group_by($groupby);
		$q=	$this->db->get();

		return $q->num_rows()?$q->result():'';
	}

	public function plowtype() {
		$this->db->select('idplowtype, plowtype');
		$this->db->from('plowtype');
		$this->db->where('isactive', 1);
		$this->db->order_by('plowtype');
		$query = $this->db->get();
		$plowtypeArr = $query->result();

		return $plowtypeArr;

	}

	public function plowsize() {
		$this->db->select('idplowsize, plowsize');
		$this->db->from('plowsize');
		$this->db->where('isactive', 1);
		$this->db->order_by('plowsize');
		$query = $this->db->get();
		$plowsize = $query->result();

		return $plowsize;

	}




	public function driverReg($userData, $truckData, $friendlyURL) {
		$this->db->insert('user', $userData);
		$uid = $this->db->insert_id();
		if($uid > 0) {
			$data['friendlyURL'] = $uid . '-' . trim($friendlyURL);
			$this->db->where('iduser', $uid);
			if($this->db->update('user', $data)) {
				$truckData['iduser'] = $uid;
				if($this->db->insert('usertruck', $truckData)) {
					return true;
				}
				else {
					return false;
				}
			}
		}
		else {
			return false;
		}

	}




}
?>
