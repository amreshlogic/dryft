<?php
class Driver extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Driver_model");
	}

	public function index()
	{
		$this->load->view("site/driver/index");
	}

	public function registration() {

		$data['plowtype'] = $this->Driver_model->plowtype();
		$data['plowsize'] = $this->Driver_model->plowsize();

		$this->load->view("site/driver/register-driver", $data);
		
	}

	public function register() {

		$usertype = 'DRIVER';
		$isactive = '0';
		$password = randomPassword();
	
		$fullname = trim(validHTML($_POST['fullname']));
		$address = trim(validHTML($_POST['address']));
		$city = trim(validHTML($_POST['city']));
		$state = trim(validHTML($_POST['state']));
		$zipcode = trim(validHTML($_POST['zipcode']));
		$phone = trim(validHTML($_POST['phone']));
		$email = trim(validHTML($_POST['email']));
	
		$experience = trim(validHTML($_POST['experience']));
		$areaofwork = trim(validHTML($_POST['areaofwork']));
		$drivinglicense = trim(validHTML($_POST['drivinglicense']));
		$driverinsuranceno = trim(validHTML($_POST['driverinsuranceno']));
		$ssnumber = trim(validHTML($_POST['ssnumber']));
		
		$truckname = trim(validHTML($_POST['truckname']));
		$vehiclenumber = trim(validHTML($_POST['vehiclenumber']));
		$truckinsuranceno = trim(validHTML($_POST['truckinsuranceno']));
		
		$idplowtype = trim(validHTML($_POST['idplowtype']));
		$idplowsize = trim(validHTML($_POST['idplowsize']));
	
		if(!empty($_POST['saltspreader'])){
			$saltspreader = validHTML($_POST['saltspreader']);
		}
		if(!empty($_POST['rubberblade'])){
			$rubberblade = validHTML($_POST['rubberblade']);
		}
		if(!empty($_POST['bobcat'])){
			$bobcat = validHTML($_POST['bobcat']);
		}
		if(!empty($_POST['snowblower'])){
			$snowblower = validHTML($_POST['snowblower']);
		}
		if(!empty($_POST['shovellers'])){
			$shovellers = validHTML($_POST['shovellers']);
		}
		
		if(!empty($_POST['driveragreement'])){
			$driveragreement = $_POST['driveragreement'];
		}
	
		// CHECK IMAGE EXT
		$uploadImagePIName = basename($_FILES['profileimage']['name']);
		$uploadImagePISize = basename($_FILES['profileimage']['size']);
		$uploadImagePIExt = pathinfo($uploadImagePIName,PATHINFO_EXTENSION);
		
		$uploadImageDLName = basename($_FILES['drivinglicenseimage']['name']);
		$uploadImageDLSize = basename($_FILES['drivinglicenseimage']['size']);
		$uploadImageDLExt = pathinfo($uploadImageDLName,PATHINFO_EXTENSION);
		
		$uploadImageDIName = basename($_FILES['driverinsurancenoimage']['name']);
		$uploadImageDISize = basename($_FILES['driverinsurancenoimage']['size']);
		$uploadImageDIExt = pathinfo($uploadImageDIName,PATHINFO_EXTENSION);
		
		$uploadImageSSNName = basename($_FILES['ssnumberimage']['name']);
		$uploadImageSSNSize = basename($_FILES['ssnumberimage']['size']);
		$uploadImageSSNExt = pathinfo($uploadImageSSNName,PATHINFO_EXTENSION);
		
		$uploadImageTIName = basename($_FILES['truckimage']['name']);
		$uploadImageTISize = basename($_FILES['truckimage']['size']);
		$uploadImageTIExt = pathinfo($uploadImageTIName,PATHINFO_EXTENSION);
		
		$uploadImageRCName = basename($_FILES['registrationcertificateimage']['name']);
		$uploadImageRCSize = basename($_FILES['registrationcertificateimage']['size']);
		$uploadImageRCExt = pathinfo($uploadImageRCName,PATHINFO_EXTENSION);
		
		$uploadImageTINName = basename($_FILES['truckinsuranceimage']['name']);
		$uploadImageTINSize = basename($_FILES['truckinsuranceimage']['size']);
		$uploadImageTINExt = pathinfo($uploadImageTINName,PATHINFO_EXTENSION);
	
		$friendlyURL = strtolower($fullname);
		$friendlyURL = friendlyURL($friendlyURL);
	
	
	/*if(!empty($uploadImagePIName) AND strtolower($uploadImagePIExt) != "jpg" AND strtolower($uploadImagePIExt) != "jpeg" AND strtolower($uploadImagePIExt) != "png"){
		//$errorMsg = 'Please upload driving lincense imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'PROFILE IMAGE FORMAT';
	} else if(!empty($uploadImagePIName) AND ($uploadImagePISize == 0 OR $uploadImagePISize > 10000000)){
		//$errorMsg = 'Please upload driving lincense image size less than 500KB';
		$errorMsg[] = 'PROFILE IMAGE SIZE';
	}
	if(empty($uploadImageDLName)){
		$errorMsg[] = 'DL IMAGE EMPTY';
	}
	if(!empty($uploadImageDLName) AND strtolower($uploadImageDLExt) != "jpg" AND strtolower($uploadImageDLExt) != "jpeg" AND strtolower($uploadImageDLExt) != "png"){
		//$errorMsg = 'Please upload driving lincense imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'DL IMAGE FORMAT';
	} else if(!empty($uploadImageDLName) AND ($uploadImageDLSize == 0 OR $uploadImageDLSize > 10000000)){
		//$errorMsg = 'Please upload driving lincense image size less than 500KB';
		$errorMsg[] = 'DL IMAGE SIZE';
	} 
	if(empty($uploadImageDIName)){
		$errorMsg[] = 'DI IMAGE EMPTY';
	}
	if(!empty($uploadImageDIName) AND strtolower($uploadImageDIExt) != "jpg" AND strtolower($uploadImageDIExt) != "jpeg" AND strtolower($uploadImageDIExt) != "png"){
		//$errorMsg = 'Please upload driver insurance imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'DI IMAGE FORMAT';
	} else if(!empty($uploadImageDIName) AND ($uploadImageDISize == 0 OR $uploadImageDISize > 10000000)){
		//$errorMsg = 'Please upload driver insurance image size less than 500KB';
		$errorMsg[] = 'DI IMAGE SIZE';
	} 
	if(empty($uploadImageSSNName)){
		$errorMsg[] = 'SSN IMAGE EMPTY';
	}
	if(!empty($uploadImageSSNName) AND strtolower($uploadImageSSNExt) != "jpg" AND strtolower($uploadImageSSNExt) != "jpeg" AND strtolower($uploadImageSSNExt) != "png"){
		//$errorMsg = 'Please upload imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'SSN IMAGE FORMAT';
	} else if(!empty($uploadImageSSNName) AND ($uploadImageSSNSize == 0 OR $uploadImageSSNSize > 10000000)){
		//$errorMsg = 'Please upload image size less than 500KB';
		$errorMsg[] = 'SSN IMAGE SIZE';
	}
	if(empty($uploadImageTIName)){
		$errorMsg[] = 'TRUCK IMAGE EMPTY';
	}
	if(!empty($uploadImageTIName) AND strtolower($uploadImageTIExt) != "jpg" AND strtolower($uploadImageTIExt) != "jpeg" AND strtolower($uploadImageTIExt) != "png"){
		//$errorMsg = 'Please upload imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'TRUCK IMAGE FORMAT';
	} else if(!empty($uploadImageTIName) AND ($uploadImageTISize == 0 OR $uploadImageTISize > 10000000)){
		//$errorMsg = 'Please upload image size less than 500KB';
		$errorMsg[] = 'TRUCK IMAGE SIZE';
	}
	if(empty($uploadImageRCName)){
		$errorMsg[] = 'REGISTRATION IMAGE EMPTY';
	}
	if(!empty($uploadImageRCName) AND strtolower($uploadImageRCExt) != "jpg" AND strtolower($uploadImageRCExt) != "jpeg" AND strtolower($uploadImageRCExt) != "png"){
		//$errorMsg = 'Please upload imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'REGISTRATION IMAGE FORMAT';
	} else if(!empty($uploadImageRCName) AND ($uploadImageRCSize == 0 OR $uploadImageRCSize > 10000000)){
		//$errorMsg = 'Please upload image size less than 500KB';
		$errorMsg[] = 'REGISTRATION IMAGE SIZE';
	}
	if(empty($uploadImageTINName)){
		$errorMsg[] = 'TRUCK INSURANCE IMAGE EMPTY';
	}
	if(!empty($uploadImageTINName) AND strtolower($uploadImageTINExt) != "jpg" AND strtolower($uploadImageTINExt) != "jpeg" AND strtolower($uploadImageTINExt) != "png"){
		//$errorMsg = 'Please upload imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'TRUCK INSURANCE IMAGE FORMAT';
	} else if(!empty($uploadImageTINName) AND ($uploadImageTINSize == 0 OR $uploadImageTINSize > 10000000)){
		//$errorMsg = 'Please upload image size less than 500KB';
		$errorMsg[] = 'TRUCK INSURANCE IMAGE SIZE';
	}*/
	//var_dump($errorMsg);
	
	/*$mySQL = "";
	$mySQL = "SELECT `email`, `mobile` FROM `user` WHERE `usertype` = 'DRIVER' AND (`email` = '".validSQL($email,"")."' OR `mobile` = '".validSQL($phone,"")."')";
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	if(!empty($rsTemp)){
		if($rsTemp['email'] == $email AND $rsTemp['mobile'] == $phone){
			$errorMsg[] = 'DRIVER EMAIL & PHONE EXIST';
		} else if($rsTemp['email'] == $email){
			$errorMsg[] = 'DRIVER EMAIL EXIST';
		} else if($rsTemp['mobile'] == $phone){
			$errorMsg[] = 'DRIVER PHONE EXIST';
		}
	}*/

	

	//$fileDir = '../driverimage/';
	$fileDir = 'driverimage/';
	
	//define('UPLOAD_DIR', 'driverimage/');
	
	if(empty($errorMsg)){
		// PROFILE IMAGE
		if(!empty($uploadImagePIName)) {
			$uploadImagePIName = time() . '_' . str_replace('.php', '', $uploadImagePIName);
			move_uploaded_file($_FILES['profileimage']['tmp_name'], $fileDir.$uploadImagePIName);
			//copy($_FILES['profileimage']['tmp_name'], $fileDir.$uploadImagePIName);
		}else{
			$uploadImagePIName = $uploadPIImage_Old;
		}
		
		// DL IMAGE
		if(!empty($uploadImageDLName)) {
			$uploadImageDLName = time() . '_' . str_replace('.php', '', $uploadImageDLName);
			move_uploaded_file($_FILES['drivinglicenseimage']['tmp_name'], $fileDir.$uploadImageDLName);
			//copy($_FILES['drivinglicenseimage']['tmp_name'], $fileDir.$uploadImageDLName);
		}else{
			$drivinglicenseimage = $uploadDLImage_Old;
		}
		
		// DI IMAGE
		if(!empty($uploadImageDIName)) {
			$uploadImageDIName = time() . '_' . str_replace('.php', '', $uploadImageDIName);
			move_uploaded_file($_FILES['driverinsurancenoimage']['tmp_name'], $fileDir.$uploadImageDIName);
			//copy($_FILES['driverinsurancenoimage']['tmp_name'], $fileDir.$uploadImageDIName);
		}else{
			$uploadImageDIName = $uploadDIImage_Old;
		}
		
		// DI IMAGE
		if(!empty($uploadImageSSNName)) {
			$uploadImageSSNName = time() . '_' . str_replace('.php', '', $uploadImageSSNName);
			move_uploaded_file($_FILES['ssnumberimage']['tmp_name'], $fileDir.$uploadImageSSNName);
			//copy($_FILES['ssnumberimage']['tmp_name'], $fileDir.$uploadImageSSNName);
		}else{
			$uploadImageSSNName = $uploadSSNImage_Old;
		}
		
		// RC IMAGE
		if(!empty($uploadImageRCName)) {
			$uploadImageRCName = time() . '_' . str_replace('.php', '', $uploadImageRCName);
			move_uploaded_file($_FILES['registrationcertificateimage']['tmp_name'], $fileDir.$uploadImageRCName);
			//copy($_FILES['registrationcertificateimage']['tmp_name'], $fileDir.$uploadImageRCName);
		}else{
			$uploadImageRCName = $uploadRCImage_Old;
		}
		
		// TI IMAGE
		if(!empty($uploadImageTINName)) {
			$uploadImageTINName = time() . '_' . str_replace('.php', '', $uploadImageTINName);
			move_uploaded_file($_FILES['truckinsuranceimage']['tmp_name'], $fileDir.$uploadImageTINName);
			//copy($_FILES['truckinsuranceimage']['tmp_name'], $fileDir.$uploadImageTINName);
		}else{
			$uploadImageTINName = $uploadTIImage_Old;
		}
		
		// TRUCK IMAGE
		if(!empty($uploadImageTIName)) {
			$uploadImageTIName = time() . '_' . str_replace('.php', '', $uploadImageTIName);
			move_uploaded_file($_FILES['truckimage']['tmp_name'], $fileDir.$uploadImageTIName);
			//copy($_FILES['truckimage']['tmp_name'], $fileDir.$uploadImageTIName);
		} else {
			$uploadImageTIName = $uploadTIImage_Old;
		}


		$userData['fullname'] = @validSQL($fullname,"");
		$userData['usertype'] = @validSQL($usertype,"");
		$userData['password'] = @validSQL($password,"");
		$userData['address'] = @validSQL($address,"");
		$userData['city'] = @validSQL($city,"");
		$userData['state'] = @validSQL($state,"");
		$userData['zipcode'] = @validSQL($zipcode,"");
		$userData['mobile'] = @validSQL($phone,"");
		$userData['email'] = @validSQL($email,"");
		$userData['experience'] = @validSQL($experience,"");
		$userData['areaofwork'] = @validSQL($areaofwork,"");
		$userData['drivinglicense'] = @validSQL($drivinglicense,"");
		$userData['drivinglicenseimage'] = @validSQL($uploadImageDLName,"");
		$userData['driverinsuranceno'] = @validSQL($driverinsuranceno,"");
		$userData['driverinsurancenoimage'] = @validSQL($uploadImageDIName,"");
		$userData['ssnumber'] = @validSQL($ssnumber,"");
		$userData['ssnumberimage'] = @validSQL($uploadImageSSNName,"");
		$userData['profileimage'] = @validSQL($uploadImagePIName,"");


		$usertruck['idplowtype'] =  @validSQL($idplowtype,"");
		$usertruck['plowtype'] =  @validSQL($plowtype,"");
		$usertruck['truckname'] =  @validSQL($truckname,"");
		$usertruck['vehiclenumber'] =  @validSQL($vehiclenumber,"");
		$usertruck['registrationcertificateimage'] =  @validSQL($uploadImageRCName,"");
		$usertruck['truckinsuranceno'] =  @validSQL($truckinsuranceno,"");
		$usertruck['truckinsuranceimage'] =  @validSQL($uploadImageTINName,"");
		$usertruck['truckimage'] =  @validSQL($uploadImageTIName,"");
		$usertruck['plowsize'] =  @validSQL($plowsize,"");
		$usertruck['rubberblade'] =  @validSQL($rubberblade,"");
		$usertruck['snowblower'] =  @validSQL($snowblower,"");
		$usertruck['saltspreader'] =  @validSQL($saltspreader,"");
		$usertruck['bobcat'] =  @validSQL($bobcat,"");
		$usertruck['shovellers'] =  @validSQL($shovellers,"");

		$this->regStatus = $this->Driver_model->driverReg($userData, $usertruck, $friendlyURL);

		if($this->regStatus === true) {

			$this->session->set_flashdata('message', 'Drive has been added successfully.');
			redirect('driver-registration');
		}
		else {
			$this->session->set_flashdata('message', 'Drive has fail to  added successfully.');
			redirect('driver-registration');
		}
		
		/*header("location:".$baseURL."/register/driver?result=1");
		exit;*/
	}
	
	}

}
