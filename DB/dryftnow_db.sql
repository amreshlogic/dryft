-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 13, 2021 at 03:25 PM
-- Server version: 5.6.42
-- PHP Version: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dryftnow_db`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`dryft2018`@`localhost` PROCEDURE `sp_ContentAdd` (IN `ctitle` VARCHAR(250), IN `friendlyURL` VARCHAR(500), IN `detail` TEXT, IN `contentfor` VARCHAR(50), IN `sort` INT(11), IN `isactive` INT(1), OUT `result` VARCHAR(50))  BEGIN
	DECLARE resultStatus VARCHAR(50);
	SET resultStatus = (SELECT COUNT(0) AS recExist FROM `content` WHERE `title` = ctitle);
	IF(resultStatus = 0) THEN
		INSERT INTO `content` (
			`title`,
            `friendlyURL`,
            `detail`,
            `contentfor`,
            `sort`,
            `isactive`
		) VALUES (
			ctitle,
			friendlyURL,
            detail,
            contentfor,
            sort,
            isactive
        );
		SET result = 0;
    ELSEIF(resultStatus > 0) THEN
		SET result = 1;
	ELSE
		SET result = -1;
    END IF;
END$$

CREATE DEFINER=`dryft2018`@`localhost` PROCEDURE `sp_ContentEdit` (IN `id` INT(11), IN `ctitle` VARCHAR(250), IN `friendlyURL` VARCHAR(500), IN `detail` TEXT, IN `contentfor` VARCHAR(50), IN `sort` INT(11), IN `isactive` INT(1), OUT `result` VARCHAR(50))  BEGIN
	DECLARE resultStatus VARCHAR(50);
	SET resultStatus = (SELECT COUNT(0) AS recExist FROM `content` WHERE `idcontent` <> id AND `title` = ctitle);
	IF(resultStatus = 0) THEN
		UPDATE `content` SET
			`title` = ctitle,
            `friendlyURL` = friendlyURL,
            `detail` = detail,
            `contentfor` = contentfor,
            `sort` = sort,
            `isactive` = isactive
		WHERE `idcontent` = id;
		SET result = 0;
    ELSEIF(resultStatus > 0) THEN
		SET result = 1;
	ELSE
		SET result = -1;
    END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_UserEdit` (IN `id_User` INT(11), IN `firstname` VARCHAR(250), IN `lastname` VARCHAR(250), IN `userEmail` VARCHAR(250), IN `countrycode` VARCHAR(50), IN `phone` VARCHAR(15), IN `address` VARCHAR(500), IN `address1` VARCHAR(500), IN `city` VARCHAR(100), IN `state` VARCHAR(100), IN `zipcode` VARCHAR(10), IN `country` VARCHAR(50), IN `breakfasttime` VARCHAR(250), IN `lunchtime` VARCHAR(250), IN `dinnertime` VARCHAR(250), IN `emailnotification` INT(1), IN `pushnotification` INT(1), IN `isactive` INT(1), IN `isdeleted` INT(1), IN `weightlose` VARCHAR(250), IN `age` VARCHAR(250), IN `height` VARCHAR(250), IN `weight` VARCHAR(250), IN `areyouactive` VARCHAR(250), IN `mealeatinday` VARCHAR(250), OUT `result` VARCHAR(50))  BEGIN
	DECLARE resultStatus VARCHAR(50);
	DECLARE iduserservey INT(11);
    -- DECLARE iduser INT(11);
	SET resultStatus = (SELECT COUNT(0) AS recExist FROM `vgetuser` WHERE `iduser` <> id_User AND `email` = userEmail);
    IF(resultStatus = 0) THEN 
		UPDATE `user` SET 
			`firstname` = firstname,
			`lastname` = lastname,
			`email` = userEmail,
			`countrycode` = countrycode,
			`phone` = phone,
			`address` = address,
			`address1` = address,
			`city` = city,
			`state` = state,
			`zipcode` = zipcode,
			`country` = country,
			`fcmtoken` = fcmtoken,
			`breakfasttime` = breakfasttime,
			`lunchtime` = lunchtime,
			`dinnertime` = dinnertime,
			`emailnotification` = emailnotification,
			`pushnotification` = pushnotification,
			`isactive` = isactive
		WHERE `iduser` = id_User;
		-- SET iduser = id_User;
        IF (isactive = 1 AND isdeleted = 0) THEN
			UPDATE `user` SET 
				`isactive` = 1
				, `isdeleted` = 0
			WHERE `iduser` = id_User;
        ELSEIF ((isactive = 1 OR isactive = 0) AND isdeleted = 1) THEN
			UPDATE `user` SET 
				`isactive` = 0
				, `isdeleted` = 1
			WHERE `iduser` = id_User;
		END IF;
        DELETE FROM `userservey` WHERE `iduser` = id_User;
        INSERT INTO `userservey` SET 
			`idplan` = idplan,
            `iduser` = id_User,
			`firstname` = firstname,
			`lastname` = lastname,
			`email` = userEmail,
			-- `planname` = planname,
			-- `planprice` = planprice,
			-- `planduration` = planduration,
			-- `planpromocode` = planpromocode,
			-- `areyoulookingto` = areyoulookingto,
			`age` = age,
			`height` = height,
			`weight` = weight,
			`weightlose` = weightlose,
			`areyouactive` = areyouactive,
			`mealeatinday` = mealeatinday;
		SET iduserservey = (SELECT LAST_INSERT_ID());
		SET result = id_User;
	ELSEIF(resultStatus > 0) THEN
		SET result = -1;
	END IF;
END$$

CREATE DEFINER=`primewomen_user`@`localhost` PROCEDURE `sp_UserFCMAddEdit` (IN `id_user` INT(11), IN `fcmtoken` VARCHAR(250), OUT `result` VARCHAR(50))  BEGIN
	UPDATE `user` SET 
		`fcmtoken` = fcmtoken
	WHERE `iduser` = id_user;
	
    DELETE FROM `userfcmtoken` WHERE `iduser` = id_user; 
	INSERT INTO `userfcmtoken` SET 
		`iduser` = id_user,
		`fcmtoken` = fcmtoken;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `adminuser`
--

CREATE TABLE `adminuser` (
  `iduser` int(11) NOT NULL,
  `usertype` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `plevel` int(1) NOT NULL DEFAULT '0',
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Password in SHA1 Format',
  `securitycode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `userphoto` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isactive` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `adminuser`
--

INSERT INTO `adminuser` (`iduser`, `usertype`, `plevel`, `name`, `email`, `password`, `securitycode`, `mobile`, `userphoto`, `createdon`, `isactive`) VALUES
(2, 'Admin', 2, 'Admin', 'info@domain.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin', 'admin', '', '2018-03-31 08:24:30', 'Active'),
(3, 'Editor', 0, 'Arun Singh', 'arun@gmail.com', 'd5f12e53a182c062b6bf30c1445153faff12269a', '4321', '9999999999', '', '2018-04-01 00:59:56', 'Inactive');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `idbooking` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `iddriver` int(11) DEFAULT '1',
  `bookingno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bookingdate` date NOT NULL,
  `subscriptionenddate` date DEFAULT NULL,
  `starttime` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `endtime` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `originaddress` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `origincity` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `originstate` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `originzipcode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `originlatlng` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `destinationaddress` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `destinationcity` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `destinationstate` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `destinationzipcode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `destinationlatlng` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `area` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `beforeimage` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `afterimage` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` float NOT NULL,
  `totalamount` float NOT NULL,
  `featuresextracharge` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `featuresextrachargeamt` float DEFAULT '0',
  `featuresextrachargetype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taxamount` float DEFAULT NULL,
  `taxtype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `extracharges` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `extrachargesamount` float DEFAULT NULL,
  `extrachargestype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paymentmode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paymentstatus` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transactionid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `promocode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `promovalue` float DEFAULT NULL,
  `promotype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `walletamount` float DEFAULT NULL,
  `bookedbyuser` int(1) NOT NULL DEFAULT '0',
  `acceptbydriver` int(1) NOT NULL DEFAULT '0',
  `acceptbydriveron` datetime DEFAULT NULL,
  `rejectbydriver` int(1) NOT NULL DEFAULT '0',
  `rejectbydriveron` datetime DEFAULT NULL,
  `cancelbyuser` int(1) NOT NULL DEFAULT '0',
  `cancelbyuseron` datetime DEFAULT NULL,
  `cancelbydriver` int(1) NOT NULL DEFAULT '0',
  `cancelbydriveron` datetime DEFAULT NULL,
  `bookingstatus` enum('0','1','2','3') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '0 = Job Not Started, 1 = On The Way, 2 = Job Started, 3 = Job Complete',
  `bookingcomplete` int(1) NOT NULL DEFAULT '0',
  `bookingcompleteon` datetime DEFAULT NULL,
  `bookingclosedopt` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isactive` int(1) NOT NULL DEFAULT '0',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`idbooking`, `iduser`, `iddriver`, `bookingno`, `bookingdate`, `subscriptionenddate`, `starttime`, `endtime`, `fullname`, `mobile`, `email`, `originaddress`, `origincity`, `originstate`, `originzipcode`, `originlatlng`, `destinationaddress`, `destinationcity`, `destinationstate`, `destinationzipcode`, `destinationlatlng`, `area`, `beforeimage`, `afterimage`, `price`, `totalamount`, `featuresextracharge`, `featuresextrachargeamt`, `featuresextrachargetype`, `tax`, `taxamount`, `taxtype`, `extracharges`, `extrachargesamount`, `extrachargestype`, `paymentmode`, `paymentstatus`, `transactionid`, `discount`, `promocode`, `promovalue`, `promotype`, `walletamount`, `bookedbyuser`, `acceptbydriver`, `acceptbydriveron`, `rejectbydriver`, `rejectbydriveron`, `cancelbyuser`, `cancelbyuseron`, `cancelbydriver`, `cancelbydriveron`, `bookingstatus`, `bookingcomplete`, `bookingcompleteon`, `bookingclosedopt`, `isactive`, `createdon`) VALUES
(1, 158, 1, '20210102081656740', '2021-01-02', NULL, '08:16', NULL, 'Tricia Hadley', '9134497180', 'thadley@kc.surewest.net', NULL, NULL, NULL, NULL, NULL, 'N 75th Dr Victory Hills Kansas City Wyandotte, United States, 1319 N 75th Dr', NULL, NULL, NULL, '39.1193023,-94.7525964', '1-1500', NULL, NULL, 50, 68, '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, NULL, 0, NULL, 0, NULL, '0', 0, NULL, NULL, 0, '2021-01-02 14:16:56'),
(2, 158, 1, '20210102081713410', '2021-01-02', NULL, '10:17', NULL, 'Tricia Hadley', '9134497180', 'thadley@kc.surewest.net', NULL, NULL, NULL, NULL, NULL, 'N 75th Dr Victory Hills Kansas City Wyandotte, United States, 1319 N 75th Dr', NULL, NULL, NULL, '39.1193023,-94.7525964', '1-1500', NULL, NULL, 50, 50, '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, NULL, 0, NULL, 0, NULL, '0', 0, NULL, NULL, 0, '2021-01-02 14:17:13'),
(3, 158, 1, '20210102081802080', '2021-01-02', NULL, '10:17', NULL, 'Tricia Hadley', '9134497180', 'thadley@kc.surewest.net', NULL, NULL, NULL, NULL, NULL, 'N 75th Dr Victory Hills Kansas City Wyandotte, United States, 1319 N 75th Dr', NULL, NULL, NULL, '39.1193023,-94.7525964', '1-1500', NULL, NULL, 50, 50, '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, NULL, 0, NULL, 0, NULL, '0', 0, NULL, NULL, 0, '2021-01-02 14:18:02');

-- --------------------------------------------------------

--
-- Table structure for table `bookingdetail`
--

CREATE TABLE `bookingdetail` (
  `idbookingdetail` int(11) NOT NULL,
  `idbooking` int(11) NOT NULL,
  `iddriver` int(11) DEFAULT NULL,
  `idservice` int(11) DEFAULT NULL,
  `idcategory` int(11) DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idfeatures` int(11) DEFAULT NULL,
  `featurename` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `featureareafrom` float DEFAULT NULL,
  `featureareato` float DEFAULT NULL,
  `featureprice` float DEFAULT NULL,
  `featuredepthfrom` float DEFAULT NULL,
  `featuredepthto` float DEFAULT NULL,
  `featuredepthprice` float DEFAULT NULL,
  `iddrivewaytype` int(11) DEFAULT NULL,
  `drivewaytypename` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bookingdetail`
--

INSERT INTO `bookingdetail` (`idbookingdetail`, `idbooking`, `iddriver`, `idservice`, `idcategory`, `name`, `idfeatures`, `featurename`, `featureareafrom`, `featureareato`, `featureprice`, `featuredepthfrom`, `featuredepthto`, `featuredepthprice`, `iddrivewaytype`, `drivewaytypename`) VALUES
(1, 1, NULL, NULL, NULL, NULL, 1, 'Steps in Back', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 1, NULL, NULL, NULL, NULL, 2, 'Deck', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 1, NULL, NULL, NULL, NULL, 9, 'Salt walk ways', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 1, NULL, NULL, NULL, NULL, 5, 'Clear side walk way', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 1, NULL, NULL, NULL, NULL, 8, 'Salt driveway', 1, 1500, 18, NULL, NULL, NULL, NULL, NULL),
(6, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Asphalt driveway'),
(7, 2, NULL, NULL, NULL, NULL, 1, 'Steps in Back', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 2, NULL, NULL, NULL, NULL, 2, 'Deck', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 2, NULL, NULL, NULL, NULL, 9, 'Salt walk ways', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 2, NULL, NULL, NULL, NULL, 5, 'Clear side walk way', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Asphalt driveway'),
(12, 3, NULL, NULL, NULL, NULL, 1, 'Steps in Back', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 3, NULL, NULL, NULL, NULL, 2, 'Deck', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 3, NULL, NULL, NULL, NULL, 9, 'Salt walk ways', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Asphalt driveway');

-- --------------------------------------------------------

--
-- Table structure for table `businesshour`
--

CREATE TABLE `businesshour` (
  `idbusinesshour` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `day` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `starttime` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `endtime` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `breakstattime` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `breakendtime` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `idcart` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `idstylish` int(11) NOT NULL,
  `idservice` int(11) NOT NULL,
  `bookingno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `userfullname` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `usermobile` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `useremail` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `stylishname` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `stylishmobile` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `stylishemail` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `starttime` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `endtime` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `mrp` float NOT NULL,
  `discount` float NOT NULL,
  `promocode` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `idcategory` int(11) NOT NULL,
  `parentid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) DEFAULT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`idcategory`, `parentid`, `name`, `sort`, `isactive`, `createdon`) VALUES
(1, 0, 'Barber', 0, 1, '2018-05-11 16:15:23'),
(2, 0, 'Hair Cut', 1, 1, '2018-05-11 16:16:16'),
(3, 0, 'Color', 2, 1, '2018-05-11 16:17:18'),
(4, 0, 'Natural Hair', 3, 1, '2018-05-11 16:17:18'),
(5, 0, 'Straightening', 4, 1, '2018-05-11 16:17:26'),
(6, 1, 'Sideburn Shave', 5, 1, '2018-05-11 16:17:55'),
(7, 0, 'Style', 6, 1, '2018-05-11 16:18:49'),
(8, 0, 'Nails', 7, 1, '2018-05-11 16:19:15'),
(9, 0, 'Massage', 8, 1, '2018-05-11 16:19:51'),
(10, 1, 'Men\'s Cut', 1, 1, '2018-05-11 16:19:51'),
(11, 1, 'Razoring', 2, 1, '2018-05-11 16:19:51'),
(12, 1, 'Line Up', 3, 1, '2018-05-11 16:19:51'),
(13, 1, 'Edge Up', 4, 1, '2018-05-11 16:19:51'),
(14, 2, 'Men\'s Cut', 1, 1, '2018-05-11 16:19:51'),
(15, 2, 'Men\'s Trim', 2, 1, '2018-05-11 16:19:51'),
(16, 2, 'Neck Trim', 3, 1, '2018-05-11 16:19:51'),
(17, 2, 'Women\'s Cut', 4, 1, '2018-05-11 16:19:51'),
(18, 2, 'Women\'s Dry Cut', 5, 1, '2018-05-11 16:19:51'),
(19, 2, 'Women\'s Trim', 6, 1, '2018-05-11 16:19:51'),
(20, 2, 'Bang Trim', 7, 1, '2018-05-11 16:19:51'),
(21, 2, 'Kid\'s Trim', 8, 1, '2018-05-11 16:19:51'),
(22, 3, 'Color Touch-Up', 1, 1, '2018-05-11 16:19:51'),
(23, 3, 'Highlights', 2, 1, '2018-05-11 16:19:51'),
(24, 3, 'Lowlights', 3, 1, '2018-05-11 16:19:51'),
(25, 3, 'Foil Highlights', 0, 1, '2018-05-11 16:19:51'),
(26, 3, 'Balayage', 5, 1, '2018-05-11 16:19:51'),
(27, 3, 'Ombre', 6, 1, '2018-05-11 16:19:51'),
(28, 3, 'Toner', 7, 1, '2018-05-11 16:19:51'),
(29, 3, 'Hair Color', 8, 1, '2018-05-11 16:19:51'),
(30, 4, 'Shampoo', 1, 1, '2018-05-11 16:19:51'),
(31, 4, 'Relaxer', 2, 1, '2018-05-11 16:19:51'),
(32, 4, 'Silk Wrap', 3, 1, '2018-05-11 16:19:51'),
(33, 4, 'Roller Set', 4, 1, '2018-05-11 16:19:51'),
(34, 4, 'Silk Press', 5, 1, '2018-05-11 16:19:51'),
(35, 4, 'Natural Style', 6, 1, '2018-05-11 16:19:51'),
(36, 5, 'Brazilian Blowout', 1, 1, '2018-05-11 16:19:51'),
(37, 5, 'Japanese Hair Straightening', 2, 1, '2018-05-11 16:19:51'),
(38, 7, 'Style', 1, 1, '2018-05-11 16:19:51'),
(39, 7, 'Flat Iron', 2, 1, '2018-05-11 16:19:51'),
(40, 7, 'Wand / Barrel Cuts', 3, 1, '2018-05-11 16:19:51'),
(41, 7, 'Updo', 4, 1, '2018-05-11 16:19:51'),
(42, 7, 'Braid Bar Style', 5, 1, '2018-05-11 16:19:51'),
(43, 7, 'Olaplex Treatment', 6, 1, '2018-05-11 16:19:51'),
(44, 8, 'Manicure', 1, 1, '2018-05-11 16:19:51'),
(45, 8, 'Padicure', 2, 1, '2018-05-11 16:19:51'),
(46, 8, 'Gel Manicure', 3, 1, '2018-05-11 16:19:51'),
(47, 8, 'Acrylic Nails', 4, 1, '2018-05-11 16:19:51'),
(48, 8, 'Shellac manicure', 5, 1, '2018-05-11 16:19:51'),
(49, 8, 'Acrylic Fill', 6, 1, '2018-05-11 16:19:51'),
(50, 8, 'Gel Fill', 7, 1, '2018-05-11 16:19:51'),
(51, 8, 'Nail Art', 8, 1, '2018-05-11 16:19:51'),
(52, 9, 'Swedish Massage', 1, 1, '2018-05-11 16:19:51'),
(53, 9, 'Hot Stone Massage', 2, 1, '2018-05-11 16:19:51'),
(54, 9, 'Deep Tissue Massage', 3, 1, '2018-05-11 16:19:51'),
(55, 9, 'Body Scrub', 4, 1, '2018-05-11 16:19:51'),
(56, 9, 'Facial', 5, 1, '2018-05-11 16:19:51'),
(57, 9, 'Dermaplaning', 6, 1, '2018-05-11 16:19:51'),
(58, 1, 'Design', 6, 1, '2018-05-11 16:19:51'),
(59, 1, 'Buzz Cut', 7, 1, '2018-05-11 16:19:51'),
(60, 1, 'Fade', 8, 1, '2018-05-11 16:19:51'),
(61, 1, 'Shave', 9, 1, '2018-05-11 16:19:51'),
(62, 1, 'Mustache Trim', 10, 1, '2018-05-11 16:19:51'),
(63, 1, 'Bread Trim', 11, 1, '2018-05-11 16:19:51'),
(64, 1, 'Hot Towel Serivce', 12, 1, '2018-05-11 16:19:51');

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `idchat` int(11) NOT NULL,
  `idsender` int(11) NOT NULL,
  `idreceiver` int(11) NOT NULL,
  `usertype` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `filename` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `filetype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `viewstatus` int(1) NOT NULL DEFAULT '0',
  `chatstatus` int(1) NOT NULL DEFAULT '1',
  `idchatroom` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`idchat`, `idsender`, `idreceiver`, `usertype`, `message`, `filename`, `filetype`, `viewstatus`, `chatstatus`, `idchatroom`, `createdon`) VALUES
(1, 1, 4, 'STYLIST', 'kaise ho?', NULL, NULL, 1, 1, 'S4R1', '2018-06-01 18:22:41'),
(2, 4, 1, 'CUSTOMER', 'kaise ho?', NULL, NULL, 1, 1, 'S4R1', '2018-06-01 18:23:19'),
(3, 4, 1, 'CUSTOMER', 'kaise ho?', NULL, NULL, 1, 1, 'S4R1', '2018-06-01 18:25:17'),
(4, 1, 4, 'STYLIST', 'kaise ho?', NULL, NULL, 0, 1, 'S4R1', '2018-06-01 18:25:24'),
(5, 1, 4, 'STYLIST', 'kaise ho?', NULL, NULL, 1, 1, 'S4R1', '2018-06-01 18:50:49'),
(6, 4, 1, 'STYLIST', 'badia', NULL, NULL, 1, 1, 'S4R1', '2018-06-01 18:51:03'),
(7, 1, 4, 'STYLIST', 'main bhi thik hun.', NULL, NULL, 1, 1, 'S4R1', '2018-06-01 18:51:23'),
(8, 1, 4, 'STYLIST', 'Kaam kar lo, bas baithe ho jab se aaye ho.', NULL, NULL, 0, 1, 'S4R1', '2018-06-01 18:53:35'),
(9, 1, 4, 'STYLIST', 'Kaam kar lo, bas baithe ho jab se aaye ho.', NULL, NULL, 1, 1, 'S4R1', '2018-06-01 19:08:33'),
(10, 1, 4, 'STYLIST', 'Kaam kar lo, bas baithe ho jab se aaye ho.', NULL, NULL, 1, 1, 'S4R1', '2018-06-01 19:09:32'),
(11, 1, 4, 'STYLIST', 'Kaam kar lo, bas baithe ho jab se aaye ho.', NULL, NULL, 0, 1, 'S4R1', '2018-06-01 19:12:36'),
(12, 1, 4, 'STYLIST', 'Kaam kar lo, bas baithe ho jab se aaye ho.', NULL, NULL, 0, 1, 'S4R1', '2018-06-01 19:16:29'),
(13, 4, 1, 'STYLIST', 'How are you?', '', '', 0, 1, NULL, '2018-06-08 14:28:48'),
(14, 4, 1, 'STYLIST', 'Thanks bro, I am good', '', '', 0, 1, ' 20180608', '2018-06-08 22:15:58'),
(15, 6, 1, 'CUSTOMER', 'Vinay, How are you?', '', '', 0, 1, 'S1R6', '2018-06-11 23:09:26');

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `idconfig` int(11) NOT NULL,
  `companyname` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `toplogoimage` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `mobiletoplogoimage` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footerlogoimage` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `copyright` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `infomail` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salesmail` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adminmail` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supportmail` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customersupport` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `twilio_account_sid` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twilio_account_token` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twilio_phone_number` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`idconfig`, `companyname`, `toplogoimage`, `mobiletoplogoimage`, `footerlogoimage`, `copyright`, `infomail`, `salesmail`, `adminmail`, `supportmail`, `customersupport`, `phone`, `address`, `city`, `zipcode`, `state`, `country`, `twilio_account_sid`, `twilio_account_token`, `twilio_phone_number`, `isactive`) VALUES
(1, 'DRYFT Corporation', 'dryft_logo.png', NULL, 'dryft_logo_white.png', 'DRYFT Corporation. All rights reserved', 'info@dryftnow.com', 'sales@dryftnow.com\r ', 'admin@dryftnow.com', 'support@dryftnow.com', NULL, '1-800-489-1234', '', '', '', '', '', 'ACf45488ce70780184d0637e813b1a241f', 'bb7fba1feebb5199f46ece003f97ecc3', '+16308127560', 1);

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `idcontent` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `friendlyURL` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `contentfor` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`idcontent`, `title`, `friendlyURL`, `detail`, `contentfor`, `sort`, `isactive`, `createdon`) VALUES
(1, 'Legal Agreement', 'legal-agreement', '<h3>Dryft Terms and Conditions</h3><p>Please read these Terms of Service and our Privacy Policy carefully before using any services offered by Dryft.</p><p>Whenever using the Dryft Application, you agree to be bound by all these Terms of Service. If you do not agree to all the terms and conditions you must not use the service.</p><p>Dryft is not affiliated with any brands, services, or products that may appear from time to time on the application. These brands, services or products do not endorse or sponsor the Dryft App and are in no way affiliated with Dryft in any way.</p><p><b>THE SECTION TITLES IN THESE TERMS OF SERVICE ARE FOR CONVENIENCE ONLY AND HAVE NO LEGAL OR CONTRACTUAL EFFECT.</b><p>Acceptance of Terms</p><p>These Terms and Conditions (\"Terms\") are Terms of Service that operate as a legal agreement between Dryft and the Dryft Application User (\"User\"). By downloading and/or accessing the Dryft Application (\"Dryft App\") in any way, User agrees that they have read, understood, accept and agree to be bound by these Terms as outlined by Dryft. If User does not agree to these terms, they should not use or download the Dryft App. If User does not agree with these Terms at any time, User should immediately stop using the Dryft App.</p><p>Dryft reserves the right to modify, revise or revoke portions of these Terms, Application Rules (\"Rules\"), or Privacy Policy at any time. Any revision, modification or revocation of any of these Terms, Rules or Privacy Policy will be done in the sole discretion of Dryft, and User will be responsible for reviewing these Terms, Rules and Privacy Policy on a monthly basis. Continued use of the Dryft, after any revision, modification, or revocation made to these Terms, Rules or Privacy Policy by Dryft, shall constitute acceptance of the revised or modified Terms, Rules, or Privacy Policy. The sole remedy for any User who does not agree with these Terms, Rules, or Privacy Policy, or any revised or modified Terms, Rules or Privacy Policy is to discontinue use of the Dryft App indefinitely.</p><p>Unless stated otherwise, changes are effective when posted. Dryft may, in its sole discretion, provide additional notice to User regarding the revision, modification or revocation of any material terms. Dryft may, in its sole discretion, determine what terms are considered material. User cannot make changes to the Terms, Rules or Privacy Policy. If these Terms have provisions or policies that conflict with any other Dryft App provisions or policies, these Terms and the Privacy Policy will govern.</p><p>Dryft has the right to enforce these Terms and reserves the right to issue an initial warning regarding violation of these terms. The right of Dryft to issue an initial warning will not preclude Dryft from immediately terminating or suspending any and all User accounts that have violated these Terms. The User agrees that Dryft does not need to provide User with notice prior to terminating or suspending User account.</p><p>User agrees that Dryft may change any part of the Dryft App, including its content or services offered, at any time or discontinue the Dryft App or any part thereof, for any reason, without notice to User and without liability. The sole remedy for any User who does not agree with these Terms is to discontinue use of the Dryft App indefinitely.</p><p><b>IMPORTANT NOTE:</b> These Terms contain a Dispute Resolution and  Arbitration Provision, including Class Action Waiver that affects User rights under these Terms with respect to any disputes that may arise with Dryft through use of the Dryft App. User\'\'s may opt out of binding individual and class action waiver as provided below.</p><p>Privacy Policy</p><p>Dryft\'\'s Privacy Policy informs User how Dryft collects and uses User information about User and User\'\'s computer or mobile device. User understands that through the use of the Dryft App, User acknowledges the collection, use and sharing of this information as described in Dryft\'\'s Privacy Policy. If User does not agree with the Privacy Policy, User must immediately stop using the Dryft App.</p><p>Dryft will only use personal information provided by User in connection with their use of the Dryft App. The use of any personal information will be in accordance with these Terms and the Dryft Privacy Policy, which are available through the Dryft App dryftnow.com. By accessing, downloading or using the Dryft App, Users consents to the collection, use and storage of User information as outlined in these Terms and the Privacy Policy. Questions regarding privacy issues should be directed to info@dryft.com.</p><p>Dryft encourages User to read the Privacy Policy carefully and use it to make informed decisions</p><p>Account Information</p><p>In order to create a User account (\"Account\" or \"User Account\") for use of the Dryft App, User may be required to create an Account username and unique password in order to allow User to access their Account, or User may also allow Dryft to access User account information from a social networking service (\"Login Information\"). Login Information shall include any account and account information, including usernames, passwords or security questions, whether or not created for the purpose of using the Dryft App.</p><p>Dryft may ask User to provide other personal information such as birth date, e-mail address, and when necessary, payment information. This information will be held and used in accordance with the Privacy Policy of Dryft. User agrees to supply Dryft with accurate, complete, and updated information.</p><p>User may create an Account through User\'\'s Facebook account (\"Facebook Account\"). When registering User Account through Facebook, User will be asked to login to their Facebook Account, and configure the privacy settings of their Facebook Account to permit User activities and achievements to be shared through this Facebook Account. If User creates an account using their Facebook Account, User agrees to allow Dryft to access User Facebook Account information. User also agrees that the use of the Facebook website is governed by the terms and conditions of Facebook\'\'s terms of use and its privacy policy, including, without limitation, Facebook\'\'s passwords and account security policies.</p><p>By creating an Account through Facebook for use of the Dryft App, User agrees to protect the security of their Facebook Account and Login Information. User agrees to be solely responsible for maintaining the confidentiality of their Facebook Account, to protect the security of their Facebook Account and Login Information by implementing, but not limited to, the following: 1) Keeping this information confidential. 2) Restricting any other person from accessing User Facebook Account, or do any other activities that will compromise the security of User Facebook Account. Dryft will not ask User to reveal User Facebook password and will not initiate contact with User regarding answers to User Facebook Account security question answers. 3) Monitoring User Account and Facebook Account for any irregular or suspicious activity. 4) Restrict use by minors under the age of 18. 5) Immediately notifying Dryft in the event User becomes aware of a security breach or compromise, which includes but is not limited to, any loss, theft, or unauthorized use of User Facebook Account.</p><p>User is responsible for anything that happens on the Dryft App, including purchases, through use of User\'\'s Facebook Account. User will be responsible for any purchases made through User\'\'s Account, whether or not such actions were taken by User. User agrees that Dryft may suspend or terminate User Account if any activity through either account violates these Terms.</p><p>Governing Rules for Account and Login Information</p><p>Any references made under these Terms to User Account or Login Information shall include any user names, passwords or security questions used to access the Dryft App, regardless of whether or not this information was created solely for use of the Dryft App. Examples of User Account Login Information not created solely for the purpose of accessing the Dryft App includes account information created for use of a social media service that is also used to access the Dryft App.</p><p>User shall not share or publish their Login Information with any third party or make such information available through any social networking medium. User is also prohibited from allowing any other person or entity (this includes any person who is not the User who created the account, as well as any robot, spider, scraper, sniping software, or any similar program) from accessing or using User Account. User shall not do anything that may jeopardize the security of their Account, as determined in the sole discretion of Dryft.</p><p>Dryft will never ask User to disclose their password or answers to User security questions. Any User who reasonably believes they have encountered or suspect a security breach of their account should notify Dryft immediately. User should notify Dryft immediately in the event of any loss, theft, or unauthorized disclosure of User Login Information, or unauthorized access to User Account.</p><p>User is responsible for maintaining the confidentiality and security of their Account.</p>\r\n<p>User is responsible for any activities or actions that take place through User Account, whether or not such activities or actions were taken or authorized by User, including purchases made using any payment instrument (ex: credit card, PayPal, or social network or platform virtual currency). User acknowledges that their Account may be suspended or terminated if a person who is not User engages in any activities that violate these Terms or engages in activities considered improper or illegal in the sole discretion of Dryft.</p><p>User shall always be responsible for monitoring their account to restrict access to children under the age of 18. User accepts full responsibility for any unauthorized use of the Dryft App by minors, and User acknowledges that User is responsible for any use of the credit card or any other payment method (ex: PayPal) on file with Dryft.</p><p>Dryft reserves the right to remove or reclaim any usernames at any time and for any reason, including but not limited to claims by a third party that a username violates the third party\'\'s rights, or is found by Dryft, in its sole discretion, to be offensive, explicit or inappropriate for any reason. Users understand that their username and profile picture will be publicly available, and that search engines may index Username and profile photo.</p><p>Dryft may collect certain information from Users during account creation or updating, including username, birth date, e-mail address and payment information. This information will be held in accordance with Dryft\'\'s Privacy Policy that can also be viewed through the Dryft App. User agrees to supply true and accurate information, and User also agrees to update their personal information after any changes.</p><p>Rules of Conduct and Usage</p><p>User represents and warrants that User has the full right and authority to use the Dryft App and be bound by these Terms. User agrees to comply fully with all applicable laws, regulations, statutes, ordinances, and these Terms set forth herein. User agrees that they will not defraud, or attempt to defraud, Dryft or any other users of the Dryft App, and shall not act in bad faith while using the Dryft App. If Dryft determines that User actions were undertaken in bad faith or violate these Terms, or if such User activity falls outside of reasonable community standards, Dryft may, in its sole discretion, terminate User Account and ban User from using the Dryft App.</p><p>User agrees that use of the Dryft App shall be lawful, and that User will comply with usage rules and Terms. User agrees that they shall not undertake any of the following types of behavior, offered as examples and not as a limitation, while using the Dryft App:</p><ul>\r\n<li>Libel, defame, stalk, intimidate, threaten, harass, or abuse anyone hatefully, racially, ethnically or in any other manner.</li><li>Upload or transmit (or attempt to upload or transmit) files that contain viruses, Trojan horses, worms, time bombs, cancelbots, corrupted files, or data, or any other similar software or programs that may damage the operation of the Dryft App or the computers or phones of other users of the Dryft App;</li><li>Transmit any spam or commercial advertising.</li><li>Violate the contractual, personal, intellectual property or other rights of any party including by using, uploading, transmitting, distributing, or otherwise making available any information or material made available through the Dryft App in any manner that infringes any copyright, trademark, patent, trade secret, or other right of any party</li><li>Create multiple Accounts or create an Account on behalf of another person.</li><li>Use bots or other automated software programs to defraud or which otherwise violate these Terms or terms of service of any third-party.</li><li>Attempt to obtain passwords, financial information or any personal information in any way from any other User on the Dryft App.</li><li>Upload or transmit (or attempt to upload or to transmit), any material that acts as an information collection or transmission mechanism, including, without limitation, clear graphics interchange formats (\"gifs\"), 1x1 pixels, web bugs, cookies or other similar devices (sometimes referred to as \"spyware\", \"passive collection mechanisms\" or \"pcms\").</li><li>Make false reports to Dryft.</li><li>Violate any applicable laws or regulations, or encourage or promote any illegal activity of any kind including, but not limited to, copyright or trademark infringement, defamation, invasion of privacy, identity theft, hacking, cracking or distributing counterfeit software, or cheats or hacks for the Dryft App;</li><li>Attempt to use the Dryft App through any service not authorized by Dryft. Any User who uses the Dryft App through any service not authorized by Dryft does so at their own risk, and agrees that Dryft bears no liability and takes no responsibility regarding such use of the Dryft App.</li><li>Communicate the real-world personal information of any person in any way, whether or not they use the Dryft App, through use of the Dryft App.</li><li>Attempt to interfere with, hack into or decipher any transmissions to or from the servers for the Dryft App; or</li><li>Interfere in any way with the ability of others to use and enjoy the Dryft App or take actions that interfere with or materially increase the cost to provide the Dryft App for the enjoyment of all its users.</li></ul><p>You may not use the Dryft App if any of the following restrictions apply:</p><ul><li>You cannot enter into a binding contract with Dryft</li><li>You are not allowed to receive products from the United States (ex: you are located in a country embargoed by the United States or if you are on the U.S. Treasury Department\'\'s list of Specially Designated Nationals).</li><li>You have been previously banned from using on the Dryft App</li></ul><p>License</p><p>In order to access the Dryft App, User must have a compatible device. Dryft does not warrant that the Dryft App will be compatible with User\'\'s mobile device or tablet.</p><p>Subject to User agreement and compliance with these Terms, Privacy Policy, and Rules, Dryft grants User a personal, non-exclusive, non-transferable, non-sublicense able, revocable, limited scope license to install and use an object code copy of the Dryft App for one registered account on one mobile device or tablet owned or used solely by User. Dryft also grants User a personal, non-exclusive, non-transferable, non-sublicense able, revocable, limited scope license to access and use portions of the services offered by the Dryft App that are not part of the Dryft App.</p>\r\n<p>User acknowledges that their license to use the Dryft App is limited by these Terms. If User violates any of these Terms, or does not agree to any of these Terms, User license to use the Dryft App will immediately terminate, and User shall immediately refrain from using the Dryft App. If the Dryft App or any part of the Dryft App is determined to be illegal under any laws of the country in which User resides or currently lives, User must refrain from using the Dryft App and will not be granted a license.</p><p>User agrees and acknowledges that Dryft may, in its sole discretion, issue upgraded versions of the Dryft App. User consents to any automatic upgrades of the Dryft App, and agrees that these Terms, Privacy Policy and Rules will apply to any such upgrades.</p><p>User may not: 1) modify, disassemble, decompile or reverse engineer the Dryft App; 2) rent, lease, loan, resell, sublicense, distribute or otherwise transfer the Dryft App to any third party; 3) make any copies of the Dryft App; 4) delete, circumvent, render inoperative, damage or otherwise interfere with any security-related features of the Dryft App; 5) delete the copyright and other proprietary notices on the Dryft App.</p><p>User agrees that the grant of above-mentioned license is not a sale of the Dryft App or any copy thereof, and that Dryft retains all right, title, and interest in the Dryft App. Standard phone service carrier rates may apply to User use of the Dryft App.</p><p>Account Termination</p><p>Dryft may deny or refuse access to the Dryft App or may terminate User account without notice for any reason. Reasons for refusal or termination include, but are not limited to, a suspected violation of these Terms, illegal or improper use of User Account, or illegal or improper use of the Dryft App, User Content (defined below), products, or Dryft\'\'s intellectual property (IP) as determined by Dryft in its sole discretion. User may lose their username as a result of User Account termination, without any responsibility on the part of Dryft for any damage that may result from account termination or refusal to use the Dryft App. If a User has multiple accounts, Dryft may terminate all of those accounts.</p>\r\n<p>User Content</p><p>User agrees that any content published by User through use of the Dryft App is done so through the use of technology, tools, and substantial effort provided by Dryft. User agrees that any such content is published willingly, and User represents that User owns such content, and has the right to publish such content in compliance with applicable publishing laws. User acknowledges and agrees that User may not distribute, sell, transfer or license this content and/or application in any manner, medium or country. User agrees and grants Dryft the right to act as an agent on User behalf as the Dryft App operator.</p><p>Any data, text, graphics, photographs, or any other content, including the selection and arrangement of such content, uploaded to the Dryft App by any user (\"User Content\") are subject, whether whole or in part, to unlimited worldwide commercial, non-commercial and/or promotional use by Dryft. Any text, graphics, photographs, files or other User Content uploaded by User shall be the sole responsibility of the User, and User hereby agrees that User may be held liable for any User Content uploaded by the User. Dryft shall not bear any responsibility and shall have no liability for any of the above-mentioned content.</p><p>Dryft may or may not regulate User Content and provides no representations or guarantees regarding any content published by User. User acknowledges that use of the Dryft App may expose them to material they find offensive of objectionable. User agrees that Dryft will not be responsible or liable for any User Content including, but not limited to, errors in User Content or any loss or damage incurred by the use of User Content or for any failure to or delay in removing User Content.</p><p>Dryft reserves the right to, in its sole discretion, remove, edit, modify or permanently delete User Content from the Dryft App without notice for any reason. User agrees that, to the maximum extent provided by law, Dryft shall in no way be responsible or held liable for the removal, modification or blocking of any material or any User Content, even if such material or content may be considered offensive. Dryft shall at no time be obligated to affect such removal other than under applicable law.</p><p>Communication Features</p><p>The Dryft App will provide communication features that will enable User to communicate with other users of the Dryft App. Such communication features may include forum, or any other method made available to allow User to communicate through the Dryft App (\"Communication Features\"). Dryft is under no obligation to monitor these Communication Features but may choose to do so in the sole discretion of Dryft. Dryft reserves the right, in its sole discretion, to remove any material from these Communication Features at any time without prior notice. Dryft, in its sole discretion, may also terminate or suspend User access to any Communication Features at any time, without notice, for any reason.</p><p>User acknowledges that any chat or content posted by Users through the Communication Features of Dryft are in no way endorsed or controlled by Dryft, and these communications should not be considered at any time to have been reviewed or approved by Dryft. At no time or under any circumstances will Dryft be held liable for any activity within the Communication Features. User agrees that all communications through the Communication Features are public and that User will not have any expectation of privacy regarding these communications or the use of the Communication Features. Dryft will not be responsible for information that User chooses to share through the Communication Features, or for the actions or communications of any other user of the Dryft App.</p><p>Inactive Account</p><p>If User does not login to their Account within 30 days, that Account will be deemed \"inactive\". User can, at any time, reactivate their account by logging in to the Dryft App.</p><p>Intellectual Property</p><p>Use of the Dryft App, and the Dryft App logo is protected by copyright, trademark, and other laws of the United States and foreign countries. Unless otherwise provided in these Terms, Dryft and its licensors own all right, title and interest in and to Dryft, all of its content, including all associated intellectual property rights. User or any third party may not remove, alter or obscure any copyright, trademark, service mark or other proprietary rights notices incorporated in or accompanying the Dryft App. User agrees they shall not:</p><p>Use the intellectual property of Dryft, or any Dryft licensor, to alter, adapt, or create derivative works based on the intellectual property of Dryft.</p><p>Use, exhibit, or frame the Dryft App, or any individual element within the service.</p><p>Rent, lease, loan, trade, sell/re-sell access to the Dryft App or any information therein, in whole or in part.</p><p>Modify, reverse engineer, decompile, disassemble, decipher or otherwise attempt to derive the source code for any underlying software or other intellectual property used to provide the Dryft App without the express written consent of Dryft; or</p><p>Use or recreate any Dryft licensor, or third-party trademark or logo without the prior express written consent of the owner of such intellectual property.</p><p>Disclaimer of Warranty; Limitation of Liability</p><p>User agrees that use of the Dryft App shall be done at the sole risk of User. To the fullest extent permitted by law, Dryft, its officers, directors, employees, and agents disclaim all warranties, explicit or implied, in connection with the Dryft App and User use thereof including implied warranties of merchantability, title, fitness for a particular purpose or non-infringement, usefulness, authority, accuracy, completeness, and timeliness. Dryft makes no warranties or representations about the accuracy or completeness of the content of the Dryft App or the content of any sites linked to the Dryft App and assume no liability or responsibility for any.</p><p>Errors, mistakes, or inaccuracies of content.</p><p>Personal injury or property damage of any kind resulting from User access of and use of the Dryft App.</p><p>Any unauthorized access to or use of Dryft\'\'s secure servers and/or any  and all personal information and/or financial information stored therein; Any interruption or cessation of transmission to or from the Dryft App; Any bugs, viruses, Trojan horses, or anything similar in nature which may be transmitted to or through the Dryft App by any third party; or</p><p>Any errors or omissions in any content or for any loss or damage of any kind incurred as a result of the use of any content posted, emailed, transmitted, or otherwise made available through the Dryft App.</p><p>In no event will Dryft, its directors, officers, agents, contractors, partners, or employees, be liable to User or any third person for any special, direct, indirect, incidental, special, punitive, or consequential damages whatsoever including any lost profits or lost data arising from User use of the Dryft App or other materials accessed through or downloaded from the Dryft App, whether based on  warranty, contract, tort, or any other legal theory, and whether or not Dryft has been advised of the possibility of these damages. The foregoing limitation of liability shall apply to the fullest extent permitted by law in the applicable jurisdiction. User specifically acknowledges that Dryft shall not be liable for user submissions or defamatory, offensive, or illegal conduct by any third party and that the risk of harm or damage from these activities will rest entirely on the User.</p><p>User agrees to indemnify and hold Dryft and each of its directors, officers, agents, contractors, partners and employees, harmless from and against any loss, liability, claim, demand, damages, costs and expenses, including reasonable attorney\'\'s fees, arising out of or in connection with:</p><p>User use and access to the Dryft App; User violation of any of these terms.</p><p>User violation of any third party right, including without limitation any copyright, property, or privacy right.</p><p>Any claim that a user submission made by User has caused damage to a third party; or\r\nAny User Content User posts or shares through the Dryft App.</p><p>General</p><p>By using or visiting the Dryft App, User agrees that the laws of the State of Illinois, without regard to principles of conflict of laws and regardless of User location, will govern these Terms and any dispute that may arise between User and Dryft. Any claims or disputes that may arise between User and Dryft shall be decided exclusively by a court of competent jurisdiction located in Illinois, and User hereby consents to, and waives all defenses of lack of personal jurisdiction and forum non convenience with respect to venue and jurisdiction in the courts of Illinois.</p><p>Dryft reserves the right to amend these Terms at any time and without notice, and it is the responsibility of User to review these Terms periodically for any changes. User use of the Dryft App following any changes to these Terms will signify User assent to and acceptance of the revised terms.</p><p>User and Dryft agree that any cause of action arising out of or related to the use of the Dryft App must commence within one (1) year after the cause of action accrues. If such cause of action arising out of or related to the use of the Dryft App is not commenced within one (1) year after the cause of action accrues, such cause of action will be permanently barred.</p><p>If User disagrees with these Terms, User must immediately discontinue use of Dryft\'\'s services, including use of the Dryft App. Any questions regarding these Terms can be directed to info@dryft.com.</p><p>About</p><p>Dryft is incorporated in the state of Illinois and will be subject to the state laws of Illinois. Dryft also requires users to agree to its Terms prior to using the application. Use of the Dryft application is an acknowledgement of user acceptance of Dryft\'\'s Terms. Included in these Terms is a choice of law clause that require any disputes between the user and Dryft to be litigated in the state of Illinois, which would allow Illinois state law to determine the outcome of the case.</p><p>Digital Millennium Copyright Act (DMCA) Notice This Act is included in these Terms because it criminalizes production and dissemination of technology, devices, or services intended to circumvent measures that control access to copyrighted works. This Act also criminalizes the circumvention of access controls, regardless of whether or not there is actual infringement of a copyright itself. Dryft takes the protection of its intellectual property seriously, including the intellectual property of its partners and licensors, and will not allow any User to violate this Act, or the intellectual property rights of Dryft or any other third-party.</p><p>If User or any third-party reading this is a copyright owner or agent thereof, and believes such work has been the subject of copyright infringement on the Dryft App, User, owner or agent may submit a notification of claimed infringement under this Act. You must provide notice to Dryft through one if it\'\'s designated agents. The following information must be submitted.\r\na)	Identification of the copyrighted work claimed to have been infringed, including a representative list of such works if multiple works are claimed to have been infringed upon.\r\nb)	Identification of the disputed material that is claimed to be infringing on, with such information included that is reasonably sufficient to permit Dryft to locate the subject material in order to investigate and determine whether or not such material should be removed.\r\nc)	Contact information of the copyright owner or agent, such as address, phone number and e-mail address.\r\nd)	A statement that there is a good faith belief that use of the material in the manner complained of was not authorized by the copyright owner, its agent, or the law.\r\ne)	A statement that the information in the notification is accurate, and under penalty of perjury, that You are authorized to act on behalf of the owner of an exclusive right that is allegedly infringed; and\r\nf)	A physical or electronic signature of a person authorized to act on behalf of the owner of a copyright that is allegedly infringed.</p><p>Please also note that under Section 512(f) of the Copyright Act, any person who knowingly materially misrepresents that material or activity is infringing may be subject to liability.</p><p>info@dryft.com</p><p>Dispute Resolution and Arbitration</p><p>This section will outline how disputes between User and Dryft will be handled. Arbitration is a form of private Dispute resolution that is more efficient and costs significantly less than settling Disputes through the court system. Instead of proceeding with a Dispute through the court system where the final decision is left to a judge or jury, arbitration allows a neutral third party to make a final and binding decision as to the parties Dispute. The arbitrator will follow this agreement when deciding the Dispute and can award the same damages that could have been awarded in court, including attorney\'\'s fees. For the purpose of this section, \"Dryft\" means Dryft and its parents, subsidiary, and affiliate companies, and each of their respective officers, directors, employees, and agents. The term \"Dispute\" means any dispute, claim, or controversy between you and User regarding any aspect of your relationship with Dryft, whether based in contract, statute, regulation, ordinance, tort (including, but not limited to, fraud, misrepresentation, fraudulent inducement, or negligence), or any other legal or equitable theory, and includes the validity, enforceability or scope of this section (with the exception of the enforceability of the Class Action Waiver clause below). \"Dispute\" is to be given the broadest possible meaning that will be enforced. User AND Dryft EACH AGREE THAT, EXCEPT AS PROVIDED BELOW, ANY AND ALL DISPUTES, AS DEFINED ABOVE, WHETHER PRESENTLY IN EXISTENCE OR BASED ON ACTS OR OMISSIONS IN THE PAST OR IN THE FUTURE, WILL BE RESOLVED EXCLUSIVELY AND FINALLY BY BINDING ARBITRATION RATHER THAN IN COURT IN ACCORDANCE WITH THIS PROVISION.</p><p>User is encouraged to read this section carefully and may opt out of this section (as provided below) if User so chooses. If User opts out of the arbitration section, User will have the right to proceed through the court system with their dispute or participate in a case filed by a third party such as a class action suit. Except as provided below, entering into this agreement will waive User\'\'s right to litigate before a judge or jury.</p><p>Pre-Arbitration Claims</p><p>Prior to filing any dispute of cause of action against Dryft, and whether pursued through court or arbitration, User must allow Dryft the opportunity to settle the dispute or cause of action within 45 days of receiving notice of the dispute or cause of action from User. User must send written notice to Dryft Legal, c/o Matthew & Drnovsek Law LLC, 1200 W. 35th Street, Chicago, IL 60609, that includes 1) User Name, 2) User Address, 3) a written description of the claim, and 4) a description of the relief User is seeking in order to resolve the dispute. User may pursue your Dispute in a court only under the circumstances described below.</p><p>Exclusion from Arbitration/Right to Opt Out Notwithstanding the above, User or Dryft may choose to bring a Dispute in court and not by arbitration if:</p><p>(a)	The Dispute qualifies, it may be initiated in small claims court; or\r\n(b)	User OPTS-OUT OF THESE ARBITRATION PROCEDURES WITHIN 30 DAYS FROM THE DATE THAT User FIRST CONSENTS TO THIS AGREEMENT (the \"Exclusion Deadline\"). User may opt out of this section by mailing written notification to Dryft Legal, c/o Matthew & Drnovsek Law LLC, 1200 W. 35th Street, Chicago, IL 60609</p><p>User\'\'s written notification must include (1) Username, (2) User address, and (3) an unambiguous statement that User does not wish to resolve disputes with Dryft through arbitration. User\'\'s decision to opt-out of this section will have no adverse effect on your relationship with Dryft.  Any opt-out request received after the Exclusion Deadline will not be valid and you must pursue your Dispute in arbitration or small claims court.</p><p>Arbitration Process</p><p>Either User or Dryft may begin the arbitration process. The American Arbitration Association (\"AAA\"), www.adr.org, or JAMS, www.jamsadr.com, will arbitrate all Disputes, and the arbitration will be conducted before a single arbitrator. All arbitrations shall be individual dispute resolutions, and no class action arbitrations will be commenced against Dryft at any time. The AAA shall use its Supplementary Procedures for Consumer-Related Disputes for all Disputes under $75,000, the Commercial Arbitration Rules for Disputes in excess of $75,000; and AAA\'\'s Optional Rules for Emergency Measures of Protection shall apply to either instance. The AAA rules are available at www.adr.org or by calling 1-800-778-7879. If User or Dryft elects to settle their Dispute through JAMS, the JAMS Comprehensive Arbitration Rules & Procedures and the JAMS Recommended Arbitration Discovery Protocols for Domestic, Commercial Cases will apply. The JAMS rules are available at www.jamsadr.com or by calling 1-800-352-5267. This section controls even if there is conflict with the applicable arbitration rules. Under no circumstances will class action procedures or rules apply to the arbitration. Because the Dryft App and these Terms concern interstate commerce, the Federal Arbitration Act (\"FAA\") governs the arbitrability of all Disputes. However, the arbitrator will apply applicable substantive law consistent with the FAA and the applicable statute of limitations or condition precedent to suit.</p><p>The arbitrator may award damages that could have been awarded to either individual party through the court system but may not award relief or damages to anyone who is not a part of the proceeding. Awards will be issued in writing, but the arbitrator is not obligated to explain the determination.</p><p>Any determination made to any Dispute by the arbitrator will be final and binding, except the right to appeal to the FAA, and may be entered in any court having jurisdiction over the parties for purposes of enforcement.</p><p>Location of Arbitration - User or Dryft may begin arbitration in either Chicago, Illinois or the federal judicial district that includes the address provided to Dryft in User\'\'s written notification of Pre-Arbitration Claims. If User selects the federal judicial district that includes the address provided to Dryft in the written notification of Pre-Arbitration Claims, Dryft may transfer the arbitration to Chicago, Illinois if Dryft agrees to pay any additional fees or costs User may incur as a result of the transfer, as determined by the arbitrator.</p><p>Dryft will pay all arbitration filing fees and arbitrator\'\'s costs and expenses upon User\'\'s written request given prior to the start of arbitration. User is responsible for any and all other costs incurred by User through the arbitration process, including but not limited to, attorney\'\'s fees and expert witness costs. Fees and costs may be awarded as provided pursuant to applicable law. If User provides notice to Dryft prior to the start of the arbitration process, and attempts to settle all Disputes with Dryft in good faith, the arbitrator may determine that User shall be awarded reasonable attorney\'\'s fees and costs if User is the prevailing party.</p><p>Class Action Waiver</p><p>Except as otherwise provided in these Terms, Disputes may not be consolidated into any form of a class or representative proceeding (ex: class action, consolidated action or private attorney general action) unless both User and Dryft specifically agree to do so. If User exercises their right to Opt-Out (as specified above), this waiver will not apply to User. User or any other user of the Dryft App can be a class representative, class member, or otherwise participate in a class, consolidated, or representative proceeding without having complied with the opt-out requirements above.</p><p>Jury Waiver</p>User understand that entering into this agreement and acceptance of these Terms waives User\'\'s right to a trial by judge or jury in a public court. User understands that absent acceptance of these Terms, User would have these rights, but except as otherwise provided, these rights have been waived.</p><p>Severability</p><p>If any section regarding arbitration or any waiver is found to be illegal or unenforceable, that section will be severed from these Terms, and the remainder of these Terms will be given full force and effect. If the Class Action Waiver clause is found to be illegal or unenforceable, this entire section will be unenforceable, and the Dispute will be decided by a court.</p><p>Continuation</p><p>The sections regarding arbitration and waivers shall survive the termination of your service with Dryft.</p><p>Indemnity</p><p>User agrees to indemnify, save, and hold the Dryft Parties harmless from any claims, losses, damages, liabilities, including legal fees and expenses, arising out of User\'\'s use or misuse of the Dryft App, any violation by User of these Terms, any of Your User Content, or any breach of the representations, warranties, and covenants made by User herein. Dryft reserves the right, at User\'\'s expense, to assume the exclusive defense and control of any matter for which User is required to indemnify Dryft, and User agrees to cooperate with Dryft\'\'s defense of these claims. Dryft will use reasonable efforts to notify User of any such claim, action, or proceeding upon becoming aware of it. User agrees that the provisions in this Indemnity section will survive any termination of User Account (if applicable) or of User access to or use of the Dryft App.</p><p>Entire Agreement</p><p>These Terms constitute the entire and exclusive understanding and agreement between Dryft and User regarding the Dryft App, and these Terms supersede and replace any and all prior oral or written understandings or agreements between Dryft and User regarding the Dryft App.</p>', 'Mobile / Website', 1, 1, '2019-05-24 10:18:41'),
(2, 'Help', 'help', '<h2>Dryft Corp.</h2>\r\n<p>P.O. Box 3461</p>\r\n<p>Barrington, IL 60011</p>\r\n\r\n<p>info@dryftnow.com</p>\r\n\r\n<p>24/7 snow hotline</p>\r\n<p>847-542-SNOW (7669)</p>\r\n', 'Mobile / Website', 1, 1, '2019-08-19 12:11:21');
INSERT INTO `content` (`idcontent`, `title`, `friendlyURL`, `detail`, `contentfor`, `sort`, `isactive`, `createdon`) VALUES
(4, 'Privacy & Policy', 'privacy-policy', '<h2>USER PRIVACY STATEMENT</h2>\r\n<p>Effective Date: August 30, 2018</p>\r\n<p>Dryft collects information about you when you use our mobile applications, websites, and other online products and services (collectively, the \"Services\") and through other interactions and communications you have with us. The Services are provided by Dryft, Inc., an Illinois corporation and its subsidiaries and affiliates (collectively \"Dryft\" or \"we\"), and this Privacy Statement applies to information collected and used by Dryft.</p>\r\n<p>1. Scope and Application</p>\r\n<p>This Privacy Statement (\"Statement\") applies to persons anywhere in the U.S. who use our apps or Services to request on-demand professional services or merchandise (\"Users\") as described in the Terms and Conditions, incorporated herein by reference. This Statement does not apply to information we collect from or about professionals, couriers, partner supplier companies, or any other persons who use the Dryft platform under license (collectively \"Suppliers\"). If you interact with the Services as both a User and a Supplier, the respective privacy statements apply to your different interactions.</p>\r\n<p>2. Collection of Information</p>\r\n<p>Information You Provide to Us.</p>\r\n<p>We collect information you provide directly to us, such as when you create or modify your account, request on-demand services, contact customer support, or otherwise communicate with us. This information may include: name, email, phone number, postal address, profile picture, payment method, items requested (for delivery services), delivery notes, and other information you choose to provide. We make all reasonable efforts to securely collect and store such data.</p>\r\n<p>Information We Collect Through Your Use of Our Services.</p>\r\n<p>When you use our Services, we collect information about you in the following general categories:\r\n	<ul>\r\n		<li>Location Information: When you use the Services, we collect precise location data about the trip from the Dryft app used by the Supplier. If you permit the Dryft app to access location services through the permission system used by your mobile operating system (\"platform\"), we may also collect the precise location of your device when the app is running in the foreground or background. We may also derive your approximate location from your IP address.\r\n		<p>Contacts Information: If you permit the Dryft app to access the address book on your device through the permission system used by your mobile platform, we may access and store names and contact information from your address book to facilitate social interactions through our Services and for other purposes described in this Statement or at the time of consent or collection.</p>\r\n		</li>\r\n		<li>Transaction Information: We collect transaction details related to your use of our Services, including the type of service requested, date and time the service was provided, amount charged, distance traveled, and other related transaction details. Additionally, if someone uses your promo code, we may associate your name with that person.</li>\r\n		<li>Usage and Preference Information: We collect information about how you and site visitors interact with our Services, preferences expressed, and settings chosen. In some cases, we do this through the use of cookies, pixel tags, and similar technologies that create and maintain unique identifiers. To learn more about these technologies, please see our Cookie Statement (see below), incorporated by reference and made a part of this Agreement.</li>\r\n		<li>Device Information: We may collect information about your mobile device, including, for example, the hardware model, operating system and version, software and file names and versions, preferred language, unique device identifier, advertising identifiers, serial number, device motion information, and mobile network information.</li>\r\n		<li>Call and SMS Data: Our Services facilitate communications between Users and Suppliers. In connection with facilitating this service, we receive call data, including the date and time of the call or SMS message, the parties phone numbers, and the content of the SMS message.</li>\r\n		<li>Log Information: When you interact with the Services, we collect server logs, which may include information like device IP address, access dates and times, app features or pages viewed, app crashes and other system activity, type of browser, and the third-party site or service you were using before interacting with our Services.</li>\r\n	</ul>\r\n</p>\r\n<p>Important Information About Platform Permissions.\r\n<p>Most mobile platforms (iOS, Android, etc.) have defined certain types of device data that apps cannot access without your consent. And these platforms have different permission systems for obtaining your consent. The iOS platform will alert you the first time the Dryft app wants permission to access certain types of data and will let you consent (or not consent) to that request. Android devices will notify you of the permissions that the Dryft app seeks before you first use the app, and your use of the app constitutes your consent.</p>\r\n<p>Information We Collect from Other Sources.</p>\r\n<p>We may also receive information from other sources and combine that with information we collect through our Services. For example:\r\n	<ul>\r\n		<li>If you choose to link, create, or log in to your Dryft account with a payment provider (e.g., Google Wallet) or social media service (e.g., Facebook), or if you engage with a separate app or website that uses our API (or whose API we use), we may receive information about you or your connections from that site or app.</li>\r\n		<li>When you request on demand services, our Suppliers may provide us with a User rating after providing services to you.</li>\r\n		<li>If you also interact with our Services in another capacity, for instance as a Supplier or user of other apps we provide, we may combine or associate that information with information we have collected from you in your capacity as a User or rider.</li>\r\n	</ul>\r\n</p>\r\n<p>3. Use of Information</p>\r\n<p>We may use the information we collect about you to:\r\n	<ul>\r\n		<li>Provide, maintain, and improve our services, including, for example, to facilitate payments, send receipts, provide products and services you request (and send related information), develop new features, provide customer support to Users and Suppliers, develop safety features, authenticate users, and send product updates and administrative messages.</li>\r\n		<li>Perform internal operations, including, for example, to prevent fraud and abuse of our Services; to troubleshoot software bugs and operational problems; to conduct data analysis, testing, and research; and to monitor and analyze usage and activity trends.</li>\r\n		<li>Send or facilitate communications.\r\n			<ul>\r\n				<li>(I) between you and a Supplier, such as estimated times of arrival (ETAs), or</li>\r\n				<li>(II) between you and a contact of yours at your direction in connection with your use of certain features, such as referrals, invites, split fare requests, or ETA sharing.</li>\r\n			</ul>\r\n		</li>\r\n		<li>Send you communications we think will be of interest to you, including information about products, services, promotions, news, and events of Dryft and other companies, where permissible and according to local applicable laws; and to process contest, sweepstake, or other promotion entries and fulfill any related awards;</li>\r\n		<li>Personalize and improve the Services, including to provide or recommend features, content, social connections, referrals, and advertisements.</li>\r\n	</ul>\r\n</p>\r\n<p>We may transfer the information described in this Statement to, and process and store it in, the United States and other countries, some of which may have less protective data protection laws than the region in which you reside. Where this is the case, we will take appropriate measures to protect your personal information in accordance with this Statement.</p>\r\n\r\n<p>4. Sharing of Information</p>\r\n<p>We may share the information we collect about you as described in this Statement or as described at the time of collection or sharing, including as follows:</p>\r\n<p>Through Our Services:</p>\r\n<p>We may share your information:\r\n	<ul>\r\n		<li>With Suppliers to enable them to provide the Services you request. For example, we share your name, photo (if you provide one), average User rating given by Suppliers, and pickup and/or drop-off locations with Suppliers.</li>\r\n		<li>With third parties to provide you a service you requested through a partnership or promotional offering made by a third party or us.</li>\r\n		<li>With the general public if you submit content in a public forum, such as blog comments, social media posts, or other features of our Services that are viewable by the general public.</li>\r\n		<li>With third parties with whom you choose to let us share information, for example other apps or websites that integrate with our API or Services, or those with an API or Service with which we integrate; and</li>\r\n	</ul>\r\n</p>\r\n<p>Other Important Sharing:</p>\r\n<p>We may share your information:\r\n	<ul>\r\n		<li>With Dryft subsidiaries and affiliated entities that provide services or conduct data processing on our behalf, or for data centralization and / or logistics purposes.</li>\r\n		<li>With vendors, consultants, marketing partners, and other service providers who need access to such information to carry out work on our behalf.</li>\r\n		<li>In response to a request for information by a competent authority if we believe disclosure is in accordance with, or is otherwise required by, any applicable law, regulation, or legal process.</li>\r\n		<li>With law enforcement officials, government authorities, or other third parties if we believe your actions are inconsistent with our Terms of Service, or policies, or to protect the rights, property, or safety of Dryft or others.</li>\r\n		<li>In connection with, or during negotiations of, any merger, sale of company assets, consolidation or restructuring, financing, or acquisition of all or a portion of our business by or into another company.</li>\r\n		<li>If we otherwise notify you and you consent to the sharing; and</li>\r\n		<li>In an aggregated and/or anonymized form which cannot reasonably be used to identify you.</li>\r\n	</ul>\r\n</p>\r\n<p>5. Social Sharing Features</p>\r\n<p>The Services may integrate with social sharing features and other related tools which let you share actions you take on our Services with other apps, sites, or media, and vice versa. Your use of such features enables the sharing of information with your friends or the public, depending on the settings you establish with the social sharing service. Please refer to the privacy policies of those social sharing services for more information about how they handle the data you provide to or share through them.</p>\r\n<p>6. Analytics and Advertising Services Provided by Others</p>\r\n<p>We may allow others to provide audience measurement and analytics services for us, to serve advertisements on our behalf across the Internet, and to track and report on the performance of those advertisements. These entities may use cookies, web beacons, SDKs, and other technologies to identify your device when you visit our site and use our Services, as well as when you visit other online sites and services. For more information about these technologies and service providers, please refer to our Cookie Statement.</p>\r\n<p>7. Your Choices</p>\r\n<p>Account Information</p>\r\n<p>You may correct your account information at any time by logging into your online or in-app account. If you wish to cancel your account, please email us at info@dryft.com . Please note that in some cases we may retain certain information about you as required by law, or for legitimate business purposes to the extent permitted by law. For instance, if you have a standing credit or debt on your account, or if we believe you have committed fraud or violated our Terms, we may seek to resolve the issue before deleting your information.</p>\r\n\r\n<p>Access Rights</p>\r\n<p>Dryft will comply with individual\'s requests regarding access, correction, and/or deletion of the personal data it stores in accordance with applicable law.</p>\r\n<p>Location Information</p>\r\n<p>We request permission for our app\'s collection of precise location from your device per the permission system used by your mobile operating system. If you initially permit the collection of this information, you can later disable it by changing the location settings on your mobile device. However, this will limit your ability to use certain features of our Services. Additionally, disabling our app\'s collection of precise location from your device will not limit our ability to collect your trip location information from a Supplier\'s device nor our ability to derive approximate location from your IP address.</p>\r\n<p>Contact Information</p>\r\n<p>We may also seek permission for our app\'s collection and syncing of contact information from your device per the permission system used by your mobile operating system. If you initially permit the collection of this information, iOS users can later disable it by changing the contacts settings on your mobile device. The Android platform does not provide such a setting.</p>\r\n<p>Promotional Communications</p>\r\n<p>You may opt out of receiving promotional messages from us by following the instructions in those messages. If you opt out, we may still send you non-promotional communications, such as those about your account, about Services you have requested, or our ongoing business relations.</p>\r\n<p>Cookies and Advertising</p>\r\n<p>Please refer to our Cookie Statement (see below) for more information about your choices around cookies and related technologies</p>\r\n\r\n<p>8. Changes to the Statement</p>\r\n<p>We may change this Statement from time to time. If we make significant changes in the way we treat your personal information, or to the Statement, we will provide you notice through the Services or by some other means, such as email. Your continued use of the Services after such notice constitutes your consent to the changes. We encourage you to periodically review the Statement for the latest information on our privacy practices.</p>\r\n<p>9. Contact Us</p>\r\n<p>If you have any questions about this Privacy Statement, please contact us at info@dryftnow.com. </p>\r\n<p>SUPPLIER PRIVACY STATEMENT</p>\r\n<p>Effective Date: August 30, 2018</p>\r\n<p>Dryft collects information about you when you use our mobile applications, websites, and other online products and services (collectively, the \"Services\") and through other interactions and communications you have with us. For persons residing in the United States, the Services are provided by Dryft Corp. and its subsidiaries and affiliates (hereinafter \"Dryft\" or \"we\").</p>\r\n\r\n<p>1. Scope and Application</p>\r\n<p>This U.S. Supplier Privacy Statement (\"Statement\") applies to professionals, couriers, partner supplier companies, and any other persons that use the Dryft platform under license (collectively \"Suppliers,\" or individually \"you\") and reside in the United States. This Statement does not apply to Suppliers who reside outside the United States. The Statement also does not apply to persons (e.g. riders) who use our app or Services to request on-demand professional services or merchandise (\"Users\"). If you interact with the Services as both a User and a Supplier, the respective privacy statements apply to your different interactions.</p>\r\n\r\n<p>2. Collection of Information</p>\r\n<p>Dryft may collect personal information from or about you when you create an account for, and use, the Dryft Services, including location data, which information may be stored, processed, and/or accessed by Dryft, as well as its service providers, for business purposes, including for marketing, lead generation, service development and improvement, analytics, industry and market research, and such other purposes consistent with Dryft\'s and its Affiliates\' legitimate business needs. By submitting information to Dryft during the account creation process and/or by using the Services, you consent to such collection and use of personal data. Please keep in mind, however, that whenever you give out personal data online there is a risk that third parties may intercept and use that information. While Company strives to protect your Personal Information and privacy, we cannot guarantee the security of any information you disclose online. By using this Site, the App, or Services, you expressly acknowledge and agree that we do not guarantee the security of any data provided to or received by us and that any personal data or information received from you through the Site, App, or Services is provided to us at your own risk, which you expressly assume.</p>\r\n<p>3. Disclosure of Supplier Information to Third Parties</p>\r\n<p>Subject to all applicable laws, Dryft may provide to a third party any information (including personal data and any Dryft Data) about Suppliers provided hereunder if:\r\n	<ul>\r\n		<li>(a) There is a complaint, dispute or conflict, including an accident, relating to a Supplier.</li>\r\n		<li>(b) It is necessary to enforce the terms of the driver agreement.</li>\r\n		<li>(c) It is required, in Dryft\'s sole discretion, by applicable law, regulation, ordinance, license, or operating agreement.</li>\r\n		<li>(d) It is necessary, in Dryft\'s sole discretion, to protect the safety, rights, property, or security of Dryft, the Dryft Services, or any third party; to detect, prevent or otherwise address fraud, security or technical issues; and/or to prevent or stop activity which Dryft, in its sole discretion, considers to be, or to pose a risk of being, illegal, unethical, or legally actionable.</li>\r\n	</ul>\r\n</p>\r\n<p>4. Location-Based Services Consent</p>\r\n<p>You hereby expressly consent to Dryft\'s use of location-based services and expressly waive and release Dryft from any and all liability, claims, causes of action or damages arising from your use of the Dryft Services, or in any way relating to the use of the precise location and other location-based services.</p>\r\n<p>COOKIE STATEMENT</p>\r\n<p>Effective Date: August 30, 2018</p>\r\n<p>We and our affiliates, third parties, and other partners use cookies and other identification technologies on our websites, mobile applications, electronic communications, advertisements, and other online services (collectively, the \"Services\") for a number of purposes, including: authenticating users, remembering user preferences and settings, determining the popularity of content, delivering and measuring the effectiveness of advertising campaigns, analyzing site traffic and trends, and generally understanding the online behaviors and interests of people who interact with our Services. You can read more here about the types of cookies we use, why we use them, and how you can exercise your choices.</p>\r\n\r\n<p>1. Cookies Overview</p>\r\n<p>Cookies are small text files that are stored on your browser or device by websites, apps, online media, and advertisements. There are different types of cookies. Cookies served by the entity that operates the domain you are visiting are called \"first party cookies.\" So cookies served by Dryft while you are on www.dryftnow.com are first party cookies. Cookies served by companies that are not operating the domain you are visiting are called \"third party cookies.\" So, we may allow Google to set a cookie on your browser while you visit www.dryftnow.com, and that would be a third party cookie. Cookies may also endure for different periods of time. \"Session Cookies\" only last only if your browser is open. These are deleted automatically once you close your browser. Other cookies are \"persistent cookies\" meaning that they survive after your browser is closed. For example, they may recognize your device when you re-open your browser and browse the internet again.</p>\r\n\r\n<p>2. Other Identification Technologies</p>\r\n<p>We call this a Cookie Statement, but the statement addresses cookies and other identification technologies we may use or permit on our Services. \"Pixel tags\" (also called beacons or pixels) are small blocks of code installed on (or called by) a webpage, app, or advertisement which can retrieve certain information about your device and browser, including for example: device type, operating system, browser type and version, website visited, time of visit, referring website, IP address, and other similar information, including the small text file (the cookie) that uniquely identifies the device. Pixels provide the means by which third parties can set and read browser cookies from a domain that they do not themselves operate and collect information about visitors to that domain, typically with the permission of the domain owner. \"Local storage\" refers generally to other places on a browser or device where information can be stored by websites, ads, or third parties (such as HTML5 local storage and browser cache). \"Software Development Kits\" (also called SDKs) function like pixels and cookies but operate in the mobile app context where pixels and cookies cannot always function. The primary app developer can install pieces of code (the SDK) from partners in the app, and thereby allow the partner to collect certain information about user interaction with the app and information about the user device and network information.</p>\r\n<p>3. Your Choices</p>\r\n<p>You have the right to choose whether or not to accept cookies. However, they are an important part of how our Services work, so you should be aware that if you choose to refuse or remove cookies, this could affect the availability and functionality of the Services.</p>\r\n<p>Most web browsers are set to accept cookies by default. If you prefer, you can usually choose to set your browser to remove or reject browser cookies. To do so, please follow the instructions provided by your browser which are usually located within the \"Help\" or \"Preferences\" menu. Some third parties also provide the ability to refuse their cookies directly by clicking on an opt-out link, and we have indicated where this is possible in the table above.</p>\r\n<p>Removing or rejecting browser cookies does not necessarily affect third-party flash cookies which may be used by us or our partners in connection with our Services. To delete or disable flash cookies please visit this site for more information. For further information about cookies, including how to see what cookies have been set on your device and how to manage and delete them, you can visit www.allaboutcookies.org.</p>', 'Mobile / Website', 1, 1, '2019-08-19 12:11:21');

-- --------------------------------------------------------

--
-- Table structure for table `driver_rating`
--

CREATE TABLE `driver_rating` (
  `idrating` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `iddriver` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `review` text COLLATE utf8_unicode_ci,
  `idbooking` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `driver_rating`
--

INSERT INTO `driver_rating` (`idrating`, `iduser`, `iddriver`, `rating`, `review`, `idbooking`) VALUES
(1, 5, 1, 5, 'very good', 18),
(2, 5, 1, 5, 'Good worked on', 4),
(3, 3, 1, 4, 'very good', 86),
(4, 2, 1, 5, 'Good work', 87),
(5, 2, 1, 5, 'let , l', 88),
(6, 3, 1, 0, 'very good', 85),
(7, 3, 1, 5, 'very good', 124),
(8, 2, 1, 5, 'fffff', 126);

-- --------------------------------------------------------

--
-- Table structure for table `drivewayprice`
--

CREATE TABLE `drivewayprice` (
  `iddrivewayprice` int(11) NOT NULL,
  `iddrivewaytype` int(11) NOT NULL,
  `areafrom` float NOT NULL,
  `areato` float NOT NULL,
  `price` float NOT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `drivewayprice`
--

INSERT INTO `drivewayprice` (`iddrivewayprice`, `iddrivewaytype`, `areafrom`, `areato`, `price`, `isactive`, `createdon`) VALUES
(1, 3, 1, 1500, 18, 1, '2018-07-04 17:35:33'),
(2, 3, 1501, 5000, 23, 1, '2018-07-04 17:36:48'),
(3, 3, 5001, 10000, 32, 1, '2018-07-04 17:39:00'),
(4, 3, 10001, 20000, 60, 1, '2018-07-04 17:39:00'),
(5, 3, 20001, 50000, 90, 1, '2018-07-04 17:39:00'),
(6, 4, 1, 1500, 18, 1, '2018-07-04 17:35:33'),
(7, 4, 1501, 5000, 23, 1, '2018-07-04 17:36:48'),
(8, 4, 5001, 10000, 32, 1, '2018-07-04 17:39:00'),
(9, 4, 10001, 20000, 60, 1, '2018-07-04 17:39:00'),
(10, 4, 20001, 50000, 90, 1, '2018-07-04 17:39:00'),
(11, 5, 1, 1500, 18, 1, '2018-07-04 17:35:33'),
(12, 5, 1501, 5000, 23, 1, '2018-07-04 17:36:48'),
(13, 5, 5001, 10000, 32, 1, '2018-07-04 17:39:00'),
(14, 5, 10001, 20000, 60, 1, '2018-07-04 17:39:00'),
(15, 5, 20001, 50000, 90, 1, '2018-07-04 17:39:00'),
(16, 6, 1, 1500, 18, 1, '2018-07-04 17:35:33'),
(17, 6, 1501, 5000, 23, 1, '2018-07-04 17:36:48'),
(18, 6, 5001, 10000, 32, 1, '2018-07-04 17:39:00'),
(19, 6, 10001, 20000, 60, 1, '2018-07-04 17:39:00'),
(20, 6, 20001, 50000, 90, 1, '2018-07-04 17:39:00');

-- --------------------------------------------------------

--
-- Table structure for table `drivewaytype`
--

CREATE TABLE `drivewaytype` (
  `iddrivewaytype` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `drivewaytype`
--

INSERT INTO `drivewaytype` (`iddrivewaytype`, `name`, `isactive`, `createdon`) VALUES
(1, 'Asphalt driveway', 1, '2018-07-04 18:48:31'),
(2, 'Gravel driveway', 1, '2018-07-04 18:49:54'),
(3, 'Cobblestone driveway', 1, '2018-07-04 18:49:55'),
(4, 'Concrete driveway', 1, '2018-07-04 18:49:55'),
(5, 'Brick driveway', 1, '2018-07-04 18:49:55'),
(6, 'Other stone', 1, '2018-07-04 18:49:55');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `idfeatures` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `iscall` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `isdefault` int(1) NOT NULL DEFAULT '1',
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`idfeatures`, `name`, `iscall`, `isdefault`, `isactive`, `createdon`) VALUES
(1, 'Steps in Back', '', 1, 1, '2018-07-04 17:02:59'),
(2, 'Deck', 'Call Now', 1, 1, '2018-07-04 17:02:59'),
(3, 'Clear front walk way', '', 0, 1, '2018-07-04 17:02:59'),
(4, 'Clear back walk way', '', 0, 1, '2018-07-04 17:02:59'),
(5, 'Clear side walk way', '', 0, 1, '2018-07-04 17:02:59'),
(7, 'Clear front of garage doors', '', 0, 1, '2018-07-04 17:06:42'),
(8, 'Salt driveway', '', 0, 1, '2018-07-04 17:07:06'),
(9, 'Salt walk ways', '', 1, 1, '2018-07-04 17:07:26');

-- --------------------------------------------------------

--
-- Table structure for table `featuresareaprice`
--

CREATE TABLE `featuresareaprice` (
  `idfeaturesareaprice` int(11) NOT NULL,
  `idfeatures` int(11) NOT NULL,
  `areafrom` float NOT NULL COMMENT 'If area size is greater then 50000 then call now',
  `areato` float NOT NULL,
  `price` float NOT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `featuresareaprice`
--

INSERT INTO `featuresareaprice` (`idfeaturesareaprice`, `idfeatures`, `areafrom`, `areato`, `price`, `isactive`, `createdon`) VALUES
(1, 8, 1, 1500, 18, 1, '2018-07-04 17:35:33'),
(2, 8, 1501, 5000, 23, 1, '2018-07-04 17:36:48'),
(3, 8, 5001, 10000, 32, 1, '2018-07-04 17:39:00'),
(4, 8, 10001, 20000, 60, 1, '2018-07-04 17:39:00'),
(5, 8, 20001, 50000, 90, 1, '2018-07-04 17:39:00'),
(6, 8, 50001, 100000, 0, 1, '2018-07-04 17:39:00');

-- --------------------------------------------------------

--
-- Table structure for table `featuresdepthprice`
--

CREATE TABLE `featuresdepthprice` (
  `idfeaturesdepthprice` int(11) NOT NULL,
  `idfeatures` int(11) NOT NULL,
  `snowdepthfrom` float NOT NULL COMMENT 'If depth is greater then 8 then call now',
  `snowdepthto` float NOT NULL,
  `price` float NOT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `featuresdepthprice`
--

INSERT INTO `featuresdepthprice` (`idfeaturesdepthprice`, `idfeatures`, `snowdepthfrom`, `snowdepthto`, `price`, `isactive`, `createdon`) VALUES
(1, 1, 0, 2, 10, 1, '2018-07-04 17:44:25'),
(2, 1, 2.1, 4, 15, 1, '2018-07-04 17:48:14'),
(3, 1, 4.1, 6, 20, 1, '2018-07-04 17:48:14'),
(4, 1, 6.1, 8, 25, 1, '2018-07-04 17:48:14'),
(5, 1, 8.1, 12, 30, 1, '2018-07-04 17:48:14');

-- --------------------------------------------------------

--
-- Table structure for table `featuresextracharge`
--

CREATE TABLE `featuresextracharge` (
  `idextracharge1` int(11) NOT NULL,
  `idextracharge` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `amount` float NOT NULL,
  `amounttype` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` int(1) NOT NULL,
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `featuresextracharge`
--

INSERT INTO `featuresextracharge` (`idextracharge1`, `idextracharge`, `name`, `type`, `amount`, `amounttype`, `isactive`, `createdon`) VALUES
(1, 1, 'Calcium chloride', 'TOTAL AMOUNT', 25, 'Percent', 1, '2018-07-04 18:07:08');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `idnotification` int(11) NOT NULL,
  `idsender` int(11) NOT NULL,
  `idreceiver` int(11) NOT NULL,
  `idsource` int(11) NOT NULL,
  `notificationtype` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '1 = Friend Request, 2 = New Booking, 3 = Booking Completed, 4 = Cancelled By User, 5 = Request Accepted, 6= OFFERS',
  `notificatiomode` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '1 = TEXT, 2 = IMAGE',
  `sort` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `isviewed` int(1) NOT NULL DEFAULT '0',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`idnotification`, `idsender`, `idreceiver`, `idsource`, `notificationtype`, `notificatiomode`, `sort`, `description`, `isactive`, `isviewed`, `createdon`) VALUES
(1, 32, 64, 1, 'New Booking', 'TEXT', 2, 'New Booking fro customer booking No. - 20180716343456', 1, 0, '2018-07-16 20:46:24'),
(2, 0, 64, 1, 'OFFERS', 'IMAGE', 2, 'Big Sale 30% Discount All Service', 1, 0, '2018-07-16 20:46:24'),
(3, 32, 32, 1, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 9175 for booking no. : 201807090945503453', 1, 0, '2018-07-22 02:09:22'),
(4, 32, 32, 1, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 6064 for booking no. : 201807090945503453', 1, 0, '2018-07-22 02:10:35'),
(5, 32, 32, 1, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 4981 for booking no. : 201807090945503453', 1, 0, '2018-07-22 02:10:56'),
(6, 32, 32, 1, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 4738 for booking no. : 201807090945503453', 1, 0, '2018-07-22 02:16:19'),
(7, 32, 32, 1, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 9586 for booking no. : 201807090945503453', 1, 0, '2018-07-22 02:19:25'),
(8, 32, 32, 1, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 3203 for booking no. : 201807090945503453', 1, 0, '2018-07-22 02:23:40'),
(9, 32, 32, 1, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 8805 for booking no. : 201807090945503453', 1, 0, '2018-07-22 02:24:59'),
(10, 32, 32, 1, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 4089 for booking no. : 201807090945503453', 1, 0, '2018-07-22 02:25:50'),
(11, 32, 32, 1, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 2323 for booking no. : 201807090945503453', 1, 0, '2018-07-22 02:33:37'),
(12, 32, 32, 1, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 6093 for booking no. : 201807090945503453', 1, 0, '2018-07-22 02:35:13'),
(13, 32, 32, 2, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 5216 for booking no. : 201807090945503453', 1, 0, '2018-07-22 02:56:02'),
(14, 5, 1, 21, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190809105929610', 1, 0, '2019-08-09 12:01:40'),
(15, 5, 1, 21, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190809105929610', 1, 0, '2019-08-09 12:03:00'),
(16, 5, 1, 21, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190809105929610', 1, 0, '2019-08-09 12:04:39'),
(17, 5, 0, 19, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190809105451730', 1, 0, '2019-08-09 12:09:29'),
(18, 5, 0, 20, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190809105557080', 1, 0, '2019-08-09 12:09:29'),
(19, 5, 0, 18, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190809092615210', 1, 0, '2019-08-09 12:09:29'),
(20, 2, 0, 8, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190809063739120', 1, 0, '2019-08-09 12:09:29'),
(21, 5, 0, 13, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190809072946690', 1, 0, '2019-08-09 12:09:29'),
(22, 5, 0, 12, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190809072821070', 1, 0, '2019-08-09 12:09:29'),
(23, 5, 0, 14, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190809073145290', 1, 0, '2019-08-09 12:09:29'),
(24, 5, 0, 10, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190809065735050', 1, 0, '2019-08-09 12:09:29'),
(25, 5, 0, 22, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190809110657390', 1, 0, '2019-08-09 12:09:30'),
(26, 5, 0, 15, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190809074056600', 1, 0, '2019-08-09 12:09:30'),
(27, 5, 0, 9, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190809064652120', 1, 0, '2019-08-09 12:09:30'),
(28, 5, 0, 11, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190809072710260', 1, 0, '2019-08-09 12:09:30'),
(29, 5, 0, 17, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190809083716430', 1, 0, '2019-08-09 12:09:30'),
(30, 2, 0, 24, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190809111822900', 1, 0, '2019-08-09 12:18:37'),
(31, 5, 1, 29, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190809112128700', 1, 0, '2019-08-09 12:22:01'),
(32, 5, 1, 29, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190809112128700', 1, 0, '2019-08-09 12:22:39'),
(33, 5, 1, 30, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190809112303440', 1, 0, '2019-08-09 12:23:15'),
(34, 5, 1, 1, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190809112814680', 1, 0, '2019-08-09 12:29:01'),
(35, 5, 1, 1, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190809112814680', 1, 0, '2019-08-09 12:29:19'),
(36, 1, 5, 1, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-09 12:29:44'),
(37, 1, 5, 1, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190809112814680', 1, 0, '2019-08-09 12:30:14'),
(38, 1, 5, 1, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190809112814680', 1, 0, '2019-08-09 12:30:46'),
(39, 5, 5, 1, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 1312 for booking no. : 20190809112814680', 1, 0, '2019-08-09 12:33:19'),
(40, 1, 5, 1, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190809112814680', 1, 0, '2019-08-09 12:33:42'),
(41, 5, 1, 2, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190812045630540', 1, 0, '2019-08-12 05:57:01'),
(42, 5, 1, 3, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190812050052200', 1, 0, '2019-08-12 06:03:02'),
(43, 5, 1, 3, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190812050052200', 1, 0, '2019-08-12 06:03:14'),
(44, 1, 5, 3, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-12 06:07:59'),
(45, 1, 5, 3, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190812050052200', 1, 0, '2019-08-12 06:11:51'),
(46, 1, 5, 3, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190812050052200', 1, 0, '2019-08-12 06:12:55'),
(47, 5, 5, 3, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 7507 for booking no. : 20190812050052200', 1, 0, '2019-08-12 06:13:54'),
(48, 1, 5, 3, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190812050052200', 1, 0, '2019-08-12 06:14:44'),
(49, 2, 1, 4, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190812052423260', 1, 0, '2019-08-12 06:25:02'),
(50, 2, 1, 4, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190812052423260', 1, 0, '2019-08-12 06:25:17'),
(51, 1, 2, 4, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-12 06:26:05'),
(52, 1, 2, 4, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190812052423260', 1, 0, '2019-08-12 06:27:15'),
(53, 1, 2, 4, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190812052423260', 1, 0, '2019-08-12 06:28:06'),
(54, 2, 2, 4, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 5980 for booking no. : 20190812052423260', 1, 0, '2019-08-12 06:29:12'),
(55, 1, 2, 4, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190812052423260', 1, 0, '2019-08-12 06:29:56'),
(56, 5, 1, 5, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190812111953260', 1, 0, '2019-08-12 12:20:02'),
(57, 5, 1, 5, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190812111953260', 1, 0, '2019-08-12 12:20:19'),
(58, 1, 5, 5, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-12 12:20:51'),
(59, 1, 5, 5, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190812111953260', 1, 0, '2019-08-12 12:22:09'),
(60, 1, 5, 5, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190812111953260', 1, 0, '2019-08-12 12:23:04'),
(61, 5, 5, 5, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 3072 for booking no. : 20190812111953260', 1, 0, '2019-08-12 12:23:57'),
(62, 1, 5, 5, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190812111953260', 1, 0, '2019-08-12 12:24:22'),
(63, 5, 1, 6, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190812112637820', 1, 0, '2019-08-12 12:27:02'),
(64, 5, 1, 6, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190812112637820', 1, 0, '2019-08-12 12:27:14'),
(65, 5, 1, 6, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190812112637820', 1, 0, '2019-08-12 12:27:24'),
(66, 1, 5, 6, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-12 12:27:52'),
(67, 1, 5, 6, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190812112637820', 1, 0, '2019-08-12 12:29:07'),
(68, 1, 5, 6, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190812112637820', 1, 0, '2019-08-12 12:30:05'),
(69, 5, 5, 6, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 2595 for booking no. : 20190812112637820', 1, 0, '2019-08-12 12:31:11'),
(70, 1, 5, 6, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190812112637820', 1, 0, '2019-08-12 12:33:32'),
(71, 5, 1, 7, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190813035927530', 1, 0, '2019-08-13 05:00:02'),
(72, 5, 1, 7, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190813035927530', 1, 0, '2019-08-13 05:13:31'),
(73, 1, 5, 7, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-13 05:48:12'),
(74, 1, 5, 7, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190813035927530', 1, 0, '2019-08-13 05:55:10'),
(75, 1, 5, 7, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190813035927530', 1, 0, '2019-08-13 06:04:01'),
(76, 5, 5, 7, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 8171 for booking no. : 20190813035927530', 1, 0, '2019-08-13 06:16:51'),
(77, 1, 5, 7, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190813035927530', 1, 0, '2019-08-13 06:17:22'),
(78, 5, 1, 8, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190813052236870', 1, 0, '2019-08-13 06:23:01'),
(79, 5, 1, 8, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190813052236870', 1, 0, '2019-08-13 06:23:18'),
(80, 1, 5, 8, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-13 06:24:16'),
(81, 1, 5, 8, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190813052236870', 1, 0, '2019-08-13 06:26:11'),
(82, 1, 5, 8, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190813052236870', 1, 0, '2019-08-13 06:53:39'),
(83, 5, 5, 8, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 2430 for booking no. : 20190813052236870', 1, 0, '2019-08-13 06:54:31'),
(84, 1, 5, 8, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190813052236870', 1, 0, '2019-08-13 06:55:23'),
(85, 5, 1, 9, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190813060209520', 1, 0, '2019-08-13 07:03:01'),
(86, 5, 1, 9, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190813060209520', 1, 0, '2019-08-13 07:03:26'),
(87, 1, 5, 9, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-13 07:05:34'),
(88, 1, 5, 9, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190813060209520', 1, 0, '2019-08-13 07:07:15'),
(89, 1, 5, 9, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190813060209520', 1, 0, '2019-08-13 07:08:22'),
(90, 5, 5, 9, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 6888 for booking no. : 20190813060209520', 1, 0, '2019-08-13 07:09:19'),
(91, 1, 5, 9, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190813060209520', 1, 0, '2019-08-13 07:10:14'),
(92, 5, 1, 10, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190813061619990', 1, 0, '2019-08-13 07:17:01'),
(93, 5, 1, 10, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190813061619990', 1, 0, '2019-08-13 07:17:20'),
(94, 1, 5, 10, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-13 07:18:09'),
(95, 1, 5, 10, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190813061619990', 1, 0, '2019-08-13 07:19:20'),
(96, 1, 5, 10, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190813061619990', 1, 0, '2019-08-13 07:20:44'),
(97, 5, 5, 10, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 4112 for booking no. : 20190813061619990', 1, 0, '2019-08-13 07:21:24'),
(98, 1, 5, 10, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190813061619990', 1, 0, '2019-08-13 07:22:52'),
(99, 5, 1, 11, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190813063303920', 1, 0, '2019-08-13 07:34:02'),
(100, 5, 1, 11, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190813063303920', 1, 0, '2019-08-13 07:34:18'),
(101, 1, 5, 11, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-13 07:34:57'),
(102, 1, 5, 11, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190813063303920', 1, 0, '2019-08-13 07:36:15'),
(103, 1, 5, 11, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190813063303920', 1, 0, '2019-08-13 07:37:06'),
(104, 5, 5, 11, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 5350 for booking no. : 20190813063303920', 1, 0, '2019-08-13 07:37:38'),
(105, 1, 5, 11, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190813063303920', 1, 0, '2019-08-13 07:38:20'),
(106, 5, 1, 12, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190813065431210', 1, 0, '2019-08-13 07:55:01'),
(107, 5, 1, 12, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190813065431210', 1, 0, '2019-08-13 07:55:29'),
(108, 1, 5, 12, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-13 08:02:34'),
(109, 1, 5, 12, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190813065431210', 1, 0, '2019-08-13 08:12:34'),
(110, 1, 5, 12, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190813065431210', 1, 0, '2019-08-13 08:40:02'),
(111, 5, 5, 12, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 8939 for booking no. : 20190813065431210', 1, 0, '2019-08-13 08:57:50'),
(112, 1, 5, 12, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190813065431210', 1, 0, '2019-08-13 08:58:37'),
(113, 5, 1, 13, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190813080159280', 1, 0, '2019-08-13 09:03:01'),
(114, 5, 1, 13, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190813080159280', 1, 0, '2019-08-13 09:03:30'),
(115, 1, 5, 13, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-13 09:04:29'),
(116, 2, 1, 15, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190813081420440', 1, 0, '2019-08-13 09:15:02'),
(117, 2, 1, 16, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190813081436020', 1, 0, '2019-08-13 09:15:02'),
(118, 2, 1, 14, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190813081408370', 1, 0, '2019-08-13 09:15:03'),
(119, 2, 1, 17, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190813081450120', 1, 0, '2019-08-13 09:15:03'),
(120, 2, 1, 18, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190813081509530', 1, 0, '2019-08-13 09:16:02'),
(121, 1, 5, 13, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190813080159280', 1, 0, '2019-08-13 09:18:45'),
(122, 1, 5, 13, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190813080159280', 1, 0, '2019-08-13 09:19:52'),
(123, 5, 5, 13, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 6192 for booking no. : 20190813080159280', 1, 0, '2019-08-13 09:20:28'),
(124, 1, 5, 13, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190813080159280', 1, 0, '2019-08-13 09:21:01'),
(125, 5, 1, 19, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190813093323680', 1, 0, '2019-08-13 10:34:02'),
(126, 5, 1, 19, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190813093323680', 1, 0, '2019-08-13 10:34:18'),
(127, 1, 5, 19, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-13 10:35:23'),
(128, 1, 5, 19, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190813093323680', 1, 0, '2019-08-13 10:36:39'),
(129, 1, 5, 19, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190813093323680', 1, 0, '2019-08-13 10:37:22'),
(130, 5, 5, 19, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 3297 for booking no. : 20190813093323680', 1, 0, '2019-08-13 10:37:53'),
(131, 1, 5, 19, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190813093323680', 1, 0, '2019-08-13 10:38:11'),
(132, 5, 1, 20, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190813094015960', 1, 0, '2019-08-13 10:41:02'),
(133, 5, 1, 20, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190813094015960', 1, 0, '2019-08-13 10:41:29'),
(134, 1, 5, 20, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-13 10:41:46'),
(135, 1, 5, 20, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190813094015960', 1, 0, '2019-08-13 10:52:39'),
(136, 1, 5, 20, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190813094015960', 1, 0, '2019-08-13 10:53:52'),
(137, 5, 5, 20, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 1043 for booking no. : 20190813094015960', 1, 0, '2019-08-13 10:54:48'),
(138, 1, 5, 20, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190813094015960', 1, 0, '2019-08-13 10:55:39'),
(139, 5, 1, 21, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190813141544810', 1, 0, '2019-08-13 15:16:01'),
(140, 5, 1, 21, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190813141544810', 1, 0, '2019-08-13 15:16:15'),
(141, 1, 5, 21, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-13 15:16:35'),
(142, 1, 5, 21, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190813141544810', 1, 0, '2019-08-13 15:17:28'),
(143, 1, 5, 21, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190813141544810', 1, 0, '2019-08-13 15:17:37'),
(144, 5, 5, 21, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 8227 for booking no. : 20190813141544810', 1, 0, '2019-08-13 15:18:35'),
(145, 1, 5, 21, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190813141544810', 1, 0, '2019-08-13 15:18:55'),
(146, 5, 1, 22, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190813142626070', 1, 0, '2019-08-13 15:27:01'),
(147, 5, 1, 22, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190813142626070', 1, 0, '2019-08-13 15:27:09'),
(148, 5, 1, 23, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190813142730360', 1, 0, '2019-08-13 15:28:01'),
(149, 5, 1, 23, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190813142730360', 1, 0, '2019-08-13 15:28:06'),
(150, 1, 5, 22, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-13 15:30:00'),
(151, 1, 5, 22, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190813142626070', 1, 0, '2019-08-13 15:32:17'),
(152, 1, 5, 22, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190813142626070', 1, 0, '2019-08-13 15:32:57'),
(153, 5, 5, 22, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 2945 for booking no. : 20190813142626070', 1, 0, '2019-08-13 15:33:31'),
(154, 1, 5, 22, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190813142626070', 1, 0, '2019-08-13 15:33:47'),
(155, 5, 1, 24, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190814031311170', 1, 0, '2019-08-14 04:14:02'),
(156, 5, 1, 24, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190814031311170', 1, 0, '2019-08-14 04:20:32'),
(157, 5, 1, 24, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190814031311170', 1, 0, '2019-08-14 04:21:40'),
(158, 2, 1, 14, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190813081408370', 1, 0, '2019-08-14 04:22:30'),
(159, 5, 1, 1, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190814055313610', 1, 0, '2019-08-14 06:54:02'),
(160, 5, 1, 1, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190814055313610', 1, 0, '2019-08-14 07:07:48'),
(161, 5, 1, 2, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190814061154460', 1, 0, '2019-08-14 07:12:02'),
(162, 5, 1, 2, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190814061154460', 1, 0, '2019-08-14 07:16:22'),
(163, 1, 5, 2, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-14 07:22:44'),
(164, 5, 1, 3, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190814063011770', 1, 0, '2019-08-14 07:31:02'),
(165, 5, 1, 3, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190814063011770', 1, 0, '2019-08-14 07:31:17'),
(166, 1, 5, 3, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-14 07:44:42'),
(167, 1, 5, 3, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190814063011770', 1, 0, '2019-08-14 07:52:40'),
(168, 1, 5, 1, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-16 04:28:59'),
(169, 5, 1, 5, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816033840700', 1, 0, '2019-08-16 04:40:02'),
(170, 5, 1, 4, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816032718940', 1, 0, '2019-08-16 04:40:03'),
(171, 5, 1, 4, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816032718940', 1, 0, '2019-08-16 04:40:44'),
(172, 5, 1, 4, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816032718940', 1, 0, '2019-08-16 04:47:33'),
(173, 5, 1, 4, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816032718940', 1, 0, '2019-08-16 04:52:46'),
(174, 2, 1, 6, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816035551350', 1, 0, '2019-08-16 04:57:01'),
(175, 2, 1, 6, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816035551350', 1, 0, '2019-08-16 04:57:21'),
(176, 5, 1, 4, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816032718940', 1, 0, '2019-08-16 04:57:55'),
(177, 5, 1, 4, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816032718940', 1, 0, '2019-08-16 04:58:38'),
(178, 5, 1, 4, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816032718940', 1, 0, '2019-08-16 05:26:34'),
(179, 5, 1, 4, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816032718940', 1, 0, '2019-08-16 05:26:59'),
(180, 2, 1, 7, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816042819510', 1, 0, '2019-08-16 05:29:02'),
(181, 2, 1, 7, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816042819510', 1, 0, '2019-08-16 05:29:29'),
(182, 1, 5, 4, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-16 05:30:20'),
(183, 1, 5, 4, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190816032718940', 1, 0, '2019-08-16 05:31:15'),
(184, 1, 2, 6, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-16 05:31:38'),
(185, 1, 2, 6, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190816035551350', 1, 0, '2019-08-16 05:33:00'),
(186, 1, 2, 6, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190816035551350', 1, 0, '2019-08-16 05:33:46'),
(187, 2, 2, 6, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 9851 for booking no. : 20190816035551350', 1, 0, '2019-08-16 05:34:16'),
(188, 1, 2, 6, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190816035551350', 1, 0, '2019-08-16 05:34:41'),
(189, 1, 2, 7, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-16 05:37:28'),
(190, 1, 2, 7, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190816042819510', 1, 0, '2019-08-16 05:37:41'),
(191, 1, 2, 7, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190816042819510', 1, 0, '2019-08-16 05:37:51'),
(192, 2, 2, 7, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 5300 for booking no. : 20190816042819510', 1, 0, '2019-08-16 05:39:04'),
(193, 1, 2, 7, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190816042819510', 1, 0, '2019-08-16 05:39:20'),
(194, 2, 1, 8, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816053311120', 1, 0, '2019-08-16 06:34:01'),
(195, 2, 1, 8, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816053311120', 1, 0, '2019-08-16 06:34:10'),
(196, 1, 2, 8, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-16 06:34:40'),
(197, 1, 2, 8, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190816053311120', 1, 0, '2019-08-16 06:34:58'),
(198, 1, 2, 8, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190816053311120', 1, 0, '2019-08-16 06:35:06'),
(199, 2, 2, 8, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 2754 for booking no. : 20190816053311120', 1, 0, '2019-08-16 06:35:48'),
(200, 1, 2, 8, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190816053311120', 1, 0, '2019-08-16 06:36:08'),
(201, 2, 1, 9, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816053809890', 1, 0, '2019-08-16 06:39:02'),
(202, 2, 1, 9, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816053809890', 1, 0, '2019-08-16 06:39:11'),
(203, 5, 1, 5, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816033840700', 1, 0, '2019-08-16 06:39:54'),
(204, 2, 1, 10, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816054249910', 1, 0, '2019-08-16 06:43:02'),
(205, 1, 2, 9, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-16 06:45:20'),
(206, 1, 2, 9, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190816053809890', 1, 0, '2019-08-16 06:46:50'),
(207, 1, 2, 9, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190816053809890', 1, 0, '2019-08-16 06:47:13'),
(208, 2, 2, 9, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 3763 for booking no. : 20190816053809890', 1, 0, '2019-08-16 06:47:54'),
(209, 1, 2, 9, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190816053809890', 1, 0, '2019-08-16 06:48:04'),
(210, 2, 1, 10, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816054249910', 1, 0, '2019-08-16 06:53:50'),
(211, 2, 1, 11, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816055322800', 1, 0, '2019-08-16 06:54:01'),
(212, 2, 1, 11, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816055322800', 1, 0, '2019-08-16 06:54:13'),
(213, 5, 1, 12, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816062329160', 1, 0, '2019-08-16 07:24:01'),
(214, 5, 1, 18, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816062618890', 1, 0, '2019-08-16 07:28:01'),
(215, 5, 1, 18, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816062618890', 1, 0, '2019-08-16 07:28:17'),
(216, 1, 5, 18, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-16 07:30:15'),
(217, 1, 5, 18, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190816062618890', 1, 0, '2019-08-16 07:30:48'),
(218, 1, 5, 18, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190816062618890', 1, 0, '2019-08-16 07:31:16'),
(219, 5, 5, 18, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 1734 for booking no. : 20190816062618890', 1, 0, '2019-08-16 07:32:07'),
(220, 1, 5, 18, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190816062618890', 1, 0, '2019-08-16 07:32:30'),
(221, 5, 1, 19, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816063942610', 1, 0, '2019-08-16 07:40:02'),
(222, 5, 1, 19, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816063942610', 1, 0, '2019-08-16 07:41:18'),
(223, 5, 1, 12, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816062329160', 1, 0, '2019-08-16 07:42:18'),
(224, 5, 1, 12, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816062329160', 1, 0, '2019-08-16 07:42:20'),
(225, 5, 1, 20, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816070012880', 1, 0, '2019-08-16 08:01:01'),
(226, 1, 5, 4, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190816032718940', 1, 0, '2019-08-16 08:08:16'),
(227, 5, 5, 4, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 3619 for booking no. : 20190816032718940', 1, 0, '2019-08-16 08:09:37'),
(228, 1, 5, 4, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190816032718940', 1, 0, '2019-08-16 08:09:57'),
(229, 2, 1, 24, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816071657040', 1, 0, '2019-08-16 08:17:01'),
(230, 2, 1, 25, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816071820660', 1, 0, '2019-08-16 08:19:01'),
(231, 2, 1, 27, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816072157020', 1, 0, '2019-08-16 08:22:02'),
(232, 2, 1, 27, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816072157020', 1, 0, '2019-08-16 08:22:16'),
(233, 2, 1, 28, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816072355590', 1, 0, '2019-08-16 08:24:01'),
(234, 2, 1, 28, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816072355590', 1, 0, '2019-08-16 08:24:13'),
(235, 2, 1, 29, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816072716000', 1, 0, '2019-08-16 08:28:02'),
(236, 2, 1, 30, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816072754880', 1, 0, '2019-08-16 08:28:02'),
(237, 2, 1, 30, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816072754880', 1, 0, '2019-08-16 08:28:13'),
(238, 2, 1, 29, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816072716000', 1, 0, '2019-08-16 08:28:26'),
(239, 2, 1, 39, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816081534620', 1, 0, '2019-08-16 09:17:01'),
(240, 2, 1, 43, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816081903240', 1, 0, '2019-08-16 09:20:02'),
(241, 2, 1, 45, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816082812620', 1, 0, '2019-08-16 09:29:01'),
(242, 2, 1, 47, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816083844110', 1, 0, '2019-08-16 09:39:01'),
(243, 2, 1, 47, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816083844110', 1, 0, '2019-08-16 09:39:14'),
(244, 2, 1, 67, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816104741580', 1, 0, '2019-08-16 11:48:01'),
(245, 2, 1, 71, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816104936320', 1, 0, '2019-08-16 11:50:03'),
(246, 2, 1, 72, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816105246150', 1, 0, '2019-08-16 11:53:02'),
(247, 2, 1, 75, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816114107610', 1, 0, '2019-08-16 12:42:01'),
(248, 2, 1, 81, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816125652410', 1, 0, '2019-08-16 13:57:01'),
(249, 2, 1, 80, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816125521270', 1, 0, '2019-08-16 13:57:02'),
(250, 2, 1, 80, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816125521270', 1, 0, '2019-08-16 14:02:12'),
(251, 2, 1, 81, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816125652410', 1, 0, '2019-08-16 14:02:25'),
(252, 1, 2, 80, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-16 14:03:20'),
(253, 1, 2, 80, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190816125521270', 1, 0, '2019-08-16 14:03:42'),
(254, 1, 2, 80, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190816125521270', 1, 0, '2019-08-16 14:04:29'),
(255, 2, 2, 80, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 6868 for booking no. : 20190816125521270', 1, 0, '2019-08-16 14:05:36'),
(256, 1, 2, 80, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190816125521270', 1, 0, '2019-08-16 14:05:49'),
(257, 2, 1, 82, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816130733450', 1, 0, '2019-08-16 14:08:01'),
(258, 2, 1, 82, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816130733450', 1, 0, '2019-08-16 14:08:15'),
(259, 1, 2, 82, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-16 14:08:24'),
(260, 1, 2, 82, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190816130733450', 1, 0, '2019-08-16 14:08:40'),
(261, 1, 2, 82, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190816130733450', 1, 0, '2019-08-16 14:08:55'),
(262, 2, 1, 83, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816130954400', 1, 0, '2019-08-16 14:10:02'),
(263, 2, 1, 83, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816130954400', 1, 0, '2019-08-16 14:10:20'),
(264, 1, 2, 83, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-16 14:11:09'),
(265, 1, 2, 83, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190816130954400', 1, 0, '2019-08-16 14:11:26'),
(266, 1, 2, 83, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190816130954400', 1, 0, '2019-08-16 14:12:42'),
(267, 2, 1, 84, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190816131748590', 1, 0, '2019-08-16 14:22:02'),
(268, 2, 1, 84, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190816131748590', 1, 0, '2019-08-16 14:22:22'),
(269, 1, 2, 28, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-16 14:23:19'),
(270, 1, 2, 28, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190816072355590', 1, 0, '2019-08-16 14:23:42'),
(271, 1, 2, 28, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190816072355590', 1, 0, '2019-08-16 14:24:09'),
(272, 3, 1, 85, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819033323720', 1, 0, '2019-08-19 04:51:22'),
(273, 3, 1, 86, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819034927110', 1, 0, '2019-08-19 04:51:22'),
(274, 3, 1, 86, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190819034927110', 1, 0, '2019-08-19 04:51:49'),
(275, 3, 1, 85, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190819033323720', 1, 0, '2019-08-19 04:52:03'),
(276, 1, 3, 86, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-19 04:52:22'),
(277, 1, 3, 86, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190819034927110', 1, 0, '2019-08-19 04:52:38'),
(278, 1, 3, 86, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190819034927110', 1, 0, '2019-08-19 04:52:48'),
(279, 3, 3, 86, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 9344 for booking no. : 20190819034927110', 1, 0, '2019-08-19 04:53:36'),
(280, 1, 3, 86, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190819034927110', 1, 0, '2019-08-19 04:53:51'),
(281, 2, 1, 87, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819040012400', 1, 0, '2019-08-19 05:01:01'),
(282, 2, 1, 87, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190819040012400', 1, 0, '2019-08-19 05:01:14'),
(283, 2, 1, 88, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819040430920', 1, 0, '2019-08-19 05:08:02'),
(284, 2, 1, 88, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190819040430920', 1, 0, '2019-08-19 05:08:22'),
(285, 1, 2, 87, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-19 05:09:45'),
(286, 1, 2, 87, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190819040012400', 1, 0, '2019-08-19 05:10:18'),
(287, 1, 2, 87, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190819040012400', 1, 0, '2019-08-19 05:10:38'),
(288, 2, 2, 87, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 9438 for booking no. : 20190819040012400', 1, 0, '2019-08-19 05:11:35'),
(289, 1, 2, 87, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190819040012400', 1, 0, '2019-08-19 05:12:07'),
(290, 2, 1, 91, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819041337350', 1, 0, '2019-08-19 05:14:01'),
(291, 2, 1, 91, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190819041337350', 1, 0, '2019-08-19 05:14:51'),
(292, 1, 2, 88, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-19 05:36:28'),
(293, 3, 1, 97, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819050246750', 1, 0, '2019-08-19 06:03:02'),
(294, 1, 2, 88, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190819040430920', 1, 0, '2019-08-19 06:14:00'),
(295, 1, 2, 88, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190819040430920', 1, 0, '2019-08-19 06:14:31'),
(296, 2, 2, 88, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 8110 for booking no. : 20190819040430920', 1, 0, '2019-08-19 06:15:11'),
(297, 1, 2, 88, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190819040430920', 1, 0, '2019-08-19 06:15:48'),
(298, 2, 1, 99, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819051640720', 1, 0, '2019-08-19 06:17:02'),
(299, 2, 1, 99, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190819051640720', 1, 0, '2019-08-19 06:17:11'),
(300, 1, 2, 99, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-19 06:22:21'),
(301, 1, 2, 99, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190819051640720', 1, 0, '2019-08-19 06:23:17'),
(302, 1, 2, 99, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190819051640720', 1, 0, '2019-08-19 06:23:39'),
(303, 2, 1, 103, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819052629200', 1, 0, '2019-08-19 06:27:01'),
(304, 2, 1, 104, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819052829670', 1, 0, '2019-08-19 06:29:02'),
(305, 3, 1, 118, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819053642510', 1, 0, '2019-08-19 06:37:02'),
(306, 3, 1, 117, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819053619980', 1, 0, '2019-08-19 06:37:02'),
(307, 3, 1, 120, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819053840450', 1, 0, '2019-08-19 06:39:02'),
(308, 3, 1, 119, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819053803370', 1, 0, '2019-08-19 06:39:02'),
(309, 3, 1, 119, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190819053803370', 1, 0, '2019-08-19 06:39:35'),
(310, 3, 1, 120, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190819053840450', 1, 0, '2019-08-19 06:39:54'),
(311, 1, 3, 119, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-19 06:40:22'),
(312, 3, 1, 122, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819055130310', 1, 0, '2019-08-19 06:52:01'),
(313, 3, 1, 122, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190819055130310', 1, 0, '2019-08-19 06:52:13'),
(314, 1, 3, 122, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-19 06:53:09'),
(315, 1, 3, 122, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190819055130310', 1, 0, '2019-08-19 06:56:00'),
(316, 1, 3, 122, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190819055130310', 1, 0, '2019-08-19 06:56:22'),
(317, 3, 3, 122, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 5133 for booking no. : 20190819055130310', 1, 0, '2019-08-19 06:57:44'),
(318, 1, 3, 122, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190819055130310', 1, 0, '2019-08-19 06:57:59'),
(319, 1, 2, 91, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-19 07:00:40'),
(320, 1, 3, 85, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-19 07:01:55'),
(321, 1, 3, 85, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190819033323720', 1, 0, '2019-08-19 07:02:15'),
(322, 1, 3, 85, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190819033323720', 1, 0, '2019-08-19 07:02:30'),
(323, 3, 3, 85, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 7894 for booking no. : 20190819033323720', 1, 0, '2019-08-19 07:03:15'),
(324, 1, 3, 85, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190819033323720', 1, 0, '2019-08-19 07:03:26'),
(325, 3, 1, 123, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819060418310', 1, 0, '2019-08-19 07:05:03'),
(326, 3, 1, 125, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819061953120', 1, 0, '2019-08-19 07:20:01'),
(327, 3, 1, 124, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819061902700', 1, 0, '2019-08-19 07:20:02'),
(328, 3, 1, 124, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190819061902700', 1, 0, '2019-08-19 07:20:16'),
(329, 3, 1, 125, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190819061953120', 1, 0, '2019-08-19 07:20:28'),
(330, 1, 3, 124, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-19 07:21:29'),
(331, 1, 3, 124, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190819061902700', 1, 0, '2019-08-19 07:21:42'),
(332, 1, 3, 124, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190819061902700', 1, 0, '2019-08-19 07:21:54'),
(333, 3, 3, 124, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 4501 for booking no. : 20190819061902700', 1, 0, '2019-08-19 07:23:03'),
(334, 1, 3, 124, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190819061902700', 1, 0, '2019-08-19 07:23:12'),
(335, 2, 1, 126, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819062634500', 1, 0, '2019-08-19 07:27:02'),
(336, 2, 1, 126, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190819062634500', 1, 0, '2019-08-19 07:27:21'),
(337, 1, 2, 126, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-19 07:27:38'),
(338, 1, 2, 126, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190819062634500', 1, 0, '2019-08-19 07:28:03'),
(339, 1, 2, 126, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20190819062634500', 1, 0, '2019-08-19 07:28:25'),
(340, 2, 2, 126, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 4057 for booking no. : 20190819062634500', 1, 0, '2019-08-19 07:29:31'),
(341, 1, 2, 126, 'Job Completed', 'TEXT', 6, 'Job completed by driver, booking no. : 20190819062634500', 1, 0, '2019-08-19 07:29:48'),
(342, 3, 1, 127, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819063839290', 1, 0, '2019-08-19 07:39:01'),
(343, 3, 1, 128, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819064030340', 1, 0, '2019-08-19 07:41:02'),
(344, 3, 1, 128, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190819064030340', 1, 0, '2019-08-19 07:42:07'),
(345, 2, 1, 130, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819064914140', 1, 0, '2019-08-19 07:50:02'),
(346, 3, 1, 131, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819065015840', 1, 0, '2019-08-19 07:51:01'),
(347, 3, 1, 131, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190819065015840', 1, 0, '2019-08-19 07:51:10'),
(348, 2, 1, 146, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819072114330', 1, 0, '2019-08-19 08:24:02'),
(349, 3, 1, 153, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819074912940', 1, 0, '2019-08-19 08:50:02'),
(350, 2, 1, 218, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819102557050', 1, 0, '2019-08-19 11:26:02'),
(351, 2, 1, 219, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819102703130', 1, 0, '2019-08-19 11:28:01'),
(352, 4, 1, 222, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819102925630', 1, 0, '2019-08-19 11:30:02'),
(353, 4, 1, 227, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819103544200', 1, 0, '2019-08-19 11:37:01'),
(354, 4, 1, 228, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819103836980', 1, 0, '2019-08-19 11:39:01'),
(355, 4, 1, 228, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190819103836980', 1, 0, '2019-08-19 11:39:10'),
(356, 1, 4, 228, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-19 11:39:34'),
(357, 1, 4, 228, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20190819103836980', 1, 0, '2019-08-19 11:39:51'),
(358, 3, 1, 229, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819104745530', 1, 0, '2019-08-19 11:48:01'),
(359, 3, 1, 231, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819104923570', 1, 0, '2019-08-19 11:50:03'),
(360, 3, 1, 232, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819105136060', 1, 0, '2019-08-19 11:53:02'),
(361, 3, 1, 232, 'New Booking', 'TEXT', 2, 'Your booking accepted by driver! booking no. : 20190819105136060', 1, 0, '2019-08-19 11:53:14'),
(362, 1, 3, 232, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2019-08-19 11:53:52'),
(363, 4, 1, 234, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819111624230', 1, 0, '2019-08-19 12:17:02'),
(364, 4, 1, 235, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819114228880', 1, 0, '2019-08-19 12:43:02'),
(365, 3, 1, 236, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819114422990', 1, 0, '2019-08-19 12:45:02'),
(366, 4, 1, 237, 'New Booking', 'TEXT', 2, 'Please accept new booking! booking no. : 20190819114425040', 1, 0, '2019-08-19 12:45:02');
INSERT INTO `notification` (`idnotification`, `idsender`, `idreceiver`, `idsource`, `notificationtype`, `notificatiomode`, `sort`, `description`, `isactive`, `isviewed`, `createdon`) VALUES
(367, 1, 6, 27, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2020-10-15 12:28:54'),
(368, 1, 6, 27, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20201015063529970', 1, 0, '2020-10-15 12:35:28'),
(369, 1, 5, 21, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2020-10-15 12:40:09'),
(370, 1, 5, 21, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20201014055358920', 1, 0, '2020-10-15 12:40:20'),
(371, 1, 5, 5, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2020-10-15 12:41:17'),
(372, 1, 5, 5, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20201014032514070', 1, 0, '2020-10-15 12:41:31'),
(373, 1, 5, 5, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20201014032514070', 1, 0, '2020-10-15 12:42:01'),
(374, 1, 6, 29, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2020-10-15 13:23:16'),
(375, 1, 6, 28, 'New Booking', 'TEXT', 2, 'Driver on the way! You don\'t go any where', 1, 0, '2020-10-15 16:55:06'),
(376, 1, 6, 28, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20201015080718720', 1, 0, '2020-10-15 16:55:44'),
(377, 1, 6, 28, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20201015080718720', 1, 0, '2020-10-15 16:57:24'),
(378, 6, 6, 28, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 3390 for booking no. : 20201015080718720', 1, 0, '2020-10-16 11:52:44'),
(379, 5, 5, 64, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 2970 for booking no. : 20201016100609470', 1, 0, '2020-10-16 15:07:04'),
(380, 5, 5, 65, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 9996 for booking no. : 20201019031049760', 1, 0, '2020-10-19 08:13:14'),
(381, 5, 5, 65, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 5075 for booking no. : 20201019031049760', 1, 0, '2020-10-19 08:24:13'),
(382, 5, 5, 65, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 7862 for booking no. : 20201019031049760', 1, 0, '2020-10-19 08:30:21'),
(383, 5, 5, 65, 'Job Completed OTP', 'TEXT', 6, 'You have recieved job completed OTP 3526 for booking no. : 20201019031049760', 1, 0, '2020-10-19 10:01:18'),
(384, 5, 5, 65, 'Job Completed OTP', 'TEXT', 6, 'Thank you for payment! Your booking successfully saved and booking no. : 20201019031049760', 1, 0, '2020-10-19 10:24:11'),
(385, 5, 5, 65, 'Job Completed OTP', 'TEXT', 6, 'Thank you for payment! Your booking successfully saved and booking no. : 20201019031049760', 1, 0, '2020-10-19 10:29:50'),
(386, 5, 5, 65, 'Job Completed OTP', 'TEXT', 6, 'Thank you for payment! Your booking successfully saved and booking no. : 20201019031049760', 1, 0, '2020-10-19 10:31:20'),
(387, 5, 5, 65, 'Job Completed OTP', 'TEXT', 6, 'Thank you for payment! Your booking successfully saved and booking no. : 20201019031049760', 1, 0, '2020-10-19 10:31:24'),
(388, 5, 5, 65, 'Job Completed OTP', 'TEXT', 6, 'Thank you for payment! Your booking successfully saved and booking no. : 20201019031049760', 1, 0, '2020-10-19 10:33:24'),
(389, 5, 5, 65, 'Job Completed OTP', 'TEXT', 6, 'Thank you for payment! Your booking successfully saved and booking no. : 20201019031049760', 1, 0, '2020-10-19 10:58:38'),
(390, 6, 6, 97, 'Job Completed OTP', 'TEXT', 6, 'Thank you for payment! Your booking successfully saved and booking no. : 20201019120702130', 1, 0, '2020-10-19 17:21:08'),
(391, 6, 6, 102, 'Job Completed OTP', 'TEXT', 6, 'Thank you for payment! Your booking successfully saved and booking no. : 20201020024645180', 1, 0, '2020-10-20 07:47:21'),
(392, 1, 5, 21, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20201014055358920', 1, 0, '2020-10-20 08:49:16'),
(393, 1, 5, 21, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20201014055358920', 1, 0, '2020-10-20 08:50:14'),
(394, 1, 5, 21, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20201014055358920', 1, 0, '2020-10-20 08:51:02'),
(395, 1, 5, 21, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20201014055358920', 1, 0, '2020-10-20 09:07:53'),
(396, 1, 5, 21, 'New Booking', 'TEXT', 2, 'Your booking  work started by driver! booking no. : 20201014055358920', 1, 0, '2020-10-20 09:09:38'),
(397, 1, 5, 21, 'New Booking', 'TEXT', 2, 'Your booking  work finished by driver! booking no. : 20201014055358920', 1, 0, '2020-10-20 09:10:36');

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `idoffers` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `offertype` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `couponcode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offervalue` float DEFAULT NULL,
  `offervaluetype` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `startdate` date NOT NULL,
  `enddate` date DEFAULT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `otp`
--

CREATE TABLE `otp` (
  `idotp` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `otp` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otp`
--

INSERT INTO `otp` (`idotp`, `email`, `otp`, `date`, `status`) VALUES
(1, '9555311628', 9999, '2018-09-10', 0),
(2, '9555311628', 9999, '2018-09-10', 0),
(3, '8800522966', 9999, '2019-04-16', 0),
(4, '8800522966', 8243, '2019-04-16', 0),
(5, '8800522966', 2103, '2019-04-16', 0),
(6, '8800522966', 6331, '2019-04-16', 0),
(7, '8800522966', 9117, '2019-04-16', 0),
(70, '3294329834', 7615, '2019-08-16', 0),
(71, '3294329834', 5484, '2019-08-16', 0),
(72, '3294329834', 2856, '2019-08-16', 0),
(73, '3294329834', 2819, '2019-08-16', 0),
(74, '3294329834', 8106, '2019-08-16', 0),
(79, '3294329835', 4982, '2019-08-16', 0),
(80, '3294329835', 8047, '2019-08-16', 0),
(81, '3294329835', 3391, '2019-08-16', 0),
(95, '1234567890', 9734, '2019-08-16', 0),
(96, '1234567890', 7709, '2019-08-16', 0),
(97, '9999999999', 3002, '2019-08-16', 0),
(98, '9999999999', 2246, '2019-08-16', 0),
(123, '6209838206', 8377, '2020-09-28', 0),
(124, '7418347640', 6962, '2020-09-28', 0),
(125, '7418347640', 8589, '2020-09-28', 0),
(126, '7418347640', 2854, '2020-09-28', 0),
(127, '6209836011', 8075, '2020-09-28', 0),
(129, '8840212173', 3404, '2020-10-14', 0),
(130, '8840212173', 5500, '2020-10-14', 0),
(131, '8840212173', 4335, '2020-10-14', 0),
(132, '8840212173', 4934, '2020-10-14', 0),
(133, '8840212173', 7613, '2020-10-14', 0),
(134, '8840212173', 5772, '2020-10-14', 0),
(135, '8840212173', 2402, '2020-10-14', 0),
(136, '8840212173', 1280, '2020-10-14', 0),
(137, '8840212173', 3610, '2020-10-14', 0),
(138, '8840212173', 9895, '2020-10-14', 0),
(139, '8840212173', 9120, '2020-10-14', 0),
(140, '8447627885', 4548, '2020-10-14', 0),
(145, '26768244561234', 1436, '2020-10-15', 0),
(148, '8787779328', 2949, '2020-10-15', 0),
(149, '87877793281234', 4829, '2020-10-15', 0),
(151, '6308127560', 2601, '2020-10-15', 0),
(160, '2676824456', 6344, '2020-10-23', 0),
(161, '8052512968', 3830, '2020-10-23', 0),
(162, '8800522966', 4470, '2020-10-23', 0),
(163, '8840212173', 8511, '2020-10-23', 0),
(164, '8447627885', 1895, '2020-10-23', 0),
(165, '8447627885', 4862, '2020-10-23', 0),
(166, '8447627885', 4624, '2020-10-23', 0),
(167, '8447627885', 3865, '2020-10-23', 0),
(168, '8447627885', 3535, '2020-10-23', 0),
(169, '8447627867', 5470, '2020-10-23', 0),
(170, '8447627867', 7784, '2020-10-23', 0),
(171, '8052512968', 9858, '2020-10-23', 0),
(172, '8447627867', 7392, '2020-10-23', 0),
(173, '8447627885', 2052, '2020-10-23', 0),
(174, '8447627885', 3177, '2020-10-23', 0),
(175, '8447627885', 3147, '2020-10-23', 0),
(176, '8800522966', 4019, '2020-10-23', 0),
(177, '8800522966', 4766, '2020-10-23', 0),
(178, '8800522966', 5387, '2020-10-23', 0),
(179, '8800522966', 7479, '2020-10-23', 0),
(180, '8800522966', 3571, '2020-10-23', 0),
(181, '8800522966', 4564, '2020-10-23', 0),
(182, '8800522966', 4993, '2020-10-23', 0),
(183, '8800522966', 2759, '2020-10-23', 0),
(184, '8800522966', 4615, '2020-10-23', 0),
(185, '8800522966', 8797, '2020-10-23', 0),
(186, '8800522966', 7364, '2020-10-23', 0),
(187, '8800522966', 1846, '2020-10-23', 0),
(188, '8800522966', 3954, '2020-10-23', 0),
(189, '8800522966', 4765, '2020-10-23', 0),
(190, '8800522966', 4283, '2020-10-23', 0),
(191, '8800522966', 3705, '2020-10-23', 0),
(192, '8800522966', 5860, '2020-10-23', 0),
(193, '8800522966', 9667, '2020-10-23', 0),
(194, '8800522966', 8847, '2020-10-23', 0),
(195, '8800522966', 8093, '2020-10-23', 0),
(196, '8800522966', 2525, '2020-10-23', 0),
(197, '8800522966', 8113, '2020-10-23', 0),
(198, '8800522966', 6078, '2020-10-23', 0),
(199, '8800522966', 9823, '2020-10-23', 0),
(200, '8800522966', 9800, '2020-10-23', 0),
(201, '8800522966', 2455, '2020-10-23', 0),
(202, '8800522966', 2391, '2020-10-23', 0),
(203, '8800522966', 7697, '2020-10-23', 0),
(204, '8800522966', 9808, '2020-10-23', 0),
(205, '8800522966', 4879, '2020-10-23', 0),
(206, '8800522966', 6074, '2020-10-23', 0),
(207, '8052512968', 5157, '2020-10-23', 0),
(208, '8052512968', 3178, '2020-10-23', 0),
(209, '8800522966', 1139, '2020-10-23', 0),
(210, '8800522966', 3908, '2020-10-23', 0),
(211, '8800522966', 6239, '2020-10-23', 0),
(214, '7866991352', 2168, '2020-10-28', 0),
(237, '8477368498', 9173, '2020-12-10', 0),
(238, '8477368498', 6688, '2020-12-10', 0),
(239, '8477368498', 4951, '2020-12-10', 0),
(240, '8477368498', 9333, '2020-12-10', 0),
(270, '4014508874', 4729, '2020-12-17', 0),
(271, '4014508874', 1412, '2020-12-17', 0),
(273, '4014508874', 7536, '2020-12-17', 0),
(294, '9082308268', 6388, '2020-12-17', 0),
(295, '9082308268', 2044, '2020-12-17', 0),
(296, '9082308268', 2064, '2020-12-17', 0);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `idpayment` int(11) NOT NULL,
  `paymentmethod` varchar(250) NOT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`idpayment`, `paymentmethod`, `isactive`, `createdon`) VALUES
(1, 'Paypal', 1, '0000-00-00 00:00:00'),
(2, 'Authorize.Net', 1, '0000-00-00 00:00:00'),
(4, 'Credit Card / Debit Card', 1, '2018-05-11 09:34:16');

-- --------------------------------------------------------

--
-- Table structure for table `plowsize`
--

CREATE TABLE `plowsize` (
  `idplowsize` int(11) NOT NULL,
  `plowsize` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plowsize`
--

INSERT INTO `plowsize` (`idplowsize`, `plowsize`, `isactive`, `createdon`) VALUES
(1, '6\'', 1, '2018-07-02 21:51:20'),
(2, '7\'', 1, '2018-07-02 21:51:20'),
(3, '7.5\'', 1, '2018-07-02 21:51:20'),
(4, '8\'', 1, '2018-07-05 16:33:56'),
(5, '8.5\'', 1, '2018-07-05 16:33:56'),
(6, '9+', 1, '2018-07-05 16:33:56');

-- --------------------------------------------------------

--
-- Table structure for table `plowtype`
--

CREATE TABLE `plowtype` (
  `idplowtype` int(11) NOT NULL,
  `plowtype` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plowtype`
--

INSERT INTO `plowtype` (`idplowtype`, `plowtype`, `description`, `isactive`, `createdon`) VALUES
(1, 'Steel Edge', NULL, 1, '2018-07-02 21:51:20'),
(2, 'Rubber Edge', NULL, 1, '2018-07-02 21:51:20');

-- --------------------------------------------------------

--
-- Table structure for table `pricedetail`
--

CREATE TABLE `pricedetail` (
  `idpricedetail` int(11) NOT NULL,
  `snowdepthfrom` float NOT NULL,
  `snowdepthto` float NOT NULL,
  `areafrom` float NOT NULL,
  `areato` float NOT NULL,
  `price` float NOT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pricedetail`
--

INSERT INTO `pricedetail` (`idpricedetail`, `snowdepthfrom`, `snowdepthto`, `areafrom`, `areato`, `price`, `isactive`, `createdon`) VALUES
(1, 0, 2, 1, 1500, 50, 1, '2018-07-04 16:09:15'),
(2, 2.1, 4, 1, 1500, 50, 1, '2018-07-04 16:11:06'),
(3, 4.1, 6, 1, 1500, 75, 1, '2018-07-04 16:11:06'),
(4, 6.1, 8, 1, 1500, 99, 1, '2018-07-04 16:11:06'),
(5, 0, 2, 1501, 5000, 68, 1, '2018-07-04 16:09:15'),
(6, 2.1, 4, 1501, 5000, 68, 1, '2018-07-04 16:11:06'),
(7, 4.1, 6, 1501, 5000, 90, 1, '2018-07-04 16:11:06'),
(8, 6.1, 8, 1501, 5000, 117, 1, '2018-07-04 16:11:06'),
(9, 0, 2, 5001, 10000, 96, 1, '2018-07-04 16:09:15'),
(10, 2.1, 4, 5001, 10000, 96, 1, '2018-07-04 16:11:06'),
(11, 4.1, 6, 5001, 10000, 145, 1, '2018-07-04 16:11:06'),
(12, 6.1, 8, 5001, 10000, 175, 1, '2018-07-04 16:11:06'),
(13, 0, 2, 10001, 20000, 125, 1, '2018-07-04 16:09:15'),
(14, 2.1, 4, 10001, 20000, 125, 1, '2018-07-04 16:11:06'),
(15, 4.1, 6, 10001, 20000, 165, 1, '2018-07-04 16:11:06'),
(16, 6.1, 8, 10001, 20000, 195, 1, '2018-07-04 16:11:06'),
(17, 0, 2, 20001, 50000, 150, 1, '2018-07-04 16:09:15'),
(18, 2.1, 4, 20001, 50000, 150, 1, '2018-07-04 16:11:06'),
(19, 4.1, 6, 20001, 50000, 210, 1, '2018-07-04 16:11:06'),
(20, 6.1, 8, 20001, 50000, 259, 1, '2018-07-04 16:11:06');

-- --------------------------------------------------------

--
-- Table structure for table `promos`
--

CREATE TABLE `promos` (
  `idpromo` int(11) NOT NULL,
  `promocode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `promovalue` float NOT NULL,
  `promotype` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `starttime` date NOT NULL,
  `endtime` date NOT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `promos`
--

INSERT INTO `promos` (`idpromo`, `promocode`, `promovalue`, `promotype`, `starttime`, `endtime`, `isactive`, `createdon`) VALUES
(1, 'DR10', 20, 'Percent', '2018-05-01', '2019-11-30', 1, '2018-05-20 01:19:56'),
(2, 'DRY10', 30, 'USD', '2019-06-21', '2020-02-28', 1, '2019-05-29 16:06:33');

-- --------------------------------------------------------

--
-- Table structure for table `sampledriver`
--

CREATE TABLE `sampledriver` (
  `fullname` varchar(18) DEFAULT NULL,
  `address` varchar(22) DEFAULT NULL,
  `city` varchar(11) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `zipcode` int(5) DEFAULT NULL,
  `country` varchar(3) DEFAULT NULL,
  `lat` decimal(7,5) DEFAULT NULL,
  `lng` decimal(8,5) DEFAULT NULL,
  `email` varchar(26) DEFAULT NULL,
  `mobile` varchar(12) DEFAULT NULL,
  `ssnumber` varchar(10) DEFAULT NULL,
  `experience` int(2) DEFAULT NULL,
  `areaofwork` varchar(10) DEFAULT NULL,
  `drivinglicense` varchar(10) DEFAULT NULL,
  `driverinsuranceno` varchar(10) DEFAULT NULL,
  `plowtype` varchar(3) DEFAULT NULL,
  `truckname` varchar(8) DEFAULT NULL,
  `vehiclenumber` varchar(10) DEFAULT NULL,
  `registrationcertificateimage` varchar(10) DEFAULT NULL,
  `truckinsuranceno` varchar(16) DEFAULT NULL,
  `truckinsuranceimage` varchar(52) DEFAULT NULL,
  `truckimage` varchar(25) DEFAULT NULL,
  `plowsize` varchar(6) DEFAULT NULL,
  `rubberblade` varchar(7) DEFAULT NULL,
  `snowblower` varchar(7) DEFAULT NULL,
  `saltspreader` varchar(7) DEFAULT NULL,
  `bobcat` varchar(10) DEFAULT NULL,
  `shovellers` varchar(10) DEFAULT NULL,
  `misc` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sampledriver`
--

INSERT INTO `sampledriver` (`fullname`, `address`, `city`, `state`, `zipcode`, `country`, `lat`, `lng`, `email`, `mobile`, `ssnumber`, `experience`, `areaofwork`, `drivinglicense`, `driverinsuranceno`, `plowtype`, `truckname`, `vehiclenumber`, `registrationcertificateimage`, `truckinsuranceno`, `truckinsuranceimage`, `truckimage`, `plowsize`, `rubberblade`, `snowblower`, `saltspreader`, `bobcat`, `shovellers`, `misc`) VALUES
('Mike Murray', '21278 N. Crestview Dr.', 'Barrington', 'IL', 60010, 'USA', '42.17280', '-88.13520', 'mikemurray0717@hotmail.com', '847-346-5004', '', 15, 'chicago', '', '', 'reg', 'Nissan', '', '', 'National General', '1531148245_Screen Shot 2017-07-20 at 11.56.53 AM.png', '1531148245_Ford-F-150.jpg', '7ft', '', 'checked', 'checked', '', '', ''),
('Andy Laino', '4704 Ringwood Rd', 'Ringwood', 'IL', 60072, 'USA', '42.39882', '-88.28961', 'laino51@msn.com', '847-858-1539', '', 25, 'Chicago NW', '', '', 'reg', 'F250', '', '', '', '', '', '8ft', '', '', 'checked', '', '', ''),
('Rocco Laino', '4704 Ringwood Rd', 'Ringwood', 'IL', 60072, 'USA', '42.39882', '-88.28961', 'rooco71@msn.com', '847-366-6508', '', 40, 'Chicago', '', '', 'reg', 'F150', '', '', '', '', '', '8ft', '', '', '', '', '', ''),
('Antonio Martinez', '2560 N. Haymond Ave.', 'River Grove', 'IL', 60171, 'USA', '41.92663', '-87.82749', 'tonymtz409@gmail.com', '312-593-8869', '', 10, 'Chicago S', '', '', 'reg', 'F350', '', '', '', '', '', '8.5 ft', 'checked', 'checked', 'checked', '', '', ''),
('Frances Montemayor', '1519 Parkside Dr', 'Plainfield', 'IL', 60586, 'USA', '41.55057', '-88.18654', 'kiko2369@yahoo.com', '815-666-4671', '', 15, 'Chicago S', '', '', 'reg', 'Ram 1600', '', '', '', '', '', '8ft', '', '', 'checked', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `searchbanner`
--

CREATE TABLE `searchbanner` (
  `idsearchbanner` int(11) NOT NULL,
  `image` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `searchbanner`
--

INSERT INTO `searchbanner` (`idsearchbanner`, `image`, `isactive`, `createdon`) VALUES
(1, '1.jpg', 1, '2018-05-11 16:26:55'),
(2, '22.jpg', 1, '2018-05-11 16:26:55'),
(3, '3.jpg', 0, '2018-05-11 16:26:55');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `idservice` int(11) NOT NULL,
  `idcategory` int(11) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `subtitle` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mrp` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `timetaken` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`idservice`, `idcategory`, `iduser`, `name`, `subtitle`, `duration`, `mrp`, `discount`, `timetaken`, `code`, `description`, `isactive`, `createdon`) VALUES
(1, NULL, NULL, 'One Time', NULL, '', 88, NULL, NULL, NULL, NULL, 1, '2018-07-04 16:54:49'),
(2, NULL, NULL, 'DRYFT Elite', NULL, '1 Month', 10, NULL, NULL, NULL, NULL, 1, '2018-07-04 16:54:49');

-- --------------------------------------------------------

--
-- Table structure for table `servicearea`
--

CREATE TABLE `servicearea` (
  `idservicearea` int(11) NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `servicearea`
--

INSERT INTO `servicearea` (`idservicearea`, `city`, `state`, `isactive`) VALUES
(1, 'Chicago', 'IL', 1);

-- --------------------------------------------------------

--
-- Table structure for table `servicecharges`
--

CREATE TABLE `servicecharges` (
  `idservicecharges` int(11) NOT NULL,
  `tax` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `taxamount` float NOT NULL,
  `taxtype` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `extracharges` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `extrachargesamount` float NOT NULL,
  `extrachargestype` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `servicecharges`
--

INSERT INTO `servicecharges` (`idservicecharges`, `tax`, `taxamount`, `taxtype`, `extracharges`, `extrachargesamount`, `extrachargestype`, `isactive`, `createdon`) VALUES
(1, 'Tax', 3.5, 'Percent', 'Invenia Services', 0.25, 'Percent', 1, '2018-06-05 20:18:09');

-- --------------------------------------------------------

--
-- Table structure for table `servicefeature`
--

CREATE TABLE `servicefeature` (
  `idservicefeature` int(11) NOT NULL,
  `idservice` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `servicefeature`
--

INSERT INTO `servicefeature` (`idservicefeature`, `idservice`, `title`, `image`, `isactive`, `createdon`) VALUES
(1, 2, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2018-07-23 08:56:41'),
(2, 2, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2018-07-23 08:57:34'),
(3, 2, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2018-07-23 08:56:41'),
(4, 2, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2018-07-23 08:57:34'),
(5, 2, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2018-07-23 08:56:41');

-- --------------------------------------------------------

--
-- Table structure for table `trucksareaaso`
--

CREATE TABLE `trucksareaaso` (
  `idtrucksareaaso` int(11) NOT NULL,
  `idusertruck` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `idfeaturesareaprice` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `parentid` int(11) NOT NULL DEFAULT '0',
  `reffererid` int(11) DEFAULT NULL,
  `usertype` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `companyname` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `companyphone` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `profession` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `state` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `zipcode` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `country` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lat` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lng` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `securitycode` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `profileimage` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `profilebackground` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `otp` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `refferalid` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `profileprivacy` int(1) NOT NULL DEFAULT '0',
  `isnotification` int(1) NOT NULL DEFAULT '1',
  `ssnumber` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ssnumberimage` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `experience` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `areaofwork` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `drivinglicense` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `drivinglicenseimage` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `driverinsuranceno` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `driverinsurancenoimage` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `subscribe` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fcmtoken` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ipaddress` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `about` text COLLATE utf8_unicode_ci,
  `weburl` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `friendlyURL` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `isactive` int(1) NOT NULL DEFAULT '0',
  `isdeleted` int(11) NOT NULL DEFAULT '0',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedon` datetime DEFAULT NULL,
  `deletedon` datetime DEFAULT NULL,
  `usercol` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`iduser`, `parentid`, `reffererid`, `usertype`, `fullname`, `companyname`, `companyphone`, `profession`, `address`, `city`, `state`, `zipcode`, `country`, `lat`, `lng`, `email`, `mobile`, `password`, `securitycode`, `profileimage`, `profilebackground`, `otp`, `refferalid`, `profileprivacy`, `isnotification`, `ssnumber`, `ssnumberimage`, `experience`, `areaofwork`, `drivinglicense`, `drivinglicenseimage`, `driverinsuranceno`, `driverinsurancenoimage`, `subscribe`, `fcmtoken`, `ipaddress`, `about`, `weburl`, `friendlyURL`, `isactive`, `isdeleted`, `createdon`, `updatedon`, `deletedon`, `usercol`) VALUES
(1, 0, NULL, 'customer', 'Chris pratt', '', '', '', '', '', '', '', '', '', '', 'prattlandscapingnky@gmail.com', '8593079682', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'e2a5d1mCks4:APA91bHCVaLLBg5zV9Zj6-99J6SdMvrcx9veX1GalM5B8sO7Vxc1L3A475EAalcHe2c9NKjqF4qyJhXv6vtikqOeCWbAezh7eBx1GdDa-r-JvaV4MVTiPOcQL2eqXbPLXMsO1nPP-PBU', '', NULL, '', '', 1, 0, '2020-12-11 01:50:48', NULL, NULL, NULL),
(2, 0, NULL, 'customer', 'Bret Monahan', '', '', '', '146 Colleen dr Blakeslee pa 18610', '', '', '', '', '', '', 'wwwrescue51@aol.com', '4843576653', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '1', 'eKq6tqOhRhk:APA91bHnxfLkjBZi92RBcncjugw22liIp9nZGISmH7O9-t9B67ulINr4yNhjPimZacKLCnxUuiNOumjHcoLWK-a1J0oSS6XcoxgOz41e-V8YoO4JDbWEmQfR1k-rDsKyGM4PGSuOT4RE', '', NULL, '', '', 1, 0, '2020-12-11 04:35:54', NULL, NULL, NULL),
(3, 0, NULL, 'CUSTOMER', 'Stephanie Espino', '', '', '', '', '', '', '', '', '', '', 'sespino@bsd220.org', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'ej_MLRmNXQM:APA91bEU4rhZCEv44XUUcYD6uDvV816DZyS6TXL97gT6AlZilhTH4wBlV1NP0Nji15_JigrE4wiC6ITdoycNmrHKWSG0YBbLAsbI7YNkzINMmBGNjgDz9z4Srpw1I2G-fy6QfMlQKqRr', '', NULL, '', '', 1, 0, '2020-12-12 04:01:40', NULL, NULL, NULL),
(4, 0, NULL, 'CUSTOMER', 'MrTooSerious 96', '', '', '', '', '', '', '', '', '', '', 'sharp.clifford6813@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fT5zd0WuTRI:APA91bEj65UcjBIpf_n2X7CQjYS9Yv8JSFnBhNYIej9RS69koLw15-Viaa9rT38GlrrTICXUUgmPO5QpwM1jqB9PMAu8LMBopJ-ARq4JDB7ITn38n_Cnm-9G8qFKynL0aHV5NQ2JOwJV', '', NULL, '', '', 1, 0, '2020-12-12 07:50:24', NULL, NULL, NULL),
(5, 0, NULL, 'customer', 'Jessica Cree', '', '', '', '', '', '', '', '', '', '', 'jr4377@aol.com', '1203536028', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'emqgw50afJI:APA91bE7kfUbStohk2A-4Jwk8IzlPN0o5dHcMjZgcyysiBwndcKGrJaNWO616NpamyLunBP_AQm7heWkGXUYvbERLEpW3xZusf3ZpxBaPa0Pnfd745-hzGkBuLCiKe5Quz1qH0fxMJJr', '', NULL, '', '', 1, 0, '2020-12-12 14:33:00', NULL, NULL, NULL),
(6, 0, NULL, 'customer', 'Kaine Eggert', '', '', '', '', '', '', '', '', '', '', 'kaineeggert@gmail.com', '9209154044', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cDmx7VuqfBE:APA91bG9xH9OB4iL390QMl4961Cv8wDKXdiOO7vj0b43Fl-FNqVAOiv2CEOaJFyc2AyrcV7Msdg2dP9XG-XsG0J0ocYIEDNaGIpuqjkVf23InrQDh2CwW4aKAd-i-y7muJCpAUnStXKg', '', NULL, '', '', 1, 0, '2020-12-12 18:38:47', NULL, NULL, NULL),
(7, 0, NULL, 'CUSTOMER', 'Gina Sclafani', '', '', '', '', '', '', '', '', '', '', 'sclafani.gina@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'euocuRuCh5c:APA91bHuz6zwvvlM6HpvCO-mhASpvqLkJLd7ypCRhRACp4lMPqdVrJNPdcKYYn527dsib1cofnPWS6M2IjPvkjIjDOj8JWA1-my-WggSLQI19gkobJ7K7-UKxfsPs9hFYVpGsmdDAp4t', '', NULL, '', '', 1, 0, '2020-12-12 22:35:36', NULL, NULL, NULL),
(8, 0, NULL, 'customer', 'Gabrielle', '', '', '', '', '', '', '', '', '', '', 'gbravoco@gmail.com', '9735809417', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'd5gX-NNZzMc:APA91bGf7YIf1yuwfPgfjI-5STyBs985LAvit4MxCXVaec-ilhoyfAlqjZA1cS2WZMIlLYbpHJOWrZlP6Uv8d2yMTJVGgbV_immMS5a44usqyMB2D_qx6tvR0guOTdhjf5gekpTXcAch', '', NULL, '', '', 1, 0, '2020-12-13 05:38:36', NULL, NULL, NULL),
(9, 0, NULL, 'CUSTOMER', 'Elijah Osborne', '', '', '', '', '', '', '', '', '', '', 'eosborne1280@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'c7YDOG6Iba8:APA91bHD9V-WhoNNsCHw4L3AOlzEgr8WmhqJZt7S9T5o7KYqZsdwA3DTsQ70Z5U1bIMuzbIbKosEPg-An_Gp5W_qLdAfdu37CgLJ0RwFBhIp2eFPSgkGQp2e3Xvafq6aCH2-u8bRREMN', '', NULL, '', '', 1, 0, '2020-12-13 05:46:01', NULL, NULL, NULL),
(10, 0, NULL, 'customer', 'Rui', '', '', '', '', '', '', '', '', '', '', 'rui@jjfoundations.com', '2039483679', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'd-uwY30YpBs:APA91bHbliEJKU7Q07Hmg08trdmelPo-8vuvwLnxFYTNo-NZx9KO-taxUbfl1jL3On-HSpXryQA0HwEbPQN2N7tRQAqmhN0Ack5qVE7w7Ia8iP1lUTTwLsufDGVXnrFIjciUs2LhlM02', '', NULL, '', '', 1, 0, '2020-12-13 20:15:18', NULL, NULL, NULL),
(11, 0, NULL, 'CUSTOMER', 'Jeff Bowen', '', '', '', '', '', '', '', '', '', '', 'jbowen0048@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '1', 'dyTpFwZUYz8:APA91bHzHkql8KKU6pNBWkl5f7wFqC5d-aQVzDi2HUgmxUuN7lxbUF6hur6PN0wHceGYz-Se0Mz0sUNZfmAdkQa2RMW2HF-lfLpbfU1v25C1GKZVjrQ5aOBhpHBYe-wRV_JZQOuEqSvD', '', NULL, '', '', 1, 0, '2020-12-13 23:23:20', NULL, NULL, NULL),
(12, 0, NULL, 'DRIVER', 'Steve Pfister', '', '', '', '440 W Colfax St # 212', 'Palatine', 'Il', '60067', '', '', '', 'spfister@mail.com', '8474018600', 'fb489ae32d73780eebec4ccf40d84ce75778a42a', 'xnBw9awB7s', '1607921042_1607921019_profileimage.jpg', '', '', '', 1, 1, '320606992', '1607921042_1607921019_ssnumberimage.jpg', '40', 'Palatine,Arlington Heights,NW Suburbs', 'P23679360112', '1607921042_1607921019_drivinglicenseimage.jpg', '9112j066113', '1607921042_1607921019_driverinsurancenoimage.jpg', '', '', '', NULL, '', '12-steve-pfister', 1, 0, '2020-12-14 04:44:02', NULL, NULL, NULL),
(13, 0, NULL, 'customer', 'Jack', '', '', '', '', '', '', '', '', '', '', 'jfortus3@gmail.com', '7327898919', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cK9eZ_ouxnU:APA91bHm3ljg6DN4JMbRWEu3-h7mpxFL_QP-uezBNGIXqkLQMpcvRDcUFS-6G7zlP-xyIDmjc7NyuDUsSms-ii-Th3JZjayzsz8yxO1_D_O6bodALwT3VY-UY7gYUzDQ_ixUwHWFWXVX', '', NULL, '', '', 1, 0, '2020-12-14 12:16:56', NULL, NULL, NULL),
(14, 0, NULL, 'CUSTOMER', 'S P', '', '', '', '', '', '', '', '', '', '', 'midwestpaintco@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eHvcwddHlbw:APA91bHCqyFyPP4TIteU5WQDWheIe22-NXXC5ivAs3OiFU6Np7v20yrwXvU3hMECeJxfBGnSVAz6Ms8rRkHwLDp8VClkOLJqHGIP-P3YQLuwKjaJLWtz1RIGH_imWQJadoK5-iqccZkZ', '', NULL, '', '', 1, 0, '2020-12-14 19:23:36', NULL, NULL, NULL),
(15, 0, NULL, 'customer', 'ANDREW bernstein', '', '', '', '', '', '', '', '', '', '', 'apb9974@gmail.com', '9173041357', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cUiP3OXOGgg:APA91bGmjxKrgmeyEiYO-fgiTwuynIX2iVdZeF2MWlYpMGSnD3c2G5kLYjJX45f5msKantQuAIOZpUQiVqHOVYvq_5if43nZXoX6ogZd4xsAkZB7qOW38VZ81-QkUnGGCX9HRvjtLJuA', '', NULL, '', '', 1, 0, '2020-12-14 20:21:17', NULL, NULL, NULL),
(16, 0, NULL, 'customer', 'Ang', '', '', '', '', '', '', '', '', '', '', 'angsoter@gmail.com', '8605972276', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cHHItz3Jy5k:APA91bEq6SLAFxkRgcYiMQAp-BbFHj42-AO8AEt47XAhvIejCfgS3CLghcrp_tMeoWw_HNXi_FQelFuw0aky-4o4Qf0hAGIX6CCHFklwlzkj3uRi-ItxKNTul0ed2VlsaKa3-vBnUeFe', '', NULL, '', '', 1, 0, '2020-12-15 04:32:42', NULL, NULL, NULL),
(17, 0, NULL, 'customer', 'Jaime Lemus', '', '', '', '', '', '', '', '', '', '', 'lemusdavid23@gmail.con', '6178608770', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '1', 'dHM0Kixsu68:APA91bG8PzHAWkfAy-pm1mlrNemXSFrKpV2VDxJV4X9alMwDRR8Sz6ryYIvgdnfCfATVXEe3NvLgudyWcUC-C3CjjdRATzeklEQj9K2OK3ey_G0TqUhJgAEa3fnwXLdLFhpZwfJEaYc1', '', NULL, '', '', 1, 0, '2020-12-15 15:02:09', NULL, NULL, NULL),
(18, 0, NULL, 'customer', 'anthony laurenzi', '', '', '', '', '', '', '', '', '', '', 'anthonylisalaurenzi@gmail.com', '9735131670', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fILtzbW6z1o:APA91bGEw8eIFQ8xCyNm0qpXradYh5lJgMMRnobH4hvzN4-lXOWjg9VrtH9MrBND-fvDWQs0xAXZFe28yn0VZtdUM_tDVFuqbDHeVcRat27LvROdvw3q2IEcZYZAkOJymxEVR6iLVYBT', '', NULL, '', '', 1, 0, '2020-12-15 18:02:34', NULL, NULL, NULL),
(19, 0, NULL, 'CUSTOMER', 'Jim Hyde', '', '', '', '', '', '', '', '', '', '', 'hyde.jim@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fzTLU5d0-a4:APA91bGpgXhqV6SrgaF9UKzvE2wlrwZrDA4qKBiMoEGRj8mIKD-CbR70b1B9ldSlPk17cgX7wUHvelxuj_o7Qn2egfA79Tkhf7qHNduBfvGtZ9s_9DmQb-Q8SG6RqOA27rV8-GMzPQpO', '', NULL, '', '', 1, 0, '2020-12-15 18:29:23', NULL, NULL, NULL),
(20, 0, NULL, 'CUSTOMER', 'Michelle Goodman', '', '', '', '', '', '', '', '', '', '', 'shelarella@aol.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eSPkeC2J38Q:APA91bEBnUIOtC4FaPhjFWMl7alVs7wly8htNh-8pdDhaaxdUVBNnsq9xnoXb-q7L6gsRq9g46P-Q1uWxAdlg1nS6J09pKXh6IyqlW6Ep8wT-osXSzwX4naUtm8ST4Fx4DOQshtrV22h', '', NULL, '', '', 1, 0, '2020-12-15 20:22:59', NULL, NULL, NULL),
(21, 0, NULL, 'customer', 'Bridget', '', '', '', '', '', '', '', '', '', '', 'bridget_mccullough@verizon.net', '6105643952', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cnKzLJDIVE0:APA91bG-iT6yBRKsrt_mcxQJoGeEnvNP-S4rjmSiIoWQKpc3mDi0iXbMekSIB1EjUr5CofRLET9iC8xeNfQ-sk-91EmP44IjPjoDMAVUC5KFnEsqSkUnVNUQkgY5kRnMfyzuay6oyr4v', '', NULL, '', '', 1, 0, '2020-12-15 22:10:40', NULL, NULL, NULL),
(22, 0, NULL, 'customer', 'Roy Patterson', '', '', '', '', '', '', '', '', '', '', 'rpatter532@yahoo.com', '9176717472', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'daIAG1hfAxg:APA91bGmdmi2ocY7Ssku6xytYxHil8Rv4VXiL6nzdz0ZkVWAmTd0CVGIx2U-cP5ufRPC8XE4_ooPD-tQSpccqvzZIHJIKxhQi1wiI41BkMlxL-pu-iIzEhVcXqDKE6V4AK52Dljl4yMc', '', NULL, '', '', 1, 0, '2020-12-16 10:15:33', NULL, NULL, NULL),
(23, 0, NULL, 'customer', 'Michele Joseph', '', '', '', '', '', '', '', '', '', '', 'michelepellerito@gmail.com', '2013163200', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eKtCSzFZyG0:APA91bHRPi9DZy7wuK7W-jK1F-bC-zeD9DvMDGr7vQn7-7gUM9pDwWZC_fgMqFhY0 1TdILPnPBFikHWIEvpJ9XDZVH0KySfA6neoo8pSai951OStGUgO7l-8fm2v8UYO2tpuZlVyK5', '', NULL, '', '', 1, 0, '2020-12-16 12:36:05', NULL, NULL, NULL),
(24, 0, NULL, 'customer', 'Michael', '', '', '', '', '', '', '', '', '', '', 'spike456654@yahoo.com', '4015332616', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'etmTGjL-Hrk:APA91bGiM2M399JlOz6IzZxINX9UAJsuPwWr8uA_5qos72J80kV-yBzT8dYplFwwdWY0qBm8kDunM4mZKOhYq_ovkppXhepWH23AJ7BYD2-KjamDlE_m0zG7lLQGGU5l3TRylywft0jU', '', NULL, '', '', 1, 0, '2020-12-16 15:45:07', NULL, NULL, NULL),
(25, 0, NULL, 'customer', 'H Flower', '', '', '', '', '', '', '', '', '', '', 'hnrquintanilla@gmail.com', '4435346621', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '1', 'cb5NANhgN-c:APA91bGVYapWoUSnsB_zlVrvYqPFXESW7gAWe0THbCDSg7QCVW6D4Vemigqf-YPFbW4fXWaUNV-8pAw6WSnPLy3B8bxL-33YnoQJyrD6nXy7DR_ybCG-Jea8oWwSX7qLrhhT1Vu9-fOb', '', NULL, '', '', 1, 0, '2020-12-16 16:06:45', NULL, NULL, NULL),
(26, 0, NULL, 'customer', 'Andrew hill', '', '', '', '', '', '', '', '', '', '', 'allindetailswc@gmail.com', '6108833490', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eKngrSenIO4:APA91bGMC4uiGkoh-dnNlzQTVgD9dckaKG0n4m1YJ6YCa1QI-AEsUzkI0-WNvB0J4FOeVG2zoSpN4h-5eLJhr7_Qjk39HQuCyygVY_1J7HMZaRXljosKsqnO02ETBus09Xy0YK7bX4Cl', '', NULL, '', '', 1, 0, '2020-12-16 17:17:46', NULL, NULL, NULL),
(27, 0, NULL, 'CUSTOMER', 'Damian Kennedy', '', '', '', '', '', '', '', '', '', '', 'dmk1226@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'd7yjYHSGImU:APA91bFU4gENv-YSioY-orrgUWHsYRjyVASmo-SnEpmTcMK8E13XWwIn-76s6SF-RUJ3qh-PROSWbdJWme8LXhUU7uzrGm2RA7MgXxF_YrIAUCZQio5QppJqFHuPCQphSJL8AedWq5nT', '', NULL, '', '', 1, 0, '2020-12-16 17:30:51', NULL, NULL, NULL),
(28, 0, NULL, 'CUSTOMER', 'Michael Dwyer', '', '', '', '', '', '', '', '', '', '', 'michaeledwyer@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'd0CyOIRqgME:APA91bGbeBff3sJvQ4c5O5vDhsyiT0Itq9jwoEOIvD2E02a4kIkBtwWRo5a0pHQBH_pE3oS8v0iTrYsbaee2OaqJGNutT5VxZsbC7Y5sVecQclitbOFetqUk-_vscBQdibl86butlH7y', '', NULL, '', '', 1, 0, '2020-12-16 21:09:08', NULL, NULL, NULL),
(29, 0, NULL, 'CUSTOMER', 'Matthew Roman', '', '', '', '', '', '', '', '', '', '', 'romanmatthew98@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '1', 'fEYl_Uvbgj0:APA91bFlFjyr8tzk646a2HffB_ghDzOi4GFohs-TQ1rUZQCFSm5GWg6US4U0-l4b73pKVWvEGepODyubEfVrM9IofMOKjPf0MhyoaS-GfaxshbObN4yG56ObuodQjqRyMiyXQAHE3uXt', '', NULL, '', '', 1, 0, '2020-12-17 01:35:56', NULL, NULL, NULL),
(30, 0, NULL, 'DRIVER', 'luis rodriguez', '', '', '', '40 Hampton st', 'new Britain', 'Connecticut', '06053', '', '', '', 'onestopwireless42@yahoo.com', '9592001230', 'qIqqszgpEo', '', '1608170154_1608170148_profileimage.jpg', '', '', '', 0, 1, '583-97-8521', '1608170154_1608170148_ssnumberimage.jpg', '10', 'new Britain', '177544545', '1608170154_1608170148_drivinglicenseimage.jpg', 'State Farm', '1608170154_1608170148_driverinsurancenoimage.jpg', '', '', '', NULL, '', '30-luis-rodriguez', 0, 0, '2020-12-17 01:55:54', NULL, NULL, NULL),
(31, 0, NULL, 'CUSTOMER', 'Lito Rodriguez', '', '', '', '', '', '', '', '', '', '', 'onestopwireless41@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '1', 'eLKQ7_AdCJo:APA91bHIjtBMXsdcMm9iAlm6BxHEoJBbn2FoCSASz2cusLBijxi8U3OzY5w398T-xoxvHhDExd3BavqLE7iF_2UtDuw8YGmQoTxhkKckKN2YGROLdgYip4qSCpGv6X2pxp8k_bGqChSy', '', NULL, '', '', 1, 0, '2020-12-17 01:56:41', NULL, NULL, NULL),
(32, 0, NULL, 'customer', 'preeti', '', '', '', '', '', '', '', '', '', '', 'preethibasava1802@gmail.com', '3473650131', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'ctkj3PveYqQ:APA91bFfJZ4Wmrs50nwQP1_mrgK68Xuzg_LSaZUEAEbsLjCCJJh9I7RmYgIihDw3_P5gHRktc2tdUG3uzO2SQD-xgj8YaOmcCdKVZS0RNDfWt1t4z3kevbkcGu52gDsGgPBaeBrqXdT4', '', NULL, '', '', 1, 0, '2020-12-17 02:05:43', NULL, NULL, NULL),
(33, 0, NULL, 'customer', 'Melvin Tejada', '', '', '', '', '', '', '', '', '', '', 'Melvin5710@live.com', '2014718456', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dDWFeqP13Wo:APA91bEE9nQZcm-FKLWdaCJ_bf-NaMYvs4SRWLCUhpNo3h-bpWgh2Tzd1r3CQSmFeoBPpFnS0W5re6YWhtb-G6xA06IL2gc8878HcwZ2pelM_CP10s9Mdt_9I9DWzUcOd9ifkFcYfGy1', '', NULL, '', '', 1, 0, '2020-12-17 04:48:10', NULL, NULL, NULL),
(34, 0, NULL, 'customer', 'Franklin', '', '', '', '', '', '', '', '', '', '', 'fttzx_69@ymail.com', '5168510241', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eHYlkRzfGsw:APA91bFs9vjQu8GxqWc0ZLCU_MEU76NGwYnRCUmr9Y8rTHMQm7lFkEyx8e9JER7SncbclggcIKnhy9n5jxPwKfcB3oMvfzs77UZqIJmoSizXEKwO4Yt-w5Y1Ydi4IjtX9WCpW8nRvT9Y', '', NULL, '', '', 1, 0, '2020-12-17 05:14:03', NULL, NULL, NULL),
(35, 0, NULL, 'customer', 'Rick Brown', '', '', '', '', '', '', '', '', '', '', 'rickbrown410@ymail.com', '5082598097', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '1', 'eJzXe21QZz8:APA91bG0xxmA0c9qcVlhw-pDEuDufFt1v3H2n1VzdjM2zLBOc3yYgNJpyt5kR-4nF2Bb2pSAFKx3AXTo9yfZQ36gpiBsX8-tFeHHrUL_p2pIjp9K4Q_4zLs4U6hJmUT3AoejAfYcXlr4', '', NULL, '', '', 1, 0, '2020-12-17 05:25:40', NULL, NULL, NULL),
(36, 0, NULL, 'customer', 'Joseph Burke', '', '', '', '', '', '', '', '', '', '', 'joeyb2185@aol.com', '8572476661', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cJbbh65OrZU:APA91bEkBVKByl-5UNfL7KhwutiqlGoG-c3HxlrFOzrnWvVvVEo5t01x1C6bA-nS1c-k44JZBynqO2OG0XJzunuayx5rEY2AN4KCPIQu_WuaOavzZ5Ay6VRf4XjUlpXN1etqTR7Rxqxq', '', NULL, '', '', 1, 0, '2020-12-17 05:34:25', NULL, NULL, NULL),
(37, 0, NULL, 'customer', 'THOMAS', '', '', '', '', '', '', '', '', '', '', 'Thomasre1989@gmail.com', '3474852455', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dDBgsvufvCY:APA91bEbd2A9b8WsNbkZc1MlWeCS0dYBAj-7Hy5JWLjOflP2d1AZl7x9fn41dINYMnTQAqV-wK9R6YV0MXF-fzJOsLuJXkSvTWU1zzb3fu9PTu-8mVrQjZ6pd6l7vpPu-LzAD6df7-6n', '', NULL, '', '', 1, 0, '2020-12-17 07:16:12', NULL, NULL, NULL),
(38, 0, NULL, 'customer', 'Deirdre Tucker', '', '', '', '', '', '', '', '', '', '', 'ddtucker07@gmail.com', '7185015770', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'c_mNIHXRg4A:APA91bH0V1rmRF9Ct2FEJ2X2nEyUM3usyDDOy2YIHcnP3WgGwk1RSX8e7cpiIHZSxgWEEn13ghF43tKzKDN8jT6yzAxsWb84vFyRlR28sWNbmT0lPST7Uihp_NiWeOSzBrxD14V6lgk9', '', NULL, '', '', 1, 0, '2020-12-17 08:57:27', NULL, NULL, NULL),
(39, 0, NULL, 'customer', 'Erik Johnson', '', '', '', '', '', '', '', '', '', '', 'johnsonerik65@yahoo.com', '9085778647', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fPj_Mq4WNCc:APA91bEQDbFCfF1A0IFQnQJg81Nslp_PpiBamp6OE5AddVbcSkgc0AtHTSqFAZqPgiy2cqdeuDsAn-WoEmaI_2gg4VdgRrWUWOIX50QQ28UBRLcer85XpUVBR3xj_FM30u1xRJfatd9M', '', NULL, '', '', 1, 0, '2020-12-17 11:00:33', NULL, NULL, NULL),
(40, 0, NULL, 'CUSTOMER', 'mo M', '', '', '', '', '', '', '', '', '', '', 'mmoccia777@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'f6S6eH8QTO8:APA91bEmqos7-ux641fXaEv3TCq2Q_b4YRsrQPdk9z8E5TlTyghCqnQ7PFJafZ4Xt6-fryGAGBZKgY8d2y62uo4tmoHm1CuPmi5pkv9pCYa867If74QJj-LQGXSxwAIAQXarwNHWlqtW', '', NULL, '', '', 1, 0, '2020-12-17 11:22:18', NULL, NULL, NULL),
(41, 0, NULL, 'customer', 'Aaron', '', '', '', '', '', '', '', '', '', '', 'pway314@aol.com', '2817884255', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cnJSE0-_pr0:APA91bG7FW0vo9Y548z37A2bs-U8kHXcV_0W-jJhUwW7gVlu08uWcf4yaorT1fQNHwSIDsrbUuYc-_dxzWtmnZok9BOCnUJ0VjI-5jAunEzaOPekYK9sS_C6Lfq7oL_7EuFilWZUg7n9', '', NULL, '', '', 1, 0, '2020-12-17 12:26:57', NULL, NULL, NULL),
(42, 0, NULL, 'CUSTOMER', 'Steve Amshen', '', '', '', '', '', '', '', '', '', '', 'steveamshen@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'c2ceoDUrx7E:APA91bE9gqtZUcvr8zPHJ8gOT1UOBcr_QEkZRdbB5bAqvU6utRPgExlwmxn6jvwHUmOEvjgcf_n-VLpCBrn6htRnnlz_uAykiTu8_emqKBebRjpDZKv5YENqT469kpm1XGBPEqtE0TUx', '', NULL, '', '', 1, 0, '2020-12-17 12:30:40', NULL, NULL, NULL),
(43, 0, NULL, 'customer', 'shame Galus', '', '', '', '', '', '', '', '', '', '', 's.galus2019@gmail.com', '7743190334', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fGFGJiYDSTg:APA91bHZb5iXuc7BbF6JVz60-VDll5W9TV15NS4ImlOaaV2wpiTiHzBH6kZyMchXfw_Mpd6EvntggHfZyEA796RkcHRbQZHjDNFNEX6My4C8CCETt_Jv_dl-wgKtEqltDtbKA80YR4au', '', NULL, '', '', 1, 0, '2020-12-17 12:59:07', NULL, NULL, NULL),
(44, 0, NULL, 'CUSTOMER', 'Cristopher Vasquez', '', '', '', '', '', '', '', '', '', '', 'laststopservices23@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '1', 'f1kZT89eZX4:APA91bGS-LJZUjP93nGYJnoa5y4l-9eahHdbFxn2e1ifsVy1sX3QNZ1fveFxZQPXVhpq36no7ZtQ1mBQXBydjf1QXsCOHQwQ4okzV8jFz_yiFqzOMm_rJNiIT7lrZguiCeSeE4dYqeh6', '', NULL, '', '', 1, 0, '2020-12-17 13:19:04', NULL, NULL, NULL),
(45, 0, NULL, 'customer', 'Lauren', '', '', '', '', '', '', '', '', '', '', 'Lauren.kasper@mac.com', '9739536089', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dsl_94rnoMw:APA91bELk649Louu4itzSPFw-slNNFlgpOSZ1s6zsp_JuFzgELYcQkHDqkcOui44MrWjgzH_6ciQ-d_ytlzRRl7aSkhW84ncJdzNy-F6LvIyLhYMJTs-gVRD2uZXHlm1DzfH-3vePo3x', '', NULL, '', '', 1, 0, '2020-12-17 13:19:42', NULL, NULL, NULL),
(46, 0, NULL, 'customer', 'Jeffrey', '', '', '', '', '', '', '', '', '', '', 'sales@giftsofjoy.co', '2034173410', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fmXhBC66sq0:APA91bF_vK3KQVFaYzJPovvD91jpF-FDk4G-NztUBkvQ-gcJ39W9i6BVwFhk82-20AVtoI7mtypj3wcOoDMUfovvdG60gTMEAU18TdsC4q5IKuhTteorp8hdg8bfb7LreEpH3gI-KRgK', '', NULL, '', '', 1, 0, '2020-12-17 13:26:15', NULL, NULL, NULL),
(47, 0, NULL, 'customer', 'Louis Bishop', '', '', '', '', '', '', '', '', '', '', 'lbishop@pronetisp.net', '6077618095', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fQ2-m71ta1s:APA91bEtjeOxammUwM7d615elGydEgm1-lfh-PIOX5UPK4fIzp6e1GOgg9SmrRaRUXEq81Iekon5Gpe0kBqWvusRkszSTBP-zbtLFCxsGisC4H0Glxdvsqgugm0vrlUM8Q0gWxHva5KG', '', NULL, '', '', 1, 0, '2020-12-17 13:51:28', NULL, NULL, NULL),
(48, 0, NULL, 'customer', 'Darren', '', '', '', '', '', '', '', '', '', '', 'DarrenOkin@gmail.com', '5165573238', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eR9DhoSlDNc:APA91bGrHo9oEpoLnoraxvTMCoLAiGOvJQGGPvhP_ZYq2dJfr3hIpYkh-lF4fyzTDKsq_6UnxKIxW1ons6NtClsHevFika5Y5N4FYJ1sOVAn7V6Zel8RFo195-66XD1DGJ9U4_Q89GCV', '', NULL, '', '', 1, 0, '2020-12-17 14:04:59', NULL, NULL, NULL),
(49, 0, NULL, 'customer', 'aarfat kazi', '', '', '', '', '', '', '', '', '', '', 'aarfat@miskshoppe.com', '6096477037', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fHNuSHKyKwM:APA91bEXYlHDjn2Ah1qMEL_r4NX9Kh_MZ33Axlid_ewGn0bT8oNDo4Ygz4qJ03MCxyCNEiKlzFb4Y3LjMr6KK3SBf5vLIYko3Qzv0Whtnw8QuLbsxzCx1zV1wqPWkuZZ_xWQMwO7ZTjo', '', NULL, '', '', 1, 0, '2020-12-17 14:15:06', NULL, NULL, NULL),
(50, 0, NULL, 'CUSTOMER', 'Rahul Chaudhary', '', '', '', '', '', '', '', '', '', '', 'rahul.c.chaudhary@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'e-cXC73utCI:APA91bF7Gbr8xLbPmTD0VFA9ZvdhaqJg5ohVZJKap4dVvIyoJq1Fy1ZzEDmSyKVpffDRBskWCokLEYauTUw6Oy8ZTlU-xnNrzy9yRnzAZiL461wgQhHR7krh2tTLhzY58dEwCxcQkw4c', '', NULL, '', '', 1, 0, '2020-12-17 14:18:07', NULL, NULL, NULL),
(51, 0, NULL, 'customer', 'Nancy', '', '', '', '', '', '', '', '', '', '', 'nmilne1@yahoo.com', '4014508847', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '1', 'cm6S8hnS_jA:APA91bHIBcdLAz5X4FsU3_tozUuWpWu9YYsTFi1DPcQi5seFfroM9ttRkEksl8FuiIVmEHILgnBWj2D5lKo5umNNPEl0fDiyb3_5L8qBmcjmcJxT0ieHtMBxqYbeT0NezKPC1CAORPPP', '', NULL, '', '', 1, 0, '2020-12-17 14:29:02', NULL, NULL, NULL),
(52, 0, NULL, 'CUSTOMER', 'Patricia Fitzgerald', '', '', '', '', '', '', '', '', '', '', 'pfitzgerald@njl2l.org', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dorLKx5lsyE:APA91bErQD8wYSWSe_QA3lZJ8S-zGzRb_x6-458sM9nZ55-LDbwStTE71jRi_H_A0jbAk1eMqJPlQmkDMQPyEnFG8_OFr8HUtvWgN7w8TiGQuZeOix-IRxNBZdK50N6NItUOJmkDX-4Z', '', NULL, '', '', 1, 0, '2020-12-17 14:36:35', NULL, NULL, NULL),
(53, 0, NULL, 'customer', 'Kevin Quinn', '', '', '', '', '', '', '', '', '', '', 'qparma@yahoo.com', '7326877929', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'c5pz3OB_5R4:APA91bFOYtco228h8jY-UdeH8Mufc62rRX1MB7HqFfZr4Gu9duYTfuHvN2IRimHXPY2wL1vWAulKlzshjWa_1Y8caIlY30tikJTDAK9a93OrYvHCrB3Bxfh_PzOuT-Yg3-050jHpvMxo', '', NULL, '', '', 1, 0, '2020-12-17 15:04:09', NULL, NULL, NULL),
(54, 0, NULL, 'customer', 'danielle', '', '', '', '', '', '', '', '', '', '', 'mendola_d@yahoo.com', '8457977558', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dw6enbSgl80:APA91bGrbZkPIB-4LcS1gBhFuicdtdK03l_rdi_E3-MYOjTxSaDDsip7-YE_vXhkmxfjoSx5D6Fu6xVSymujswc_SJsvdYsP-fuzJ9MZi6L4XcHkTo8V84ixXB9u8cHWEn65FKZSaP2e', '', NULL, '', '', 1, 0, '2020-12-17 15:06:34', NULL, NULL, NULL),
(55, 0, NULL, 'customer', 'Tristar To', '', '', '', '', '', '', '', '', '', '', 'to.tristar@gmail.com', '7812151396', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fv3qtUdgBEo:APA91bEFlxnpB8M4LJswkBW5BywnyXsToTGQ6eZ9ktOH74CylaVlaZEVIeW1MhyqoFg_Mefpd6L2PObIqfrLsGwXdsDx5kJfjP59GV0DBZR9PW_JNcFl7T6ah0P_ITrJ6lUnjGuOXAri', '', NULL, '', '', 1, 0, '2020-12-17 15:27:48', NULL, NULL, NULL),
(56, 0, NULL, 'customer', 'Kim', '', '', '', '', '', '', '', '', '', '', 'Kim.huynh0196@gmail.com', '8606188488', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fSyJ1XaDK5Q:APA91bEZoVIuWV86jGj4KwodB7q50D-hkhybSzhhbpAGuigMe0cdmk_o7bRfG7oUy2fWxh0pqr6w4fPeEdaohM_exN3sxC8WfVelXCkdWn69UiP__MqJ8L6HKLYR8LZ2Nk3lfE4NvxTD', '', NULL, '', '', 1, 0, '2020-12-17 15:45:02', NULL, NULL, NULL),
(57, 0, NULL, 'CUSTOMER', 'Denniz Balucha', '', '', '', '', '', '', '', '', '', '', 'dennizbalucha@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '1', 'cY IgSjiQA:APA91bEqp3RX81Gl6FZcic2OKO_Er-U1XZVykFjD_ixNiM2of6R_T9UkrSAgrfQXrBCFIkn9S80XbNfl2zWev0MZVcCzLSoMtmpHzi8NkfUCe6JADo_lNE6VIpJA1r-P4SPvV0mO1WOy', '', NULL, '', '', 1, 0, '2020-12-17 16:35:29', NULL, NULL, NULL),
(58, 0, NULL, 'customer', 'Melissa P', '', '', '', '', '', '', '', '', '', '', 'powers.mel@gmail.com', '2013942408', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'f8x24GYQSfI:APA91bFxGGJ_BH7i9eLFlY7tiE3fLdVQ77Lr5fF2x-oK88sz56WQzuqgQnqCXdH-R2yJ2yEgl9pQRe1W04-0TwOOMddCRSGvTstGKN8aY95iH_NKRuRFp9PnfuhoQILFQholglZYktKo', '', NULL, '', '', 1, 0, '2020-12-17 16:39:04', NULL, NULL, NULL),
(59, 0, NULL, 'customer', 'Michael Amin', '', '', '', '', '', '', '', '', '', '', 'mike@ruscologistics.com', '5164461261', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dPZTBc9iaS8:APA91bEED8mPJuX0PZ7b_XMQ1SSysbQXN-eV1Ar7xJbmapehKA5W2h8M_fo8fhIruWE14d5Q2F3ggIQOFiRttiYeHJot5Pd_X_tcNUqrtrP3U56FFqKMbjGc8LMgaqThLO6nNpKnP0Ce', '', NULL, '', '', 1, 0, '2020-12-17 16:58:19', NULL, NULL, NULL),
(60, 0, NULL, 'customer', 'Eric', '', '', '', '', '', '', '', '', '', '', 'iphoneric@icloud.com', '9733370875', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cZqH_2IN5ls:APA91bG8N5feOuk_wbvtL0Hb7Sk2MS-yUDHLcfuSmzc60kefsV4FwO32f2rxMu8rbnhFaLNaTY80tY58AZUYuz8AQ8p4ksmmi7ZA32JpVUtfQECNUY5RAluBiMl8IoCkvc2gHwOJeCLm', '', NULL, '', '', 1, 0, '2020-12-17 17:19:09', NULL, NULL, NULL),
(61, 0, NULL, 'customer', 'Mark', '', '', '', '', '', '', '', '', '', '', 'mark.mcbride@me.com', '4019542648', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'f3nhXtijtds:APA91bGkDGtlgHuLQYbSy-JSdC1tx318sidw9xK6xOIgRYAw_Quz6IutBKc-DLgwFKq-Y0t62U41FVaOBn585FaPbhxcef7Y7bwjTCMquFyAQ1tZQeL90aCg3X0CE7YTWfmaBPRoWgye', '', NULL, '', '', 1, 0, '2020-12-17 17:20:03', NULL, NULL, NULL),
(62, 0, NULL, 'customer', 'Bob Soloff', '', '', '', '', '', '', '', '', '', '', 'bobsoloff@gmail.com', '7182086071', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dt6OfccQQXo:APA91bEuifYJKc7BBbEqPDjQC4_RWhhMHu_VVjOgU68py3U080Ab1a1OoZq-_cXm4sEC_j7bPrdf2dh15612S7SvPCTD9jD1TzSrt3Tl56RJ1YbiQvbYODq_nvj08I3p6TOVbhn0Z6DV', '', NULL, '', '', 1, 0, '2020-12-17 17:41:16', NULL, NULL, NULL),
(63, 0, NULL, 'customer', 'alice ngowi', '', '', '', '', '', '', '', '', '', '', 'misnagowi@gmail.com', '6316642590', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cmy96Eh_R6w:APA91bE1z2q7NIn_5pLeijj2M-GziMEgsyTzvFF572db5bcdKlFzEGE7AxPncmWCI3JVo2JZoFDhd2VqaZk4POQsnJ_eHkBXf5ePZJO2VhwUdsnsv0d_F1JgT8zAM0-v38qneqvkS14H', '', NULL, '', '', 1, 0, '2020-12-17 17:50:01', NULL, NULL, NULL),
(64, 0, NULL, 'customer', 'jay', '', '', '', '', '', '', '', '', '', '', 'ZDragon007@aol.com', '2036732764', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eM8tQPLti5U:APA91bHa2vzfTNftImjgoEac-UQ4WxMzn-KVdxMI5jozfosMg343jU3kl6ZV1rhBOh1SvOdw_fM8qx2ke9TDjT6Fb4fPeLiVaE6mbi3-QIXwYewN7GuuFGgILglryKHA22YXJmRtAdrJ', '', NULL, '', '', 1, 0, '2020-12-17 17:55:34', NULL, NULL, NULL),
(65, 0, NULL, 'customer', 'Brian', '', '', '', '', '', '', '', '', '', '', 'brian.johnstonct@gmail.com', '8606557865', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cpB020X5h94:APA91bGJDzEMZ0w40fc2ktEFCwv4KG11tLy2Y0uk_nIp7Z6A-qiPznFbufDyZ4nL7j3b0cC9EEqk4N8FHtxWbCVKCc3oHSMo185QgODnYASZJa9k4nI8_dvDCFgeIQrGD2UUIf76EbpR', '', NULL, '', '', 1, 0, '2020-12-17 18:22:34', NULL, NULL, NULL),
(66, 0, NULL, 'customer', 'julia cataldo', '', '', '', '', '', '', '', '', '', '', 'j_cataldo@aol.com', '6318715428', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fsFyhsU9vW8:APA91bHZNmbrLqsLhtd1AA97aOzV2WoSvFtyYafQL-uKikD4KUz6-rbCXRWB0R8EwPJ50OYCXDt96aoAc-YpPBmcAcKA6RXaflLgR_b85hEfiY9_HTst1sj7HUUaWznNFqXfVYI0pz4t', '', NULL, '', '', 1, 0, '2020-12-17 18:36:47', NULL, NULL, NULL),
(67, 0, NULL, 'customer', 'stephane Bellande', '', '', '', '', '', '', '', '', '', '', 'stephan7b7@comcast.net', '8575261360', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'd5cZPnnRP5k:APA91bHFiSwAG4KI1ji-AUnJwSKqWRpcg2zG2xY1bLLnw9XAdU_kTSLcfFNAGNlv1glVWtJ-vqxNspnB9CXXUDafUxHp4GVr0MtRfqMZ2ex9BZEcDRSfxmqn_qJNdLocqMgZ7DeR3Gry', '', NULL, '', '', 1, 0, '2020-12-17 18:37:00', NULL, NULL, NULL),
(68, 0, NULL, 'customer', 'sadi', '', '', '', '', '', '', '', '', '', '', 'sadiqa@aya.Yale.Edu', '2036457021', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dCUualDLAwc:APA91bGO1vLuM5HIgn4r-iSiJTRC7ijbgoLDf2f5LFaEIQRciz6McTzQgXlybnMlS00SJ2tSTJKnLe8V9yx86Ua7RnUPo6aUKRf56TC8b4M-GXpkl7Cr3nx1091iG9lY2Ld9sT_QDj4G', '', NULL, '', '', 1, 0, '2020-12-17 18:56:01', NULL, NULL, NULL),
(69, 0, NULL, 'customer', 'Justin Hayward', '', '', '', '', '', '', '', '', '', '', 'justinhayward88@icloud.com', '9733883704', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eHWNhsLmAJc:APA91bEh-Lu7mdH7vWyAFm3NRSWp8HbfZBdv8DV0oQ_GQTmhJbj288lIF1OA9NoWfs7QrH9FwQh0hAbru1TgEaxRNB3rhRVRvryHGdTobR_sTBFj_nuslruF9uh7II9u_RduS4WsZrh0', '', NULL, '', '', 1, 0, '2020-12-17 19:15:52', NULL, NULL, NULL),
(70, 0, NULL, 'customer', 'Jamie oleary', '', '', '', '', '', '', '', '', '', '', 'joleary1976@yahoo.com', '2034349686', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cwJ30lMcGVA:APA91bFNqypj2iCG1mbp0lyCNrMvwai-ZEg-U29MnEN8QzA1XjNo56ZuV5baqaX8Dq2HQOv0J5kXje2eC_lToHZUqcVN8Jjr37o2STVeQ-VvT8LmEHE0cMLdk9qW2UjTbs8t7FIw3fgY', '', NULL, '', '', 1, 0, '2020-12-17 19:35:17', NULL, NULL, NULL),
(71, 0, NULL, 'CUSTOMER', 'jason huskie', '', '', '', '', '', '', '', '', '', '', 'jason.huskie@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'du46zCL66zg:APA91bEPoiO8frDHVFF7BSRwGFXJ-ElDb71kuHbOld8jWD_sbhDXrV455LrrtVfmtwTRDvAagjskXASJiqTitDR-CvjAATySBIryKlMyASnJRnTPopaKifYe0MBdP9qqjkxblKPYnZ2B', '', NULL, '', '', 1, 0, '2020-12-17 19:43:19', NULL, NULL, NULL),
(72, 0, NULL, 'customer', 'Nicholas Kosiavelon', '', '', '', '', '', '', '', '', '', '', 'Kosiavelon@yahoo.com', '6178521143', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dKwTAljmk9k:APA91bElKu1BN-a7zReZXRcKAoOQxoMIYLMDeNJECWWfBZ7n-eG36pea3_D2A4ERvWkPiQ67Oceh8tXKCu-PQ9cj9-PkSidNOWS7O2PRoBmwjdTkkdOH9RgdkBqlU6efJuMbMhL_SDn5', '', NULL, '', '', 1, 0, '2020-12-17 20:48:57', NULL, NULL, NULL),
(73, 0, NULL, 'CUSTOMER', 'mary ann alexander-ellis', '', '', '', '', '', '', '', '', '', '', 'maryannalexanderellis@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eghvL5ukSFQ:APA91bGrHuR_64p6fZEcRn5wrkWI6xALY5CS2pZdTVY9VGZNlGy3KHRtu7N3zKuGrL8ip9bZIxvVpYj7mLKKieuD6W5a2PvGluf6AW7jnLkguHPsv8o9_AH3d_H-pjQEsVdM7ndLd_et', '', NULL, '', '', 1, 0, '2020-12-17 21:33:05', NULL, NULL, NULL),
(74, 0, NULL, 'CUSTOMER', 'Kevin Muschett', '', '', '', '', '', '', '', '', '', '', 'kevinmuschett@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eeKt0Wt3aLc:APA91bFixW1UCiw-RWhuYoTpB8KeFqsynGQetG6XL2mljUhYZvVDHR91KMh5GDRyH9ltryIb-0gTxWdd0e0SUXuXFkJWo0wZCuLan_Au1wIV7Y-ttXtGJimijpld3vw_Q1l9OiAFNaA-', '', NULL, '', '', 1, 0, '2020-12-17 22:04:33', NULL, NULL, NULL),
(75, 0, NULL, 'customer', 'Patrick Young', '', '', '', '', '', '', '', '', '', '', 'patyoungpiano@gmail.com', '5183685430', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fc_ajjUCmgE:APA91bG4yxYd3euKNb_EVz_pMFVrDLUg4F4hL4euQDgPMEKwPo16lwBRwSrnCmTWdhIgg7J_BXxei18mQkkNdKUM_JF8v7KIeHYrrFwu-dIAg8_yAyCPu56CP1U5qYWAjxE2SfcJePDD', '', NULL, '', '', 0, 1, '2020-12-17 22:24:58', NULL, '2020-12-17 16:25:50', NULL),
(76, 0, NULL, 'customer', 'Jacob Bonzo', '', '', '', '', '', '', '', '', '', '', 'bonzamo10@yahoo.com', '2147556641', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cROJ_xXWbDM:APA91bEWeqLuEC_olRQhiKNByZgBwV8IVm3BCm7lFdB2_UQdmu0IHWqKnl5x_xqbT9Es_hyPshtkZPSmh1o3HnO0RyQotHkNvMd5aXOzahB2rXHgFOBgAr23XIKNt1JaJufNLd3SMgN-', '', NULL, '', '', 1, 0, '2020-12-17 22:52:03', NULL, NULL, NULL),
(77, 0, NULL, 'customer', 'David G', '', '', '', '', '', '', '', '', '', '', 'davidjgaul@gmail.com', '9089074313', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cwSyyE-VgvI:APA91bFVUXA_ygnJTbVibHluL43Pw8pRH0mdj2bQwC2x0mg9V1wr0OxGea5q_MJefXP6s25rGUDjclW55X1uA71YaAjrxM_gLnDix5IFchUupsupsVPc7HRHOYe7WHG3j13Huxei8KMJ', '', NULL, '', '', 1, 0, '2020-12-17 23:18:12', NULL, NULL, NULL),
(78, 0, NULL, 'CUSTOMER', 'Alex Poholek', '', '', '', '', '', '', '', '', '', '', '80ihscout2@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'e8iBPrFV-ms:APA91bGgiinLMsjeNfZwIs5mYf30xlc5bxu0FS3afgnmX0HzVvO9MGFHhvPquN30apMuvuUc8YAmvQTr-W2WKmPAF32ndnTX6BWhO8XEGrhuq9xvrrNMz12gcOAzwc6EaaBLvulSnM9S', '', NULL, '', '', 1, 0, '2020-12-17 23:57:10', NULL, NULL, NULL),
(79, 0, NULL, 'CUSTOMER', 'Joanne McCaffrey', '', '', '', '', '', '', '', '', '', '', 'joanne122937@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dcZ7hP45AZ8:APA91bELJgIevuSfry6kboMTLfEOPxAz7bu3RJbT7RR_XgNxW2eBWRoNjayf7JHs1v9ocjO4fBdMMV3RIUDGeFCKC1yKWGaZvGAouOp2WumOIRoXu780fJCF5nixQRvF2UjD-s7IVSYq', '', NULL, '', '', 1, 0, '2020-12-18 00:09:07', NULL, NULL, NULL),
(80, 0, NULL, 'CUSTOMER', 'Douglas Cirillo', '', '', '', '', '', '', '', '', '', '', 'douglas.cirillo@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fdZKZgMCbqo:APA91bEnc8dGRPSIH5PgrowHzzP3ycQjFuNRXrwuiKR3umjBV59000MKoDJVOp-3HJqWdsFogyg6mbojNg0SUl9oNlz88RibI4WOABpkZe81PFNYCTQvnUEkad7WbmYuZXvJT5EvMUCi', '', NULL, '', '', 1, 0, '2020-12-18 00:11:10', NULL, NULL, NULL),
(81, 0, NULL, 'customer', 'Bellinda', '', '', '', '', '', '', '', '', '', '', 'bellema241@hotmail.com', '7742577769', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'djptfOg0WvE:APA91bH_mwlRwJzu4DBWL7BFnwVGKJHoCKwadPKqK7TokuUiENXqlBKMihxJO8v_vuK_ut11e2kf9M9u6b7U0DRGx0M9E7UisC1ayj74EBmcqv0EkMnsMkldVdb6A7fZi6vgbcV1_N4l', '', NULL, '', '', 1, 0, '2020-12-18 00:22:23', NULL, NULL, NULL),
(82, 0, NULL, 'customer', 'Elena Cervone', '', '', '', '', '', '', '', '', '', '', 'ecervone1@hotmail.com', '6172936645', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'edwXtGs6iEU:APA91bG1o5MfILuF6RDRDNYYw3I32tKT95ozitA0kBEww3L3fZpleWEkgqVnYR8mN0L5-fW2co9sIMgNXFyzsMBFreRA9tarOFMLitbGaEYLInKtudOtfxeErkERBOwloqoA2Yj_4juk', '', NULL, '', '', 1, 0, '2020-12-18 01:34:05', NULL, NULL, NULL),
(83, 0, NULL, 'customer', 'Amanda', '', '', '', '', '', '', '', '', '', '', 'amandamorgado87@gmail.com', '2039190243', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'c1WVGCdJci0:APA91bGELvJc9B4T2Cmj4mUFn7S1rdoU5AJzj9VptN97IHoEpDpNIzQcP_lpxxgZrBr5ju2ATeFn4lMPM9kLqxl-sw7D9owG3MAdDg34kVpMhRR6e-y8vKk9MulZH-7LXFQkq3ndYOM8', '', NULL, '', '', 1, 0, '2020-12-18 03:17:15', NULL, NULL, NULL),
(84, 0, NULL, 'customer', 'Danielle McBride', '', '', '', '', '', '', '', '', '', '', 'daniellehmcbride@yahoo.com', '8564548770', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dL4W6k_LG2A:APA91bFYaU3kBNvn7kryL3qCkoWuZDo5H37cKb0MVeS6mtzSCU0OxULL0U8eZ06AXuyhOymV7Qxo6KOK6gKhJUxDnQQMIbnfgpt-MkFydZQlJzi2GTStWif2ofGYHpvwhiuh1ao-OeNX', '', NULL, '', '', 1, 0, '2020-12-18 05:15:24', NULL, NULL, NULL),
(85, 0, NULL, 'CUSTOMER', 'Christopher Carey', '', '', '', '', '', '', '', '', '', '', 'ccarey4952@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cNTDZm4JEXc:APA91bGgcYqisTHBbWHh_1umTp7anmy0hdzE6pq0plcpbLCCu0Ln-dpnBsNsIqGAyBll__VUKzfdbM8KqaHMD-LqwwiW6m3Tem_ow-agNBFI6A2UhQlGaki_mDiowclF2vFQDzit7XSX', '', NULL, '', '', 1, 0, '2020-12-18 08:44:59', NULL, NULL, NULL),
(86, 0, NULL, 'CUSTOMER', 'Amy Kate', '', '', '', '', '', '', '', '', '', '', 'amycatherine777@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'd5OL6QyZOqE:APA91bFZ_xtZT_HKXMZfoj1xHlxwK3ht9lX0Dx4kzhrZb6btdS5g7AHx9FmOirHYTdcGIM4S3K4o6NDBONdjOVsyqzXzsuBI3FQVYna7FKqjsDWB4wxicFuZwFfwaA51K-DL6_O7cLPN', '', NULL, '', '', 1, 0, '2020-12-18 11:04:47', NULL, NULL, NULL),
(87, 0, NULL, 'customer', 'Piyush', '', '', '', '', '', '', '', '', '', '', 'chintoo.Samaiya@gmail.com', '5512269876', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eLA5D6_SlLg:APA91bH48w_qOiWKraKfUG6LE8ArpXMPxTplCTkZYy3dx47ffAA8Ptvu0OmTHRC6sK86ci5WJEXPOX3u0EX42coOGgZ_aEaB9NkDsZGXgdBrp946G85yaWcU9kEEOjRs0DhLYfTQqASh', '', NULL, '', '', 1, 0, '2020-12-18 13:11:09', NULL, NULL, NULL),
(88, 0, NULL, 'customer', 'Tamy cohen', '', '', '', '', '', '', '', '', '', '', 'manyac123@aol.com', '6313351488', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dwwfCP-L5i0:APA91bFaNej1YztgLsLgZtlxPQ_DBICx6MW9RH8zqrRlPfhruCjFfRy4BN7pphNAVmwFAHxpIssAsIA0_JRat-wdILq0Yk8X17Ma6cHQaixGOQsQ79E-vIt8pWjc563xrjvs4XPGVN_A', '', NULL, '', '', 1, 0, '2020-12-18 14:28:42', NULL, NULL, NULL),
(89, 0, NULL, 'CUSTOMER', 'Nabraco Ware', '', '', '', '', '', '', '', '', '', '', 'nabracoware@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '1', 'fG1GAw1Ghcw:APA91bGA-ffHyaZOeb9BOtFrDPZslZpx9NUui5PMJoMTd6frGyqMfm42hUZaGmqVOzg52207oq9n2e1dwnVYBWVEi23huaTlPl32-di-OYSewfPKMIYZnSmiShUgAEPdXvLWEI-rTQyU', '', NULL, '', '', 1, 0, '2020-12-18 14:28:49', NULL, NULL, NULL),
(90, 0, NULL, 'customer', 'tim', '', '', '', '', '', '', '', '', '', '', 'tmcelroy07@yahoo.com', '5188796700', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eW6U1Whn6PM:APA91bEHc5J_iP0Gsc1KnsWqqtZa5V8CehnYnp3_MbRcteyQEcTFKSc4oYS7I_QmEECHhM-poj1v6Ho28-fFbYxzz4_xY5a_8Qfx1dOk8t6RSqiexEQxUkh6luRkYDBUs4RfPQXRmuIK', '', NULL, '', '', 1, 0, '2020-12-18 16:34:12', NULL, NULL, NULL),
(91, 0, NULL, 'customer', 'Alonzo', '', '', '', '', '', '', '', '', '', '', 'alonzo.perry@gmail.com', '7324856282', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fBiS2dT0qFw:APA91bFWFmbkXD4ZckIKpwjlEKQfyyFncSFrwo3JPa6nz7sIfSIydtBCOzgR_yBC4z7ckcDGylt78q8yXHt9bFtUM9aGqWYP6OGSnZCvzY-AUkeYjmMcNN3b4f4lHPf8TtFn-v2MpaUT', '', NULL, '', '', 1, 0, '2020-12-19 02:40:09', NULL, NULL, NULL),
(92, 0, NULL, 'CUSTOMER', 'Naif Alrawaili', '', '', '', '', '', '', '', '', '', '', 'naifalrawaili@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fsAp4Xd9c_U:APA91bFD9KveniteWPa1nDBkP_Ss5ibKpwkCsRlNM-fpwKdlyU5G21vk_PymwSb8YaSnnz0gWMHDLWVGNQTaGNc7e1oS13LG0h55CWTe4OFKI6wLxmh-2U6FsIQLAYbr4XA6qZ8FtMnw', '', NULL, '', '', 1, 0, '2020-12-19 03:52:59', NULL, NULL, NULL),
(93, 0, NULL, 'CUSTOMER', 'Heather Daniels', '', '', '', '', '', '', '', '', '', '', 'heather.c.daniels@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dp2LMtflq7U:APA91bFfs2Rjl_V8XOle8gHrJifhUMQxTmllFZwlHDJhMxrMfszX4GMUQfgod_sO9fuFdpXmAlnZE-S6i0qzGu2KbREv7K0KU6C9jDXzhmGLdoT0pyOtwBaf0ttOiIc1Bi4fkG_DnZU0', '', NULL, '', '', 1, 0, '2020-12-19 16:23:01', NULL, NULL, NULL),
(94, 0, NULL, 'CUSTOMER', 'Jesse Smiley', '', '', '', '', '', '', '', '', '', '', 'jesse.d.smiley@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'csNv6wcSPgY:APA91bHOBHO2rqoh6JO6kVJjhaIf3n5HRFHEDES0EBgqoKMXQlqHrHoLqCptNfz_-78nmDMEvAf_JlFlwEYsMPgLX3soe-NE-7XIL8Umux5TizLeL2O9QlO39u1yAjuUtCMOj8EuX55p', '', NULL, '', '', 1, 0, '2020-12-19 16:29:14', NULL, NULL, NULL),
(95, 0, NULL, 'CUSTOMER', 'Tony Rocha', '', '', '', '', '', '', '', '', '', '', 'tonitoroc@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fE336bDzydk:APA91bFhK27AJ62PfGvQvw3dTzdS-N-t1zsUN4e3QVzatNlK8PS5fSondZp0HxXfolsfbFizUEFcOOqmXJbQRU_2gciiAVckRioWb_WClf0FZDG5C-rFS11i7W_v6TpTeXcdp80ly5o4', '', NULL, '', '', 1, 0, '2020-12-19 17:22:14', NULL, NULL, NULL),
(96, 0, NULL, 'CUSTOMER', 'Amelia Whitaker', '', '', '', '', '', '', '', '', '', '', 'whitaka316@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dd2ksRVE814:APA91bGQlR1FqTJYdcL0EZpcCyNLNaIrzakNxSb0Xgf_Zk3-A6JRkRVt1azVE9x3oqnLZx-diML3YR25UxIT2SAdLbaS9VQWzgmQesYv8iAqIbyyY_b_0OMm1xG1426U31638fsn5KnS', '', NULL, '', '', 1, 0, '2020-12-19 18:18:52', NULL, NULL, NULL),
(97, 0, NULL, 'CUSTOMER', 'Ashton Graham', '', '', '', '', '', '', '', '', '', '', 'grahamashton27@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'c5hCdvjM4iI:APA91bH2h_UFu3W9DZOBQzlRnna3LbOsFBXTXbFvI3tZH0EG1A7wY8nsEHchwXbm4MMlShi_HFZ0RXZv5vMs6aNaOvq7n8iX4Cd_ZY9EIz7slt0deqSxZg8Oj5O-l6uT0YDpkNtj7HdH', '', NULL, '', '', 1, 0, '2020-12-21 02:55:21', NULL, NULL, NULL),
(98, 0, NULL, 'customer', 'Jeanne', '', '', '', '', '', '', '', '', '', '', 'jmazz0509@yahoo.com', '7186731120', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cPxEsdwSES8:APA91bGsUmqLwRat__cIB7ZYVKgp5zf_t14e98TP2y3WRnv9kENH_RwbOPu-kmxMOxzWwp-NvNHTdnfY1zg106EYRNUYGdQm3Zp2Rs6gzInKWgdmG9W35nZoYoXMPHfKwBzuG-lkPk1C', '', NULL, '', '', 1, 0, '2020-12-22 01:07:15', NULL, NULL, NULL),
(99, 0, NULL, 'customer', 'Steve', '', '', '', '', '', '', '', '', '', '', 'renrawmpls@gmail.com', '9525940600', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'ddtPmBWbVy0:APA91bEzrfjZGwSoZPqcuxuUqNRKZSnAwfPQzHaPOPi3HOy5DqWoYsCuM5IJ6hUsG96Q_sFrzW_SiRVgw_wRfskIj2hFUSehArHZrMzI2KCNVEYn2W-rM5vQuEMt8xXjYARGMfPEzpWY', '', NULL, '', '', 1, 0, '2020-12-24 04:23:20', NULL, NULL, NULL),
(100, 0, NULL, 'customer', 'Ryan', '', '', '', '', '', '', '', '', '', '', 'ryan@decibelpromo.com', '6127502816', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dyBNXE_CrJo:APA91bEHMPwPe2JEfkHiUTeLNzEu6L3QPLvQqHS2bhAU8yFmIZ77wJVZsVsvMDkuDizezz7yduO2I-CEZik7MWyySq4LXtomd2z0FuyhGPWAg0Jq0usSYsRyqHpe_ib_NEzFp17zfyTA', '', NULL, '', '', 1, 0, '2020-12-24 15:02:25', NULL, NULL, NULL),
(101, 0, NULL, 'customer', 'Uday', '', '', '', '', '', '', '', '', '', '', 'dayinholland@gmail.com', '6168366699', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'e9I8ApKuQhQ:APA91bE0HVf-jFDt_8dzRKbMdWO0KPeuZHAEejFLXY9HSxLaQu6K_pW5iP2XnO1svq7i03F2bfpHkYBKyy_pUe0i6BbzId0hyrgbY1Kl9ESCSrptNfYrP9Igliqtp2-EnA3twk_t7_iU', '', NULL, '', '', 1, 0, '2020-12-24 22:33:42', NULL, NULL, NULL),
(102, 0, NULL, 'customer', 'DAC', '', '', '', '', '', '', '', '', '', '', 'dacentllc20@gmail.com', '2168590096', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eRdEZqsfOcU:APA91bFcCdXgQYLLGdGdrNyWiUVVhNYa-19mgZMBIHDsmXUiXmEHaIN6DDugb7zlXLYSlLQSWcALcTi675aUzitkPErPfLlTGn87ocefNwQ4uIPSuq_0BG0ERA6FXbulvgFwXKN2FiX4', '', NULL, '', '', 1, 0, '2020-12-25 05:06:42', NULL, NULL, NULL),
(103, 0, NULL, 'CUSTOMER', 'sole stick', '', '', '', '', '', '', '', '', '', '', 'solestick05@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'f8GAPty924k:APA91bEp19VIWVQbg4_JEc6h0k_GRLEH5M72X9-oixrMwcR-bOjWGSjWkg_K0bEi6-22gL4ANj55gpb8_22wtffwTMEgUGanV350v9VueMHzn-Er58-svO3feYo9SplL-cdkcWARHpvG', '', NULL, '', '', 1, 0, '2020-12-25 14:26:39', NULL, NULL, NULL),
(104, 0, NULL, 'customer', 'Mary', '', '', '', '', '', '', '', '', '', '', 'widgets6@yahoo.com', '4403423549', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dNrE0jZNve0:APA91bEPbSITM_pK_lk0hIRnrYUK5sAL6vYB_V8-Fa6QucVFLc-7o_QEAAPWKr6vUvM0O7nrIOZHV4M9LlSHljyzB5V6zpbH5mLaryXg8ln6cldFCToVIQSUK5IpZMr7lfFPC2DbZXtw', '', NULL, '', '', 1, 0, '2020-12-25 22:10:54', NULL, NULL, NULL),
(105, 0, NULL, 'customer', 'Hassan Ayoub', '', '', '', '', '', '', '', '', '', '', 'hayoub11@outlook.com', '3134060776', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eDqYRCJrpWI:APA91bHpLlRv04FSo4jT-Un1oKy6OFk1yt705qA9xy0tnCvgOx6Y3gwPYdQ__Y0q_XbAtowKRrkTehfZ5_NgAmaQHL9SKJlToJYAWkTO9AF6iwTfIhoM3imP4loy2N46s8e5e6xc9Eki', '', NULL, '', '', 1, 0, '2020-12-25 22:51:53', NULL, NULL, NULL),
(106, 0, NULL, 'customer', 'Mike', '', '', '', '', '', '', '', '', '', '', 'mgra508@aol.com', '7164463489', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fDxbo0ziUkg:APA91bGh-s8Gf73IMmEr7eATg314vJ_XiRXzj7jSJIjC1D6Gs39YEciwydOk2R7Rw1a9IS9XMMgMhBSpXAbwMtxJyQ77WKRXoqmZd3jSeV2vK99_LdNEakbR5RAR3iNGYkYcBgDmjwsJ', '', NULL, '', '', 1, 0, '2020-12-26 13:35:19', NULL, NULL, NULL),
(107, 0, NULL, 'CUSTOMER', 'Jonathan Thomas', '', '', '', '', '', '', '', '', '', '', 'jonathan.thomas.me@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cFApvJBOUO8:APA91bFtnwjGfJEE1vk3KJaWuNDMCcEM0TGniiXo0zA7kw8Z44ioWhK3Klatz3Sr80JqH0GQbukz0nypmgW8gQFt5KgQ1eergzEdKNd2QjhRHZu1tjuLj7lFCsycL-BQdGYTFiN_VbOb', '', NULL, '', '', 1, 0, '2020-12-26 14:53:50', NULL, NULL, NULL),
(108, 0, NULL, 'customer', 'Steve Volpe', '', '', '', '', '', '', '', '', '', '', 'steve@stevevolpe.com', '7163082405', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cWO8p_guQE4:APA91bEmbaCjsRFlDi_mBgYePa24daFYelb4jCfjucOH9igV2DUVc0lQbMifaRkrKaSfanw2_Y4M0X9UFbnjo1oJONY38C8OUahq0pNj1uLc9H2hDxarZ5UdveSU_D-DevktLYY7APVu', '', NULL, '', '', 1, 0, '2020-12-26 18:28:44', NULL, NULL, NULL),
(109, 0, NULL, 'customer', 'Louis', '', '', '', '', '', '', '', '', '', '', 'serild76@gmail.com', '7169983324', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dKrdMOMnjRQ:APA91bGKkdbLryklqEBiSFq6ANPLu20vTvvrclcZQt233By2aCwuxazlxLQAPzeoF8o7sNeXq7rQAKO5w4OnLYD-h9rQHa99tf1vDy7IVMByvdY3V1jgGrUvPZFWrl_3IbNU9rS9Hpde', '', NULL, '', '', 1, 0, '2020-12-27 14:44:21', NULL, NULL, NULL),
(110, 0, NULL, 'customer', 'Scott N', '', '', '', '', '', '', '', '', '', '', 'snick@jlnick.com', '8144498445', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eJgI3XiGK_Q:APA91bF-pPjFmmzA2OVrdK7sf_KkyVTf7_WHDxtZIbrq9sip2VHK9NrZ0gnDCItXx49s8t8l2SqrL3HYzqVzNeuF2m8r3NiRwGm3evbjBOx1GEoDYNXq32cP-jExG_UF4VHBrk3lh4Sj', '', NULL, '', '', 1, 0, '2020-12-27 17:07:13', NULL, NULL, NULL),
(111, 0, NULL, 'customer', 'Danny Hernandez', '', '', '', '', '', '', '', '', '', '', 'Dannyhernandez76@yahoo.com', '2037526430', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '1', 'enMEp0Rgu-A:APA91bG2Ds8nry7e-3-fX1NnWOWOPqpFMeh5y3hUETFpQKA1edsHu7DiFZKrL_IbMSitOY7dpOgIq_ZLZFXxBAmkdQI7OcTuBjCEM1qg5rcBeNl3pGlYDyhAj1OrumydYRe_fRTff9gY', '', NULL, '', '', 1, 0, '2020-12-27 22:54:45', NULL, NULL, NULL),
(112, 0, NULL, 'DRIVER', 'Juan Fierro', '', '', '', '8942 Jasmine Ln S', 'Cottage Grove', 'Minnesota', '55016', '', '', '', 'fierrojuan@ymail.com', '6513220842', 'WryeeRaXe7', '', '1609114225_1609114219_profileimage.jpg', '', '', '', 0, 1, '474358926', '1609114225_1609114219_ssnumberimage.jpg', '2', 'Liquor store manager', 'X634260992913', '1609114225_1609114219_drivinglicenseimage.jpg', '910958794', '1609114225_1609114219_driverinsurancenoimage.jpg', '', '', '', NULL, '', '112-juan-fierro', 0, 0, '2020-12-28 00:10:25', NULL, NULL, NULL),
(113, 0, NULL, 'customer', 'Stephen Fraser', '', '', '', '', '', '', '', '', '', '', 'Stephen.a.fraser@gmail.com', '6092407099', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fsxGxCaUz2c:APA91bGGM0obxunbOofWfy_3mOA41HdYk_XJTudtXRKAFIiZt5GqixyLy2zZpjjcBndb6bodxG2aRTl8udIvBgfjDEDlf2L7JEgWAc8wxDtAosajtZYLPrPq0B2QeBFjYCwvwiRT52ME', '', NULL, '', '', 1, 0, '2020-12-29 00:10:00', NULL, NULL, NULL),
(114, 0, NULL, 'customer', 'julia', '', '', '', '', '', '', '', '', '', '', 'brokenirishleg@hotmail.von', '2624427035', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fXcaoZK9Bpg:APA91bEjx1OMAPe_3zuvCaQgsVU6zzXDZbTIOJg6wHIomGgj4k5f65BbfDDmK0iXmHEgm_0IRgaa7teb31G5Uj1GjZM576AwcaOjok_H4P4EgRNKocdFvrSffm1Af_ZmdjjkEeBKXR7S', '', NULL, '', '', 1, 0, '2020-12-29 00:48:54', NULL, NULL, NULL),
(115, 0, NULL, 'CUSTOMER', 'Malachi Key', '', '', '', '', '', '', '', '', '', '', 'keyandsons97@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '1', 'dE4G8TReLro:APA91bFpjqWUXD-vEVrcqzjXim_9iPcq5WAb7EBQ6c8ixkKZtvbQwEjIh0LB_4hdLD_qe7bYjSEgCsraCHl2MJUKbHyBQqEWYc7MTqukUE2xQfizXzfgyCuN3jQL-noMV10OyoCSSuCe', '', NULL, '', '', 1, 0, '2020-12-29 03:15:35', NULL, NULL, NULL),
(116, 0, NULL, 'customer', 'Steven Jones', '', '', '', '', '', '', '', '', '', '', 'sj_usa@yahoo.com', '6123825559', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'c-FwWS0eIzg:APA91bGhP1jVWC9mp4KYYciRPIiIiKSLbz783bHWF3v1tdhdUYk-ykRo6Urlze4GQTVcZeyfR7IAzdvFnx90jPKy9Qf267Asks7qbqr7o05mKGlZe0GsVT-3wsHmSkVNSAMvY9DB95F4', '', NULL, '', '', 1, 0, '2020-12-29 03:18:56', NULL, NULL, NULL),
(117, 0, NULL, 'customer', 'christine juma', '', '', '', '', '', '', '', '', '', '', 'christinejuma11@yahoo.com', '4023204182', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'ed3ZvnvTywc:APA91bFrIxuYLD3NJYPchdKYHQV7CN_y6wqPGhQzkh5svlULADSFttF7q4OkIzJ9TYsrOxkAdHzrfncmBSW3bxQOgGC0EVioVnV7EnMX0sywHxk-30MgJ8rhTtYFeT7z14hPXXNniqMU', '', NULL, '', '', 1, 0, '2020-12-29 18:57:50', NULL, NULL, NULL),
(118, 0, NULL, 'customer', 'frank saelens', '', '', '', '', '', '', '', '', '', '', 'frank.saelens@swagelok.com', '8473634185', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eqM7ZTf6ILs:APA91bFnDZvJsQ4T5Ub7sPWg1zmEQibS9yIEAqbid5gbsWtnQduTS15aQ1f59Q7pB-JTVZA3cBPNJxFoJnd0vb6jEggKTbglVhXuZmoGNyv7EI0pStuqyrPfkxpKVMBvBfqs9Gu3huPp', '', NULL, '', '', 1, 0, '2020-12-29 21:38:34', NULL, NULL, NULL),
(119, 0, NULL, 'customer', 'Gerald', '', '', '', '', '', '', '', '', '', '', 'gerrymax00@yahoo.com', '6303279119', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'efSm2X1lfHc:APA91bGnswpt67aJm25NIFz3i04Ipv0kJJPcid_aRB9mM0fxMwCr6P3MVyzkflzbAcjYiKQfB0Wqd9Dv4gYOc9tU51os2CgbHQD65svgcqdaiT86HFWOmyvNwuuoGpjQv7V0u1TafZhw', '', NULL, '', '', 1, 0, '2020-12-29 23:13:25', NULL, NULL, NULL),
(120, 0, NULL, 'customer', 'Bradley', '', '', '', '', '', '', '', '', '', '', 'elliottb533@gmail.com', '6308865146', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'et3DzeEJajM:APA91bHfmJEn4eURI-aewmpZ8k73qiRQQ0jN5Zf0y7aurj3XGrWnX4WxszdvnLgghn1kf7JmwtZzCdHdONUF8WvXhDR5YR_6OvrLnuj_6-KfLsOJHvjwY01BFRj04Caii-yctGZORXT9', '', NULL, '', '', 1, 0, '2020-12-29 23:31:00', NULL, NULL, NULL),
(121, 0, NULL, 'customer', 'Kevin Uribe', '', '', '', '', '', '', '', '', '', '', 'kurib3985@gmail.com', '7738473496', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'c1SxO_Ku9jQ:APA91bGkarqjoX4E4BpOF57-PAZbuWWsi52QxyrMU2a_GsxK5YtjpZoHy85Yrl0mfXZ7VBUj6fRGcXjrU2YpQQKrJ0-stuWzC6qPpnP2sIDxDtBnOlDDI_kHOQIBhPNHSQbZnSP6bGiG', '', NULL, '', '', 1, 0, '2020-12-30 00:05:56', NULL, NULL, NULL);
INSERT INTO `user` (`iduser`, `parentid`, `reffererid`, `usertype`, `fullname`, `companyname`, `companyphone`, `profession`, `address`, `city`, `state`, `zipcode`, `country`, `lat`, `lng`, `email`, `mobile`, `password`, `securitycode`, `profileimage`, `profilebackground`, `otp`, `refferalid`, `profileprivacy`, `isnotification`, `ssnumber`, `ssnumberimage`, `experience`, `areaofwork`, `drivinglicense`, `drivinglicenseimage`, `driverinsuranceno`, `driverinsurancenoimage`, `subscribe`, `fcmtoken`, `ipaddress`, `about`, `weburl`, `friendlyURL`, `isactive`, `isdeleted`, `createdon`, `updatedon`, `deletedon`, `usercol`) VALUES
(122, 0, NULL, 'CUSTOMER', 'Rick G', '', '', '', '', '', '', '', '', '', '', 'reboot82184@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dfhncZvHops:APA91bGI5DKlVnpjLnTShbkRdO5iM8V9t2DFd8dpakACM36XoD6D_ijwGQy8724fmtA4g5bSFBkPDzAFkYYLVEehM7tX9Bdrz-LJ7yyNxKmO_rUEJMnkD2pjgXyDp-cWOGrSQ_m_CZ5Q', '', NULL, '', '', 1, 0, '2020-12-30 00:31:59', NULL, NULL, NULL),
(123, 0, NULL, 'customer', 'Robert Ziegler', '', '', '', '', '', '', '', '', '', '', 'bobziegler@gmail.com', '3123155541', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'e_owKyoYrdw:APA91bEDzYqVusvmZux6ejeRNda0sIwDEFNgfa7NLwME4mv7jayd3Qs4tnMU3jaY8W7NdzF7KAiPU-_FZ5lxF7W70k6ZjMMQzgv9ipSMCUgSsIDLlUn7ZVoIcQP20vpfeCkAJbIj4iDH', '', NULL, '', '', 1, 0, '2020-12-30 02:19:54', NULL, NULL, NULL),
(124, 0, NULL, 'customer', 'Edward Hauder', '', '', '', '', '', '', '', '', '', '', 'edward@hauder.com', '8474068150', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cHduKdT9NzI:APA91bFvHOGvnRe8qsitgiZqezp1QavH8eXtoWbkfYGG43QMcmDp7Syoxsbd7tieXrhSmKsZAIpD74rNaAAzJM4x3-1XYxiejJGN5x1lse0QmfD8mSzQ6OskacgI90dHS6neTwIyYz3z', '', NULL, '', '', 1, 0, '2020-12-30 02:24:19', NULL, NULL, NULL),
(125, 0, NULL, 'customer', 'Jonathan JuliÃ³n', '', '', '', '', '', '', '', '', '', '', 'jonathanjulion@outlook.com', '7792370248', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '1', 'cbxEb7LsPEY:APA91bGkWoogCOw2KkZUDv1Kq_OJQn3E8WK-cfZ771Dg6fyT80AD08x51-HFi7lwzun_14tKal5MMVAlqb_6x7Y5W06aRwJFPRQh96zr47yOgxfqOMXYAX_P36TJ3tsMuaphOU3APD-Z', '', NULL, '', '', 1, 0, '2020-12-30 02:58:12', NULL, NULL, NULL),
(126, 0, NULL, 'CUSTOMER', 'Cristian Tirado', '', '', '', '', '', '', '', '', '', '', 'cristianftirado@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '1', 'eRPqRwTYXSw:APA91bF7iXIlJJ2S49j21MLcqeVJFt4Rq_Jmpf5Afpdy23_nmIgPgF8RPHXSmszjbDrqm0HGr3xULOZFGrZbqGVhGe8SoygBz4csqziSshX37rkL3v7TP8apjjYj2sgQ6VSQtVAnwSKW', '', NULL, '', '', 1, 0, '2020-12-30 03:14:07', NULL, NULL, NULL),
(127, 0, NULL, 'customer', 'Monique Moquin', '', '', '', '', '', '', '', '', '', '', 'Moquinmj@yahoo.com', '2486008710', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fCbJqrK5Cmw:APA91bFfU-c6iyYQABOCoHuPEd3WpfUFbtz2L7bwfzdO_kV-dPglNMBIxvHY1O5MHwSlbhMLXB-KRBPw-x9poGWRpvPpq-O6gp9_v69Vl3Q0VA4kuvq8M6nN7FdO0bur2yMRreAdkEjL', '', NULL, '', '', 1, 0, '2020-12-30 13:47:12', NULL, NULL, NULL),
(128, 0, NULL, 'customer', 'GerriAnn', '', '', '', '', '', '', '', '', '', '', 'gateach@comcast.net', '6033007913', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fFO9lVGQzIE:APA91bHZCv9_B4Cu1MoEN0O5o2YNXd71y-19a6Pms-NK2DmLOIIU52VveCGdKoD0DXBFqK_ZSuTSXMcYP77pa9vngqVL4QF6BJW1pIErmdmVAiH3lN_jlEAe_CNxNGtSgfRGJjm6Ptqr', '', NULL, '', '', 1, 0, '2020-12-30 13:49:10', NULL, NULL, NULL),
(129, 0, NULL, 'CUSTOMER', 'Steven Saraceno', '', '', '', '', '', '', '', '', '', '', 'saracenoconstruction@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'drS1nl8kyEU:APA91bFsYgxcxYw36DsX8EeMzfdI5sKKpx3f4sybbYdUbM_7dLzuJwm4qqJXDLpgAYjuEDDj0k5r2Enet2AFIIrHyHuvODgjocQPsSL46vA4a2KNNjE4ry60ThbSa7jQ9Byto4LakPxP', '', NULL, '', '', 1, 0, '2020-12-30 13:51:18', NULL, NULL, NULL),
(130, 0, NULL, 'customer', 'James  Walsh', '', '', '', '', '', '', '', '', '', '', 'Christophersalon@gmail.com', '9176878086', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dqHNNUjy_9o:APA91bFzlDQJJgEkU2mL63f sN8Wp2iWM9iLEIoFhlkJ6m1_WlBCU7qnPCzlki5rbn0KOTD1YLh8moa2rLKrwnBltEvnWEifjBT2PpJrsnpNkNL6l9uwZyD9LS06X8mqT4opvpMt2ae', '', NULL, '', '', 1, 0, '2020-12-30 13:53:30', NULL, NULL, NULL),
(131, 0, NULL, 'customer', 'Hlavac', '', '', '', '', '', '', '', '', '', '', 'exposa.inc@gmail.com', '7734471644', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cxt1jvMu5AQ:APA91bGpge2QCXlAUT-ZCQDysqjrVErF7k3qj92svbEaZ9AXZtrsuCsLT8cILC6JjjMSvjs3I1pKgfAlly-sO8c3KUG45B4m7IvEWhdUvEtlEY2h5NQs9HeKOhkdq8zFMl3jyQvTSYVX', '', NULL, '', '', 1, 0, '2020-12-30 14:28:51', NULL, NULL, NULL),
(132, 0, NULL, 'customer', 'cary', '', '', '', '', '', '', '', '', '', '', 'caryrosenbloom@sbcglobal.net', '8473700947', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'c6_f8_o5y7w:APA91bEnj_LCCEtPR4vtEWfyNDbPy0l20ZzfMJQfPUNL05C4osmqcBA5Q7BFmfClKJnJWJuhoav6_bsu9yG-IZit2KXG7LNUfPzYggUKlD3rrduUq2BdkLp1qW_mrPDrZ_ivYVa5fppO', '', NULL, '', '', 1, 0, '2020-12-30 14:48:18', NULL, NULL, NULL),
(133, 0, NULL, 'customer', 'Margie Zimmer', '', '', '', '', '', '', '', '', '', '', 'zimfamily5@gmail.com', '8479773042', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dsA8Rw_6jMM:APA91bGemiof0OvKa3i5t6_hKm1pLBwFGOAYi-9gvgTsmNx2REyEcrCEIZrjxCAgE2wqRt0cAmJELB68ZwtpaE9k0mfZeEwVbwZ8Le2h9hu4Vz-9IZv2aOcw1Xr0qo7Q0MlQpm5A-uRY', '', NULL, '', '', 1, 0, '2020-12-30 14:48:38', NULL, NULL, NULL),
(134, 0, NULL, 'customer', 'Martin Butler', '', '', '', '', '', '', '', '', '', '', 'mgbmd@me.com', '8155660537', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cDmYZ8axo08:APA91bEMmXnwY5afdKlwB1wfAjmqh7HoUkUm5xvZwciNW25CIiKu FqF59gXQhclJUCmiwRmMpa-7oErotfXxWUs_jxi4n4T13jqR6fUOHg7GDLJfwZjwL90xobBIvtmVqZ20YUW2M1', '', NULL, '', '', 1, 0, '2020-12-30 14:51:12', NULL, NULL, NULL),
(135, 0, NULL, 'customer', 'Virginia', '', '', '', '', '', '', '', '', '', '', 'vcollins2310@gmail.com', '4026696657', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dBL1-UO_e7c:APA91bG1zHNuMvkvUwgkh5imuxyAUxRtzXtScNUQkECJtsX67yEIsEmHpCjulYdAI5t__VQ5S4nFb5ygZDbs1j8Fo5I_DWNmGCB3bk5t0EMOm7pnQEG0NAJ0iuwQ_wmWLmX5DQ9yjN7N', '', NULL, '', '', 1, 0, '2020-12-30 14:55:18', NULL, NULL, NULL),
(136, 0, NULL, 'customer', 'thedora duncan', '', '', '', '', '', '', '', '', '', '', 'theebeem@yahoo.com', '3128606506', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fonXBKdz9eg:APA91bH2jcrHuQCR3WAvEnKS3sqo0qtsMySs_kXnB5TzPU2T4H0hiSShAWIyAFmFjYY77qHi98drPl09eo3uZbQR5nAwD4E2eebTDbRjRH-PKRZsZX7jj9skS8HdGI_p6hUB1-EuimFP', '', NULL, '', '', 1, 0, '2020-12-30 14:59:55', NULL, NULL, NULL),
(137, 0, NULL, 'customer', 'Pam Saturnia', '', '', '', '', '', '', '', '', '', '', 'psaturnia9@gmail.com', '5633206246', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'ceMWaywAPkA:APA91bGzKzEjzX4R2N9zMAIduiLDstYJX3fUszAkAOJRc5i9Y3VE7liQR2cRWIm490zgRMDcmtv00wLYe38DK9C8K-LBEX5aGD6QXdIiptpaUi4JkF_xYewzs9XlCWEwH_aOGDAKkomt', '', NULL, '', '', 1, 0, '2020-12-30 15:00:25', NULL, NULL, NULL),
(138, 0, NULL, 'CUSTOMER', 'Matt Schafer', '', '', '', '', '', '', '', '', '', '', 'coachmatt79@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'd-opShWdd8I:APA91bGlCkX0Pr2pbw1yvoV-YG3eFyL8j1cGutRzERzUPXzOOQdZSAWVDYo3sKcZXtzlDuVwRyzY86ZXbFmLCmv_t14FWxGRqI70ZTr_4ikR-9xyHc5HzdacKQqJb-OixuC0KOY2YeKp', '', NULL, '', '', 1, 0, '2020-12-30 15:14:18', NULL, NULL, NULL),
(139, 0, NULL, 'customer', 'Sue Flodin', '', '', '', '', '', '', '', '', '', '', 'sueflo56@gmail.com', '6164819371', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dXR7NVDks0g:APA91bFDR03IAZw9axSMTV6bwncMFDF6Gdx7U83j80PcWuYIMWGNBzPMMuYTanIdYJmhnKcP_0WsV1_upZE2IRdQ2mZGKMiO1TEyKgOFa46S588z46UjS05nMpp2FNM3Y_Xjja2ZdZmT', '', NULL, '', '', 1, 0, '2020-12-30 15:54:36', NULL, NULL, NULL),
(140, 0, NULL, 'customer', 'Terri McRae', '', '', '', '', '', '', '', '', '', '', 'terrimcrae@comcast.net', '5099512486', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'ehwg1HUI8ZQ:APA91bF1h2sMDN0z2oAto5rglWrQnUrcA2_pn_kh3rqRyxKPHDbGWfHGsPIwI91FLTUykbwUWeWzGdv6o1_8xMN1LrtypY5pWzxOd4Whe3hKrgognSnTlDVk0NzdOYIsMNDNR19TBbfn', '', NULL, '', '', 1, 0, '2020-12-30 16:45:27', NULL, NULL, NULL),
(141, 0, NULL, 'CUSTOMER', 'Leslie Yates', '', '', '', '', '', '', '', '', '', '', 'leslie.yates@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'e47laaSSvdM:APA91bGdRtp3RakVzs_xOCUHHYkQXBsd9Yi8whIJv15k0U6QBgKhG2ZmU-tM_9-CSxFyib6XnnvM8GrC96aBpIkDV8PxJtigBStVZEO8hkR4BNmCmEqq_imU9oYuo391GXr57215Lw0x', '', NULL, '', '', 1, 0, '2020-12-30 16:46:37', NULL, NULL, NULL),
(142, 0, NULL, 'customer', 'Shaun Golden', '', '', '', '', '', '', '', '', '', '', 'shaungolden15@gmail.com', '7792365980', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cvfgQDEv6M4:APA91bGhm_pwo9x5kWMluwZCqCPF0I5RGfYl0a12anAb6P1BkhfR17fxprRkGGa5wQwRwEtyBgUWOENWvkeMy5sBg16M1UUtaSA0crIzMRxX9YS8ZGpQhCoH_vvl-Dr6ypf4QS003Shy', '', NULL, '', '', 1, 0, '2020-12-30 17:22:02', NULL, NULL, NULL),
(143, 0, NULL, 'customer', 'Adam & Megan Hill', '', '', '', '', '', '', '', '', '', '', 'adameghill@outlook.com', '5159792853', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eXxj5jwbvjU:APA91bGq85PHDjEuP_bC39qQ_svu6xdmPVwmKrIdocUwKFs10xXGWnmJkZh-zYEyRMkWglsALm5hesURz5TIYSGAZWTFnBzBybt8isL92QmH0N9SNElzYMfjGX2Spr_1ubtfuDxJaa5Z', '', NULL, '', '', 1, 0, '2020-12-30 18:59:00', NULL, NULL, NULL),
(144, 0, NULL, 'customer', 'Liz Etheridge', '', '', '', '', '', '', '', '', '', '', 'liz.etheridge70@comcast.net', '8473935988', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dta0T2S-4pM:APA91bEru05FyLZlMjmqZKGBketkKl3e65WX2bu_wqmJxrn2XVW3mKA1gOqXGH8OASzshzb7EeRNLWbN7yGMrNtNQHF6s5KBvP1BNXDsuBMJnGkI0LLv4-pPjd16oSuLLqvcCBVC3LE9', '', NULL, '', '', 1, 0, '2020-12-30 19:41:57', NULL, NULL, NULL),
(145, 0, NULL, 'customer', 'norbert', '', '', '', '', '', '', '', '', '', '', '4hhhherrera@gmail.com', '3092365577', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cnoj2-uDLxs:APA91bGJwzdPtTGSIzgXIBJtOAON2beJrhyN8kMEnG58G05mww45qTEzTolxlBMFxGuBApu12y2GZZnWozudmwi2FBhFpztpR256OiN2nTA8YBy1aupCBNo-t7MtmnstpDfUpAFrCE0k', '', NULL, '', '', 1, 0, '2020-12-30 21:36:57', NULL, NULL, NULL),
(146, 0, NULL, 'customer', 'Wayne Gambin', '', '', '', '', '', '', '', '', '', '', 'WGambin@comcast.net', '8608612634', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'faEBR4IbyX4:APA91bEQWRcPYs2pzPzisx9l8Mz1lU7cTFWAfe9gTXK_kROhG2vqm3BlXOCIsfwpDTocVMO1ULjM5_FpG4lrZMdNq9QIurVbgFq24RmZmEBtFO2ky6i245cBvsaNskefwH_Rday1b9pH', '', NULL, '', '', 1, 0, '2020-12-30 23:13:36', NULL, NULL, NULL),
(147, 0, NULL, 'customer', 'Beth', '', '', '', '', '', '', '', '', '', '', 'wittebethann@aol.com', '2182692596', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'enJVLLPgmCE:APA91bG4szR7mYFVukFOHxq9TjaAvWq1QsOZiVRAeAEB5HJ-IAI-WksSN3yudELsrm-AtlS4iA8O2yVfUm3pUAU8RIgck4KS0JyEI1VSvX20x3ryhlWoTLDeB9vRnrTh7Dg45AhjH-SV', '', NULL, '', '', 1, 0, '2020-12-30 23:18:30', NULL, NULL, NULL),
(148, 0, NULL, 'customer', 'Aastha Jain', '', '', '', '', '', '', '', '', '', '', 'aastha77@yahoo.com', '8475328216', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fvUUbOx-sKM:APA91bEvAhQ0zOR8A8lVHOGkOQ40wS3U_0SuQcFiiD_q-q6oBXKwooryWzBbOhdxnXDlozxXOtgDaJN8vWT2biumMnYomKlpYo5EsI0zUaO0Kx4geAGqIr0lyRWnNCTRaAFMQs3-Wf4H', '', NULL, '', '', 1, 0, '2020-12-31 03:57:46', NULL, NULL, NULL),
(149, 0, NULL, 'CUSTOMER', 'Bobby Wadhwa', '', '', '', '', '', '', '', '', '', '', 'bills0000@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dssvBQXdOb0:APA91bEOWCfvnR27dH7WlNmDn6rr8OaFmbxhKSikxHZDG4NLvtbY03tEWN_xGLKMeBwSGtc_iT8Rg8HGTw3RuQP5aHpoKfNvDXsAJqkWTjq9pRQsWEfojKoVWrPzdIPXJEIQhi7o93QR', '', NULL, '', '', 1, 0, '2020-12-31 05:12:38', NULL, NULL, NULL),
(150, 0, NULL, 'customer', 'Duane Stine', '', '', '', '', '', '', '', '', '', '', 'dscco5@comcast.net', '6512616119', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dWXaj72pQfk:APA91bFpkvi0WDamMdsPi1jJhgf96UCKnsf5a0r7Txr3SjVf3PCvdIuDezVBUKsSE-qEabi-NwgfDy_03K7daIlXkgexHkUIy-uXnYmEgMVk-moh2r7gFgPdvm2ca8YNdpEwu-X8S8BV', '', NULL, '', '', 1, 0, '2020-12-31 08:32:36', NULL, NULL, NULL),
(151, 0, NULL, 'customer', 'Christopher nunnery', '', '', '', '', '', '', '', '', '', '', 'chrisnunnery1221@gmail.com', '7087596835', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'efFL7eLn-fo:APA91bFDyee2_Arw2TNlqAYCh8BpDxKtju2RjFQBsU0IXa3PF6MdCyQ4c9Ip2AMOXEo03SBGa83KDXrpN0ApjkC5RjHkTLds0zwwBEVBhijwlgknOee7zusmwifBNlJV9i6e8A6-IBZu', '', NULL, '', '', 1, 0, '2020-12-31 16:17:29', NULL, NULL, NULL),
(152, 0, NULL, 'CUSTOMER', 'Elizabeth Tricarico', '', '', '', '', '', '', '', '', '', '', 'elizabethtricarico@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cxFPmsJIWpM:APA91bGrzWC42eXYusICa7nQTSEGSyyNTDX6d3xwE2qGOvkL973nbcGox2f8-KDjecaZsTQNaIAmRd618wRgg4VNcbdTzonj3qWdPgkwEwzctK99zjwbZexrSoq3KS1qlP69DWhKTSx0', '', NULL, '', '', 1, 0, '2020-12-31 17:13:31', NULL, NULL, NULL),
(153, 0, NULL, 'customer', 'Dan', '', '', '', '', '', '', '', '', '', '', 'ds80909@hotmail.com', '7192177017', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'c2n3_WJW8Rs:APA91bH2KL0-LhrQIjKc3-I7is67KP9EzA8GnN4r2snyS-RKMECHO2Nx04pR_mb7_xzFnbQY9RoCYUjtf5Mirz3Itcyy0X4f3CV1bYWKb01HIrmKX5m9QxPBnRx-Ac7zTfwcNr9rUlAH', '', NULL, '', '', 1, 0, '2020-12-31 19:49:19', NULL, NULL, NULL),
(154, 0, NULL, 'customer', 'Edgar Daniel', '', '', '', '', '', '', '', '', '', '', 'edgardaniel63@yahoo.com', '7732636970', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fPOYG8BPYiI:APA91bFWMxeNERklQ03pbwPSFgWBG9rrO5OnMOvamLPre44635K4xXZb8UPykFxpjXVSmc0PZD2X5Arc25FtMbwXHBMDA5NQd4h-3YBbTrMbDMXtJ8BYiIOeJG13oDsAS5VAbeMlEEpM', '', NULL, '', '', 1, 0, '2021-01-01 05:27:20', NULL, NULL, NULL),
(155, 0, NULL, 'customer', 'marvin horn', '', '', '', '610 n frandsen rd. independence mo 64050', '', '', '', '', '39.0967208147413', '-94.39351678834706', 'mycrazylifenow@yanoo.com', '8165211783', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '1', 'fbjO49hvtw8:APA91bFIopWSbTqt8oOtsAqIpPqojfAYs7CMM-pCx7INM5xzbQ-ID4OPjSUFf6yD7S2XbBQ3YInv1dFThUJlCK6SOx_-vhQP9bPmrZPrxXwCczp3zhWtuc10cL9DcU2ZWI6KX-6utTqF', '', NULL, '', '', 1, 0, '2021-01-01 13:49:47', NULL, NULL, NULL),
(156, 0, NULL, 'customer', 'Cheryl', '', '', '', '', '', '', '', '', '', '', 'Cheryl.c@me.com', '8475531455', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cUqgv6S5GI4:APA91bFkxgyhpE3Wch-l1VQ53f9zUW7H4ocVLA9r82nVM-k9uyuR2IS106NDkbfoOaghckwM6fx6rWgGH31GNM3NNfqrGO8ecLvdMhxMjDmlkP-BOZ7x-9zIn3vHVvACHoG-h2_3u1qG', '', NULL, '', '', 1, 0, '2021-01-01 17:22:09', NULL, NULL, NULL),
(157, 0, NULL, 'customer', 'Nidia', '', '', '', '', '', '', '', '', '', '', 'nidiarcarrero@yahoo.com', '2013044147', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'efDPMHdhKhs:APA91bH9jy91d4Ae2fJ0jyLxvIaWiVPGJZpFxyWn7cNexNoWqCdFM1rVGBORAJImKZ-j2xdCacjd796hf-T-yYdDsNZn8HaTKXJryarxFhO2onAz2_Q-Ry6rXNFuHnzLqhWVRc0Mir3H', '', NULL, '', '', 1, 0, '2021-01-01 22:46:00', NULL, NULL, NULL),
(158, 0, NULL, 'customer', 'Tricia Hadley', '', '', '', '', '', '', '', '', '', '', 'thadley@kc.surewest.net', '9134497180', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'epniEB4RNiE:APA91bE6DM2huO9KbaNDH1cB-Cr6pICfvaH7EtvkWFb8OiR7nLeZW5ngj0cWFXEppuHXXf7ut5mjItvANcV8NLzK9W3k7NC0Ws46azhl6Uxs52EHoSaTCM00Whw5lhLYuWacSkEjaKFX', '', NULL, '', '', 1, 0, '2021-01-02 14:15:42', NULL, NULL, NULL),
(159, 0, NULL, 'customer', 'Michael Schiebner', '', '', '', '', '', '', '', '', '', '', 'mschiebner@att.net', '9892136217', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'e9_Ty4P37DU:APA91bHG2ZA8DL0hwTDjlFWqVX_fji9469d4pNZOYx4f-CXdKsfx-8Vdx0wqqUHopssxCcastlaX8tg9yc8OggwpS9zuOSGOI20LFxoF7HgzJWcVDMMubOjubpEa1TyareuuXTGYC0YL', '', NULL, '', '', 1, 0, '2021-01-02 14:30:45', NULL, NULL, NULL),
(160, 0, NULL, 'customer', 'Karyn Shoemaker', '', '', '', '', '', '', '', '', '', '', 'kshoe2002@msn.com', '6103314996', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dspdb1_prac:APA91bF3naIGc_SIKskLewdjJ9lmTY6Ky-TWFh2NwXxg8KTtyKlk772Pw7s3Yqwd4CmjFy3K2ipzHFNbnDnAH3LLxpk2KJtNgkavAV7lJQ1Cj2mIt8885JISU4h1osD3WXU_ig5IOMqI', '', NULL, '', '', 1, 0, '2021-01-02 15:44:40', NULL, NULL, NULL),
(161, 0, NULL, 'customer', 'adnan Mulalic', '', '', '', '', '', '', '', '', '', '', 'Mulalic.adhnan@hotmail.com', '5157072509', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eDQetQJU1hU:APA91bFkUBfkx4t5a-My5QP9d0aWqkFU8gH8fLvM5_8SW3miO1wUMkRKT-tE1C2u1PGaG60zUX0YPZreVwXFzC6YxlI5QgbIYes7SIVzAoxBjiIYuzwshh2qZayhYYkHnrlsAHl_nH23', '', NULL, '', '', 1, 0, '2021-01-02 23:39:50', NULL, NULL, NULL),
(162, 0, NULL, 'customer', 'Kahari Jones', '', '', '', '', '', '', '', '', '', '', 'kaharijones2@gmail.com', '8167152330', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eM-fmSdK0go:APA91bGWr_yyxqry7UFP7LewrHH-NkgZU9FbA3Z3uNw2cJC5tspu4GzY-dDd4UT94EmbUwrNkfqO3tgTnABs77cancv-qsKKES1juu8IUx-5vBaG6SdgCwWBhKGysI93I5M3Ps6ez7Ac', '', NULL, '', '', 1, 0, '2021-01-03 02:11:59', NULL, NULL, NULL),
(163, 0, NULL, 'customer', 'Mary Byrnes', '', '', '', '', '', '', '', '', '', '', 'marybyrnes.dtw@gmail.com', '3135730811', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'cSPDn6BMjow:APA91bG_yoDC40awAWCO9dU69dJM6ihdfXr6SnXd61ElxVyPhVXGqs-irRIRTToToRXTJGUKoH-H2DxfOICI lAnCyrEemtqzR4LhrrvCUWaD3hY-CISUnL6Ikb6LZzMZVIW3Z7udUJ', '', NULL, '', '', 1, 0, '2021-01-03 19:22:38', NULL, NULL, NULL),
(164, 0, NULL, 'CUSTOMER', 'M.O.E KERB', '', '', '', '', '', '', '', '', '', '', 'moekerb.biz@gmail.com', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '1', 'fp78l__0ZhU:APA91bFwTPdq7Q3GR4bu3d-0CIBOwSCzj7a1Ghz58KELWANX9InK7EzxRhMUir6r-fxnQLTwc_d7yiUAUuXQ1cxH5DXaPkVehoB7dVv6lUVOzsymfHEyCJmOA_eHDIaW826_bCwYR26v', '', NULL, '', '', 1, 0, '2021-01-04 17:22:36', NULL, NULL, NULL),
(165, 0, NULL, 'customer', 'salvador  santiago', '', '', '', '', '', '', '', '', '', '', 'salvadordominguez13@gmail.com', '7736476991', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '1', 'cMLQkUKPMw4:APA91bHOyCorg5zJe8ZuVUqp26Hxq7HAjaqevfgdfQfRrKCf3M0hB1TFO-cJeWNhfs6P5XfT8nWN_IS7MbcWKXbK59nfVvYAYdhr2AWGt9xVKj-826bC8H20lHRiQvPgQHKG2z8R87fW', '', NULL, '', '', 1, 0, '2021-01-05 14:47:18', NULL, NULL, NULL),
(166, 0, NULL, 'customer', 'Holly Geneva', '', '', '', '', '', '', '', '', '', '', 'hgeneva@voyageadvisory.com', '7734127497', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'fAgaSIqe6Wc:APA91bEkb0y u70kYnBDhCyhPgV3Jafij49D j_4sg6vHmI89fn9FNCfafNIj8_8l_idYsgz5ub1XVy6AEBeEMgdJ9BTRWwtQec_PVRwcFa3bvDZ6O6Kjz5tTxNmGso_VFgczT4sYz', '', NULL, '', '', 1, 0, '2021-01-06 18:26:13', NULL, NULL, NULL),
(167, 0, NULL, 'DRIVER', 'Tom', '', '', '', 'Newark', 'US', 'America', '325533', '', '', '', 'qwerwer@t.com', '8765438762', 'cKTpHmJGF2', '', '1610098208_1610098076_profileimage.jpg', '', '', '', 0, 1, '', '', '3', 'erwqreret', '4243 TR', '1610098208_1610098076_drivinglicenseimage.jpg', '123ew', '1610098208_1610098076_driverinsurancenoimage.jpg', '', '', '', NULL, '', '167-tom', 0, 0, '2021-01-08 09:30:08', NULL, NULL, NULL),
(168, 0, NULL, 'customer', 'Jackie murray', '', '', '', '', '', '', '', '', '', '', 'jacqueline.1018@hotmail.com', '8479096973', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dHA3fp_yaZc:APA91bEDHYhgSv7a_zubVTyEPQ0HHTyZSQhsfgqAOiqf2-rIFzuO_lKC6JQn7evaSrf6HqaJLoNSiSH_MKaD0eAa8kcsdrjbnJcJrTMb28YQ6mb9RBgTjz_SWxNh0lRPhY8Lj64xya-V', '', NULL, '', '', 1, 0, '2021-01-12 05:05:52', NULL, NULL, NULL),
(169, 0, NULL, 'customer', 'zadonius', '', '', '', '', '', '', '', '', '', '', 'zadoniusm@icloud.com', '4148427480', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'eQ3WKG2WxfM:APA91bFrLDqKW_MhkY7pFhBujKNlZAZYegZBYoQCPLfgf0JaBs1Bd4sndh5SjDCkpfC0aonue6sRaV3N1_B0Qb8CHOsuL5m740y5mM6kXojfPKrj0gTg06dlNFDLrBuI-eU6N9BK_88f', '', NULL, '', '', 1, 0, '2021-01-12 20:42:04', NULL, NULL, NULL),
(170, 0, NULL, 'customer', 'Bryce', '', '', '', '', '', '', '', '', '', '', 'bdl61984@gmail.com', '2187790304', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '', '', '', '', 'dmKK_9kdt54:APA91bGLL-QesdjoVoXrBtAM0Z1OHBmTbopERiabw-f_UPF5d-eGKxfMWyJ2c_eZLjUB9xt_1qb4-WySUKEcKaxflOIbvJUrZ6JsxuDQQ0u01YzSZwTmTGM7YCXMDnWyHqpfBpd_ptd2', '', NULL, '', '', 1, 0, '2021-01-13 06:04:15', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `userfriend`
--

CREATE TABLE `userfriend` (
  `iduserfriend` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `idstylist` int(11) NOT NULL,
  `idchatroom` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isfriend` int(1) NOT NULL DEFAULT '0',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `userfriend`
--

INSERT INTO `userfriend` (`iduserfriend`, `iduser`, `idstylist`, `idchatroom`, `isfriend`, `createdon`) VALUES
(1, 4, 11, '', 2, '2018-06-01 16:52:53'),
(2, 14, 1, '', 1, '2018-06-01 16:52:53'),
(6, 4, 1, '', 1, '2018-06-05 18:36:34');

-- --------------------------------------------------------

--
-- Table structure for table `userpayment`
--

CREATE TABLE `userpayment` (
  `iduserpayment` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `idpayment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userpayment`
--

INSERT INTO `userpayment` (`iduserpayment`, `iduser`, `idpayment`) VALUES
(36, 14, 4),
(42, 16, 3),
(43, 6, 4),
(50, 4, 4),
(52, 1, 4),
(53, 15, 4);

-- --------------------------------------------------------

--
-- Table structure for table `usersubscription`
--

CREATE TABLE `usersubscription` (
  `idusersubscription` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `subscriptioncode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `idservice` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `subtitle` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `paymentmode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paymentstatus` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transactionid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usersubscription`
--

INSERT INTO `usersubscription` (`idusersubscription`, `iduser`, `subscriptioncode`, `idservice`, `name`, `subtitle`, `price`, `paymentmode`, `paymentstatus`, `transactionid`, `startdate`, `enddate`, `isactive`, `createdon`) VALUES
(1, 32, '20180718062817270', 2, 'DRYFT elite', '', 10, '', '', '', '2018-07-22', '2018-08-21', 1, '2018-07-24 17:14:22'),
(2, 34, '20180718062817234', 2, 'DRYFT elite', '', 10, '', '', '', '2018-07-22', '2018-08-21', 1, '2018-07-24 17:14:22'),
(3, 37, '20180726094744490', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2018-07-26 20:17:44'),
(4, 37, '20180726101843930', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2018-07-26 20:48:43'),
(5, 37, '20180726101918750', 2, 'DRYFT Elite', '', 10, 'Credit / Debit Card', 'SUCCESSFUL', '545454545', NULL, NULL, 1, '2018-07-26 20:49:18'),
(6, 37, '20180726123245130', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2018-07-26 23:02:45'),
(7, 5, '20190809110640110', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2019-08-09 12:06:40'),
(8, 2, '20190813081822260', 2, 'DRYFT Elite', '', 10, 'Credit / Debit Card', 'Successful.', '', '2019-08-13', '2019-09-12', 1, '2019-08-13 09:18:22'),
(9, 5, '20190816071127990', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2019-08-16 08:11:27'),
(10, 6, '20201015095523480', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-10-15 14:55:23'),
(11, 8, '20201020102622820', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-10-20 15:26:22'),
(12, 9, '20201022102158870', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-10-22 15:21:58'),
(13, 64, '20201106061958390', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-06 12:19:58'),
(14, 9, '20201106163752550', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-06 22:37:52'),
(15, 9, '20201106163803420', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-06 22:38:03'),
(16, 15, '20201109173023390', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-09 23:30:23'),
(17, 13, '20201110170124120', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-10 23:01:24'),
(18, 18, '20201111192342970', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-12 01:23:42'),
(19, 18, '20201111192907480', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-12 01:29:07'),
(20, 18, '20201112235007470', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-13 05:50:07'),
(21, 18, '20201114103947880', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-14 16:39:47'),
(22, 21, '20201115095948350', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-15 15:59:48'),
(23, 29, '20201206064634680', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-12-06 12:46:34'),
(24, 29, '20201206064650990', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-12-06 12:46:50'),
(25, 5, '20201212083439850', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-12-12 14:34:39'),
(26, 8, '20201212234044570', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-12-13 05:40:44'),
(27, 9, '20201213060737250', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-12-13 12:07:37'),
(28, 10, '20201213145031970', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-12-13 20:50:31'),
(29, 23, '20201216063631390', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-12-16 12:36:31'),
(30, 40, '20201217052424540', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-12-17 11:24:24'),
(31, 60, '20201217112004500', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-12-17 17:20:04'),
(32, 68, '20201217125716960', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-12-17 18:57:16'),
(33, 80, '20201217181258950', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-12-18 00:12:58'),
(34, 92, '20201220141457730', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-12-20 20:14:57'),
(35, 102, '20201227200742250', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-12-28 02:07:42'),
(36, 113, '20201228204420040', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-12-29 02:44:20'),
(37, 116, '20201228212014860', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-12-29 03:20:14'),
(38, 146, '20201230172322690', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2020-12-30 23:23:22'),
(39, 164, '20210104112709880', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2021-01-04 17:27:09'),
(40, 165, '20210105090615660', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2021-01-05 15:06:15'),
(41, 166, '20210106122823240', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2021-01-06 18:28:23'),
(42, 170, '20210113000508540', 2, 'DRYFT Elite', '', 10, NULL, NULL, NULL, NULL, NULL, 0, '2021-01-13 06:05:08');

-- --------------------------------------------------------

--
-- Table structure for table `usersubscriptionfeature`
--

CREATE TABLE `usersubscriptionfeature` (
  `idservicefeature` int(11) NOT NULL,
  `idusersubscription` int(11) NOT NULL,
  `idservice` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usersubscriptionfeature`
--

INSERT INTO `usersubscriptionfeature` (`idservicefeature`, `idusersubscription`, `idservice`, `iduser`, `title`, `image`, `isactive`, `createdon`) VALUES
(1, 1, 2, 32, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2018-07-23 08:56:41'),
(2, 1, 2, 32, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2018-07-23 08:57:34'),
(3, 1, 2, 32, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2018-07-23 08:56:41'),
(4, 1, 2, 32, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2018-07-23 08:57:34'),
(5, 1, 2, 32, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2018-07-23 08:56:41'),
(6, 6, 2, 37, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2018-07-26 23:02:45'),
(7, 6, 2, 37, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2018-07-26 23:02:45'),
(8, 6, 2, 37, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2018-07-26 23:02:45'),
(9, 6, 2, 37, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2018-07-26 23:02:45'),
(10, 6, 2, 37, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2018-07-26 23:02:45'),
(11, 7, 2, 5, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2019-08-09 12:06:40'),
(12, 7, 2, 5, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2019-08-09 12:06:40'),
(13, 7, 2, 5, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2019-08-09 12:06:40'),
(14, 7, 2, 5, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2019-08-09 12:06:40'),
(15, 7, 2, 5, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2019-08-09 12:06:40'),
(18, 8, 2, 2, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2019-08-13 09:18:22'),
(19, 8, 2, 2, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2019-08-13 09:18:22'),
(20, 8, 2, 2, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2019-08-13 09:18:22'),
(21, 8, 2, 2, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2019-08-13 09:18:22'),
(22, 8, 2, 2, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2019-08-13 09:18:22'),
(25, 9, 2, 5, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2019-08-16 08:11:27'),
(26, 9, 2, 5, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2019-08-16 08:11:27'),
(27, 9, 2, 5, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2019-08-16 08:11:27'),
(28, 9, 2, 5, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2019-08-16 08:11:27'),
(29, 9, 2, 5, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2019-08-16 08:11:27'),
(30, 10, 2, 6, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-10-15 14:55:23'),
(31, 10, 2, 6, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-10-15 14:55:23'),
(32, 10, 2, 6, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-10-15 14:55:23'),
(33, 10, 2, 6, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-10-15 14:55:23'),
(34, 10, 2, 6, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-10-15 14:55:23'),
(37, 11, 2, 8, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-10-20 15:26:22'),
(38, 11, 2, 8, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-10-20 15:26:22'),
(39, 11, 2, 8, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-10-20 15:26:22'),
(40, 11, 2, 8, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-10-20 15:26:22'),
(41, 11, 2, 8, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-10-20 15:26:22'),
(44, 12, 2, 9, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-10-22 15:21:58'),
(45, 12, 2, 9, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-10-22 15:21:58'),
(46, 12, 2, 9, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-10-22 15:21:58'),
(47, 12, 2, 9, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-10-22 15:21:58'),
(48, 12, 2, 9, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-10-22 15:21:58'),
(49, 13, 2, 64, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-06 12:19:58'),
(50, 13, 2, 64, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-06 12:19:58'),
(51, 13, 2, 64, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-06 12:19:58'),
(52, 13, 2, 64, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-06 12:19:58'),
(53, 13, 2, 64, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-06 12:19:58'),
(56, 14, 2, 9, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-06 22:37:52'),
(57, 14, 2, 9, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-06 22:37:52'),
(58, 14, 2, 9, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-06 22:37:52'),
(59, 14, 2, 9, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-06 22:37:52'),
(60, 14, 2, 9, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-06 22:37:52'),
(63, 15, 2, 9, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-06 22:38:03'),
(64, 15, 2, 9, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-06 22:38:03'),
(65, 15, 2, 9, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-06 22:38:03'),
(66, 15, 2, 9, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-06 22:38:03'),
(67, 15, 2, 9, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-06 22:38:03'),
(70, 16, 2, 15, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-09 23:30:23'),
(71, 16, 2, 15, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-09 23:30:23'),
(72, 16, 2, 15, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-09 23:30:23'),
(73, 16, 2, 15, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-09 23:30:23'),
(74, 16, 2, 15, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-09 23:30:23'),
(77, 17, 2, 13, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-10 23:01:24'),
(78, 17, 2, 13, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-10 23:01:24'),
(79, 17, 2, 13, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-10 23:01:24'),
(80, 17, 2, 13, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-10 23:01:24'),
(81, 17, 2, 13, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-10 23:01:24'),
(84, 18, 2, 18, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-12 01:23:42'),
(85, 18, 2, 18, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-12 01:23:42'),
(86, 18, 2, 18, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-12 01:23:42'),
(87, 18, 2, 18, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-12 01:23:42'),
(88, 18, 2, 18, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-12 01:23:42'),
(91, 19, 2, 18, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-12 01:29:07'),
(92, 19, 2, 18, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-12 01:29:07'),
(93, 19, 2, 18, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-12 01:29:07'),
(94, 19, 2, 18, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-12 01:29:07'),
(95, 19, 2, 18, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-12 01:29:07'),
(98, 20, 2, 18, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-13 05:50:07'),
(99, 20, 2, 18, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-13 05:50:07'),
(100, 20, 2, 18, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-13 05:50:07'),
(101, 20, 2, 18, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-13 05:50:07'),
(102, 20, 2, 18, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-13 05:50:07'),
(105, 21, 2, 18, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-14 16:39:47'),
(106, 21, 2, 18, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-14 16:39:47'),
(107, 21, 2, 18, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-14 16:39:47'),
(108, 21, 2, 18, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-14 16:39:47'),
(109, 21, 2, 18, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-14 16:39:47'),
(112, 22, 2, 21, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-15 15:59:48'),
(113, 22, 2, 21, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-15 15:59:48'),
(114, 22, 2, 21, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-15 15:59:48'),
(115, 22, 2, 21, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-11-15 15:59:48'),
(116, 22, 2, 21, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-11-15 15:59:48'),
(119, 23, 2, 29, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-06 12:46:34'),
(120, 23, 2, 29, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-06 12:46:34'),
(121, 23, 2, 29, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-06 12:46:34'),
(122, 23, 2, 29, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-06 12:46:34'),
(123, 23, 2, 29, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-06 12:46:34'),
(126, 24, 2, 29, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-06 12:46:50'),
(127, 24, 2, 29, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-06 12:46:50'),
(128, 24, 2, 29, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-06 12:46:50'),
(129, 24, 2, 29, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-06 12:46:50'),
(130, 24, 2, 29, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-06 12:46:50'),
(133, 25, 2, 5, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-12 14:34:39'),
(134, 25, 2, 5, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-12 14:34:39'),
(135, 25, 2, 5, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-12 14:34:39'),
(136, 25, 2, 5, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-12 14:34:39'),
(137, 25, 2, 5, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-12 14:34:39'),
(140, 26, 2, 8, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-13 05:40:44'),
(141, 26, 2, 8, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-13 05:40:44'),
(142, 26, 2, 8, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-13 05:40:44'),
(143, 26, 2, 8, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-13 05:40:44'),
(144, 26, 2, 8, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-13 05:40:44'),
(147, 27, 2, 9, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-13 12:07:37'),
(148, 27, 2, 9, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-13 12:07:37'),
(149, 27, 2, 9, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-13 12:07:37'),
(150, 27, 2, 9, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-13 12:07:37'),
(151, 27, 2, 9, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-13 12:07:37'),
(154, 28, 2, 10, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-13 20:50:31'),
(155, 28, 2, 10, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-13 20:50:31'),
(156, 28, 2, 10, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-13 20:50:31'),
(157, 28, 2, 10, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-13 20:50:31'),
(158, 28, 2, 10, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-13 20:50:31'),
(161, 29, 2, 23, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-16 12:36:31'),
(162, 29, 2, 23, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-16 12:36:31'),
(163, 29, 2, 23, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-16 12:36:31'),
(164, 29, 2, 23, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-16 12:36:31'),
(165, 29, 2, 23, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-16 12:36:31'),
(168, 30, 2, 40, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-17 11:24:24'),
(169, 30, 2, 40, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-17 11:24:24'),
(170, 30, 2, 40, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-17 11:24:24'),
(171, 30, 2, 40, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-17 11:24:24'),
(172, 30, 2, 40, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-17 11:24:24'),
(175, 31, 2, 60, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-17 17:20:04'),
(176, 31, 2, 60, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-17 17:20:04'),
(177, 31, 2, 60, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-17 17:20:04'),
(178, 31, 2, 60, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-17 17:20:04'),
(179, 31, 2, 60, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-17 17:20:04'),
(182, 32, 2, 68, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-17 18:57:16'),
(183, 32, 2, 68, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-17 18:57:16'),
(184, 32, 2, 68, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-17 18:57:16'),
(185, 32, 2, 68, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-17 18:57:16'),
(186, 32, 2, 68, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-17 18:57:16'),
(189, 33, 2, 80, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-18 00:12:58'),
(190, 33, 2, 80, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-18 00:12:58'),
(191, 33, 2, 80, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-18 00:12:58'),
(192, 33, 2, 80, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-18 00:12:58'),
(193, 33, 2, 80, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-18 00:12:58'),
(196, 34, 2, 92, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-20 20:14:57'),
(197, 34, 2, 92, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-20 20:14:57'),
(198, 34, 2, 92, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-20 20:14:57'),
(199, 34, 2, 92, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-20 20:14:57'),
(200, 34, 2, 92, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-20 20:14:57'),
(203, 35, 2, 102, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-28 02:07:42'),
(204, 35, 2, 102, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-28 02:07:42'),
(205, 35, 2, 102, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-28 02:07:42'),
(206, 35, 2, 102, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-28 02:07:42'),
(207, 35, 2, 102, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-28 02:07:42'),
(210, 36, 2, 113, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-29 02:44:20'),
(211, 36, 2, 113, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-29 02:44:20'),
(212, 36, 2, 113, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-29 02:44:20'),
(213, 36, 2, 113, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-29 02:44:20'),
(214, 36, 2, 113, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-29 02:44:20'),
(217, 37, 2, 116, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-29 03:20:14'),
(218, 37, 2, 116, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-29 03:20:14'),
(219, 37, 2, 116, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-29 03:20:14'),
(220, 37, 2, 116, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-29 03:20:14'),
(221, 37, 2, 116, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-29 03:20:14'),
(224, 38, 2, 146, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-30 23:23:22'),
(225, 38, 2, 146, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-30 23:23:22'),
(226, 38, 2, 146, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-30 23:23:22'),
(227, 38, 2, 146, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2020-12-30 23:23:22'),
(228, 38, 2, 146, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2020-12-30 23:23:22'),
(231, 39, 2, 164, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2021-01-04 17:27:09'),
(232, 39, 2, 164, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2021-01-04 17:27:09'),
(233, 39, 2, 164, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2021-01-04 17:27:09'),
(234, 39, 2, 164, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2021-01-04 17:27:09'),
(235, 39, 2, 164, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2021-01-04 17:27:09'),
(238, 40, 2, 165, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2021-01-05 15:06:15'),
(239, 40, 2, 165, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2021-01-05 15:06:15'),
(240, 40, 2, 165, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2021-01-05 15:06:15'),
(241, 40, 2, 165, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2021-01-05 15:06:15'),
(242, 40, 2, 165, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2021-01-05 15:06:15'),
(245, 41, 2, 166, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2021-01-06 18:28:23'),
(246, 41, 2, 166, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2021-01-06 18:28:23'),
(247, 41, 2, 166, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2021-01-06 18:28:23'),
(248, 41, 2, 166, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2021-01-06 18:28:23'),
(249, 41, 2, 166, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2021-01-06 18:28:23'),
(252, 42, 2, 170, 'Dryft elite to deploy at 2\"', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2021-01-13 06:05:08'),
(253, 42, 2, 170, 'Priority Placement', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2021-01-13 06:05:08'),
(254, 42, 2, 170, '24/7 Support Number', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2021-01-13 06:05:08'),
(255, 42, 2, 170, 'Only 5 Star Driver', 'http://www.dryftnow.com/servicefeatureimage/priority.png', 1, '2021-01-13 06:05:08'),
(256, 42, 2, 170, 'Real Time Weather Alerts', 'http://www.dryftnow.com/servicefeatureimage/deploys_every.png', 1, '2021-01-13 06:05:08');

-- --------------------------------------------------------

--
-- Table structure for table `usertruck`
--

CREATE TABLE `usertruck` (
  `idusertruck` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `idplowtype` int(11) NOT NULL,
  `plowtype` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `truckname` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `vehiclenumber` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `registrationcertificateimage` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `truckinsuranceno` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `truckinsuranceimage` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `truckimage` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `plowsize` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rubberblade` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `snowblower` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `saltspreader` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bobcat` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `shovellers` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `misc` text COLLATE utf8_unicode_ci,
  `isactive` int(1) NOT NULL DEFAULT '1',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usertruck`
--

INSERT INTO `usertruck` (`idusertruck`, `iduser`, `idplowtype`, `plowtype`, `truckname`, `vehiclenumber`, `registrationcertificateimage`, `truckinsuranceno`, `truckinsuranceimage`, `truckimage`, `plowsize`, `rubberblade`, `snowblower`, `saltspreader`, `bobcat`, `shovellers`, `misc`, `isactive`, `createdon`) VALUES
(5, 64, 2, 'Rubber Edge', 'SUV', '345-CH-340', '1531148245_reg.jpg', '23dwcfse23', '1531148245_Screen Shot 2017-07-20 at 11.56.53 AM.png', '1531148245_Ford-F-150.jpg', '8.5\'', 'CHECKED', '', 'CHECKED', '', '', NULL, 1, '2018-07-09 14:57:25'),
(6, 0, 0, 'reg', 'Nissan', '', '', 'National General', '1531148245_Screen Shot 2017-07-20 at 11.56.53 AM.png', '1531148245_Ford-F-150.jpg', '7ft', '', 'checked', 'checked', '', '', '', 1, '2018-08-24 00:01:21'),
(7, 0, 0, 'reg', 'F250', '', '', '', '', '', '8ft', '', '', 'checked', '', '', '', 1, '2018-08-24 00:01:21'),
(8, 0, 0, 'reg', 'F150', '', '', '', '', '', '8ft', '', '', '', '', '', '', 1, '2018-08-24 00:01:21'),
(9, 0, 0, 'reg', 'F350', '', '', '', '', '', '8.5 ft', 'checked', 'checked', 'checked', '', '', '', 1, '2018-08-24 00:01:21'),
(10, 0, 0, 'reg', 'Ram 1600', '', '', '', '', '', '8ft', '', '', 'checked', '', '', '', 1, '2018-08-24 00:01:21'),
(11, 1, 2, 'Rubber Edge', 'SUV', 'dfg435dfg', '1565006939_1531145392_Screen Shot 2017-07-20 at 11.56.53 AM.png', '234dfg sd43', '1565006939_1531145437_Screen Shot 2017-07-20 at 11.56.53 AM.png', '1565006939_1531145437_reg.jpg', '7\'', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', '', NULL, 1, '2019-08-05 08:08:59'),
(12, 3, 2, 'Rubber Edge', 'truck type', '1234', '1565266491_Truck Insurance.png', '1234', '1565266491_SSN Image.jpg', '1565266491_Truck image.jpg', '8.5\'', '', '', 'CHECKED', 'CHECKED', '', NULL, 1, '2019-08-08 08:14:51'),
(13, 4, 1, 'Steel Edge', 'SUV', 'CH-34-NM-3', '1565266516_1531145437_Screen Shot 2017-07-20 at 11.56.53 AM.png', '34567uytresd', '1565266516_1531146248_download.jpg', '1565266516_1531145392_Ford-F-150.jpg', '8.5\'', '', 'CHECKED', 'CHECKED', '', '', NULL, 1, '2019-08-08 08:15:16'),
(14, 7, 1, 'Steel Edge', '12 tyre', '3254', '1602764265_1602764235_registrationcertificateimage.jpg', '432312', '1602764265_1602764235_truckinsuranceimage.jpg', '1602764265_1602764235_truckimage.jpg', '7\'', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', NULL, 1, '2020-10-15 12:17:45'),
(15, 10, 1, 'Steel Edge', 'gsdhfhfh', 'dhjffjfj', '1603708808_1603708799_registrationcertificateimage.jpg', 'dhjffjgj', '1603708808_1603708799_truckinsuranceimage.jpg', '1603708808_1603708799_truckimage.jpg', '7\'', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', NULL, 1, '2020-10-26 10:40:08'),
(16, 11, 1, 'Steel Edge', '4 wheeler', 'shsvbdsb', '1603710803_1603710728_registrationcertificateimage.jpg', 'dbbdbd', '1603710803_1603710728_truckinsuranceimage.jpg', '1603710803_1603710728_truckimage.jpg', '7\'', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', NULL, 1, '2020-10-26 11:13:23'),
(18, 24, 1, 'Steel Edge', 'Ice Remover', 'UP 9876', '1606225086_1606225041_registrationcertificateimage.jpg', '123Asdfg', '1606225086_1606225041_truckinsuranceimage.jpg', '1606225086_1606225041_truckimage.jpg', '6\'', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', NULL, 1, '2020-11-24 13:47:13'),
(19, 25, 2, 'Rubber Edge', 'pickup', 'v91353', '1606420392_1606420385_registrationcertificateimage.jpg', 'policy number 102010222401', '1606420392_1606420385_truckinsuranceimage.jpg', '1606420392_1606420385_truckimage.jpg', '8.5\'', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', NULL, 1, '2020-11-26 19:53:12'),
(20, 26, 1, 'Steel Edge', '10 wheeler', 'US 71234', '1606991520_1606991423_registrationcertificateimage.jpg', 'Qwert1234', '1606991520_1606991423_truckinsuranceimage.jpg', '1606991520_1606991423_truckimage.jpg', '7\'', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', NULL, 1, '2020-12-03 10:32:00'),
(21, 32, 2, 'Rubber Edge', 'f150', 'vin 11111111111', '1607352003_1607352002_registrationcertificateimage.jpg', '2009', '1607352003_1607352002_truckinsuranceimage.jpg', '1607352003_1607352002_truckimage.jpg', '8\'', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', NULL, 1, '2020-12-07 14:40:03'),
(22, 33, 2, 'Rubber Edge', 'escape', 'vin #', '1607361827_1607361825_registrationcertificateimage.jpg', 'policy #', '1607361827_1607361825_truckinsuranceimage.jpg', '1607361827_1607361825_truckimage.jpg', '7.5\'', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', NULL, 1, '2020-12-07 17:23:47'),
(24, 12, 2, 'Rubber Edge', 'Ford F-250', '1ft7x2b67bea32084', '1607921042_1607921019_registrationcertificateimage.jpg', 'same', '1607921042_1607921019_truckinsuranceimage.jpg', '1607921042_1607921019_truckimage.jpg', '8\'', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', NULL, 1, '2020-12-16 13:16:15'),
(25, 30, 2, 'Rubber Edge', '2001 Dodge Ram', 'ax15558', '1608170154_1608170148_registrationcertificateimage.jpg', 'State Farm', '1608170154_1608170148_truckinsuranceimage.jpg', '1608170154_1608170148_truckimage.jpg', '7.5\'', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', NULL, 1, '2020-12-17 01:55:54'),
(26, 112, 2, 'Rubber Edge', 'Chevy Silvarado', '1GCHK29U11E261656', '1609114225_1609114219_registrationcertificateimage.jpg', '910958794', '1609114225_1609114219_truckinsuranceimage.jpg', '1609114225_1609114219_truckimage.jpg', '8\'', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', NULL, 1, '2020-12-28 00:10:25'),
(27, 167, 1, 'Steel Edge', '10 Wehleer', '1234we', '1610098208_1610098076_registrationcertificateimage.jpg', '12343453', '1610098208_1610098076_truckinsuranceimage.jpg', '1610098208_1610098076_truckimage.jpg', '7\'', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', 'CHECKED', NULL, 1, '2021-01-08 09:30:08');

-- --------------------------------------------------------

--
-- Stand-in structure for view `vcontent`
-- (See below for the actual view)
--
CREATE TABLE `vcontent` (
`idcontent` int(11)
,`title` varchar(250)
,`detail` text
,`friendlyURL` varchar(250)
,`contentfor` varchar(45)
,`isactive` int(1)
,`sort` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vgetbokingdetail`
-- (See below for the actual view)
--
CREATE TABLE `vgetbokingdetail` (
`idbookingdetail` int(11)
,`idbooking` int(11)
,`iddriver` int(11)
,`idservice` int(11)
,`idcategory` int(11)
,`name` varchar(250)
,`idfeatures` int(11)
,`featurename` varchar(250)
,`featureareafrom` float
,`featureareato` float
,`featureprice` float
,`featuredepthfrom` float
,`featuredepthto` float
,`featuredepthprice` float
,`iddrivewaytype` int(11)
,`drivewaytypename` varchar(250)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vgetbooking`
-- (See below for the actual view)
--
CREATE TABLE `vgetbooking` (
`idbooking` int(11)
,`iduser` int(11)
,`iddriver` int(11)
,`bookingno` varchar(50)
,`bookingdate` date
,`subscriptionenddate` date
,`starttime` varchar(10)
,`endtime` varchar(10)
,`fullname` varchar(250)
,`mobile` varchar(10)
,`email` varchar(250)
,`originaddress` varchar(250)
,`origincity` varchar(100)
,`originstate` varchar(100)
,`originzipcode` varchar(10)
,`originlatlng` varchar(100)
,`destinationaddress` varchar(250)
,`destinationcity` varchar(100)
,`destinationstate` varchar(100)
,`destinationzipcode` varchar(10)
,`destinationlatlng` varchar(100)
,`area` varchar(100)
,`beforeimage` varchar(250)
,`afterimage` varchar(250)
,`price` float
,`totalamount` float
,`featuresextracharge` varchar(50)
,`featuresextrachargeamt` float
,`featuresextrachargetype` varchar(50)
,`tax` varchar(100)
,`taxamount` float
,`taxtype` varchar(50)
,`extracharges` varchar(100)
,`extrachargesamount` float
,`extrachargestype` varchar(50)
,`paymentmode` varchar(50)
,`paymentstatus` varchar(50)
,`transactionid` varchar(50)
,`discount` float
,`promocode` varchar(50)
,`promovalue` float
,`promotype` varchar(50)
,`walletamount` float
,`bookedbyuser` int(1)
,`acceptbydriver` int(1)
,`acceptbydriveron` datetime
,`rejectbydriver` int(1)
,`rejectbydriveron` datetime
,`cancelbyuser` int(1)
,`cancelbyuseron` datetime
,`cancelbydriver` int(1)
,`cancelbydriveron` datetime
,`bookingstatus` enum('0','1','2','3')
,`bookingcomplete` int(1)
,`bookingcompleteon` datetime
,`bookingclosedopt` varchar(10)
,`isactive` int(1)
,`createdon` datetime
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vgetuser`
-- (See below for the actual view)
--
CREATE TABLE `vgetuser` (
`iduser` int(11)
,`parentid` int(11)
,`reffererid` int(11)
,`usertype` varchar(50)
,`fullname` varchar(250)
,`companyname` varchar(250)
,`companyphone` varchar(10)
,`profession` varchar(250)
,`address` varchar(500)
,`city` varchar(100)
,`state` varchar(100)
,`zipcode` varchar(10)
,`country` varchar(50)
,`lat` varchar(50)
,`lng` varchar(50)
,`email` varchar(150)
,`mobile` varchar(10)
,`password` varchar(50)
,`securitycode` varchar(50)
,`profileimage` varchar(250)
,`profilebackground` varchar(250)
,`otp` varchar(10)
,`refferalid` varchar(50)
,`profileprivacy` int(1)
,`isnotification` int(1)
,`ssnumber` varchar(50)
,`ssnumberimage` varchar(250)
,`experience` varchar(50)
,`areaofwork` varchar(50)
,`drivinglicense` varchar(50)
,`drivinglicenseimage` varchar(250)
,`driverinsuranceno` varchar(50)
,`driverinsurancenoimage` varchar(250)
,`subscribe` varchar(50)
,`fcmtoken` varchar(250)
,`ipaddress` varchar(50)
,`about` text
,`weburl` varchar(250)
,`friendlyURL` varchar(250)
,`isactive` int(1)
,`isdeleted` int(11)
,`createdon` datetime
,`updatedon` datetime
,`deletedon` datetime
,`usercol` varchar(45)
);

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE `wallet` (
  `idwallet` int(11) NOT NULL,
  `iduser` int(11) DEFAULT NULL,
  `cr` float DEFAULT NULL,
  `dr` float DEFAULT NULL,
  `narraton` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wallet`
--

INSERT INTO `wallet` (`idwallet`, `iduser`, `cr`, `dr`, `narraton`, `date`) VALUES
(1, 4, 20, NULL, NULL, '2018-05-18 21:57:47'),
(2, 4, NULL, NULL, NULL, '2018-05-18 21:57:47');

-- --------------------------------------------------------

--
-- Structure for view `vcontent`
--
DROP TABLE IF EXISTS `vcontent`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vcontent`  AS  select `content`.`idcontent` AS `idcontent`,`content`.`title` AS `title`,`content`.`detail` AS `detail`,`content`.`friendlyURL` AS `friendlyURL`,`content`.`contentfor` AS `contentfor`,`content`.`isactive` AS `isactive`,`content`.`sort` AS `sort` from `content` ;

-- --------------------------------------------------------

--
-- Structure for view `vgetbokingdetail`
--
DROP TABLE IF EXISTS `vgetbokingdetail`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vgetbokingdetail`  AS  select `bookingdetail`.`idbookingdetail` AS `idbookingdetail`,`bookingdetail`.`idbooking` AS `idbooking`,`bookingdetail`.`iddriver` AS `iddriver`,`bookingdetail`.`idservice` AS `idservice`,`bookingdetail`.`idcategory` AS `idcategory`,`bookingdetail`.`name` AS `name`,`bookingdetail`.`idfeatures` AS `idfeatures`,`bookingdetail`.`featurename` AS `featurename`,`bookingdetail`.`featureareafrom` AS `featureareafrom`,`bookingdetail`.`featureareato` AS `featureareato`,`bookingdetail`.`featureprice` AS `featureprice`,`bookingdetail`.`featuredepthfrom` AS `featuredepthfrom`,`bookingdetail`.`featuredepthto` AS `featuredepthto`,`bookingdetail`.`featuredepthprice` AS `featuredepthprice`,`bookingdetail`.`iddrivewaytype` AS `iddrivewaytype`,`bookingdetail`.`drivewaytypename` AS `drivewaytypename` from `bookingdetail` ;

-- --------------------------------------------------------

--
-- Structure for view `vgetbooking`
--
DROP TABLE IF EXISTS `vgetbooking`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vgetbooking`  AS  select `booking`.`idbooking` AS `idbooking`,`booking`.`iduser` AS `iduser`,`booking`.`iddriver` AS `iddriver`,`booking`.`bookingno` AS `bookingno`,`booking`.`bookingdate` AS `bookingdate`,`booking`.`subscriptionenddate` AS `subscriptionenddate`,`booking`.`starttime` AS `starttime`,`booking`.`endtime` AS `endtime`,`booking`.`fullname` AS `fullname`,`booking`.`mobile` AS `mobile`,`booking`.`email` AS `email`,`booking`.`originaddress` AS `originaddress`,`booking`.`origincity` AS `origincity`,`booking`.`originstate` AS `originstate`,`booking`.`originzipcode` AS `originzipcode`,`booking`.`originlatlng` AS `originlatlng`,`booking`.`destinationaddress` AS `destinationaddress`,`booking`.`destinationcity` AS `destinationcity`,`booking`.`destinationstate` AS `destinationstate`,`booking`.`destinationzipcode` AS `destinationzipcode`,`booking`.`destinationlatlng` AS `destinationlatlng`,`booking`.`area` AS `area`,`booking`.`beforeimage` AS `beforeimage`,`booking`.`afterimage` AS `afterimage`,`booking`.`price` AS `price`,`booking`.`totalamount` AS `totalamount`,`booking`.`featuresextracharge` AS `featuresextracharge`,`booking`.`featuresextrachargeamt` AS `featuresextrachargeamt`,`booking`.`featuresextrachargetype` AS `featuresextrachargetype`,`booking`.`tax` AS `tax`,`booking`.`taxamount` AS `taxamount`,`booking`.`taxtype` AS `taxtype`,`booking`.`extracharges` AS `extracharges`,`booking`.`extrachargesamount` AS `extrachargesamount`,`booking`.`extrachargestype` AS `extrachargestype`,`booking`.`paymentmode` AS `paymentmode`,`booking`.`paymentstatus` AS `paymentstatus`,`booking`.`transactionid` AS `transactionid`,`booking`.`discount` AS `discount`,`booking`.`promocode` AS `promocode`,`booking`.`promovalue` AS `promovalue`,`booking`.`promotype` AS `promotype`,`booking`.`walletamount` AS `walletamount`,`booking`.`bookedbyuser` AS `bookedbyuser`,`booking`.`acceptbydriver` AS `acceptbydriver`,`booking`.`acceptbydriveron` AS `acceptbydriveron`,`booking`.`rejectbydriver` AS `rejectbydriver`,`booking`.`rejectbydriveron` AS `rejectbydriveron`,`booking`.`cancelbyuser` AS `cancelbyuser`,`booking`.`cancelbyuseron` AS `cancelbyuseron`,`booking`.`cancelbydriver` AS `cancelbydriver`,`booking`.`cancelbydriveron` AS `cancelbydriveron`,`booking`.`bookingstatus` AS `bookingstatus`,`booking`.`bookingcomplete` AS `bookingcomplete`,`booking`.`bookingcompleteon` AS `bookingcompleteon`,`booking`.`bookingclosedopt` AS `bookingclosedopt`,`booking`.`isactive` AS `isactive`,`booking`.`createdon` AS `createdon` from `booking` ;

-- --------------------------------------------------------

--
-- Structure for view `vgetuser`
--
DROP TABLE IF EXISTS `vgetuser`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vgetuser`  AS  select `user`.`iduser` AS `iduser`,`user`.`parentid` AS `parentid`,`user`.`reffererid` AS `reffererid`,`user`.`usertype` AS `usertype`,`user`.`fullname` AS `fullname`,`user`.`companyname` AS `companyname`,`user`.`companyphone` AS `companyphone`,`user`.`profession` AS `profession`,`user`.`address` AS `address`,`user`.`city` AS `city`,`user`.`state` AS `state`,`user`.`zipcode` AS `zipcode`,`user`.`country` AS `country`,`user`.`lat` AS `lat`,`user`.`lng` AS `lng`,`user`.`email` AS `email`,`user`.`mobile` AS `mobile`,`user`.`password` AS `password`,`user`.`securitycode` AS `securitycode`,`user`.`profileimage` AS `profileimage`,`user`.`profilebackground` AS `profilebackground`,`user`.`otp` AS `otp`,`user`.`refferalid` AS `refferalid`,`user`.`profileprivacy` AS `profileprivacy`,`user`.`isnotification` AS `isnotification`,`user`.`ssnumber` AS `ssnumber`,`user`.`ssnumberimage` AS `ssnumberimage`,`user`.`experience` AS `experience`,`user`.`areaofwork` AS `areaofwork`,`user`.`drivinglicense` AS `drivinglicense`,`user`.`drivinglicenseimage` AS `drivinglicenseimage`,`user`.`driverinsuranceno` AS `driverinsuranceno`,`user`.`driverinsurancenoimage` AS `driverinsurancenoimage`,`user`.`subscribe` AS `subscribe`,`user`.`fcmtoken` AS `fcmtoken`,`user`.`ipaddress` AS `ipaddress`,`user`.`about` AS `about`,`user`.`weburl` AS `weburl`,`user`.`friendlyURL` AS `friendlyURL`,`user`.`isactive` AS `isactive`,`user`.`isdeleted` AS `isdeleted`,`user`.`createdon` AS `createdon`,`user`.`updatedon` AS `updatedon`,`user`.`deletedon` AS `deletedon`,`user`.`usercol` AS `usercol` from `user` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminuser`
--
ALTER TABLE `adminuser`
  ADD PRIMARY KEY (`iduser`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`idbooking`),
  ADD KEY `iduser` (`iduser`),
  ADD KEY `iddriver` (`iddriver`),
  ADD KEY `bookingno` (`bookingno`);

--
-- Indexes for table `bookingdetail`
--
ALTER TABLE `bookingdetail`
  ADD PRIMARY KEY (`idbookingdetail`),
  ADD KEY `idbooking` (`idbooking`),
  ADD KEY `idstylish` (`iddriver`),
  ADD KEY `idservice` (`idservice`);

--
-- Indexes for table `businesshour`
--
ALTER TABLE `businesshour`
  ADD PRIMARY KEY (`idbusinesshour`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`idcart`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`idcategory`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`idchat`),
  ADD KEY `idsender` (`idsender`,`idreceiver`,`usertype`,`idchatroom`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`idconfig`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`idcontent`);

--
-- Indexes for table `driver_rating`
--
ALTER TABLE `driver_rating`
  ADD PRIMARY KEY (`idrating`);

--
-- Indexes for table `drivewayprice`
--
ALTER TABLE `drivewayprice`
  ADD PRIMARY KEY (`iddrivewayprice`),
  ADD KEY `iddrivewaytype` (`iddrivewaytype`);

--
-- Indexes for table `drivewaytype`
--
ALTER TABLE `drivewaytype`
  ADD PRIMARY KEY (`iddrivewaytype`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`idfeatures`);

--
-- Indexes for table `featuresareaprice`
--
ALTER TABLE `featuresareaprice`
  ADD PRIMARY KEY (`idfeaturesareaprice`),
  ADD KEY `idfeatures` (`idfeatures`);

--
-- Indexes for table `featuresdepthprice`
--
ALTER TABLE `featuresdepthprice`
  ADD PRIMARY KEY (`idfeaturesdepthprice`),
  ADD KEY `idfeatures` (`idfeatures`);

--
-- Indexes for table `featuresextracharge`
--
ALTER TABLE `featuresextracharge`
  ADD PRIMARY KEY (`idextracharge`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`idnotification`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`idoffers`);

--
-- Indexes for table `otp`
--
ALTER TABLE `otp`
  ADD PRIMARY KEY (`idotp`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`idpayment`);

--
-- Indexes for table `plowsize`
--
ALTER TABLE `plowsize`
  ADD PRIMARY KEY (`idplowsize`);

--
-- Indexes for table `plowtype`
--
ALTER TABLE `plowtype`
  ADD PRIMARY KEY (`idplowtype`);

--
-- Indexes for table `pricedetail`
--
ALTER TABLE `pricedetail`
  ADD PRIMARY KEY (`idpricedetail`);

--
-- Indexes for table `promos`
--
ALTER TABLE `promos`
  ADD PRIMARY KEY (`idpromo`);

--
-- Indexes for table `searchbanner`
--
ALTER TABLE `searchbanner`
  ADD PRIMARY KEY (`idsearchbanner`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`idservice`);

--
-- Indexes for table `servicearea`
--
ALTER TABLE `servicearea`
  ADD PRIMARY KEY (`idservicearea`);

--
-- Indexes for table `servicecharges`
--
ALTER TABLE `servicecharges`
  ADD PRIMARY KEY (`idservicecharges`);

--
-- Indexes for table `servicefeature`
--
ALTER TABLE `servicefeature`
  ADD PRIMARY KEY (`idservicefeature`);

--
-- Indexes for table `trucksareaaso`
--
ALTER TABLE `trucksareaaso`
  ADD PRIMARY KEY (`idtrucksareaaso`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`),
  ADD KEY `fullname` (`fullname`,`companyname`,`email`),
  ADD KEY `mobile` (`mobile`),
  ADD KEY `lng` (`lng`),
  ADD KEY `lat` (`lat`),
  ADD KEY `zipcode` (`zipcode`);

--
-- Indexes for table `userfriend`
--
ALTER TABLE `userfriend`
  ADD PRIMARY KEY (`iduserfriend`);

--
-- Indexes for table `userpayment`
--
ALTER TABLE `userpayment`
  ADD PRIMARY KEY (`iduserpayment`);

--
-- Indexes for table `usersubscription`
--
ALTER TABLE `usersubscription`
  ADD PRIMARY KEY (`idusersubscription`);

--
-- Indexes for table `usersubscriptionfeature`
--
ALTER TABLE `usersubscriptionfeature`
  ADD PRIMARY KEY (`idservicefeature`);

--
-- Indexes for table `usertruck`
--
ALTER TABLE `usertruck`
  ADD PRIMARY KEY (`idusertruck`),
  ADD KEY `idtrucktype` (`idplowtype`),
  ADD KEY `trucktype` (`plowtype`);

--
-- Indexes for table `wallet`
--
ALTER TABLE `wallet`
  ADD PRIMARY KEY (`idwallet`),
  ADD KEY `iduser` (`iduser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminuser`
--
ALTER TABLE `adminuser`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `idbooking` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bookingdetail`
--
ALTER TABLE `bookingdetail`
  MODIFY `idbookingdetail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `businesshour`
--
ALTER TABLE `businesshour`
  MODIFY `idbusinesshour` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `idcart` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `idcategory` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `idchat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `idconfig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `idcontent` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `driver_rating`
--
ALTER TABLE `driver_rating`
  MODIFY `idrating` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `drivewayprice`
--
ALTER TABLE `drivewayprice`
  MODIFY `iddrivewayprice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `drivewaytype`
--
ALTER TABLE `drivewaytype`
  MODIFY `iddrivewaytype` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `idfeatures` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `featuresareaprice`
--
ALTER TABLE `featuresareaprice`
  MODIFY `idfeaturesareaprice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `featuresdepthprice`
--
ALTER TABLE `featuresdepthprice`
  MODIFY `idfeaturesdepthprice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `featuresextracharge`
--
ALTER TABLE `featuresextracharge`
  MODIFY `idextracharge` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `idnotification` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=398;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `idoffers` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `otp`
--
ALTER TABLE `otp`
  MODIFY `idotp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=370;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `idpayment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `plowsize`
--
ALTER TABLE `plowsize`
  MODIFY `idplowsize` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `plowtype`
--
ALTER TABLE `plowtype`
  MODIFY `idplowtype` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pricedetail`
--
ALTER TABLE `pricedetail`
  MODIFY `idpricedetail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `promos`
--
ALTER TABLE `promos`
  MODIFY `idpromo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `searchbanner`
--
ALTER TABLE `searchbanner`
  MODIFY `idsearchbanner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `idservice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `servicearea`
--
ALTER TABLE `servicearea`
  MODIFY `idservicearea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `servicecharges`
--
ALTER TABLE `servicecharges`
  MODIFY `idservicecharges` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `servicefeature`
--
ALTER TABLE `servicefeature`
  MODIFY `idservicefeature` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `trucksareaaso`
--
ALTER TABLE `trucksareaaso`
  MODIFY `idtrucksareaaso` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `userfriend`
--
ALTER TABLE `userfriend`
  MODIFY `iduserfriend` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `userpayment`
--
ALTER TABLE `userpayment`
  MODIFY `iduserpayment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `usersubscription`
--
ALTER TABLE `usersubscription`
  MODIFY `idusersubscription` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `usersubscriptionfeature`
--
ALTER TABLE `usersubscriptionfeature`
  MODIFY `idservicefeature` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=259;

--
-- AUTO_INCREMENT for table `usertruck`
--
ALTER TABLE `usertruck`
  MODIFY `idusertruck` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `wallet`
--
ALTER TABLE `wallet`
  MODIFY `idwallet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `drivewayprice`
--
ALTER TABLE `drivewayprice`
  ADD CONSTRAINT `drivewayprice_ibfk_1` FOREIGN KEY (`iddrivewaytype`) REFERENCES `drivewaytype` (`iddrivewaytype`);

--
-- Constraints for table `featuresareaprice`
--
ALTER TABLE `featuresareaprice`
  ADD CONSTRAINT `featuresareaprice_ibfk_1` FOREIGN KEY (`idfeatures`) REFERENCES `features` (`idfeatures`);

--
-- Constraints for table `featuresdepthprice`
--
ALTER TABLE `featuresdepthprice`
  ADD CONSTRAINT `featuresdepthprice_ibfk_1` FOREIGN KEY (`idfeatures`) REFERENCES `features` (`idfeatures`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
