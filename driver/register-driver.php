<?php
if(!isset($_SESSION)) { 
	session_start(); 
} 
ob_start();
error_reporting(-1);
// echo $_SERVER["REQUEST_URI"] . '<br>';
$chkURL = str_replace("?result=1", "", strtolower(basename($_SERVER["REQUEST_URI"])));
// echo $chkURL .'<br>';

if(strtolower($chkURL) == 'driver'
	OR strtolower($chkURL) == 'contact-us'
){
	$urlExt = "../";
} else {
	$urlExt = '';
}
// echo 'urlExt = ' . $urlExt . '<br>';
// echo $baseURLphp = $urlExt.'lib/_baseURL_.php';

include ($urlExt.'lib/_baseURL_.php');
include ($urlExt.'lib/_appFunction_.php');
include ($urlExt.'lib/Connection.php');
include ($urlExt.'lib/Fileupload.php');
include ($urlExt.'lib/DBQuery.php');
include ($urlExt.'lib/Helpers.php');
include ($urlExt.'lib/ReturnMsg.php');
include ($urlExt.'lib/SentEmailSMS.php');

$connection = new Connection();
$helper = new Helpers();
$FileUpload = new Fileupload();
$dbAccess = new DBQuery();
$LoadMsg = new ReturnMsg();
$sendMailMsg = new SentEmailSMS();
$appFunctions = new appFunction();

date_default_timezone_set("America/Chicago");
$nowtime = date("Y-m-d H:i:s");
$hourMin = date("H:i");
$entryDate = date("Y-m-d H:i:s");
$currDate = date("Y-m-d");
$numDate = date("YmdHis");
$intDate = date("Ymd");
$todayDate = date('Y-m-d');

$errMsg = '';

$mySQL = "";
$mySQL = "SELECT `idconfig`, `companyname`, `toplogoimage`, `mobiletoplogoimage`, `footerlogoimage`, `copyright`, `infomail`
	, `salesmail`, `adminmail`, `supportmail`, `phone`, `address`, `city`, `zipcode`, `state`, `country`, `customersupport` 
	FROM `config`";
$rsTempConfig = $dbAccess->selectSingleStmt($mySQL);
if(!empty($rsTempConfig)){
	$idconfig = $rsTempConfig['idconfig'];
	$dryftcompanyname = $rsTempConfig['companyname'];
	$toplogoimage = $rsTempConfig['toplogoimage'];
	$mobiletoplogoimage = $rsTempConfig['mobiletoplogoimage'];
	$footerlogoimage = $rsTempConfig['footerlogoimage'];
	$copyright = $rsTempConfig['copyright'];
	$infomail = $rsTempConfig['infomail'];
	$salesmail = $rsTempConfig['salesmail'];
	$adminmail = $rsTempConfig['adminmail'];
	$supportmail = $rsTempConfig['supportmail'];
	$dryftphone = $rsTempConfig['phone'];
	$dryftaddress = $rsTempConfig['address'];
	$dryftcity = $rsTempConfig['city'];
	$dryftzipcode = $rsTempConfig['zipcode'];
	$dryftstate = $rsTempConfig['state'];
	$dryftcountry = $rsTempConfig['country'];
	$customersupport = $rsTempConfig['customersupport'];
} else {
	$idconfig = '';
	$dryftcompanyname = '';
	$toplogoimage = '';
	$footerlogoimage = '';
	$copyright = '';
	$infomail = '';
	$salesmail = '';
	$adminmail = '';
	$supportmail = '';
	$dryftphone = '';
	$dryftaddress = '';
	$dryftcity = '';
	$dryftzipcode = '';
	$dryftstate = '';
	$dryftcountry = '';
	$customersupport = '';
}

function ob_html_compress($buf){
    return str_replace(array("\n","\r","\t"),'',$buf);
}

ob_start("ob_html_compress");
?>

<?php
// error_reporting(-1);
// include '../include/_header_.php';

$errorMsg = array();

$fullname = '';
$profileimage = '';
$address = '';
$city = '';
$state = '';
$zipcode = '';
$phone = '';
$email = '';

//SELECT `idusertruck`, `iduser`, `idplowtype`, `plowtype`, `truckname`, `vehiclenumber`, `registrationcertificateimage`, `insuranceimage`, `truckimage`, `plowsize`, `rubberblade`, `snowblower`, `saltspreader`, `bobcat`, `shovellers`, `misc`, `isactive`, `createdon` FROM `usertruck` WHERE 1
$experience = '';
$areaofwork = '';
$drivinglicense = '';
$drivinglicenseimage = '';
$driverinsuranceno = '';
$driverinsurancenoimage = '';
$ssnumber = '';
$ssnumberimage = '';

// TRUCK DETAIL
$truckname = '';
$vehiclenumber = '';
$registrationcertificateimage = '';
$truckinsuranceno = '';
$truckinsuranceimage = '';
$truckimage = '';

$idplowtype = '';
$idplowsize = '';

$saltspreader = '';
$rubberblade = '';
$bobcat = '';
$snowblower = '';
$shovellers = '';
$driveragreement = '';

$selected = '';

if (!empty($_POST['submit'])){
	$usertype = 'DRIVER';
	$isactive = '0';
	$password = randomPassword();
	
	$fullname = trim($appFunctions->validHTML($_POST['fullname']));
	$address = trim($appFunctions->validHTML($_POST['address']));
	$city = trim($appFunctions->validHTML($_POST['city']));
	$state = trim($appFunctions->validHTML($_POST['state']));
	$zipcode = trim($appFunctions->validHTML($_POST['zipcode']));
	$phone = trim($appFunctions->validHTML($_POST['phone']));
	$email = trim($appFunctions->validHTML($_POST['email']));
	
	$experience = trim($appFunctions->validHTML($_POST['experience']));
	$areaofwork = trim($appFunctions->validHTML($_POST['areaofwork']));
	$drivinglicense = trim($appFunctions->validHTML($_POST['drivinglicense']));
	$driverinsuranceno = trim($appFunctions->validHTML($_POST['driverinsuranceno']));
	$ssnumber = trim($appFunctions->validHTML($_POST['ssnumber']));
	
	$truckname = trim($appFunctions->validHTML($_POST['truckname']));
	$vehiclenumber = trim($appFunctions->validHTML($_POST['vehiclenumber']));
	$truckinsuranceno = trim($appFunctions->validHTML($_POST['truckinsuranceno']));
	
	$idplowtype = trim($appFunctions->validHTML($_POST['idplowtype']));
	$idplowsize = trim($appFunctions->validHTML($_POST['idplowsize']));
	
	if(!empty($_POST['saltspreader'])){
		$saltspreader = $appFunctions->validHTML($_POST['saltspreader']);
	}
	if(!empty($_POST['rubberblade'])){
		$rubberblade = $appFunctions->validHTML($_POST['rubberblade']);
	}
	if(!empty($_POST['bobcat'])){
		$bobcat = $appFunctions->validHTML($_POST['bobcat']);
	}
	if(!empty($_POST['snowblower'])){
		$snowblower = $appFunctions->validHTML($_POST['snowblower']);
	}
	if(!empty($_POST['shovellers'])){
		$shovellers = $appFunctions->validHTML($_POST['shovellers']);
	}
	
	if(!empty($_POST['driveragreement'])){
		$driveragreement = $_POST['driveragreement'];
	}
	
	// CHECK IMAGE EXT
	$uploadImagePIName = basename($_FILES['profileimage']['name']);
	$uploadImagePISize = basename($_FILES['profileimage']['size']);
	$uploadImagePIExt = pathinfo($uploadImagePIName,PATHINFO_EXTENSION);
	
	$uploadImageDLName = basename($_FILES['drivinglicenseimage']['name']);
	$uploadImageDLSize = basename($_FILES['drivinglicenseimage']['size']);
	$uploadImageDLExt = pathinfo($uploadImageDLName,PATHINFO_EXTENSION);
	
	$uploadImageDIName = basename($_FILES['driverinsurancenoimage']['name']);
	$uploadImageDISize = basename($_FILES['driverinsurancenoimage']['size']);
	$uploadImageDIExt = pathinfo($uploadImageDIName,PATHINFO_EXTENSION);
	
	$uploadImageSSNName = basename($_FILES['ssnumberimage']['name']);
	$uploadImageSSNSize = basename($_FILES['ssnumberimage']['size']);
	$uploadImageSSNExt = pathinfo($uploadImageSSNName,PATHINFO_EXTENSION);
	
	$uploadImageTIName = basename($_FILES['truckimage']['name']);
	$uploadImageTISize = basename($_FILES['truckimage']['size']);
	$uploadImageTIExt = pathinfo($uploadImageTIName,PATHINFO_EXTENSION);
	
	$uploadImageRCName = basename($_FILES['registrationcertificateimage']['name']);
	$uploadImageRCSize = basename($_FILES['registrationcertificateimage']['size']);
	$uploadImageRCExt = pathinfo($uploadImageRCName,PATHINFO_EXTENSION);
	
	$uploadImageTINName = basename($_FILES['truckinsuranceimage']['name']);
	$uploadImageTINSize = basename($_FILES['truckinsuranceimage']['size']);
	$uploadImageTINExt = pathinfo($uploadImageTINName,PATHINFO_EXTENSION);
	
	$friendlyURL = strtolower($fullname);
	$friendlyURL = $appFunctions->friendlyURL($friendlyURL);
	
	// echo '<div style="padding:1% 10%;">';
	//echo 'uploadImageSize = ' . $uploadImageSize .'<br>';
	//echo $uploadImageExt .'<br>';
	
	if(empty($fullname)){
		$errorMsg[] = 'FULLNAME EMPTY';
	}
	if(empty($uploadImagePIName)){
		$errorMsg[] = 'PROFILE IMAGE EMPTY';
	}
	if(empty($address)){
		$errorMsg[] = 'ADDRESS EMPTY';
	}
	if(empty($city)){
		$errorMsg[] = 'CITY EMPTY';
	}
	if(empty($state)){
		$errorMsg[] = 'STATE EMPTY';
	}
	if(empty($zipcode)){
		$errorMsg[] = 'ZIPCODE EMPTY';
	}
	if(empty($phone)){
		$errorMsg[] = 'PHONE EMPTY';
	}
	if(empty($email)){
		$errorMsg[] = 'EMAIL EMPTY';
	}
	if(empty($experience)){
		$errorMsg[] = 'EXPERIENCE EMPTY';
	}
	if(empty($areaofwork)){
		$errorMsg[] = 'AREA OF WORK EMPTY';
	}
	if(empty($drivinglicense)){
		$errorMsg[] = 'DL EMPTY';
	}
	if(empty($driverinsuranceno)){
		$errorMsg[] = 'DRIVER INSURANCE EMPTY';
	}
	//if(empty($ssnumber)){
	//	$errorMsg[] = 'SSN EMPTY';
	//}
	if(empty($truckname)){
		$errorMsg[] = 'TRUCK NAME EMPTY';
	}
	if(empty($vehiclenumber)){
		$errorMsg[] = 'VEHICLE NO EMPTY';
	}
	if(empty($truckinsuranceno)){
		$errorMsg[] = 'TRUCK INSURANCE EMPTY';
	}
	if(empty($idplowtype)){
		$errorMsg[] = 'PLOW TYPE EMPTY';
	}
	if(empty($idplowsize)){
		$errorMsg[] = 'PLOW SIZE EMPTY';
	}
	if(empty($driveragreement)){
		$errorMsg[] = 'ACCEPT AGREEMENT';
	}
	if(empty($saltspreader) AND empty($rubberblade) AND empty($bobcat) AND empty($snowblower) AND empty($shovellers)){
		$errorMsg[] = 'ANY ONE SELECT';
	}
	if(!empty($uploadImagePIName) AND strtolower($uploadImagePIExt) != "jpg" AND strtolower($uploadImagePIExt) != "jpeg" AND strtolower($uploadImagePIExt) != "png"){
		//$errorMsg = 'Please upload driving lincense imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'PROFILE IMAGE FORMAT';
	} else if(!empty($uploadImagePIName) AND ($uploadImagePISize == 0 OR $uploadImagePISize > 10000000)){
		//$errorMsg = 'Please upload driving lincense image size less than 500KB';
		$errorMsg[] = 'PROFILE IMAGE SIZE';
	}
	if(empty($uploadImageDLName)){
		$errorMsg[] = 'DL IMAGE EMPTY';
	}
	if(!empty($uploadImageDLName) AND strtolower($uploadImageDLExt) != "jpg" AND strtolower($uploadImageDLExt) != "jpeg" AND strtolower($uploadImageDLExt) != "png"){
		//$errorMsg = 'Please upload driving lincense imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'DL IMAGE FORMAT';
	} else if(!empty($uploadImageDLName) AND ($uploadImageDLSize == 0 OR $uploadImageDLSize > 10000000)){
		//$errorMsg = 'Please upload driving lincense image size less than 500KB';
		$errorMsg[] = 'DL IMAGE SIZE';
	} 
	if(empty($uploadImageDIName)){
		$errorMsg[] = 'DI IMAGE EMPTY';
	}
	if(!empty($uploadImageDIName) AND strtolower($uploadImageDIExt) != "jpg" AND strtolower($uploadImageDIExt) != "jpeg" AND strtolower($uploadImageDIExt) != "png"){
		//$errorMsg = 'Please upload driver insurance imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'DI IMAGE FORMAT';
	} else if(!empty($uploadImageDIName) AND ($uploadImageDISize == 0 OR $uploadImageDISize > 10000000)){
		//$errorMsg = 'Please upload driver insurance image size less than 500KB';
		$errorMsg[] = 'DI IMAGE SIZE';
	} 
	if(empty($uploadImageSSNName)){
		$errorMsg[] = 'SSN IMAGE EMPTY';
	}
	if(!empty($uploadImageSSNName) AND strtolower($uploadImageSSNExt) != "jpg" AND strtolower($uploadImageSSNExt) != "jpeg" AND strtolower($uploadImageSSNExt) != "png"){
		//$errorMsg = 'Please upload imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'SSN IMAGE FORMAT';
	} else if(!empty($uploadImageSSNName) AND ($uploadImageSSNSize == 0 OR $uploadImageSSNSize > 10000000)){
		//$errorMsg = 'Please upload image size less than 500KB';
		$errorMsg[] = 'SSN IMAGE SIZE';
	}
	if(empty($uploadImageTIName)){
		$errorMsg[] = 'TRUCK IMAGE EMPTY';
	}
	if(!empty($uploadImageTIName) AND strtolower($uploadImageTIExt) != "jpg" AND strtolower($uploadImageTIExt) != "jpeg" AND strtolower($uploadImageTIExt) != "png"){
		//$errorMsg = 'Please upload imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'TRUCK IMAGE FORMAT';
	} else if(!empty($uploadImageTIName) AND ($uploadImageTISize == 0 OR $uploadImageTISize > 10000000)){
		//$errorMsg = 'Please upload image size less than 500KB';
		$errorMsg[] = 'TRUCK IMAGE SIZE';
	}
	if(empty($uploadImageRCName)){
		$errorMsg[] = 'REGISTRATION IMAGE EMPTY';
	}
	if(!empty($uploadImageRCName) AND strtolower($uploadImageRCExt) != "jpg" AND strtolower($uploadImageRCExt) != "jpeg" AND strtolower($uploadImageRCExt) != "png"){
		//$errorMsg = 'Please upload imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'REGISTRATION IMAGE FORMAT';
	} else if(!empty($uploadImageRCName) AND ($uploadImageRCSize == 0 OR $uploadImageRCSize > 10000000)){
		//$errorMsg = 'Please upload image size less than 500KB';
		$errorMsg[] = 'REGISTRATION IMAGE SIZE';
	}
	if(empty($uploadImageTINName)){
		$errorMsg[] = 'TRUCK INSURANCE IMAGE EMPTY';
	}
	if(!empty($uploadImageTINName) AND strtolower($uploadImageTINExt) != "jpg" AND strtolower($uploadImageTINExt) != "jpeg" AND strtolower($uploadImageTINExt) != "png"){
		//$errorMsg = 'Please upload imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'TRUCK INSURANCE IMAGE FORMAT';
	} else if(!empty($uploadImageTINName) AND ($uploadImageTINSize == 0 OR $uploadImageTINSize > 10000000)){
		//$errorMsg = 'Please upload image size less than 500KB';
		$errorMsg[] = 'TRUCK INSURANCE IMAGE SIZE';
	}
	//var_dump($errorMsg);
	
	$mySQL = "";
	$mySQL = "SELECT `email`, `mobile` FROM `user` WHERE `usertype` = 'DRIVER' AND (`email` = '".$appFunctions->validSQL($email,"")."' OR `mobile` = '".$appFunctions->validSQL($phone,"")."')";
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	if(!empty($rsTemp)){
		if($rsTemp['email'] == $email AND $rsTemp['mobile'] == $phone){
			$errorMsg[] = 'DRIVER EMAIL & PHONE EXIST';
		} else if($rsTemp['email'] == $email){
			$errorMsg[] = 'DRIVER EMAIL EXIST';
		} else if($rsTemp['mobile'] == $phone){
			$errorMsg[] = 'DRIVER PHONE EXIST';
		}
	}

	// echo COUNT($errorMsg) .'<br>';
	// echo '<pre>';
	// var_dump($errorMsg);
	// echo '<br>';
	// exit;

	$fileDir = '../driverimage/';
	if(empty($errorMsg)){
		// PROFILE IMAGE
		if(!empty($uploadImagePIName)) {
			$uploadImagePIName = time() . '_' . str_replace('.php', '', $uploadImagePIName);
			move_uploaded_file($_FILES['profileimage']['tmp_name'], $fileDir.$uploadImagePIName);
			//copy($_FILES['profileimage']['tmp_name'], $fileDir.$uploadImagePIName);
		}else{
			$uploadImagePIName = $uploadPIImage_Old;
		}
		
		// DL IMAGE
		if(!empty($uploadImageDLName)) {
			$uploadImageDLName = time() . '_' . str_replace('.php', '', $uploadImageDLName);
			move_uploaded_file($_FILES['drivinglicenseimage']['tmp_name'], $fileDir.$uploadImageDLName);
			//copy($_FILES['drivinglicenseimage']['tmp_name'], $fileDir.$uploadImageDLName);
		}else{
			$drivinglicenseimage = $uploadDLImage_Old;
		}
		
		// DI IMAGE
		if(!empty($uploadImageDIName)) {
			$uploadImageDIName = time() . '_' . str_replace('.php', '', $uploadImageDIName);
			move_uploaded_file($_FILES['driverinsurancenoimage']['tmp_name'], $fileDir.$uploadImageDIName);
			//copy($_FILES['driverinsurancenoimage']['tmp_name'], $fileDir.$uploadImageDIName);
		}else{
			$uploadImageDIName = $uploadDIImage_Old;
		}
		
		// DI IMAGE
		if(!empty($uploadImageSSNName)) {
			$uploadImageSSNName = time() . '_' . str_replace('.php', '', $uploadImageSSNName);
			move_uploaded_file($_FILES['ssnumberimage']['tmp_name'], $fileDir.$uploadImageSSNName);
			//copy($_FILES['ssnumberimage']['tmp_name'], $fileDir.$uploadImageSSNName);
		}else{
			$uploadImageSSNName = $uploadSSNImage_Old;
		}
		
		// RC IMAGE
		if(!empty($uploadImageRCName)) {
			$uploadImageRCName = time() . '_' . str_replace('.php', '', $uploadImageRCName);
			move_uploaded_file($_FILES['registrationcertificateimage']['tmp_name'], $fileDir.$uploadImageRCName);
			//copy($_FILES['registrationcertificateimage']['tmp_name'], $fileDir.$uploadImageRCName);
		}else{
			$uploadImageRCName = $uploadRCImage_Old;
		}
		
		// TI IMAGE
		if(!empty($uploadImageTINName)) {
			$uploadImageTINName = time() . '_' . str_replace('.php', '', $uploadImageTINName);
			move_uploaded_file($_FILES['truckinsuranceimage']['tmp_name'], $fileDir.$uploadImageTINName);
			//copy($_FILES['truckinsuranceimage']['tmp_name'], $fileDir.$uploadImageTINName);
		}else{
			$uploadImageTINName = $uploadTIImage_Old;
		}
		
		// TRUCK IMAGE
		if(!empty($uploadImageTIName)) {
			$uploadImageTIName = time() . '_' . str_replace('.php', '', $uploadImageTIName);
			move_uploaded_file($_FILES['truckimage']['tmp_name'], $fileDir.$uploadImageTIName);
			//copy($_FILES['truckimage']['tmp_name'], $fileDir.$uploadImageTIName);
		}else{
			$uploadImageTIName = $uploadTIImage_Old;
		}
		
		if(empty($iduser)){
			$mySQL = "";
			$mySQL .= "INSERT INTO `user` (";
			$mySQL .= "  fullname";
			$mySQL .= ", usertype";
			$mySQL .= ", password";
			$mySQL .= ", address";
			$mySQL .= ", city";
			$mySQL .= ", state";
			$mySQL .= ", zipcode";
			$mySQL .= ", mobile";
			$mySQL .= ", email";
			$mySQL .= ", experience";
			$mySQL .= ", areaofwork";
			$mySQL .= ", drivinglicense";
			$mySQL .= ", drivinglicenseimage";
			$mySQL .= ", driverinsuranceno";
			$mySQL .= ", driverinsurancenoimage";
			$mySQL .= ", ssnumber";
			$mySQL .= ", ssnumberimage";
			$mySQL .= ", profileimage";
			//$mySQL .= ", isactive";
			$mySQL .= ") VALUES (";
			$mySQL .= " '". $appFunctions->validSQL($fullname,"")."'";
			$mySQL .= ",'". $appFunctions->validSQL($usertype,"")."'";
			$mySQL .= ",'". $appFunctions->validSQL($password,"")."'";
			$mySQL .= ",'". $appFunctions->validSQL($address,"")."'";
			$mySQL .= ",'". $appFunctions->validSQL($city,"")."'";
			$mySQL .= ",'". $appFunctions->validSQL($state,"")."'";
			$mySQL .= ",'". $appFunctions->validSQL($zipcode,"")."'";
			$mySQL .= ",'". $appFunctions->validSQL($phone,"")."'";
			$mySQL .= ",'". $appFunctions->validSQL($email,"")."'";
			$mySQL .= ",'". $appFunctions->validSQL($experience,"")."'";
			$mySQL .= ",'". $appFunctions->validSQL($areaofwork,"")."'";
			$mySQL .= ",'". $appFunctions->validSQL($drivinglicense,"")."'";
			$mySQL .= ",'". $appFunctions->validSQL($uploadImageDLName,"")."'";
			$mySQL .= ",'". $appFunctions->validSQL($driverinsuranceno,"")."'";
			$mySQL .= ",'". $appFunctions->validSQL($uploadImageDIName,"")."'";
			$mySQL .= ",'". $appFunctions->validSQL($ssnumber,"")."'";
			$mySQL .= ",'". $appFunctions->validSQL($uploadImageSSNName,"")."'";
			$mySQL .= ",'". $appFunctions->validSQL($uploadImagePIName,"")."'";
			//$mySQL .= ",'". $appFunctions->validSQL($isactive,"")."'";
			$mySQL .= ");";
			//echo $mySQL .'<br>';			
			//exit;
			$dbAccess->queryExec($mySQL);
			$iduser = $dbAccess->LastId();
			
			$mySQL = "";
			$mySQL .= "UPDATE `user` SET";
			$mySQL .= " `friendlyURL` = '". $appFunctions->validSQL($iduser . '-' . trim($friendlyURL),"") ."'";
			$mySQL .= " WHERE `iduser` = '".$appFunctions->validSQL($iduser,"")."';";
			//echo $mySQL .'<br>';			
			//exit;
			$dbAccess->queryExec($mySQL);
			
			$plowtype = $dbAccess->getDetail('plowtype','plowtype','idplowtype',$idplowtype);
			$plowsize = $dbAccess->getDetail('plowsize','plowsize','idplowsize',$idplowsize);
			
			$mySQL = "";
			$mySQL = "INSERT INTO `usertruck` SET";
			$mySQL .= " `iduser` = '".$appFunctions->validSQL($iduser,"")."'";
			$mySQL .= ", `idplowtype` = '".$appFunctions->validSQL($idplowtype,"")."'";
			$mySQL .= ", `plowtype` = '".$appFunctions->validSQL($plowtype,"")."'";
			$mySQL .= ", `truckname` = '".$appFunctions->validSQL($truckname,"")."'";
			$mySQL .= ", `vehiclenumber` = '".$appFunctions->validSQL($vehiclenumber,"")."'";
			$mySQL .= ", `registrationcertificateimage` = '".$appFunctions->validSQL($uploadImageRCName,"")."'";
			$mySQL .= ", `truckinsuranceno` = '".$appFunctions->validSQL($truckinsuranceno,"")."'";
			$mySQL .= ", `truckinsuranceimage` = '".$appFunctions->validSQL($uploadImageTINName,"")."'";
			$mySQL .= ", `truckimage` = '".$appFunctions->validSQL($uploadImageTIName,"")."'";
			$mySQL .= ", `plowsize` = '".$appFunctions->validSQL($plowsize,"")."'";
			$mySQL .= ", `rubberblade` = '".$appFunctions->validSQL($rubberblade,"")."'";
			$mySQL .= ", `snowblower` = '".$appFunctions->validSQL($snowblower,"")."'";
			$mySQL .= ", `saltspreader` = '".$appFunctions->validSQL($saltspreader,"")."'";
			$mySQL .= ", `bobcat` = '".$appFunctions->validSQL($bobcat,"")."'";
			$mySQL .= ", `shovellers` = '".$appFunctions->validSQL($shovellers,"")."'";
			//$mySQL .= ", `misc` = '".$appFunctions->validSQL($misc,"")."'";
			//$mySQL .= ", `isactive` = '".$appFunctions->validSQL($iduser,"")."'";
			//$mySQL .= ", `createdon` = '".$appFunctions->validSQL($iduser,"")."'";
			//echo $mySQL .'<br>';			
			//exit;
			$dbAccess->queryExec($mySQL);
		}
		
		header("location:".$baseURL."/register/driver?result=1");
		exit;
	}
	// echo '</div>';
}
?>	
<!-- CSS Library -->
<link href="<?=$baseURL;?>/assets/css/all.css" type="text/css" rel="stylesheet">
<link href="<?=$baseURL;?>/assets/css/bootstrap.min.css" type="text/css" rel="stylesheet">
<link href="<?=$baseURL;?>/assets/css/animate.css" type="text/css" rel="stylesheet" media="all" >
<link href="<?=$baseURL;?>/assets/css/style.css" type="text/css" rel="stylesheet" media="all" >

<!-- Custom CSS -->

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->
<!-- -->

<script src="<?=$baseURL;?>/js/_Application_.js"></script>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<!-- <h2 align="center">Driver’s Registration</h2> -->
			<h3 align="center">Work With DRYFT</h3>
			
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	
	<form role="form" action="" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3>Personal Information</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<label>Full Name</label>
								<input type="text" name="fullname" id="fullname" value="<?=$fullname;?>" maxlength="250" autocomplete="off" class="form-control" placeholder="Full Name" autofocus onkeypress="return isAlpha(event)" required />
							</div>
							<div class="col-md-6">
								<label>Driver Photo</label>
								<input type="file" name="profileimage" id="profileimage" class="form-control" placeholder="Driving Photo" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Address</label>
								<input type="text" name="address" id="address" value="<?=$address;?>" maxlength="250" autocomplete="off" class="form-control" placeholder="Address" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>City</label>
								<input type="text" name="city" id="city" value="<?=$city;?>" maxlength="100" autocomplete="off" class="form-control" placeholder="City" onkeypress="return isAlpha(event)" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>State</label>
								<input type="text" name="state" id="state" value="<?=$state;?>" maxlength="100" autocomplete="off" class="form-control" placeholder="State" onkeypress="return isAlpha(event)" required />
							</div>
							<div class="col-md-6">
								<label>Zipcode</label>
								<input type="text" name="zipcode" id="zipcode" value="<?=$zipcode;?>" maxlength="10" autocomplete="off" class="form-control" placeholder="Zipcode" onkeypress="return isNumber(event)" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Phone</label>
								<input type="text" name="phone" id="phone" value="<?=$phone;?>" maxlength="10" autocomplete="off" class="form-control" placeholder="Phone" required onkeyup="removeErrClass('mobile');removeHide('mobile', 'errMobile');" pattern="\d{10}" onkeypress="return isNumber(event)" />
							</div>
							<div class="col-md-6">
								<label>Email</label>
								<input type="email" name="email" id="email" value="<?=$email;?>" maxlength="150" autocomplete="off" class="form-control" placeholder="Email" required onkeyup="removeErrClass('email');removeHide('email', 'errEmail');" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" />
							</div>
						</div>
					</div>	
					<!-- /.panel-body -->	
					
					<div class="panel-heading">
						<h3>Other Information</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<label>Experience</label>
								<select name="experience" id="experience" class="form-control" autocomplete="off" placeholder="Driving Experience" required />
									<option value="">Select Experience</option>
									<?php
									for($i=1;$i<70;$i++){
										if(!empty($experience)){
											if($i == $experience){
												$selected = 'SELECTED';
											} else {
												$selected = '';
											}
										}
									?>
										<option value="<?=$i;?>" <?=$selected;?>><?=$i;?></option>
									<?php
									}
									?>
								</select>
							</div>
							<div class="col-md-6">
								<label>Area of Work</label>
								<input type="text" name="areaofwork" id="areaofwork" value="<?=$areaofwork;?>" maxlength="255" class="form-control" placeholder="Area of Wwork" autocomplete="off" onkeypress="return isAlpha(event)" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Driving License No.</label>
								<input type="text" name="drivinglicense" id="drivinglicense" value="<?=$drivinglicense;?>" maxlength="50" class="form-control" placeholder="Driving License No." autocomplete="off" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>Driving License Image</label>
								<input type="file" name="drivinglicenseimage" id="drivinglicenseimage" class="form-control" placeholder="Driving License Image" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Insurance No.</label>
								<input type="text" name="driverinsuranceno" id="driverinsuranceno" value="<?=$driverinsuranceno;?>" maxlength="50" class="form-control" placeholder="Insurance No." autocomplete="off" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>Insurance Image</label>
								<input type="file" name="driverinsurancenoimage" id="driverinsurancenoimage" class="form-control" placeholder="Driver Personal Insurance" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Social Security No.</label>
								<input type="text" name="ssnumber" id="ssnumber" value="<?=$ssnumber;?>" maxlength="50" class="form-control" placeholder="Social Security No." autocomplete="off" onkeypress="return isAlphaNumeric(event)" />
							</div>
							<div class="col-md-6">
								<label>SSN Image</label>
								<input type="file" name="ssnumberimage" id="ssnumberimage" class="form-control" placeholder="SSN Image" autocomplete="off" />
							</div>
						</div>
					</div>	
					<!-- /.panel-body -->	
					
					<div class="panel-heading">
						<h3>Truck / Equipment</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<label>Truck Type</label>
								<input type="text" name="truckname" id="truckname" value="<?=$truckname;?>" maxlength="255" class="form-control" placeholder="Truck Type" autocomplete="off" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>Truck Image</label>
								<input type="file" name="truckimage" id="truckimage" class="form-control" placeholder="Truck Image" autocomplete="off" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Vehicle Number</label>
								<input type="text" name="vehiclenumber" id="vehiclenumber" value="<?=$vehiclenumber;?>" maxlength="25" class="form-control" placeholder="Vehicle Number" autocomplete="off" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>Registration Certificate</label>
								<input type="file" name="registrationcertificateimage" id="registrationcertificateimage" class="form-control" placeholder="Registration Certificate" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Insurance Number</label>
								<input type="text" name="truckinsuranceno" id="truckinsuranceno" value="<?=$truckinsuranceno;?>" maxlength="25" class="form-control" placeholder="Insurance Number" autocomplete="off" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>Truck Insurance</label>
								<input type="file" name="truckinsuranceimage" id="truckinsuranceimage" class="form-control" placeholder="Truck Insurance" autocomplete="off" required />
							</div>
						</div>
						
					</div>
					<div class="panel-heading">
						<h3>Truck Other Detail</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<label>Plow Type</label>
								<?php
								$mySQL = "";
								$mySQL = "SELECT `idplowtype`, `plowtype` FROM `plowtype` WHERE 1=1 AND  isactive = '1'"; 
								$mySQL .= " ORDER BY plowtype";
								//echo $mySQL;
								$rsTemp = $dbAccess->selectData($mySQL);
								
								$selected = '';
								?>
								<select name="idplowtype" id="idplowtype" class="form-control" required>
									<option value="">Select</option>
									<?php
									if (!empty($rsTemp)){
										foreach($rsTemp AS $rsTempVal){
											if(!empty($idplowtype)){
												// if(in_array($rsTempVal['idplowtype'], $idplowtype)){
												if($rsTempVal['idplowtype'] == $idplowtype){
													$selected = 'SELECTED';
												} else {
													$selected = '';
												}
											}
									?>
											<option value="<?=$rsTempVal['idplowtype'];?>" <?=$selected;?>><?=$rsTempVal['plowtype'];?></option>
									<?php
										}
									}
									?>
								</select>
							</div>
							<div class="col-md-6">
								<label>Plow Size</label>
								<?php
								$mySQL = "";
								$mySQL = "SELECT `idplowsize`, `plowsize` FROM `plowsize` WHERE 1=1 AND  isactive = '1'"; 
								$mySQL .= " ORDER BY plowsize";
								//echo $mySQL;
								$rsTemp = $dbAccess->selectData($mySQL);
								
								$selected = '';
								?>
								<select name="idplowsize" id="idplowsize" class="form-control" required>
									<option value="">Select</option>
									<?php
									if (!empty($rsTemp)){
										foreach($rsTemp AS $rsTempVal){
											if(!empty($idplowsize)){
												// if(in_array($rsTempVal['idplowsize'], $idplowsize)){
												if($rsTempVal['idplowsize'] == $idplowsize){
													$selected = 'SELECTED';
												} else {
													$selected = '';
												}
											}
									?>
											<option value="<?=$rsTempVal['idplowsize'];?>" <?=$selected;?>><?=$rsTempVal['plowsize'];?></option>
									<?php
										}
									}
									?>
								</select>								
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="checkbox">
									<label><input type="checkbox" name="saltspreader" id="saltspreader" value="CHECKED" <?=$saltspreader;?> /> Salt Spreader</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" name="rubberblade" id="rubberblade" value="CHECKED" <?=$rubberblade;?> /> Rubber Blade</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="checkbox">
									<label><input type="checkbox" name="bobcat" id="bobcat" value="CHECKED" <?=$bobcat;?> /> Bobcat</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" name="snowblower" id="snowblower" value="CHECKED" <?=$snowblower;?> /> Snow Blower</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="checkbox">
									<label><input type="checkbox" name="shovellers" id="shovellers" value="CHECKED" <?=$shovellers;?> /> Shovellers</label>
								</div>
							</div>
						</div>
					</div>
					<!-- /.panel-body -->	
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group text-center">
									<div>
										<input type="checkbox" name="driveragreement" id="driveragreement" checked value="CHECKED" required> 
										Accept Agreement. <a href="<?=$baseURL;?>/driver-agreement.pdf" target="_blank">Download Here</a>
									</div>
									<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
									<div class="text-center text-danger">
									<?php 
									if (!empty($_GET['result']) AND $_GET['result'] == '1') { 
										echo 'Thank You! Your application is successfully submitted. Please wait for DRYFT admin to approve your application.';
									}

									if (in_array('DRIVER EMAIL & PHONE EXIST', $errorMsg)){
										echo 'Driver email & phone no. already registered with us.';
									} else if(in_array('DRIVER EMAIL EXIST', $errorMsg)){
										echo 'Driver email already registered with us.';
									} else if(in_array('DRIVER PHONE EXIST', $errorMsg)){
										echo 'Driver phone no. already registered with us.';
									} else if(in_array('ANY ONE SELECT', $errorMsg)){
										echo 'Please select any one of the option from the following (Salt Spreader, Bobcat, Shovellers, Rubber Blade, Snow Blower)';
									} 
									?>
									</div>
								</div>
								<!-- /.col-lg-6 (nested) -->
							</div>
						</div>
					</div>
					<!-- /.row (nested) -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</form>
</div>

<?php
// include '../include/_footer_.php';
?>

<!-- JS Libraries --> 
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> 
<script src="<?=$baseURL;?>/assets/js/all.js"></script> 
<script src="<?=$baseURL;?>/assets/js/bootstrap.min.js"></script> 
<script src="<?=$baseURL;?>/assets/js/popper.min.js"></script> 
<script src="<?=$baseURL;?>/assets/js/wow.min.js"></script> 
<script src="<?=$baseURL;?>/assets/js/custom.js"></script> 
