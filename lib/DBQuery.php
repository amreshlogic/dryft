<?php
class DBQuery extends Connection {

    private $sql;

	//    1.insert simple value in table
    public function insertData($tableName, $fieldName, $valueName) {
        try {
            $this->sql = "INSERT INTO $tableName ($fieldName) VALUES ($valueName)";
            $this->conn->exec($this->sql);
        } catch (PDOException $ex) {
            echo $ex;
        }
    }

	//    2. count data from table
    public function countDtata($statement) {
        try {
            $this->sql = $statement;
            $this->conn->query($this->sql);
            $number = 0;
            foreach ($this->conn->query($this->sql) as $row) {
                $number++;
            }
            return $number;
        } catch (PDOException $ex) {
            echo $ex;
        }
    }

	//   3. get last inserted id
    public function LastId() {
        try {
            return $this->conn->lastInsertId();
        } catch (PDOException $ex) {
            echo $ex;
        }
    }
	//   4.  close connection
    public function closeConnection() {
        try {
            $this->conn = null;
        } catch (PDOException $ex) {
            echo $ex;
        }
    }
	//   5. select statement
    public function selectData($statement) {
        try {
			//echo $statement
            $this->sql = $statement;
            $q = $this->conn->query($this->sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
            $result = array();
            while ($r = $q->fetch()) {
                $result[] = $r;
            }
            return $result;
        } catch (PDOException $ex) {
            echo $ex;
        }
    }
    //   5. select statement
    public function selectSingleStmt($statement) {
        try {
            $this->sql = $statement;
            $q = $this->conn->query($this->sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
            $r = $q->fetch();
            return $r;
        } catch (PDOException $ex) {
            echo $ex;
        }
    }
	//    5. update statement
    public function updateData($statement) {
        try {
            $this->sql = $statement;
            $this->conn->exec($this->sql);
        } catch (Exception $ex) {
            
        }
    }
    //    5. Insert , update Delite statement
    public function queryExec($statement) {
        try {
            $this->sql = $statement;
            //$this->conn->exec($this->sql);
			return $this->conn->exec($this->sql);
        } catch (Exception $ex) {
            echo '<hr><b>System Error: '. $ex->getMessage() .'</b>';
        }
    }
	// transection bigen
	public function beginTransaction()
	{
		try {           
			return $this->conn->beginTransaction();
			} catch (Exception $ex) {            
			}	
	}
	// transection COMMIT
	public function commit()
	{
		try {           
			   return $this->conn->commit();
			} catch (Exception $ex) { 
					   
			}	
	}
	
	// transaction ROLL BACK
	public function rollback()
	{
		try {           
			   return $this->conn->rollback();
			} catch (Exception $ex) {            
			}	
	}
		
	function tableExists($table) {

		// Try a select statement against the table
		try {
			$result = $this->conn->query("SELECT 1 FROM $table LIMIT 1");
		} catch (Exception $e) {
			// We got an exception == table not found
			return FALSE;
		}

		// Result is either boolean FALSE (no table found) or PDOStatement Object (table found)
		return $result !== FALSE;
	}
	
	//******************************************************************
	//Format values inserted into SQL statements before executing the 
	//SQL statement. This is to prevent SQL injection attacks, and to 
	//ensure that certain characters are interpreted correctly.
	//******************************************************************
	function validSQL($aString, $aType){
		//Declare Variables
		$tempString = '';
		$tempString = $aString;
	
		//Check for empty values 
		if ($tempString  == null OR empty($tempString)){
			$validSQL = "";
			return $validSQL;
		}
		
		//Clean up SQL
		if (strtolower($tempString) == null){				//Nulls
			$validSQL = $tempString;
		} else {
			switch (strtoupper($aType)){
				case "I":									//Integer
					$validSQL = intval($tempString);
					break;
				case "D":									//Double
					$validSQL = string($tempString);
					break;
				default: 									//Alphanumeric
					$tempString = str_replace("--", " ", $tempString);
					$tempString = str_replace("=="," ", $tempString);
					$tempString = str_replace(";", " ", $tempString);
					$tempString = str_replace("'","''", $tempString);
					$tempString = str_replace('""',"", $tempString);
					$tempString = str_replace('/\/','', $tempString);
					
					$tempString = str_replace('INSERT INTO','', $tempString);
					$tempString = str_replace('DELETE FROM','', $tempString);
					$tempString = str_replace('UPDATE ','', $tempString);
					$tempString = str_replace('SELECT * ','', $tempString);
					$tempString = str_replace('ALTER ','', $tempString);
					$tempString = str_replace('UNION SELECT','', $tempString);
					$tempString = str_replace('UNION','', $tempString);
					$tempString = str_replace('CHAR(45,120,49,45,81,45)','', $tempString);
					$tempString = str_replace(' CHAR','', $tempString);
					
					$tempString = str_replace('insert into','', $tempString);
					$tempString = str_replace('delete from','', $tempString);
					$tempString = str_replace('update ','', $tempString);
					$tempString = str_replace('select * ','', $tempString);
					$tempString = str_replace('alter ','', $tempString);
					$tempString = str_replace('union select','', $tempString);
					$tempString = str_replace('union','', $tempString);
					$tempString = str_replace('char(45,120,49,45,81,45)','', $tempString);
					$tempString = str_replace(' char','', $tempString);
					
					$validSQL = $tempString;
			}
			return stripslashes($validSQL);
		}
	}
	
	function pagination($query, $per_page, $page, $url, $action=null)
	{   
		//echo '$action = ' . $action .'<br>';
		$query = " SELECT COUNT(0) AS num FROM $query ";
		//echo $query .'<hr>';
		$row = $this->selectData($query);
		//mysql_fetch_assoc(mysql_query($query));

		/*echo '<br>total'.*/
		if(empty($action)){
			$total = $row?$row[0]["num"]:0;
		} else {
			$total = 0;
			if(!empty($row)){
				foreach($row as $val){
					$total++;
				}
				$total = $total;
			}
		}
		//echo '$total = ' . $total .'<br>';
		
		$adjacents = "2"; 
		  
		$prevlabel = "&lsaquo; Prev";
		$nextlabel = "Next &rsaquo;";
		$lastlabel = "Last &rsaquo;&rsaquo;";
		  
		$page = ($page == 0 ? 1 : $page);  
		$start = ($page - 1) * $per_page;                               
		  
		$prev = $page - 1;                          
		$next = $page + 1;
		  
    	/*echo '<br>lastpage'.*/
		$lastpage = ceil($total/$per_page);
      
		$lpm1 = $lastpage - 1; // //last page minus 1
		  
		$pagination = "";
		$pagination .= "<div class='page_info'>Page {$page} of {$lastpage} Total record are {$total} </div>";
		if($lastpage > 1)
		{   
			$pagination .= "<ul class='pagination'>";
			if ($page > 1) 
			{
				$pagination.= "<li><a href='{$url}page={$prev}'>{$prevlabel}</a></li>";
			}
			if ($lastpage < 7 + ($adjacents * 2))
			{   
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
					{
						$pagination.= "<li><a class='current'>{$counter}</a></li>";
					}
					else
					{
						$pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";                    
					}
				}          
			} 
			elseif($lastpage > 5 + ($adjacents * 2))
			{
				if($page < 1 + ($adjacents * 2)) 
				{
					
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
					{
						if ($counter == $page)
						{
							$pagination.= "<li><a class='current'>{$counter}</a></li>";
						}
						else
						{
							$pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";                    
						}
					}
					$pagination.= "<li class='dot'>...</li>";
					$pagination.= "<li><a href='{$url}page={$lpm1}'>{$lpm1}</a></li>";
					$pagination.= "<li><a href='{$url}page={$lastpage}'>{$lastpage}</a></li>";  
						  
				} 
				elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) 
				{
					  
					$pagination.= "<li><a href='{$url}page=1'>1</a></li>";
					$pagination.= "<li><a href='{$url}page=2'>2</a></li>";
					$pagination.= "<li class='dot'>...</li>";
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) 
					{
						if ($counter == $page)
						{
							$pagination.= "<li><a class='current'>{$counter}</a></li>";
						}
						else
						{
							$pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";                    
						}
					}
					$pagination.= "<li class='dot'>...</li>";
					$pagination.= "<li><a href='{$url}page={$lpm1}'>{$lpm1}</a></li>";
					$pagination.= "<li><a href='{$url}page={$lastpage}'>{$lastpage}</a></li>";      
					  
				} 
				else 
				{
					  
					$pagination.= "<li><a href='{$url}page=1'>1</a></li>";
					$pagination.= "<li><a href='{$url}page=2'>2</a></li>";
					$pagination.= "<li class='dot'>..</li>";
					for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) 
					{
						if ($counter == $page)
						{
							$pagination.= "<li><a class='current'>{$counter}</a></li>";
						}
						else
						{
							$pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";                    
						}
					}
				}
			}
			  
				if ($page < $counter - 1) 
				{
					$pagination.= "<li><a href='{$url}page={$next}'>{$nextlabel}</a></li>";
					$pagination.= "<li><a href='{$url}page=$lastpage'>{$lastlabel}</a></li>";
				}
			  //$pagination.= "<li>Total Record Are {$total}</li>";
			$pagination.= "</ul>";        
		}
      
		return $pagination;
	}
	
	function getDetail($tableName, $fieldValue, $whereField, $conditionValue){
		$mySQL = "SELECT ".$this->validSQL($fieldValue,"")." AS fieldValue FROM ".$this->validSQL($tableName,"");
		if($conditionValue != ''){
			$mySQL .= " WHERE ".$this->validSQL($whereField,"")." ='".$this->validSQL($conditionValue,"")."'";
		}
		//echo $mySQL;
		//exit;
		$rsTemp1 = $this->selectSingleStmt($mySQL);
		if(!empty($rsTemp1)){
			return $rsTemp1['fieldValue'];
		}			
	}
	
	function countOrders($url){
		$qry = "";
		if (strtoupper($url) == 'NEW-JOBS-LIST'){
			$qry .= " AND bookedbyuser = '1'";
			$qry .= " AND acceptbydriver = '0'";
			$qry .= " AND bookingstatus = '0'";
			$qry .= " AND bookingcomplete = '0'";
		} else if (strtoupper($url) == 'DRIVER-ACCEPTED-LIST'){
			$qry .= " AND bookedbyuser = '1'";
			$qry .= " AND acceptbydriver = '1'";
			$qry .= " AND bookingstatus = '0'";
			$qry .= " AND bookingcomplete = '0'";
		} else if (strtoupper($url) == 'DRIVER-ON-THE-WAY-LIST'){
			$qry .= " AND bookedbyuser = '1'";
			$qry .= " AND acceptbydriver = '1'";
			$qry .= " AND bookingstatus = '1'";
			$qry .= " AND bookingcomplete = '0'";
		} else if (strtoupper($url) == 'JOB-STARTED-LIST'){
			$qry .= " AND bookedbyuser = '1'";
			$qry .= " AND acceptbydriver = '1'";
			$qry .= " AND bookingstatus = '2'";
			$qry .= " AND bookingcomplete = '0'";
		} else if (strtoupper($url) == 'COMPLETED-JOB-LIST'){
			$qry .= " AND bookedbyuser = '1'";
			$qry .= " AND acceptbydriver = '1'";
			$qry .= " AND bookingstatus = '3'";
			$qry .= " AND bookingcomplete = '0'";
		} else if (strtoupper($url) == 'PAYMENT-COMPLETED-JOB-LIST'){
			$qry .= " AND bookedbyuser = '1'";
			$qry .= " AND acceptbydriver = '1'";
			$qry .= " AND bookingstatus = '3'";
			$qry .= " AND bookingcomplete = '1'";
		}
		
		$mySQL = "";
		$mySQL = "SELECT COUNT(0) AS totRec";
		$mySQL .= " FROM";
		$mySQL .= " `booking` WHERE idbooking <> ''";
		$mySQL .= $qry;
		// echo $mySQL .'<hr>';
		$rsTemp = $this->selectSingleStmt($mySQL);
		
		echo $rsTemp['totRec'];
	}
	
	function dateManage($dateInerval){
		$dateVal = date_create(date('Y-m-d H:i:s'));
		date_add($dateVal, date_interval_create_from_date_string($dateInerval));
		$dateVal = date_format($dateVal , "Y-m-d");
		return $dateVal;
	}
}
?>