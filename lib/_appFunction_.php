<?php
//class appFunction extends Connection {
class appFunction {
	function validate($thisLevel, $baseURLAdmin){
		// echo $baseURLAdmin;
		// exit;
		$validate = '';
		if (empty($_SESSION['xlaANM_usr'])){
			header("location:".$baseURLAdmin."/logout");
		} else {	//if (!empty($_SESSION['xlaANM_lvl'])){
			$validate = $_SESSION['xlaANM_lvl'];
			if ($validate < $thisLevel){
				header("location:".$baseURLAdmin."/logout");
			}
		}
		return $validate;
	}
	
	function whichlevel($thislevel){
		switch($thislevel){
			case '1':
				$whichlevel = "Editor";
				break;
			case '2':
				$whichlevel = "Administrator";
				break;
			default:
				$whichlevel = "Publisher";
				break;
		}
		return ($whichlevel);
	}
	
	function chkBasePath(){
		$chkURL = explode("/", strtolower($_SERVER["REQUEST_URI"]));
		//echo $chkURL[2] . '<br>';
		//if($chkURL[1] == 'event-detail' OR $chkURL[2] == 'event-detail'){
		if(strtolower($chkURL[1]) == 'event-detail'
			OR strtolower($chkURL[1]) == 'about-us'
			OR strtolower($chkURL[1]) == 'business-post-events'
			OR strtolower($chkURL[1]) == 'contact-us'
		){
			$urlExt = "../";
		} else {
			$urlExt = '';
		}
		return $urlExt;
	}
	
	function whichstatus($status){
		switch ($status){
			case '0':
				$whichstatus = "Preview";
				break;
			case '1':
				$whichstatus = "Publish";
				break;
			case '2':
				$whichstatus = "Pending";
				break;
			case '3';
				$whichstatus = "Expired";
				break;
			//case 4
			//	$whichstatus = "Archived";
			//	break;
		}	
		return($whichstatus);
	}
	
	function friendlyURL($url){
        $url = str_replace("+", "-", $url);
        $url = str_replace("&", "-and-", $url);
        $url = str_replace(",", "-", $url);
        $url = str_replace(";", "-", $url);
        $url = str_replace(":", "-", $url);
        $url = str_replace("[", "-", $url);
        $url = str_replace('"', "-", $url);
        $url = str_replace("'", "-", $url);
        $url = str_replace(".", "-", $url);
        $url = str_replace(">", "-", $url);
        $url = str_replace("<", "-", $url);
        $url = str_replace("]", "-", $url);
        $url = str_replace("/", "-", $url);
        $url = str_replace("\\", "-", $url);
        $url = str_replace("{", "-", $url);
        $url = str_replace("~", "-", $url);
        $url = str_replace("!", "-", $url);
        $url = str_replace("@", "-", $url);
        $url = str_replace("#", "-", $url);
        $url = str_replace("%", "-", $url);
        $url = str_replace("$", "-", $url);
        $url = str_replace("^", "-", $url);
        $url = str_replace("*", "-", $url);
        $url = str_replace("(", "-", $url);
        $url = str_replace(")", "-", $url);
        $url = str_replace(" ", "-", $url);
        $url = str_replace("_", "-", $url);
        $url = str_replace("__", "-", $url);
        $url = str_replace("___", "-", $url);
        $url = str_replace("---", "-", $url);
        $url = str_replace("--", "-", $url);
        $url = str_replace("-", "-", $url);
        return $url;
    }
	
	//******************************************************************
	//Format values entered into HTML form fields to prevent cross-site 
	//scripting and other malicious HTML.
	//******************************************************************
	function validHTML($aString){
		//Declare Variables
		$tempString = '';
		$tempString = $aString;
	
		//Check for empty values
		if ($tempString  == null OR $tempString == ''){
			$validHTML = "";
			return $validHTML;
		}
		
		//Clean up HTML
		$tempString = str_replace('<', ' ', $tempString);
		$tempString = str_replace('>', ' ', $tempString);
		$tempString = str_replace('""', "'", $tempString);
		$tempString = str_replace('%', ' ', $tempString);
		$validHTML  = $tempString;
		return $validHTML;
	}
	
	//******************************************************************
	//Format values inserted into SQL statements before executing the 
	//SQL statement. This is to prevent SQL injection attacks, and to 
	//ensure that certain characters are interpreted correctly.
	//******************************************************************
	function validSQL($aString, $aType){
		//Declare Variables
		$tempString = '';
		$tempString = $aString;
	
		//Check for empty values 
		if ($tempString  == null OR empty($tempString)){
			$validSQL = "";
			return $validSQL;
		}
		
		//Clean up SQL
		if (strtolower($tempString) == null){				//Nulls
			$validSQL = $tempString;
		} else {
			switch (strtoupper($aType)){
				case "I":									//Integer
					$validSQL = intval($tempString);
					break;
				case "D":									//Double
					$validSQL = string($tempString);
					break;
				default: 									//Alphanumeric
					$tempString = str_replace("--", " ", $tempString);
					$tempString = str_replace("=="," ", $tempString);
					$tempString = str_replace(";", " ", $tempString);
					$tempString = str_replace("'","''", $tempString);
					$tempString = str_replace('""',"", $tempString);
					$tempString = str_replace('/\/','', $tempString);
					
					$tempString = str_replace('INSERT INTO','', $tempString);
					$tempString = str_replace('DELETE FROM','', $tempString);
					$tempString = str_replace('UPDATE ','', $tempString);
					$tempString = str_replace('SELECT * ','', $tempString);
					$tempString = str_replace('ALTER ','', $tempString);
					$tempString = str_replace('UNION SELECT','', $tempString);
					$tempString = str_replace('UNION','', $tempString);
					$tempString = str_replace('CHAR(45,120,49,45,81,45)','', $tempString);
					$tempString = str_replace(' CHAR','', $tempString);
					
					$tempString = str_replace('insert into','', $tempString);
					$tempString = str_replace('delete from','', $tempString);
					$tempString = str_replace('update ','', $tempString);
					$tempString = str_replace('select * ','', $tempString);
					$tempString = str_replace('alter ','', $tempString);
					$tempString = str_replace('union select','', $tempString);
					$tempString = str_replace('union','', $tempString);
					$tempString = str_replace('char(45,120,49,45,81,45)','', $tempString);
					$tempString = str_replace(' char','', $tempString);
					
					$validSQL = $tempString;
			}
			return stripslashes($validSQL);
		}
	}
	
	//******************************************************************
	//Check a string for invalid characters
	//******************************************************************
	function invalidChar($aString, $alphaNum, $addChars){
		$i; 
		$checkChar;
	
		$invalidChar = true; 	//Assume invalid chars unless proven otherwise
	
		switch ($alphaNum) {
			case "1":			// Alphanumeric [a-z, 0-9] is valid
				$addChars = strtolower("abcdefghijklmnopqrstuvwxyz0123456789" . $addChars);
			case "2":			// Numeric [0-9] is valid
				$addChars = strtolower("0123456789" . $addChars);
			case "3":			// Alpha [a-z] is valid
				$addChars = strtolower("abcdefghijklmnopqrstuvwxyz" . $addChars);
			default:			// Only characters in addChar is valid
		}
		echo strlen($aString) .'<br>';
		for ($i=1; $i=strlen($aString); $i++){
			//echo '$aString = ' . $aString . '<br>';
			//echo 'SUB aString = ' . substr($aString, $i, 1) . '<br>';
			$checkChar = strtolower(substr($aString, $i, 1));
			//echo 'POS ' . strpos($checkChar, $addChars) .'<br>';
			if (stripos($checkChar, $addChars)){
				$invalidChar = true;
				break;
			}
		}
		echo 'invalidChar = ' . $invalidChar .'<br>';
		$invalidChar = false;
		return $invalidChar;
	}
	
	function validMobile($mobile){
		if(!preg_match("/^[7-9]{1}[0-9]{9}$/i", $this->validData($mobile))){
			$returnVal = '1';
		} else {
			$returnVal = '0';
		}
		return $returnVal;
	}
	
	function validName($name){
		if(!preg_match("/^[a-zA-Z ]*$/",$this->validData($name))){
			$returnVal = '1';
		} else {
			$returnVal = '0';
		}
		return $returnVal;
	}
	
	function validEmail($email){
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
			$returnVal = '1';
		} else {
			$returnVal = '0';
		}
		return $returnVal;
	}
	
	function validData($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
	
	public function getIPAddress()
    {
        if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif(isset($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
    }
		
	function chkBrowser($baseURL){
		$headers = apache_request_headers();
		foreach ($headers as $header => $value) {
			echo 'POSITION MOZILLA = '. strpos($value,'Mozilla/') .'<br>';
			echo 'POSITION FIREFOX = '. strpos($value,'Firefox/') .'<br>';
			if($header == 'User-Agent'){
				if( (strpos($value,'Mozilla/') == 0) OR (strpos($value,'Firefox/') == 0) ){
					//echo "$header: $value <br />\n";
					//header("location:$baseURL");
				}
			}
		}
	}
	
	function chkContentType($action){
		//echo 'ACTION = ' . $action .'<br>';
		if(strtoupper($action) == 'DOCUMENT'){
			//$types = array(
			//	'pdf' => 'application/pdf'
			//	, 'doc' => 'application/msword'
			//	, 'docx' => 'vnd.openxmlformats-officedocument.wordprocessingml.document'
			//);
			$types = array('pdf', 'msword');
		} else if(strtoupper($action) == 'IMAGE'){
			//$types = array(
			//	'jpeg' => 'image/jpeg'
			//	, 'jpg' => 'image/jpeg'
			//);
			$types = array('jpg', 'jpeg');
		}
		return $types;
	}
	

	function chkPathURL($userType){
		$pathURL = '';
		if($userType == 'MLM'){
			$pathURL = "/mlm/";
		} else if($userType == 'GUEST'){
			$pathURL = "/user/";
		}
		return $pathURL;
	}
	
	function numberTowords($number = ''){
	   $no = round($number);
	   $point = round($number - $no, 2) * 100;
	   $hundred = null;
	   $digits_1 = strlen($no);
	   $i = 0;
	   $str = array();
	   $words = array('0' => '', '1' => 'one', '2' => 'two',
			'3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
			'7' => 'seven', '8' => 'eight', '9' => 'nine',
			'10' => 'ten', '11' => 'eleven', '12' => 'twelve',
			'13' => 'thirteen', '14' => 'fourteen',
			'15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
			'18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty',
			'30' => 'thirty', '40' => 'forty', '50' => 'fifty',
			'60' => 'sixty', '70' => 'seventy',
			'80' => 'eighty', '90' => 'ninety');
	   $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
	   while ($i < $digits_1) {
			$divider = ($i == 2) ? 10 : 100;
			$number = floor($no % $divider);
			$no = floor($no / $divider);
			$i += ($divider == 10) ? 1 : 2;
			if ($number) {
				$plural = (($counter = count($str)) && $number > 9) ? 's' : null;
				$hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
				$str [] = ($number < 21) ? $words[$number] .
					" " . $digits[$counter] . $plural . " " . $hundred
					:
					$words[floor($number / 10) * 10]
					. " " . $words[$number % 10] . " "
					. $digits[$counter] . $plural . " " . $hundred;
			} else $str[] = null;
		}
		$str = array_reverse($str);
		$result = implode('', $str);
		$points = ($point) ?
			" and " . $words[$point / 10] . " " . 
				$words[$point = $point % 10] : '';
		//echo "Rupees  " . ucwords($result) . ucwords($points) . " Paise";
		//return "Rupees  " . ucwords($result) . ucwords($points) . " Paise";
		
		$amuntValueInWord = "Rupees  " . ucwords($result);
		//echo 'POINTS = ' . $points . '<br>';
		if(!empty($points) AND $points <> 'and'){
			$amuntValueInWord .= ucwords($points) . " Paise";
		}
		return $amuntValueInWord;
	}
	
	public function currentUrl() {
		$url = parse_url(strtolower(basename($_SERVER["REQUEST_URI"])), PHP_URL_PATH);
		return $url;
	}
	
	public function getOrderProgress($orderstatus){
		$strData = '';
		if (strtoupper($orderstatus) == 'ACCEPTED'){
			$strData = '<div class="progress">';
				$strData .= '<div class="progress-bar progress-bar-info" role="progressbar" style="width:20%" title="Accepted">';
					$strData .= 'Accepted';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-default" role="progressbar" style="width:20%" title="Processed">';
					$strData .= 'Processed';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-default" role="progressbar" style="width:20%" title="Ready">';
					$strData .= 'Ready';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-default" role="progressbar" style="width:20%" title="Dispatched">';
					$strData .= 'Dispatched';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-default" role="progressbar" style="width:20%" title="Delivered">';
					$strData .= 'Delivered';
				$strData .= '</div>';
			$strData .= '</div>';
		}
		if (strtoupper($orderstatus) == 'PROCESSED'){
			$strData = '<div class="progress">';
				$strData .= '<div class="progress-bar progress-bar-info" role="progressbar" style="width:20%" title="Accepted">';
					$strData .= 'Accepted';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-info" role="progressbar" style="width:20%" title="Processed">';
					$strData .= 'Processed';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-default" role="progressbar" style="width:20%" title="Ready">';
					$strData .= 'Ready';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-default" role="progressbar" style="width:20%" title="Dispatched">';
					$strData .= 'Dispatched';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-default" role="progressbar" style="width:20%" title="Delivered">';
					$strData .= 'Delivered';
				$strData .= '</div>';
			$strData .= '</div>';
		}
		if (strtoupper($orderstatus) == 'READY'){
			$strData = '<div class="progress">';
				$strData .= '<div class="progress-bar progress-bar-info" role="progressbar" style="width:20%" title="Accepted">';
					$strData .= 'Accepted';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-info" role="progressbar" style="width:20%" title="Processed">';
					$strData .= 'Processed';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-info" role="progressbar" style="width:20%" title="Ready">';
					$strData .= 'Ready';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-default" role="progressbar" style="width:20%" title="Dispatched">';
					$strData .= 'Dispatched';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-default" role="progressbar" style="width:20%" title="Delivered">';
					$strData .= 'Delivered';
				$strData .= '</div>';
			$strData .= '</div>';
		}
		if (strtoupper($orderstatus) == 'DISPATCHED'){
			$strData = '<div class="progress">';
				$strData .= '<div class="progress-bar progress-bar-info" role="progressbar" style="width:20%" title="Accepted">';
					$strData .= 'Accepted';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-info" role="progressbar" style="width:20%" title="Processed">';
					$strData .= 'Processed';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-info" role="progressbar" style="width:20%" title="Ready">';
					$strData .= 'Ready';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-info" role="progressbar" style="width:20%" title="Dispatched">';
					$strData .= 'Dispatched';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-default" role="progressbar" style="width:20%" title="Delivered">';
					$strData .= 'Delivered';
				$strData .= '</div>';
			$strData .= '</div>';
		}
		if (strtoupper($orderstatus) == 'DELIVERED'){
			$strData = '<div class="progress">';
				$strData .= '<div class="progress-bar progress-bar-info" role="progressbar" style="width:20%" title="Accepted">';
					$strData .= 'Accepted';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-info" role="progressbar" style="width:20%" title="Processed">';
					$strData .= 'Processed';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-info" role="progressbar" style="width:20%" title="Ready">';
					$strData .= 'Ready';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-info" role="progressbar" style="width:20%" title="Dispatched">';
					$strData .= 'Dispatched';
				$strData .= '</div>';
				$strData .= '<div class="progress-bar progress-bar-info" role="progressbar" style="width:20%" title="Delivered">';
					$strData .= 'Delivered';
				$strData .= '</div>';
			$strData .= '</div>';
		}
		return $strData;
	}
}

function recursiveDelete($str) {
    if (is_file($str)) {
        return @unlink($str);
    }
    elseif (is_dir($str)) {
        $scan = glob(rtrim($str,'/').'/*');
        foreach($scan as $index=>$path) {
            recursiveDelete($path);
        }
        return @rmdir($str);
    }
}

// BELOW PART NOT IN CLASS //
function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 10; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function randomString($length = 10) {
	$randstr = '';
	srand((double) microtime(TRUE) * 1000000);
	//our array add all letters and numbers if you wish
	$chars = array(
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'p',
		'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5',
		'6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 
		'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

	for ($rand = 0; $rand <= $length; $rand++) {
		$random = rand(0, count($chars) - 1);
		$randstr .= $chars[$random];
	}
	return $randstr;
}
function curPageURL() {
	$pageURL = 'http';
	if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

/* $baseURL = "http://".$_SERVER['HTTP_HOST'];
if($baseURL == "http://localhost"){
	$baseURL = 'http://localhost/methuenonline.com';
} else if($baseURL == "http://192.168.1.12"){
	$baseURL = 'http://192.168.1.12/methuenonline.com';
} else {
	$baseURL = "https://".$_SERVER['HTTP_HOST'];
} */

date_default_timezone_set("America/New_York");
$nowtime = date("Y-m-d H:i:s");
$date = date("Y-m-d");
$timestamp = date("YmdHis");
$datestamp = date("dmy");

// FCM API KEY
$fcmApiKey = "AAAAHjs3cGE:APA91bGx3gtE9yBUSO266Q-ohjHmX0CwvKujZqemLuwX5r-_-1_VO2HMSZsUMoAPh9HW4ga3Y4FtXLAnjj6R4NfhvbMIAx4-2lT2-kICzbKK1G3eX36RK8lW9IrVor-8Bapqu5MA9KBg";
						
?>