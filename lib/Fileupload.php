<?php
class Fileupload 
{
    
	
    public function execute($fileName, $fileDir, $round) {
		 $handler;
		 $validation;
		 $n_width = 227;
		 $n_height = 261;
		 $new_file_name='';
        try {
            $flag = false;			
            $new_file_name = $_FILES[$fileName]['name'][$round];
            $validation = getimagesize($_FILES[$fileName]['tmp_name'][$round]);
			$new_file_name=date("Y_m_d_H_i_s").'_'.rand(9999,999999).preg_replace("/[^a-zA-Z.]/", "", $new_file_name);;
            //print_r($validation);

            if ($validation['mime'] == 'image/jpeg' || $validation['mime'] == 'image/png' || $validation['mime'] == 'image/jpg' || $validation['mime'] == 'image/gif') {
                //image type validate allowed type are jpg,jpeg,gif,png only
                $flag = true;
            } else {
                $flag = false;
            }

//            if ($validation[0] <= 760 && $validation[0] >= 250) {
//                //image width validate
//                $flag = true;
//            } else {
//                $flag = false;
//            }
//            if ($validation[1] <= 871 && $validation[1] >= 200) {
//                //image height validate
//                $flag = true;
//            } else {
//                $flag = false;
//            }
			
            if ($flag == true) {
                move_uploaded_file($_FILES[$fileName]['tmp_name'][$round], $fileDir . $new_file_name);

                if ($validation['mime'] == "image/jpeg") {
                    $im = imagecreatefromjpeg($fileDir . $new_file_name);
                    $width = imagesx($im);
                    $height = imagesy($im);
                    $n_height = ($n_width / $width) * $height;
                    $newimage = imagecreatetruecolor($n_width, $n_height);
                    imagecopyresized($newimage, $im, 0, 0, 0, 0, $n_width, $n_height, $width, $height);
                    imagejpeg($newimage, $fileDir . "thumb/" . $new_file_name);
                    chmod($fileDir . "thumb/" . $new_file_name, 0777);
                }
                if ($validation['mime'] == "image/png") {
                    $im = imagecreatefrompng($fileDir . $new_file_name);
                    $width = imagesx($im);
                    $height = imagesy($im);
                    $n_height = ($n_width / $width) * $height;
                    $newimage = imagecreatetruecolor($n_width, $n_height);
                    imagecopyresized($newimage, $im, 0, 0, 0, 0, $n_width, $n_height, $width, $height);
                    imagepng($newimage, $fileDir . "thumb/" . $new_file_name);
                    chmod($fileDir . "thumb/" . $new_file_name, 0777);
                }

                return $new_file_name;
            } else {
                return false;
            }
            throw new Exception();
        } catch (Exception $ex) {
            echo $ex;
        }
    }

}