<?php

class ReturnMsg {

    public function successDb() {
        return "Success: Data successfully inserted in database";
    }

    public function errorDb() {
        return "Error: An error occurred during process please try again";
    }

    public function dublicateSkip() {
        return "Error: The phone / email address you have entered is already registered ...";
    }

    public function logInsuccess() {
        return "Success: You have successfully registered with us please login to complete your further registration";
    }

    public function waitLogin() {
        return "Success: You have successfully submitted your documents please wait while we are authenticating your documents it can take several days to complete, you will recieve a confirmation mail when we confirmed your documents";
    }

    public function successRegister() {
        return "Success: You have successfully registered with us now you can access to your vendor panel";
    }
	public function bookingStatus($id, $action, $dateTime){		$msg = '';		if($id == '1' AND strtolower($action) == 'bookedbyuser'){			$msg = '<strong>Job Submitted ' . $dateTime . '</strong><br>Your job has been successfully submitted';		} else if($id == '1' AND strtolower($action) == 'acceptbydriver'){			$msg = '<strong>Job Assigned ' . $dateTime . '</strong><br>Your job has been successfully assigned';		} else if($id == '1' AND strtolower($action) == 'rejectbydriver'){			$msg = '<strong>Job Rejected ' . $dateTime . '</strong><br>Your job rejected by driver';		} else if($id == '1' AND strtolower($action) == 'cancelbyuser'){			$msg = '<strong>Job Cancelled ' . $dateTime . '</strong><br>Your job cancelled by you';		} else if($id == '0' AND strtolower($action) == 'bookingstatus'){			$msg = '<strong>Job Not Started ' . $dateTime . '</strong>';		} else if($id == '1' AND strtolower($action) == 'bookingstatus'){			$msg = '<strong>On The Way ' . $dateTime . '</strong><br>Driver on the way';		} else if($id == '2' AND strtolower($action) == 'bookingstatus'){			$msg = '<strong>Job Started ' . $dateTime . '</strong><br>Driver worked on site';		} else if($id == '3' AND strtolower($action) == 'bookingstatus'){			$msg = '<strong>Job Completed ' . $dateTime . '</strong><br>Job completed by driver';		} else if($id == '0' AND strtolower($action) == 'bookingcomplete'){			$msg = '<strong>Payment Due ' . $dateTime . '</strong><br>Your payment has been pending. Please make payment and enjoy other services';		} else if($id == '1' AND strtolower($action) == 'bookingcomplete'){			$msg = '<strong>Job Completed ' . $dateTime . '</strong><br>Job completed by customer';		}		return $msg;	}
}
