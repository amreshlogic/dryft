<?php
include 'include/_header_.php';

$iduser = '';
$usertype = '';
$name = '';
$fullname = '';
$email = '';
$password = '';
$securitycode = '';
$mobile = '';
$metadescription = '';
$isactive = '';
$errorMsg = '';

$errorMsg = '';

$lvl = $appFunction->validate('0', $baseURL);

if (!empty($_POST['submit'])){
	echo '<div style="padding:50px 260px;">';
	
	$iduser = trim($appFunction->validHTML($_POST['iduser']));
	$usertype = trim($appFunction->validHTML($_POST['usertype']));
	$name = trim($appFunction->validHTML($_POST['name']));
	$email = trim($appFunction->validHTML($_POST['email']));
	$password = trim($appFunction->validHTML($_POST['password']));
	$mobile = trim($appFunction->validHTML($_POST['mobile']));
	$isactive = $appFunction->validHTML($_POST['isactive']);
		
	if (empty($name)){
		$errorMsg = "Please enter user name";
	} 
	
	$mySQL = "";
	$mySQL = "SELECT iduser FROM user WHERE mobile = '".$appFunction->validSQL($mobile,"")."'";
	if(!empty($iduser)){
		$mySQL .= " AND iduser <> '".$appFunction->validSQL($iduser,"")."'";
	}
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	if (!empty($rsTemp)){
		$errorMsg = "Mobile no. already register as a username";
	}
	
	if($errorMsg == ''){
		if(empty($iduser)){
			$mySQL = "";
			$mySQL = "INSERT INTO user (
			usertype
			, fullname
			, email
			, password
			, securitycode
			, mobile
			, isactive
			) VALUES (
			'".$appFunction->validSQL($usertype,"")."'
			, '".$appFunction->validSQL($name,"")."'
			, '".$appFunction->validSQL($email,"")."'
			, '".SHA1($password)."'
			, '".$appFunction->validSQL($password,"")."'
			, '".$appFunction->validSQL($mobile,"")."'
			, '".$appFunction->validSQL($isactive,"")."')";
			//echo $mySQL;
			//exit;
			$dbAccess->queryExec($mySQL);
		} else {
			$mySQL = "";
			$mySQL = "UPDATE user SET
			fullname = '".$appFunction->validSQL($name,"")."'
			, usertype = '".$appFunction->validSQL($usertype,"")."'
			, email = '".$appFunction->validSQL($email,"")."'
			, password = '".SHA1($password)."'
			, securitycode = '".$appFunction->validSQL($password,"")."'
			, mobile = '".$appFunction->validSQL($mobile,"")."'
			, isactive = '".$appFunction->validSQL($isactive,"")."'
			WHERE iduser = '".$appFunction->validSQL($iduser,"")."'";
			// echo $mySQL;
			//exit;
			$dbAccess->queryExec($mySQL);	
		}
		header("location:".$baseURL."/admin/user-list");
		exit;
	}
	echo '</div>';
}

if(!empty($_GET['iduser'])){
	$iduser = trim($appFunction->validHTML($_GET['iduser']));
	$mySQL = "";
	$mySQL = "SELECT iduser
	, fullname
	, usertype
	, email
	, password
	, securitycode
	, mobile
	, isactive
	FROM user";
	$mySQL .= " WHERE iduser = '".$appFunction->validSQL($iduser,"")."'";
	// echo $mySQL;	
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	
	$iduser = $rsTemp['iduser'];
	$fullname = $rsTemp['fullname'];
	$usertype = $rsTemp['usertype'];
	$email = $rsTemp['email'];
	$password = $rsTemp['securitycode'];
	$mobile = $rsTemp['mobile'];
	$isactive = $rsTemp['isactive'];
}
?>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">User <small><?=$errorMsg;?></small></h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<form role="form" action="<?=$baseURL;?>/admin/user-manage/" method="post">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<!--<div class="panel-heading">
						Organization Information
					</div>-->
					<div class="panel-body">
						<div class="row">
							<div class="col-md-3">
								<label>Name</label>
								<input type="text" name="name" id="username" value="<?=$fullname;?>" maxlength="100" class="form-control" placeholder="Name" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>Email</label>
								<input type="email" name="email" id="email" value="<?=$email;?>" maxlength="100" class="form-control" placeholder="Email" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>Mobile / Login Id</label>
								<input type="text" name="mobile" id="mobile" value="<?=$mobile;?>" maxlength="10" class="form-control" placeholder="Mobile" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>Password</label>
								<input type="password" name="password" id="password" value="<?=$password;?>" maxlength="50" class="form-control" placeholder="Password" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>User Type</label>
								<select name="usertype" class="form-control" required>
									<option value="Driver" <?php if ($usertype == 'Driver'){ echo 'selected';}?>>Driver</option>
									<option value="Customer" <?php if ($usertype == 'Customer'){ echo 'selected';}?>>Customer</option>
								</select>
							</div>
						</div>			
						<div class="row">
							<div class="col-md-3">
								<label>Status</label>
								<select name="isactive" class="form-control">
									<option value="1" <?php if ($isactive == '1'){ echo 'selected';}?>>Active</option>
									<option value="0" <?php if ($isactive == '0'){ echo 'selected';}?>>Inactive</option>
								</select>
							</div>
						</div>			
					</div>	
					<!-- /.panel-body -->	
					
					<div class="panel-body">
						<div class="row">
							<div class="col-md-2">
								<input type="hidden" name="iduser" id="iduser" value="<?=$iduser;?>" />
								<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
								</div>
							</div>
						</div>
					</div>
					<!-- /.row (nested) -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</form>
</div>
<script src="<?=$baseURL;?>/admin/js/jscolor.js"></script>
<?php
include 'include/_footer_.php';
?>
