<?php
session_start();

include '../lib/_baseURL_.php';

// remove all session variables
session_unset(); 

// destroy the session 
session_destroy(); 

header("location:".$baseURLAdmin."/login");
?>