<?php
ob_start();
session_start();
error_reporting(0);

include '../lib/_baseURL_.php';
include '../lib/_appFunction_.php';
include '../lib/Connection.php';
include '../lib/Fileupload.php';
include '../lib/DBQuery.php';
include '../lib/Helpers.php';
include '../lib/ReturnMsg.php';
include '../lib/SentEmailSMS.php';

if (!empty($_SESSION['username'])) {
	header("location:".$baseURL."/admin/dashboard");
}

$connection = new Connection();
$helper = new Helpers();
$FileUpload = new Fileupload();
$dbAccess = new DBQuery();
$LoadMsg = new ReturnMsg();
$sendMailMsg = new SentEmailSMS();
$appFunction = new appFunction();

$errMsg = '';
$username = '';
$password = '';

if(!empty($_POST['submit'])){
	$username = $appFunction->validHTML(trim($_POST['username']));
	$password = $appFunction->validHTML(trim($_POST['password']));
	if(empty($username)){
		$errMsg = "Please enter username";
	} else if(empty($password)){
		$errMsg = "Please enter password";
	}
	
	if(empty($errMsg)){
		// Check if there's a Default Administrator, otherwise create one
	    $mySQL = "";
		$mySQL = "SELECT iduser, plevel, usertype, name, mobile FROM adminuser WHERE plevel = '2'";
		$rsTemp = $dbAccess->selectSingleStmt($mySQL);
		
		if(empty($rsTemp)){
			$mySQL = "";
			$mySQL = "INSERT INTO adminuser (plevel, usertype, name, email, mobile, password, securitycode, isactive) 
				VALUES ('2', 'Admin', 'Admin', 'info@domain.com', '8800522966', '".SHA1(admin)."', 'admin', 'Active')";
				$dbAccess->queryExec($mySQL);
		}
		
		$mySQL = "";
		$mySQL = "SELECT iduser, plevel, usertype, name, email, mobile FROM adminuser WHERE isactive = 'Active' AND mobile = '".$appFunction->validSQL($username,"")."' AND password = '".$appFunction->validSQL(SHA1($password),"")."'";
		$rsTemp = $dbAccess->selectSingleStmt($mySQL);
		
		if(!empty($rsTemp)){
			$_SESSION['xlaANM_usr'] = $rsTemp['iduser'];
			$_SESSION['xlaANM_lvl'] = $rsTemp['plevel'];
			$_SESSION['xlaANM_max'] = $rsTemp['maxarticles'];
			$_SESSION['CompanyName'] = $rsTemp['name'];
			$_SESSION['username'] = $rsTemp['mobile'];
			header("location:".$baseURL."/admin/dashboard");
		} else {
			$errMsg = "Please enter valid username & password";
			//unset($_SESSION['xlaANM_usr']);
			//unset($_SESSION['xlaANM_lvl']);
			//unset($_SESSION['xlaANM_max']);
			//unset($_SESSION['CompanyName']);
			//unset($_SESSION['username']);
		}
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Login</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?=$baseURLAdmin;?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?=$baseURLAdmin;?>/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=$baseURLAdmin;?>/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?=$baseURLAdmin;?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
	.login-back {background: url(<?=$baseURL;?>/assets/images/parallax/history_image.jpg) top center no-repeat fixed; height:auto; width: 100%; background-size: cover; }
	</style>
</head>

<body class="login-back">
	<div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
				<div class="login-panel text-center">
					<img src="<?=$baseURL;?>/assets/images/icons/dryft_logo_white.png" alt="DRYFT NOW">
				</div>
                
				<div class=" panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In <span class="text-danger"><?=$errMsg;?></span></h3>
                    </div>
                    <div class="panel-body">
                        <form name="form" method="post" role="form">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="username" type="text" autofocus />
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="" />
                                </div>
                                <!--<div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>-->
                                <!-- Change this to a button or input when using this as a form -->
                                <!-- <a href="index.html" class="btn btn-lg btn-success btn-block">Login</a> -->
								<input name="submit" type="submit" value="Login" class="btn btn-lg btn-primary btn-block" />								
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="<?=$bsaeURl;?>/admin/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?=$bsaeURl;?>/admin/bootstrap/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?=$bsaeURl;?>/admin/metisMenu/metisMenu.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?=$bsaeURl;?>/admin/js/sb-admin-2.js"></script>
</body>

</html>