<?php
include 'include/_header_.php';

$idcategory = '';
$categoryname = '';
$sort = '';
$friendlyurl = '';
$metatitle = '';
$metakeyword = '';
$metadescription = '';
$isactive = '';
$errorMsg = '';

$lvl = $appFunction->validate('0', $baseURL);

if (!empty($_POST['submit'])){
	$idcategory = trim($appFunction->validHTML($_POST['idcategory']));
	$categoryname = trim($appFunction->validHTML($_POST['categoryname']));
	$sort = trim($appFunction->validHTML($_POST['sort']));
	$friendlyurl = trim($appFunction->validHTML($_POST['friendlyurl']));
	$metatitle = trim($appFunction->validHTML($_POST['metatitle']));
	$metakeyword = trim($appFunction->validHTML($_POST['metakeyword']));
	$metadescription = trim($appFunction->validHTML($_POST['metadescription']));
	$isactive = $appFunction->validHTML($_POST['isactive']);
	
	if(empty($friendlyurl)){
		$friendlyurl = strtolower($categoryname);
		$friendlyurl = $appFunction->friendlyURL($friendlyurl);
	} else {
		$friendlyurl = strtolower($friendlyurl);
		$friendlyurl = $appFunction->friendlyURL($friendlyurl);
	}
	
	if (empty($categoryname)){
		$errorMsg = "Please enter category name";
	} else {
		if(empty($idcategory)){
			$mySQL = "";
			$mySQL = "INSERT INTO category (
			categoryname
			, sort
			, friendlyurl
			, metatitle
			, metakeyword
			, metadescription
			, isactive
			) VALUES (
			'".$appFunction->validSQL($categoryname,"")."'
			, '".$appFunction->validSQL($sort,"")."'
			, '".$appFunction->validSQL($friendlyurl,"")."'
			, '".$appFunction->validSQL($metatitle,"")."'
			, '".$appFunction->validSQL($metakeyword,"")."'
			, '".$appFunction->validSQL($metadescription,"")."'
			, '".$appFunction->validSQL($isactive,"")."')";
			//echo $mySQL;
			//exit;
			$dbAccess->queryExec($mySQL);
		} else {
			$mySQL = "";
			$mySQL = "UPDATE category SET
			categoryname = '".$appFunction->validSQL($categoryname,"")."'
			, sort = '".$appFunction->validSQL($sort,"")."'
			, friendlyurl = '".$appFunction->validSQL($friendlyurl,"")."'
			, metatitle = '".$appFunction->validSQL($metatitle,"")."'
			, metakeyword = '".$appFunction->validSQL($metakeyword,"")."'
			, metadescription = '".$appFunction->validSQL($metadescription,"")."'
			, isactive = '".$appFunction->validSQL($isactive,"")."'
			WHERE idcategory = '".$appFunction->validSQL($idcategory,"")."'";
			//echo $mySQL;
			//exit;
			$dbAccess->queryExec($mySQL);	
		}
		header("location:".$baseURL."/admin/category-list");
		exit;
	}
}

if(!empty($_GET['idcategory'])){
	$idcategory = trim($appFunction->validHTML($_GET['idcategory']));
	$mySQL = "";
	$mySQL = "SELECT idcategory
	, categoryname
	, sort
	, friendlyurl
	, metatitle
	, metakeyword
	, metadescription
	, isactive
	FROM category";
	$mySQL .= " WHERE idcategory = '".$appFunction->validSQL($idcategory,"")."'";
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	
	$idcategory = $rsTemp['idcategory'];
	$categoryname = $rsTemp['categoryname'];
	$sort = $rsTemp['sort'];
	$friendlyurl = $rsTemp['friendlyurl'];
	$metatitle = $rsTemp['metatitle'];
	$metakeyword = $rsTemp['metakeyword'];
	$metadescription = $rsTemp['metadescription'];
	$isactive = $rsTemp['isactive'];
}
?>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Category <small><?=$errorMsg;?></small></h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<form role="form" action="<?=$baseURL;?>/admin/category-manage/" method="post">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<!--<div class="panel-heading">
						Organization Information
					</div>-->
					<div class="panel-body">
						<div class="row">
							<div class="col-md-3">
								<label>Category Name</label>
								<input type="text" name="categoryname" id="categoryname" value="<?=$categoryname;?>" maxlength="100" class="form-control" placeholder="Category Name" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>Sort</label>
								<input type="text" name="sort" id="sort" value="<?=$sort;?>" maxlength="2" class="form-control" placeholder="Sort" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>Status</label>
								<select name="isactive" class="form-control">
									<option value="Active" <?php if ($isactive == 'Active'){ echo 'selected';}?>>Active</option>
									<option value="Inactive" <?php if ($isactive == 'Inactive'){ echo 'selected';}?>>Inactive</option>
								</select>
							</div>
						</div>			
					</div>	
					<!-- /.panel-body -->	
					
					<div class="panel-heading">
						SEO Detail
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-3">
								<label>English URL</label>
								<input type="text" placeholder="Enter English URL" id="friendlyurl" name="friendlyurl" value="<?=$friendlyurl;?>" class="form-control" maxlength="62" required  />
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<label>Meta Title</label>
								<input type="text" placeholder="Meta title" id="metatitle" name="metatitle" value="<?=$metatitle;?>" class="form-control" maxlength="62"  />
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<label>Meta Keyword</label>
								<input type="text" name="metakeyword" id="metakeyword" value="<?=$metakeyword;?>" maxlength="255" class="form-control" placeholder="Meta Keyword" autocomplete="off" />
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<label>Meta Description</label>
								<input type="text" name="metadescription" id="metadescription" value="<?=$metadescription;?>" maxlength="255" class="form-control" placeholder="Meta Description" autocomplete="off" />
							</div>
						</div>
					</div>	
					<!-- /.panel-body -->
					
					<div class="panel-body">
						<div class="row">
							<div class="col-md-2">
								<input type="hidden" name="idcategory" id="idcategory" value="<?=$idcategory;?>" />
								<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
								</div>
							</div>
						</div>
					</div>
					<!-- /.row (nested) -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</form>
</div>
<script src="<?=$baseURL;?>/admin/js/jscolor.js"></script>
<?php
include 'include/_footer_.php';
?>
