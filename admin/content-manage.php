<?php
include 'include/_header_.php';

error_reporting(-1);

$lvl = $appFunction->validate('0', $baseURL);

$errorMsg = '';
$idcontent = '';
$title = '';
$friendlyURL = '';
$detail = '';
$contentfor = '';
$sort = '';
$isactive = '';

if (!empty($_POST['submit'])){
	$idcontent = trim($appFunction->validHTML($_POST['idcontent']));
	$title = trim($appFunction->validHTML($_POST['title']));
	$friendlyURL = $appFunction->validHTML($_POST['friendlyURL']);
	$detail = $_POST['detail'];
	$contentfor = $appFunction->validHTML($_POST['contentfor']);
	$sort = $appFunction->validHTML($_POST['sort']);
	$isactive = $appFunction->validHTML($_POST['isactive']);
	
	if(empty($friendlyURL)){
		$friendlyURL = strtolower($title);
		$friendlyURL = $appFunction->friendlyURL($title);
	} else {
		$friendlyURL = strtolower($friendlyURL);
		$friendlyURL = $appFunction->friendlyURL($friendlyURL);
	}
	
	echo '<div style="padding-left:300px;">';
	//echo 'uploadImageSize = ' . $uploadImageSize .'<br>';
	//echo $uploadImageExt .'<br>';
	
	if(empty($title)){
		$errorMsg = 'Please enter title';
	} else if(empty($detail)){
		$errorMsg = 'Please content detail';
	}
	
	if(empty($errorMsg)){
		if(empty($idcontent)){
			$mySQL = "";
			$mySQL = "CALL sp_ContentAdd (
			  '".$appFunction->validSQL($title,"")."'
			, '".$appFunction->validSQL($friendlyURL,"")."'
			, '".$detail."'
			, '".$appFunction->validSQL($contentfor,"")."'
			, '".$appFunction->validSQL($sort,"")."'
			, '".$isactive."'
			, @result)";
			//echo $mySQL;
			//exit;
			$dbAccess->queryExec($mySQL);
			
			$mySQL = "";
			$mySQL = "SELECT @result AS result"; 
			//echo $mySQL;
			$rsTemp = $dbAccess->selectSingleStmt($mySQL);
			$result = $rsTemp['result'];
		} else {
			$mySQL = "";
			$mySQL = "CALL sp_ContentEdit (
			  '".$idcontent."'
			, '".$appFunction->validSQL($title,"")."'
			, '".$appFunction->validSQL($friendlyURL,"")."'
			, '".$detail."'
			, '".$appFunction->validSQL($contentfor,"")."'
			, '".$appFunction->validSQL($sort,"")."'
			, '".$isactive."'
			, @result)";
			//echo $mySQL;
			//exit;
			$dbAccess->queryExec($mySQL);
			
			$mySQL = "";
			$mySQL = "SELECT @result AS result"; 
			//echo $mySQL;
			$rsTemp = $dbAccess->selectSingleStmt($mySQL);
			$result = $rsTemp['result'];
		}
		if ($result == '0'){
			$result = 1;
			header("location:".$baseURLAdmin."/content-list?msg=".$result);
			exit;
		} else if ($result == '1'){
			$errorMsg = 'Content title already exist.';
		}
	}
	echo '</div>';
}

if(!empty($_GET['idcontent'])){
	$idcontent = trim($appFunction->validHTML($_GET['idcontent']));
	$mySQL = "";
	$mySQL = "SELECT idcontent";
	$mySQL .= ", title";
	$mySQL .= ", detail";
	$mySQL .= ", friendlyURL";
	$mySQL .= ", contentfor";
	$mySQL .= ", sort";
	$mySQL .= ", isactive";
	$mySQL .= " FROM vcontent";
	$mySQL .= " WHERE idcontent = '".$appFunction->validSQL($idcontent,"")."'";
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	
	$idcontent = $rsTemp['idcontent'];
	$title = $rsTemp['title'];
	$detail = $rsTemp['detail'];
	$friendlyURL = $rsTemp['friendlyURL'];
	$contentfor = $rsTemp['contentfor'];
	$sort = $rsTemp['sort'];
	$isactive = $rsTemp['isactive'];
}
?>

<?php include 'fckeditor/fckeditor.php';?>
		
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Manage Content <small><?=$errorMsg;?></small></h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<form role="form" action="<?=$baseURLAdmin;?>/content-manage" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<label>Title</label>
								<input type="text" name="title" id="title" value="<?=$title;?>" maxlength="250" autocomplete="off" class="form-control" placeholder="Title" autofocus required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label>Content</label>
								
								<?php
								$oFCKeditor = new FCKeditor('detail');
								$oFCKeditor->BasePath = $baseURL . "/admin/fckeditor/";
								$oFCKeditor->ToolbarSet = 'MyToolbar';
								$oFCKeditor->Value = $detail;
								$oFCKeditor->Create();
								$oFCKeditor->Height = 400;
								$oFCKeditor->Width = 900;
								?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label>Friendly URL</label>
								<input type="text" name="friendlyURL" id="friendlyURL" value="<?=$friendlyURL;?>" maxlength="250" autocomplete="off" class="form-control" placeholder="Friendly URL" autofocus required />
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-3">
								<label>Content For</label>
								<select name="contentfor" class="form-control">
									<option value="Mobile App" <?php if ($contentfor == 'Mobile App'){ echo 'selected';}?>>Mobile App</option>
									<option value="Website" <?php if ($contentfor == 'Website'){ echo 'selected';}?>>Website</option>
									<option value="Mobile / Website" <?php if ($contentfor == 'Mobile / Website'){ echo 'selected';}?>>Mobile / Website</option>
								</select>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12">
								<label>Friendly URL</label>
								<input type="text" name="sort" id="sort" value="<?=$sort;?>" maxlength="3" autocomplete="off" class="form-control" placeholder="Sort" autofocus required />
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-3">
								<label>Status</label>
								<select name="isactive" class="form-control">
									<option value="1" <?php if ($isactive == '1'){ echo 'selected';}?>>Active</option>
									<option value="0" <?php if ($isactive == '0'){ echo 'selected';}?>>Inactive</option>
								</select>
							</div>
						</div>
					</div>	
					<!-- /.panel-body -->	
					
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<input type="hidden" name="idcontent" id="idcontent" value="<?=$idcontent;?>" />
									<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
								</div>
								<!-- /.col-lg-6 (nested) -->
							</div>
						</div>
					</div>
					<!-- /.row (nested) -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</form>
</div>
<?php
include 'include/_footer_.php';
?>