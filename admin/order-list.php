<?php
include 'include/_header_.php';

$lvl = $appFunction->validate('0', $baseURLAdmin);

// echo "================================== " . $url = parse_url(basename($_SERVER["REQUEST_URI"]), PHP_URL_PATH);

$url = $appFunction->currentUrl(); 
$qry = "";

if (strtoupper($url) == 'NEW-JOBS-LIST'){
	$pageURL = $url;
	$heading = "New Jobs";
	
	$qry .= " AND bookedbyuser = '1'";
	$qry .= " AND acceptbydriver = '0'";
	$qry .= " AND bookingstatus = '0'";
	$qry .= " AND bookingcomplete = '0'";
} else if (strtoupper($url) == 'DRIVER-ACCEPTED-LIST'){
	$pageURL = $url;
	$heading = "Accepted by Driver";
	
	$qry .= " AND bookedbyuser = '1'";
	$qry .= " AND acceptbydriver = '1'";
	$qry .= " AND bookingstatus = '0'";
	$qry .= " AND bookingcomplete = '0'";
} else if (strtoupper($url) == 'DRIVER-ON-THE-WAY-LIST'){
	$pageURL = $url;
	$heading = "Driver On the Way";
	
	$qry .= " AND bookedbyuser = '1'";
	$qry .= " AND acceptbydriver = '1'";
	$qry .= " AND bookingstatus = '1'";
	$qry .= " AND bookingcomplete = '0'";
} else if (strtoupper($url) == 'JOB-STARTED-LIST'){
	$pageURL = $url;
	$heading = "Started Jobs";
	
	$qry .= " AND bookedbyuser = '1'";
	$qry .= " AND acceptbydriver = '1'";
	$qry .= " AND bookingstatus = '2'";
	$qry .= " AND bookingcomplete = '0'";
} else if (strtoupper($url) == 'COMPLETED-JOB-LIST'){
	$pageURL = $url;
	$heading = "Completed Jobs";
	
	$qry .= " AND bookedbyuser = '1'";
	$qry .= " AND acceptbydriver = '1'";
	$qry .= " AND bookingstatus = '3'";
	$qry .= " AND bookingcomplete = '0'";
} else if (strtoupper($url) == 'PAYMENT-COMPLETED-JOB-LIST'){
	$pageURL = $url;
	$heading = "Payment Completed Jobs";
	
	$qry .= " AND bookedbyuser = '1'";
	$qry .= " AND acceptbydriver = '1'";
	$qry .= " AND bookingstatus = '3'";
	$qry .= " AND bookingcomplete = '1'";
} else {
	$pageURL = $url;
	$heading = "Total Jobs";
	
	$qry .= " AND bookedbyuser = '1'";
	//$qry .= " AND acceptbydriver = '0'";
	//$qry .= " AND bookingstatus = '0'";
	//$qry .= " AND bookingcomplete = '0'";
}

if(!empty($_GET['action']) == 'deleted'){
	if(!empty($_GET['id'])){
		$idorder = $appFunction->validHTML($_GET['id']);
		$mySQL = "";
		$mySQL = "SELECT COUNT(0) AS totRec FROM vgetorderdetail WHERE idorder = '".$appFunction->validSQL($idorder,"")."' 
			AND idorder IN (SELECT idorder FROM vgetorder WHERE paymentstatus = 'Success' AND idorder = '".$appFunction->validSQL($idorder,"")."')";
		//echo $mySQL .'<br>';
		$rsTemp = $dbAccess->selectSingleStmt($mySQL);
		
		if($rsTemp['totRec'] > '0'){
			$msg = '3';
		} else {
			$mySQL = "";
			$mySQL = "DELETE FROM `order` WHERE `idorder` = '".$appFunction->validSQL($idorder,"")."'";
			//echo $mySQL .'<br>';
			$dbAccess->queryExec($mySQL);
			$msg = '2';
		}
		header("location:".$baseURLAdmin."/".$url."?msg=".$msg);
		exit;
	}		
}

$qryString='';

if(!empty($_POST['searchkeyWord'])){
	$searchkeyWord = trim($_POST['searchkeyWord']);
	$qryString .= 'searchkeyWord='.$searchkeyWord.'&';
} else if(!empty($_GET['searchkeyWord'])){
	$searchkeyWord = trim($_GET['searchkeyWord']);	
	$qryString .= 'searchkeyWord='.$searchkeyWord.'&';
}

if(!empty($_POST['orderDate'])){
	$orderDate = trim($_POST['orderDate']);
	$qryString .= 'eventDate='.$orderDate.'&';
} else if(!empty($_GET['orderDate'])){
	$orderDate = trim($_GET['orderDate']);	
	$qryString .= 'orderDate='.$orderDate.'&';
}

// PAGINATION
//$targetpage = 'screen-list?'.$qryString; //your file name
$targetpage = $url.'?'.$qryString; //your file name
$limit = 25; //how many items to show per page
$page=0;
if(!empty($_GET['page']))
{
	$page = $_GET['page'];
}
// echo 'Page = ' . $page . '<br>';
if($page){ 
	$start = ($page - 1) * $limit; //first item to display on this page
	$sno = $start+1;
} else {
	$start = 0;
	$sno = 1;
}
/* Setup page vars for display. */
if ($page == 0) $page = 1; //if no page var is given, default to 1.	
// PAGINATION
?>
<style>
.progress-bar-default {
    background-color: #CCCCCC;
}
</style>

<script src="<?=$baseURLAdmin;?>/datepicker/jquery.timepicker.js"></script>
<link href="<?=$baseURLAdmin;?>/datepicker/jquery.timepicker.css" rel="stylesheet" />

<script>
$( function() {
	$("#orderDate").datepicker({ 
		format: 'yyyy-mm-dd',
		// minDate: 0, 
		maxDate: "+36M", 
		changeMonth: true,
		changeYear: true 
	});
} );
</script>

<div id="page-wrapper">
	<div class="row">
		<div class="col-sm-12">
			<h1 class="page-header"><?=$heading;?> 
			<small class="text-danger">
			<?php 
			if (!empty($_GET['msg']) AND $_GET['msg'] == '1') { 
				echo 'Screen saved';
			} else if (!empty($_GET['msg']) AND $_GET['msg'] == '2') { 
				echo 'Selected record deleted';
			} else if (!empty($_GET['msg']) AND $_GET['msg'] == '3') { 
				echo 'Selected record not deleted, Because order is confirmed';
			}
			?>
			</small></h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<div class="row">
		<div class="col-sm-12 text-center">
			<a href="<?=$baseURLAdmin;?>/all-jobs-list" class="btn btn-default">Total Jobs (<?=$dbAccess->countOrders('');?>)</a>
			<a href="<?=$baseURLAdmin;?>/new-jobs-list" class="btn btn-primary">New Jobs (<?=$dbAccess->countOrders('NEW-JOBS-LIST');?>)</a>
			<a href="<?=$baseURLAdmin;?>/driver-accepted-list" class="btn btn-danger">Accecpted By Driver (<?=$dbAccess->countOrders('DRIVER-ACCEPTED-LIST');?>)</a>
			<a href="<?=$baseURLAdmin;?>/driver-on-the-way-list" class="btn btn-info">Driver On the Way (<?=$dbAccess->countOrders('DRIVER-ON-THE-WAY-LIST');?>)</a>
			<a href="<?=$baseURLAdmin;?>/job-started-list" class="btn btn-warning">Started Jobs (<?=$dbAccess->countOrders('JOB-STARTED-LIST');?>)</a>
			<a href="<?=$baseURLAdmin;?>/completed-job-list" class="btn btn-success">Completed Job (<?=$dbAccess->countOrders('COMPLETED-JOB-LIST');?>)</a>
			<a href="<?=$baseURLAdmin;?>/payment-completed-job-list" class="btn btn-primary">Payment Done (<?=$dbAccess->countOrders('PAYMENT-COMPLETED-JOB-LIST');?>)</a>
		</div>
	</div>-->
	<form role="form" action="<?=$baseURLAdmin;?>/<?=$url;?>" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-sm-12">
				<br>
				<div class="panel panel-default">
					<div class="panel-heading">
						Booking Search
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-3">
								<label>Keyword</label>
								<input type="text" name="searchkeyWord" id="searchkeyWord" class="form-control" autocomplete="off" />
							</div>
							<div class="col-sm-2">
								<label>Booking Date</label>
								<input type="text" name="orderDate" id="orderDate" class="form-control" readonly autocomplete="off" />
							</div>
							<div class="col-sm-1">
								<label>&nbsp;<br></label>
								<input type="submit" name="submit" id="submit" value="Search" class="btn btn-primary" />
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</form>
	<br>
		
	<!-- /.row -->
	<form role="form" action="<?=$baseURLAdmin;?>/<?=$url;?>" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Booking Summary
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<?php
							$mySQL = "";
							$mySQL = "SELECT idbooking, bookingno, iduser, fullname";
							$mySQL .= ", destinationaddress AS address";
							$mySQL .= ", destinationcity AS city";
							$mySQL .= ", destinationstate AS state";
							$mySQL .= ", destinationzipcode AS zipcode";
							// $mySQL .= ", destinationcountry AS country";
							$mySQL .= ", (CASE 	WHEN bookingstatus = '1' THEN 'On The Way'
												WHEN bookingstatus = '2' THEN 'Job Started'
												WHEN bookingstatus = '3' AND bookingcomplete = '0' THEN 'Job Complete'
												WHEN bookingstatus = '3' AND bookingcomplete = '1' THEN 'Payment Done'
										 ELSE 'Job Not Started' END) AS bookingstatus";
							$mySQL .= ", DATE_FORMAT(bookingdate, '%d-%b-%Y') AS bookingdate";
							// $mySQL .= ", (CASE WHEN orderacceptdate IS NULL THEN '' ELSE orderacceptdate END) AS orderacceptdate";
							// $mySQL .= ", (CASE WHEN orderprepareddate IS NULL THEN '' ELSE orderprepareddate END) AS orderprepareddate";
							// $mySQL .= ", (CASE WHEN orderreadydate IS NULL THEN '' ELSE orderreadydate END) AS orderreadydate";
							// $mySQL .= ", (CASE WHEN orderdispatchdate IS NULL THEN '' ELSE orderdispatchdate END) AS orderdispatchdate";
							// $mySQL .= ", (CASE WHEN orderdeliverdate IS NULL THEN '' ELSE orderdeliverdate END) AS orderdeliverdate";
							// $mySQL .= ", (SELECT COUNT(0) AS totRec FROM vgetorderdetail WHERE vgetorderdetail.idorder = vgetorder.idorder AND paymentstatus = 'Success') AS totRec";
							$mySQL .= " FROM";
							$mySQL1 = " `vgetbooking` WHERE idbooking <> ''";
							$mySQL1 .= $qry;
							
							if(!empty($searchkeyWord)){
								$spt_searchkeyWord = explode(' ', $searchkeyWord);
								$mySQLTemp = "";
								for($i=0; $i < count($spt_searchkeyWord); $i++){
									$mySQLTemp .= " (fullname LIKE '%".$spt_searchkeyWord[$i]."%')";
									$mySQLTemp .= " OR (destinationaddress LIKE '%".$spt_searchkeyWord[$i]."%')";
									$mySQLTemp .= " OR (destinationcity LIKE '%".$spt_searchkeyWord[$i]."%')";
									$mySQLTemp .= " OR (destinationstate LIKE '%".$spt_searchkeyWord[$i]."%')";
									$mySQLTemp .= " OR (destinationzipcode = '".$spt_searchkeyWord[$i]."')";
									$mySQLTemp .= " OR (bookingno = '".$spt_searchkeyWord[$i]."')";
									
									if ($i < count($spt_searchkeyWord)-1){
										$mySQLTemp .= " OR ";
									}
								}
								
								$mySQL1 .= " AND (" .$mySQLTemp. ")";
							}
							
							if(!empty($orderDate)){
								$orderDate = date("Y-m-d", strtotime($orderDate));
								$mySQL1 .= " AND DATE(bookingdate) = '".$orderDate."'";
							}
							$mySQL1 .= " ORDER BY DATE_FORMAT(bookingdate, '%Y-%m-%d') DESC";
							$mySQL2 = " LIMIT " . $start . "," . $limit;
							$mySQLQry = $mySQL . $mySQL1 . $mySQL2;
							// echo $mySQLQry .'<hr>';
							$rsTemp = $dbAccess->selectData($mySQLQry);
							?>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th class="text-center">#</th>
										<th class="text-center">Booking Id</th>
										<th>Customer Detail</th>
										<th class="text-center">Date</th>
										<th class="text-center">Status</th>
										<th class="text-center"><!--Action--></th>
									</tr>
								</thead>
								<tbody>
									<?php
									if (empty($rsTemp)){
									?>
										<tr>
											<td class="text-center" colspan="6">Record Not Found</td>
										</tr>
									<?php
									} else {
										foreach($rsTemp AS $rsTempVal){
									?>
											<tr>
												<td class="text-center"><?=$sno;?></td>
												<td class="text-center"><?=$rsTempVal['bookingno'];?></td>
												<td>
													<div><strong><i><?=$rsTempVal['fullname'];?></i></strong></div>
													<div><?=$rsTempVal['address'];?></div>
												</td>
												<td class="text-center" nowrap><?=$rsTempVal['bookingdate'];?></td>
												<td class="text-center" nowrap><?=$rsTempVal['bookingstatus'];?></td>
												<td class="text-center" nowrap>
												<!--<a href="<?=$baseURLAdmin;?>/order-view/?idbooking=<?=$rsTempVal['idbooking'];?>" title="View Order Detail">
												<button type="button" class="btn btn-outline btn-info btn-circle1 btn-sm"><i class="fa fa-eye"></i></button></a>
												<a href="<?=$baseURLAdmin;?>/order-manage/?idbooking=<?=$rsTempVal['idbooking'];?>" title="Edit Order Status">
												<button type="button" class="btn btn-outline btn-success btn-circle1 btn-sm"><i class="fa fa-edit"></i></button></a>-->
												<?php 
												// if ($rsTempVal['totRec'] == '0'){
												?>
													<!--<a href="javascript:void(0);" onclick="deleteRec('<?=$rsTempVal['idbooking'];?>')" title="Delete Order?">
													<button type="button" class="btn btn-outline btn-danger btn-circle1 btn-sm"><i class="fa fa-trash-o"></i></button></a>-->
												<?php
												// }
												?>
												</td>
											</tr>
											<tr>
												<td colspan="6">
												<?=$appFunction->getOrderProgress($rsTempVal['bookingstatus']);?>
											</tr>
									<?php
											$sno++;
										}
									}
									?>
								</tbody>
							</table>
							<div><?php echo $dbAccess->pagination($mySQL1, $limit, $page, $targetpage);?></div>
						</div>
						<!-- /.table-responsive -->
					</div>
					<!-- /.panel-body -->
				</div>
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</form>
	<!-- /.row -->
</div>
<script>
function deleteRec(id){
	isDeleted = confirm("Are you sure delete this record.");
	if(isDeleted == true){
		top.document.location.href = "<?=$baseURLAdmin;?>/<?=$url;?>?action=delete&id="+id;
	} else {
		return (false);
	}
}
</script>
<?php
include 'include/_footer_.php';
?>