<?php
include 'include/_header_.php';

$start = '0';
$limit = '5';

$lvl = $appFunction->validate('0', $baseURL);
$todayDate = date('Y-m-d');
$todayDateDDMMYY = date('d-m-Y');
?>
	 
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header" style="color: #FFFFFF;">Dashboard</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<?php
	/*
	<div class="row">
		<div class="col-lg-2 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-newspaper-o fa-2x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge"><?=$dbAccess->countOrders('NEW-JOBS-LIST');?></div>
							<div>New Jobs</div>
						</div>
					</div>
				</div>
				<a href="<?=$baseURLAdmin;?>/new-jobs-list">
					<div class="panel-footer">
						<span class="pull-left">View Details</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-red">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-users fa-2x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge"><?=$dbAccess->countOrders('DRIVER-ACCEPTED-LIST');?></div>
							<div>Accecpted By Driver</div>
						</div>
					</div>
				</div>
				<a href="<?=$baseURLAdmin;?>/driver-accepted-list">
					<div class="panel-footer">
						<span class="pull-left">View Details</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-2 col-md-6">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-newspaper-o fa-2x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge"><?=$dbAccess->countOrders('DRIVER-ON-THE-WAY-LIST');?></div>
							<div>On the Way</div>
						</div>
					</div>
				</div>
				<a href="<?=$baseURLAdmin;?>/driver-on-the-way-list">
					<div class="panel-footer">
						<span class="pull-left">View Details</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-2 col-md-6">
			<div class="panel panel-yellow">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-user fa-2x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge"><?=$dbAccess->countOrders('JOB-STARTED-LIST');?></div>
							<div>Started Jobs</div>
						</div>
					</div>
				</div>
				<a href="<?=$baseURLAdmin;?>/job-started-list">
					<div class="panel-footer">
						<span class="pull-left">View Details</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-green">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-newspaper-o fa-2x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge"><?=$dbAccess->countOrders('COMPLETED-JOB-LIST');?></div>
							<div>Completed Job</div>
						</div>
					</div>
				</div>
				<a href="<?=$baseURLAdmin;?>/completed-job-list">
					<div class="panel-footer">
						<span class="pull-left">View Details</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
	</div>
	*/
	?>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-yellow">
				<div class="panel-heading">
					<i class="fa fa-newspaper-o fa-fw"></i> <strong>Today <?=$todayDateDDMMYY;?> Jobs</strong>
				</div>
				<!-- /.panel-heading -->
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table class="table table-bordered table-hover table-striped">
									<thead>
										<tr>
											<th>#</th>
											<th>Order No.</th>
											<th>Customer Name</th>
											<th>Date</th>
											<!--<th class="text-center" colspan="2">Action</th>-->
										</tr>
									</thead>
									<tbody>
										<?php
										$sno = '1';
										$mySQL = "";
										$mySQL = "SELECT 
										idbooking
										, bookingno 
										, fullname 
										, originaddress 
										, destinationaddress 
										, DATE_FORMAT(bookingdate, '%b %d, %Y') AS bookingdate 
										
										FROM vgetbooking WHERE MONTH(bookingdate) = '".date("m")."'";
										$mySQL .= " AND DATE(bookingdate) = '".$todayDate."'";
										$mySQL .= " ORDER BY bookingdate ASC LIMIT 5";
										//echo $mySQL;
										$rsTemp = $dbAccess->selectData($mySQL);
										foreach ($rsTemp AS $rsTempVal){
										?>
											<tr>
												<td><?=$sno;?></td>
												<td><?=$rsTempVal['bookingno'];?></td>
												<td>
												<?=$rsTempVal['fullname'];?>
												<div><?=$rsTempVal['destinationaddress'];?></div>
												</td>
												<td nowrap><?=$rsTempVal['bookingdate'];?></td>
												<!--<td class="text-center"><button type="button" class="btn btn-outline btn-primary btn-circle btn-sm" title="View This?"><i class="fa fa-eye" title="View This?"></i></button></td>
												<td class="text-center"><a href="<?=$baseURLAdmin;?>/order-manage?idbooking=<?=$rsTempVal['idbooking'];?>"><button type="button" class="btn btn-outline btn-success btn-circle btn-sm" title="Edit This?"><i class="fa fa-edit" title="Edit This?"></i></button></a></td>-->
											</tr>
										<?php
											$sno++;
										}
										?>
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						<!-- /.col-lg-4 (nested) -->
						<div class="col-lg-8">
							<div id="morris-bar-chart"></div>
						</div>
						<!-- /.col-lg-8 (nested) -->
					</div>
					<!-- /.row -->
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
			<div class="panel panel-primary">
				<div class="panel-heading">
					<i class="fa fa-newspaper-o fa-fw"></i> <strong>Next Month Job</strong>
					
				</div>
				<!-- /.panel-heading -->
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table class="table table-bordered table-hover table-striped">
									<thead>
										<tr>
											<th>#</th>
											<th>Headline</th>
											<th>Date</th>
											<th>Start Time</th>
											<th class="text-center" colspan="2">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$date = date('Y-m-d');
										$sno = '1';
										$mySQL = "";
										$mySQL = "SELECT 
										articleid
										, headline 
										, DATE_FORMAT(eventDate, '%b %d, %Y') AS eventDate 
										, eventStartTime
										FROM articles WHERE status <> '4' AND MONTH(eventDate) = '".date('m', strtotime($date .'+1 month'))."'";
										$mySQL .= " AND DATE(eventDate) >= '".$todayDate."'";
										/* if ($domainname != 'gooddayevents.com'){
											$mySQL .= " AND domainname = '".$domainname."'";
										} */
										if ($lvl == 0) { 
											$mySQL .= " AND articles.publisherid = '".$_SESSION['xlaANM_usr']."'";
										}
										$mySQL .= " ORDER BY eventDate ASC LIMIT 5";
										//echo $mySQL;
										/*
										$rsTemp = $dbAccess->selectData($mySQL);
										foreach ($rsTemp AS $rsTempVal){
										?>
											<tr>
												<td><?=$sno;?></td>
												<td><?=$rsTempVal['headline'];?></td>
												<td><?=$rsTempVal['eventDate'];?></td>
												<td><?=$rsTempVal['eventStartTime'];?></td>
												<td class="text-center"><button type="button" class="btn btn-outline btn-primary btn-circle btn-sm" title="View This?"><i class="fa fa-eye" title="View This?"></i></button></td>
												<td class="text-center"><a href="<?=$baseURL;?>/promos/event-manage?articleid=<?=$rsTempVal['articleid'];?>"><button type="button" class="btn btn-outline btn-success btn-circle btn-sm" title="Edit This?"><i class="fa fa-edit" title="Edit This?"></i></button></a></td>
											</tr>
										<?php
											$sno++;
										}
										*/
										?>
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						<!-- /.col-lg-4 (nested) -->
						<div class="col-lg-8">
							<div id="morris-bar-chart"></div>
						</div>
						<!-- /.col-lg-8 (nested) -->
					</div>
					<!-- /.row -->
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-8 -->
		<?php
		/*
		<div class="col-lg-4">
			<div class="panel panel-green">
				<div class="panel-heading">
					<i class="fa fa-users fa-fw"></i> Group List
				</div>
				<!-- /.panel-heading -->
				<div class="panel-body">
					<div class="list-group">
						<?php
						$sno = '1';
						$mySQL = "";
						$mySQL = "SELECT * FROM";
						$mySQL1 = " groups ORDER BY groupName";
						$mySQL2 = " LIMIT " . $start . "," . $limit;
						$mySQLQry = $mySQL . $mySQL1 . $mySQL2;
						//echo $mySQLQry .'<hr>';
						/* $rsTemp = $dbAccess->selectData($mySQLQry);
						foreach ($rsTemp AS $rsTempVal){
						?>
							<a href="<?=$baseURL;?>/promos/groups-manage/?idGroup=<?=$rsTempVal['idGroup'];?>" class="list-group-item">
								<i class="fa fa-users fa-fw"></i> <?=$rsTempVal['groupName'];?>
								<span class="pull-right text-muted small"><em>Edit</em>
								</span>
							</a>
						<?php
							$sno++;
						} //
						?>
					</div>
					<!-- /.list-group -->
					<a href="<?=$baseURL;?>/promos/groups-list" class="btn btn-default btn-block">View All Groups</a>
				</div>
				<!-- /.panel-body -->
			</div>
			<div class="panel panel-red">
				<div class="panel-heading">
					<i class="fa fa-list fa-fw"></i> Category List
				</div>
				<!-- /.panel-heading -->
				<div class="panel-body">
					<div class="list-group">
						<?php
						$sno = '1';
						$mySQL = "";
						$mySQL = "SELECT * FROM";
						$mySQL1 = " category ORDER BY categoryName";
						$mySQL2 = " LIMIT " . $start . "," . $limit;
						$mySQLQry = $mySQL . $mySQL1 . $mySQL2;
						//echo $mySQLQry .'<hr>';
						// $rsTemp = $dbAccess->selectData($mySQLQry);
						/*
						foreach ($rsTemp AS $rsTempVal){
						?>
							<a href="<?=$baseURL;?>/promos/category-manage/?idCategory=<?=$rsTempVal['idCategory'];?>" class="list-group-item">
								<i class="fa fa-list fa-fw"></i> <?=$rsTempVal['categoryName'];?>
								<span class="pull-right text-muted small"><em>Edit</em>
								</span>
							</a>
						<?php
							$sno++;
						}//
						?>
					</div>
					<!-- /.list-group -->
					<a href="<?=$baseURLAdmin;?>/category-list" class="btn btn-default btn-block">View All Category</a>
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		*/
		?>
		<!-- /.col-lg-4 -->
	</div>
	<!-- /.row -->
</div>
<!-- /#page-wrapper -->
<?php
include 'include/_footer_.php';
?>
