<?php
include 'include/_header_.php';

$lvl = $appFunction->validate('0', $baseURL);

if(!empty($_GET['action']) == 'deleted'){
	if(!empty($_GET['id'])){
		$idgallery = $appFunction->validHTML($_GET['id']);
		$mySQL = "";
		$mySQL = "DELETE FROM gallery WHERE idgallery = '".$appFunction->validSQL($idgallery,"")."'";
		//echo $mySQL .'<br>';
		$dbAccess->queryExec($mySQL);
		header("location:".$baseURL."/admin/gallery-list?msg=1");
		exit;
	}		
}

$qryString='';
// PAGINATION
$targetpage = 'gallery-list?'.$qryString; //your file name
$limit = 25; //how many items to show per page
$page=0;
if(!empty($_GET['page']))
{
	$page = $_GET['page'];
}
// echo 'Page = ' . $page . '<br>';
if($page){ 
	$start = ($page - 1) * $limit; //first item to display on this page
	$sno = $start+1;
} else {
	$start = 0;
	$sno = 1;
}
/* Setup page vars for display. */
if ($page == 0) $page = 1; //if no page var is given, default to 1.	
// PAGINATION
?>

<div id="page-wrapper">
	<div class="row">
		<div class="col-sm-10">
			<h1 class="page-header">Gallery List <?php if (!empty($_GET['msg']) == '1'){?><small class="text-danger">Selected record deleted<?php } ?></small></h1>
		</div>
		<div class="col-sm-2 text-right">
			<h1 class="page-header"><a href="<?=$baseURL;?>/admin/gallery-manage/" class="btn btn-info">Add a Gallery</a></h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<form role="form" action="<?=$baseURL;?>/admin/gallery-add/" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Gallery Summary
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<?php
							$mySQL = "";
							$mySQL = "SELECT * FROM";
							$mySQL1 = " gallery ORDER BY sort";
							$mySQL2 = " LIMIT " . $start . "," . $limit;
							$mySQLQry = $mySQL . $mySQL1 . $mySQL2;
							//echo $mySQLQry .'<hr>';
							$rsTemp = $dbAccess->selectData($mySQLQry);
							?>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th class="text-center">#</th>
										<th>Gallery Name</th>
										<th class="text-center">Sort</th>
										<th class="text-center">Status</th>
										<th class="text-center">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									if (empty($rsTemp)){
									?>
										<tr>
											<td class="text-center" colspan="5">Record Not Found</td>
										</tr>
									<?php
									} else {
										foreach($rsTemp AS $rsTempVal){
									?>
											<tr>
												<td class="text-center"><?=$sno;?></td>
												<td><?=$rsTempVal['headline'];?></td>
												<td class="text-center"><?=$rsTempVal['sort'];?></td>
												<td class="text-center"><?=$rsTempVal['isactive'];?></td>
												<td class="text-center">
												<a href="<?=$baseURL;?>/admin/gallery-manage/?idgallery=<?=$rsTempVal['idgallery'];?>">
												<button type="button" class="btn btn-outline btn-success btn-circle1 btn-sm"><i class="fa fa-edit"></i></button></a>
												<a href="javascript:void(0);" onclick="deleteRec(<?=$rsTempVal['idgallery'];?>)">
												<button type="button" class="btn btn-outline btn-danger btn-circle1 btn-sm"><i class="fa fa-trash-o"></i></button></a>
												</td>
											</tr>
									<?php
											$sno++;
										}
									}
									?>
								</tbody>
							</table>
							<div><?php echo $dbAccess->pagination($mySQL1, $limit, $page, $targetpage);?></div>
						</div>
						<!-- /.table-responsive -->
					</div>
					<!-- /.panel-body -->
				</div>
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</form>
	<!-- /.row -->
</div>
<script>
function deleteRec(id){
	isDeleted = confirm("Are you sure delete this record.");
	if(isDeleted == true){
		top.document.location.href = "<?=$baseURL;?>/admin/gallery-list?action=delete&id="+id;
	} else {
		return (false);
	}
}
</script>
<?php
include 'include/_footer_.php';
?>
