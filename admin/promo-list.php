<?php
include 'include/_header_.php';

$lvl = $appFunction->validate('0', $baseURL);

$todayDate = date('Y-m-d');

if(!empty($_GET['action']) == 'deleted'){
	if(!empty($_GET['id'])){
		$idpromo = $appFunction->validHTML($_GET['id']);
		
		$mySQL = "";
		$mySQL = "DELETE FROM promos WHERE idpromo = '".$appFunction->validSQL($idpromo,"")."'";
		// echo $mySQL .'<br>';
		$dbAccess->queryExec($mySQL);
		
		header("location:".$baseURLAdmin."/promo-list?msg=2");
		exit;
	}		
}

$qryString='';

if(!empty($_POST['searchkeyWord'])){
	$searchkeyWord = trim($_POST['searchkeyWord']);
	$qryString .= '&searchkeyWord='.$searchkeyWord;
} else if(!empty($_GET['searchkeyWord'])){
	$searchkeyWord = trim($_GET['searchkeyWord']);	
	$qryString .= '&searchkeyWord='.$searchkeyWord;
}

if(!empty($_POST['postdate'])){
	$postdate = trim($_POST['postdate']);
	$qryString .= '&postdate='.$postdate;
} else if(!empty($_GET['postdate'])){
	$postdate = trim($_GET['postdate']);	
	$qryString .= '&postdate='.$postdate;
}

// PAGINATION
$targetpage = 'promo-list?'.$qryString; //your file name
$limit = 25; //how many items to show per page
$page=0;
if(!empty($_GET['page']))
{
	$page = $_GET['page'];
}
// echo 'Page = ' . $page . '<br>';
if($page){ 
	$start = ($page - 1) * $limit; //first item to display on this page
	$sno = $start+1;
} else {
	$start = 0;
	$sno = 1;
}
/* Setup page vars for display. */
if ($page == 0) $page = 1; //if no page var is given, default to 1.	
// PAGINATION
?>

<script src="<?=$baseURL;?>/admin/datepicker/jquery.timepicker.js"></script>
<link href="<?=$baseURL;?>/admin/datepicker/jquery.timepicker.css" rel="stylesheet" />

<script>
$( function() {
	$("#postdate").datepicker({ 
		format: 'yyyy-mm-dd',
		minDate: 0, 
		maxDate: "+36M", 
		changeMonth: true,
		changeYear: true 
	});
} );
</script>

<!-- SHARE THIS -->
<script type="text/javascript">var switchTo5x = true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({ publisher: "9ee8c443-57cf-44ea-8e67-11693a461baf", doNotHash: false, doNotCopy: false, hashAddressBar: false });</script>
<!-- / SHARE THIS -->
<div id="page-wrapper">
	<form role="form" action="<?=$baseURLAdmin;?>/promo-list/" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-sm-10">
				<h1 class="page-header">Promo List <?php if (!empty($_GET['msg']) == '2'){?><small class="text-danger">Selected record deleted<?php } ?></small></h1>
			</div>
			<div class="col-sm-2 text-right">
				<h1 class="page-header"><a href="<?=$baseURLAdmin;?>/promo-manage/" class="btn btn-info">Add Promo</a></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Promo Summary
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<?php
							$mySQL = "";
							$mySQL = "SELECT idpromo";
							$mySQL .= ", promocode";
							$mySQL .= ", promovalue";
							$mySQL .= ", promotype";
							$mySQL .= ", starttime";
							$mySQL .= ", endtime";
							$mySQL .= ", (CASE WHEN isactive = '1' THEN 'Active' ELSE 'Inactive' END) AS isactive";
							$mySQL .= " FROM";
							$mySQL1 = " promos";
							$mySQL1 .= " WHERE 1=1";
							$mySQL2 = " LIMIT " . $start . "," . $limit;
							$mySQLQry = $mySQL . $mySQL1 . $mySQL2;
							$rsTemp = $dbAccess->selectData($mySQLQry);
							?>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th class="text-center">#</th>
										<th>Promo Code</th>
										<th>Promo Type</th>
										<th>Promo Value</th>
										<th>Start Date</th>
										<th>End Date</th>
										<th class="text-center">Status</th>
										<th class="text-center">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									if (empty($rsTemp)){
									?>
										<tr>
											<td class="text-center" colspan="6">Record Not Found</td>
										</tr>
									<?php
									} else {
										foreach($rsTemp AS $rsTempVal){
									?>
											<tr>
												<td class="text-center"><?=$sno;?></td>
												<td><?=$rsTempVal['promocode'];?></td>
												<td><?=$rsTempVal['promotype'];?></td>
												<td><?=$rsTempVal['promovalue'];?></td>
												<td><?=$rsTempVal['starttime'];?></td>
												<td><?=$rsTempVal['endtime'];?></td>
												<td class="text-center"><?=$rsTempVal['isactive'];?></td>
												<td class="text-center">
												<a href="<?=$baseURLAdmin;?>/promo-manage/?idpromo=<?=$rsTempVal['idpromo'];?>">
												<button type="button" class="btn btn-outline btn-success btn-circle1 btn-sm"><i class="fa fa-edit"></i></button></a>
												<a href="javascript:void(0);" onclick="deleteRec(<?=$rsTempVal['idpromo'];?>)">
												<button type="button" class="btn btn-outline btn-danger btn-circle1 btn-sm"><i class="fa fa-trash-o"></i></button></a>
												</td>
											</tr>
									<?php
											$sno++;
										}
									}
									?>
								</tbody>
							</table>
							<div><?php echo $dbAccess->pagination($mySQL1, $limit, $page, $targetpage);?></div>
						</div>
						<!-- /.table-responsive -->
					</div>
				</div>
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</form>
	<!-- /.row -->
</div>
<script>
function deleteRec(id){
	isDeleted = confirm("Are you sure delete this record.");
	if(isDeleted == true){
		top.document.location.href = "<?=$baseURLAdmin;?>/promo-list?action=delete&id="+id;
	} else {
		return (false);
	}
}
</script>
<?php
include 'include/_footer_.php';
?>