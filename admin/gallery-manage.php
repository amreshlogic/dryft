<?php
include 'include/_header_.php';

error_reporting(-1);

$lvl = $appFunction->validate('0', $baseURL);

$errorMsg = '';
$idgallery = '';
$headline = '';
$fulldescription = '';
$sort = '';

$friendlyurl = '';
$metatitle = '';
$metakeyword = '';
$metadescription = '';
$isactive = '';
$uploadImage_Old = '';

if (!empty($_POST['submit'])){
	$idgallery = trim($appFunction->validHTML($_POST['idgallery']));
	$headline = trim($appFunction->validHTML($_POST['headline']));
	//$fulldescription = $appFunction->validHTML($_POST['fulldescription']);
	$fulldescription = ($_POST['fulldescription']);
	$sort = trim($appFunction->validHTML($_POST['sort']));
	
	$friendlyurl = $appFunction->validHTML($_POST['friendlyurl']);
	$metatitle = $appFunction->validHTML($_POST['metatitle']);
	$metakeyword = $appFunction->validHTML($_POST['metakeyword']);
	$metadescription = $appFunction->validHTML($_POST['metadescription']);
	$isactive = $appFunction->validHTML($_POST['isactive']);
	
	$uploadImage_Old = $appFunction->validHTML($_POST['uploadImage_Old']);
	
	$iduser = $appFunction->validHTML($_SESSION['xlaANM_usr']);
		
	// CHECK IMAGE EXT
	$uploadImageName = basename($_FILES['uploadImage']['name']);
	$uploadImageSize = basename($_FILES['uploadImage']['size']);
	$uploadImageExt = pathinfo($uploadImageName,PATHINFO_EXTENSION);
	
	if(empty($friendlyurl)){
		$friendlyurl = strtolower($categoryname);
		$friendlyurl = $appFunction->friendlyURL($friendlyurl);
	} else {
		$friendlyurl = strtolower($friendlyurl);
		$friendlyurl = $appFunction->friendlyURL($friendlyurl);
	}
	
	echo '<div style="padding-left:300px;">';
	//echo 'uploadImageSize = ' . $uploadImageSize .'<br>';
	//echo $uploadImageExt .'<br>';
	
	if(empty($headline)){
		$errorMsg = 'Please enter headline';
	} else if(!empty($uploadImageName) AND strtolower($uploadImageExt) != "jpg" AND strtolower($uploadImageExt) != "jpeg"){
		$errorMsg = 'Please upload imgage in JPG & JPEG format';
	} else if(!empty($uploadImageName) AND ($uploadImageSize == 0 OR $uploadImageSize > 150000)){
		$errorMsg = 'Please upload image size less than 100KB';
	}
	
	$fileDir = '../galleryimage/';
	
	if (!file_exists($fileDir)) {
		mkdir($fileDir, 0777, true);
	}
	
	//echo $fileDir;
	if(empty($errorMsg)){
		if($uploadImageName != '') {
			$uploadImageName = time() . '_' . str_replace('.php', '', $uploadImageName);
			move_uploaded_file($_FILES['uploadImage']['tmp_name'], $fileDir.$uploadImageName);
		}else{
			$uploadImageName = $uploadImage_Old;
		}
		
		if(empty($idgallery)){
			$mySQL = "";
			$mySQL .= "INSERT INTO gallery (";
			$mySQL .= " iduser";
			$mySQL .= ", headline";
			$mySQL .= ", fulldescription";
			$mySQL .= ", sort";
			$mySQL .= ", image";
			$mySQL .= ", metatitle";
			$mySQL .= ", metakeyword";
			$mySQL .= ", metadescription";
			$mySQL .= ", isactive";
			$mySQL .= ") VALUES (";
			$mySQL .= "'". $appFunction->validSQL($iduser,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($headline,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($fulldescription,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($sort,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($uploadImageName,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($metatitle,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($metakeyword,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($metadescription,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($isactive,"")."'";
			$mySQL .= ")";
			//echo $mySQL .'<br>';			
			//exit;
			$dbAccess->queryExec($mySQL);
			$idgallery = $dbAccess->LastId();
			
			$mySQL = "";
			$mySQL .= "UPDATE gallery SET";
			$mySQL .= " friendlyurl = '". $appFunction->validSQL($idgallery . '-' . $friendlyurl,"") ."'";
			$mySQL .= " WHERE idgallery = '".$appFunction->validSQL($idgallery,"")."'";
			//echo $mySQL .'<br>';			
			//exit;
			$dbAccess->queryExec($mySQL);
		} else {
			$mySQL = "";
			$mySQL .= "UPDATE gallery SET";
			$mySQL .= "  headline = '". $appFunction->validSQL($headline,"")."'";
			$mySQL .= ", friendlyurl = '". $appFunction->validSQL($idgallery . '-' . $friendlyurl,"") ."'";
			$mySQL .= ", fulldescription = '". $appFunction->validSQL($fulldescription,"")."'";
			$mySQL .= ", sort = '". $appFunction->validSQL($sort,"")."'";
			$mySQL .= ", image = '". $appFunction->validSQL($uploadImageName,"")."'";
			$mySQL .= ", metatitle = '". $appFunction->validSQL($metatitle,"")."'";
			$mySQL .= ", metakeyword = '". $appFunction->validSQL($metakeyword,"")."'";
			$mySQL .= ", metadescription = '". $appFunction->validSQL($metadescription,"")."'";
			$mySQL .= ", isactive = '". $appFunction->validSQL($isactive,"")."'";
			$mySQL .= " WHERE idgallery = '".$appFunction->validSQL($idgallery,"")."'";
			//echo $mySQL .'<br>';			
			//exit;
			$dbAccess->queryExec($mySQL);
		}
		
		header("location:".$baseURL."/admin/gallery-list");
		exit;
	}
	echo '</div>';
}

if(!empty($_GET['idgallery'])){
	$idgallery = trim($appFunction->validHTML($_GET['idgallery']));
	$mySQL = "";
	$mySQL = "SELECT idgallery";
	$mySQL .= ", headline";
	$mySQL .= ", fulldescription";
	$mySQL .= ", friendlyurl";
	$mySQL .= ", sort";
	$mySQL .= ", metatitle";
	$mySQL .= ", metakeyword";
	$mySQL .= ", metadescription";
	$mySQL .= ", image";
	$mySQL .= ", CONCAT('/galleryimage/', image) AS imagepath";
	$mySQL .= ", isactive";
	$mySQL .= " FROM gallery";
	$mySQL .= " WHERE idgallery = '".$appFunction->validSQL($idgallery,"")."'";
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	
	$idgallery = $rsTemp['idgallery'];
	$headline = $rsTemp['headline'];
	$fulldescription = $rsTemp['fulldescription'];
	$sort = $rsTemp['sort'];
	
	$friendlyurl = str_replace($idgallery .'-', '', $rsTemp['friendlyurl']);
	$metatitle = $rsTemp['metatitle'];
	$metakeyword = $rsTemp['metakeyword'];
	$metadescription = $rsTemp['metadescription'];
	$uploadImage_Old = $rsTemp['image'];
	$imagepath = $baseURL . $rsTemp['imagepath'];
	
	$isactive = $rsTemp['isactive'];
}
?>

<?php include 'fckeditor/fckeditor.php';?>
		
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Gallery Information <small><?=$errorMsg;?></small></h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<form role="form" action="<?=$baseURL;?>/admin/gallery-manage/" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<label>Headline</label>
								<input type="text" name="headline" id="headline" value="<?=$headline;?>" maxlength="250" autocomplete="off" class="form-control" placeholder="Headline" autofocus required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label>Intro</label>
								<textarea name="fulldescription" id="fulldescription" class="form-control"><?=$fulldescription;?></textarea>
							</div>
						</div>
					</div>	
					<!-- /.panel-body -->	
					
					<div class="panel-heading">
						SEO Detail
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<label>English URL</label>
								<input type="text" placeholder="Enter English URL" id="friendlyurl" name="friendlyurl" value="<?=$friendlyurl;?>" class="form-control" maxlength="250" required  />
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<label>Meta Title</label>
								<input type="text" placeholder="Meta title" id="metatitle" name="metatitle" value="<?=$metatitle;?>" class="form-control" maxlength="250"  />
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<label>Meta Keyword</label>
								<input type="text" name="metakeyword" id="metakeyword" value="<?=$metakeyword;?>" maxlength="255" class="form-control" placeholder="Meta Keyword" autocomplete="off" />
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<label>Meta Description</label>
								<input type="text" name="metadescription" id="metadescription" value="<?=$metadescription;?>" maxlength="255" class="form-control" placeholder="Meta Description" autocomplete="off" />
							</div>
						</div>
					</div>	
					<!-- /.panel-body -->	
					
					<div class="panel-heading">
						More Info
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-3">
								<label>Add An Image (Image Dimensions - 743 x 448)</label>
								<input type="file" name="uploadImage" id="uploadImage" class="form-control" />
								<input type="hidden" name="uploadImage_Old" id="uploadImage_Old" value="<?=$uploadImage_Old;?>" />
								<?php
								if(!empty($uploadImage_Old)) {
								?>
									<div style="margin-top:10px;"><img src="<?=$imagepath;?>" width="200px" style="auto;" /></div>
								<?php
								}
								?>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-3">
								<label>Sort</label>
								<input type="text" placeholder="Sort" id="sort" name="sort" value="<?=$sort;?>" class="form-control" maxlength="10" required  />
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>Status</label>
								<select name="isactive" class="form-control">
									<option value="Active" <?php if ($isactive == 'Active'){ echo 'selected';}?>>Active</option>
									<option value="Inactive" <?php if ($isactive == 'Inactive'){ echo 'selected';}?>>Inactive</option>
								</select>
							</div>
						</div>
					</div>	
					<!-- /.panel-body -->	
					
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<input type="hidden" name="idgallery" id="idgallery" value="<?=$idgallery;?>" />
									<input type="submit" name="submit" value="Save Gallery" class="btn btn-primary" />
								</div>
								<!-- /.col-lg-6 (nested) -->
							</div>
						</div>
					</div>
					<!-- /.row (nested) -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</form>
</div>
<?php
include 'include/_footer_.php';
?>