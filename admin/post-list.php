<?php
include 'include/_header_.php';

$lvl = $appFunction->validate('0', $baseURL);

$todayDate = date('Y-m-d');

if(!empty($_GET['action']) == 'deleted'){
	if(!empty($_GET['id'])){
		$idpost = $appFunction->validHTML($_GET['id']);
		
		$mySQL = "";
		$mySQL = "DELETE FROM postcategory WHERE idpost = '".$appFunction->validSQL($idpost,"")."'";
		//echo $mySQL .'<br>';
		//$dbAccess->queryExec($mySQL);
		
		$mySQL = "";
		$mySQL = "DELETE FROM poststate WHERE idpost = '".$appFunction->validSQL($idpost,"")."'";
		//echo $mySQL .'<br>';
		//$dbAccess->queryExec($mySQL);
		
		$mySQL = "";
		$mySQL = "DELETE FROM post WHERE idpost = '".$appFunction->validSQL($idpost,"")."'";
		//echo $mySQL .'<br>';
		$dbAccess->queryExec($mySQL);
		header("location:".$baseURL."/admin/post-list?msg=1");
		exit;
	}		
}

$qryString='';

if(!empty($_POST['searchkeyWord'])){
	$searchkeyWord = trim($_POST['searchkeyWord']);
	$qryString .= '&searchkeyWord='.$searchkeyWord;
} else if(!empty($_GET['searchkeyWord'])){
	$searchkeyWord = trim($_GET['searchkeyWord']);	
	$qryString .= '&searchkeyWord='.$searchkeyWord;
}

if(!empty($_POST['postdate'])){
	$postdate = trim($_POST['postdate']);
	$qryString .= '&postdate='.$postdate;
} else if(!empty($_GET['postdate'])){
	$postdate = trim($_GET['postdate']);	
	$qryString .= '&postdate='.$postdate;
}

// PAGINATION
$targetpage = 'post-list?'.$qryString; //your file name
$limit = 25; //how many items to show per page
$page=0;
if(!empty($_GET['page']))
{
	$page = $_GET['page'];
}
// echo 'Page = ' . $page . '<br>';
if($page){ 
	$start = ($page - 1) * $limit; //first item to display on this page
	$sno = $start+1;
} else {
	$start = 0;
	$sno = 1;
}
/* Setup page vars for display. */
if ($page == 0) $page = 1; //if no page var is given, default to 1.	
// PAGINATION
?>

<script src="<?=$baseURL;?>/admin/datepicker/jquery.timepicker.js"></script>
<link href="<?=$baseURL;?>/admin/datepicker/jquery.timepicker.css" rel="stylesheet" />

<script>
$( function() {
	$("#postdate").datepicker({ 
		format: 'yyyy-mm-dd',
		minDate: 0, 
		maxDate: "+36M", 
		changeMonth: true,
		changeYear: true 
	});
} );
</script>

<!-- SHARE THIS -->
<script type="text/javascript">var switchTo5x = true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({ publisher: "9ee8c443-57cf-44ea-8e67-11693a461baf", doNotHash: false, doNotCopy: false, hashAddressBar: false });</script>
<!-- / SHARE THIS -->
<div id="page-wrapper">
	<form role="form" action="<?=$baseURL;?>/admin/post-list/" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-sm-10">
				<h1 class="page-header">News / Post List <?php if (!empty($_GET['msg']) == '1'){?><small class="text-danger">Selected record deleted<?php } ?></small></h1>
			</div>
			<div class="col-sm-2 text-right">
				<h1 class="page-header"><a href="<?=$baseURL;?>/admin/post-manage/" class="btn btn-info">Add Post</a></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						News / Post Search
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-3">
								<label>Keyword</label>
								<input type="text" name="searchkeyWord" id="searchkeyWord" class="form-control" />
							</div>
							<div class="col-sm-2">
								<label>News / Post Date</label>
								<input type="text" name="postdate" id="postdate" class="form-control" />
							</div>
							<div class="col-sm-1">
								<label>Action</label>
								<input type="submit" name="submit" id="submit" value="Search" class="btn btn-primary" />
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						News / Post Summary
					</div>
					<div class="panel-body">
						<?php
						$mySQL = "";
						$mySQL = "SELECT 
							`idpost`
							, `iduser`
							, (SELECT name FROM user WHERE `iduser` = post.iduser) AS postBy
							, `headline`
							, `postdate`
							, `posttime`
							, `shortdescription`
							, `fulldescription`
							, `friendlyurl`
							, `sort`
							, `posttype`
							, `image`
							, `metatitle`
							, `metakeyword`
							, `metadescription`
							, `createdby`
							, `createddate`
							, `isactive`";
						$mySQL .= " FROM";
						$mySQL1 = " post";
						$mySQL1 .= " WHERE 1=1";
						//$mySQL1 .= " AND DATE(postdate) >= '".$todayDate."'";
						
						if(!empty($searchkeyWord)){
							$spt_searchkeyWord = explode(' ', $searchkeyWord);
							$mySQLTemp = "";
							for($i=0; $i < count($spt_searchkeyWord); $i++){
								$mySQLTemp .= " (headline LIKE '%".$spt_searchkeyWord[$i]."%')";
								//$mySQLTemp .= " OR (venue_location LIKE '%".$spt_searchkeyWord[$i]."%')";
								//$mySQLTemp .= " OR (venue_address LIKE '%".$spt_searchkeyWord[$i]."%')";
								//$mySQLTemp .= " OR (venue_city LIKE '%".$spt_searchkeyWord[$i]."%')";
								//$mySQLTemp .= " OR (venue_state = '".$spt_searchkeyWord[$i]."')";
								//$mySQLTemp .= " OR (venue_zip = '".$spt_searchkeyWord[$i]."')";
								
								if ($i < count($spt_searchkeyWord)-1){
									$mySQLTemp .= " OR ";
								}
							}
							
							$mySQL1 .= " AND (" .$mySQLTemp. ")";
						}
						
						if(!empty($postdate)){
							$postdate = date("Y-m-d", strtotime($postdate));
							$mySQL1 .= " AND DATE(postdate) = '".$postdate."'";
						}
						if ($lvl == '0') { 
							$mySQL1 .= " AND iduser = '".$_SESSION['xlaANM_usr']."'";
						}
						
						$mySQL1 .= " ORDER BY DATE_FORMAT(postdate, '%Y %m %d')";
						$mySQL2 = " LIMIT " . $start . "," . $limit;
						$mySQLQry = $mySQL . $mySQL1 . $mySQL2;
						//echo $mySQLQry .'<hr>';
						//echo 'LVL = ' . $lvl .'<hr>';
						$rsTemp = $dbAccess->selectData($mySQLQry);
						if (empty($rsTemp)){
						?>
							<tr>
								<td class="text-center" colspan="5">Record Not Found</td>
							</tr>
						<?php
						} else {
						?>
							<div class="panel-group" id="accordion">
								<?php
								foreach($rsTemp AS $rsTempVal){
									$idpost = $rsTempVal['idpost'];
									$iduser = $rsTempVal['iduser'];
									$postBy = $rsTempVal['postBy'];
									$headline = $rsTempVal['headline'];
									$postdate = $rsTempVal['postdate'];
									$posttime = $rsTempVal['posttime'];
									$shortdescription = $rsTempVal['shortdescription'];
									$fulldescription = $rsTempVal['fulldescription'];
									$friendlyurl = $rsTempVal['friendlyurl'];
									$sort = $rsTempVal['sort'];
									$posttype = $rsTempVal['posttype'];
									$image = $rsTempVal['image'];
									$metatitle = $rsTempVal['metatitle'];
									$metakeyword = $rsTempVal['metakeyword'];
									$metadescription = $rsTempVal['metadescription'];
									$createdby = $rsTempVal['createdby'];
									$createddate = $rsTempVal['createddate'];
									$isactive = $rsTempVal['isactive'];
								?>
									<div class="panel panel-default">
										<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#accordion-content-<?=$sno;?>">
											<div class="row" style="cursor:pointer;">
												<div class="col-lg-9">
													<div style="text-align: left; font-weight:bold;"><?=$headline;?> 
													<br><?=$postdate; ?> - <?=$posttime; ?> 
													</div>
												</div>
												<div class="col-lg-3">
													<div style="text-align: right; padding-right: 10px;">more info</div>
												</div>
											</div>
											<div style="clear:both;"></div>
										</div>
										<div id="accordion-content-<?=$sno;?>" class="panel-collapse collapse">
											<div class="panel-body">
												<div class="section group">
													<div class="row">
														<div class="col-lg-7">
															<div style="font-size: 20px; font-weight: bold;"><?=$headline;?></div>
															<div style="font-size: 14px; font-weight: none;"><?=$postdate;?> - <?= $posttime; ?></div>
															<div style="font-size: 14px; font-weight: none;"><?=$shortdescription;?></div>
															<br><br>
															<div style="font-size: 14px; font-weight: none;">Posted by: <?=$postBy;?></div>
														</div>

														<!--COLUMN 2  -->
														<div class="col-lg-5">
															<strong>Share this event with your social networks</strong>
															<?php
															$shareUrl = $baseURL . "/post/" . $friendlyurl;
															?>
															<div>
																<div>
																	<span class='st_facebook_large' st_url="<?=$shareUrl;?>" st_title="<?=$headline;?>"></span>
																	<span class='st_twitter_large' st_url="<?=$shareUrl;?>" st_title="<?=$headline;?>"></span>
																	<span class='st_linkedin_large' st_url="<?=$shareUrl;?>" st_title="<?=$headline;?>"></span>
																	<span class='st_googleplus_large' st_url="<?=$shareUrl;?>" st_title="<?=$headline;?>"></span>
																	<span class='st_email_large' st_url="<?=$shareUrl;?>" st_title="<?=$headline;?>"></span>
																	<span class='st_whatsapp_large' st_url="<?=$shareUrl;?>" st_title="<?=$headline;?>"></span>
																	<br>
																	<h3>
																	<a href="<?=$baseURL."/post/".$friendlyurl;?>" target="new">
																	<button type="button" class="btn btn-outline btn-primary btn-circle btn-lg"><i class="fa fa-eye" title="View"></i></button></a> 
																	<a href="<?=$baseURL;?>/admin/post-manage?idpost=<?=$idpost;?>"><button type="button" class="btn btn-outline btn-success btn-circle btn-lg"><i class="fa fa-edit" title="Edit"></i></button></a> 
																	<a href="javascript:void(0);" onclick="deleteRec(<?=$rsTempVal['idpost'];?>)"><button type="button" class="btn btn-outline btn-danger btn-circle btn-lg"><i class="fa fa-trash-o" title="Delete"></i></button></a> 
																	</h3>
																</div>
																<?php
																if (!empty($contact_name)){
																?>
																	<div style="font-size: 12px; font-weight: none; line-height: 20px;">Contact: <?=$contact_name;?></div> 
																<?php
																}
																if (!empty($contact_telephone)){
																?>
																	<div style="font-size: 12px; font-weight: none; line-height: 20px;">Phone: <?=$contact_telephone;?></div><br> 
																<?php
																}
																if (!empty($contact_email)){
																?>
																	<div style="font-size: 12px; font-weight: none; line-height: 20px;"><a href="mailto:<?=$contact_email;?>">Email</a></div> 
																<?php
																}
																if (!empty($contact_web)){
																?>
																	<div style="font-size: 12px; font-weight: none; line-height: 20px;"><a href="<?=$contact_web;?>" target="new">Website</a></div> 
																<?php
																}
																?>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								<?php
									$sno++;
								}
								?>
							</div>
							<div><?php echo $dbAccess->pagination($mySQL1, $limit, $page, $targetpage);?></div>
						<?php
						}
						?>
					</div>
					<!-- /.panel-body -->
				</div>
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</form>
	<!-- /.row -->
</div>
<script>
function deleteRec(id){
	isDeleted = confirm("Are you sure delete this record.");
	if(isDeleted == true){
		top.document.location.href = "<?=$baseURL;?>/admin/post-list?action=delete&id="+id;
	} else {
		return (false);
	}
}
</script>
<?php
include 'include/_footer_.php';
?>
