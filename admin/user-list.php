<?php
include 'include/_header_.php';

$lvl = $appFunction->validate('0', $baseURLAdmin);

if(!empty($_GET['action']) == 'deleted'){
	if(!empty($_GET['id'])){
		$iduser = $appFunction->validHTML($_GET['id']);
		$mySQL = "";
		$mySQL = "DELETE FROM user WHERE usertype = 'CUSTOMER' AND iduser = '".$appFunction->validSQL($iduser,"")."'";
		//echo $mySQL .'<br>';
		$dbAccess->queryExec($mySQL);
		header("location:".$baseURLAdmin."/user-list?msg=1");
		exit;
	}		
}

$qryString='';
// PAGINATION
$targetpage = 'user-list?'.$qryString; //your file name
$limit = 25; //how many items to show per page
$page=0;
if(!empty($_GET['page']))
{
	$page = $_GET['page'];
}
// echo 'Page = ' . $page . '<br>';
if($page){ 
	$start = ($page - 1) * $limit; //first item to display on this page
	$sno = $start+1;
} else {
	$start = 0;
	$sno = 1;
}
/* Setup page vars for display. */
if ($page == 0) $page = 1; //if no page var is given, default to 1.	
// PAGINATION
?>

<div id="page-wrapper">
	<div class="row">
		<div class="col-sm-10">
			<h1 class="page-header">Customer List <?php if (!empty($_GET['msg']) == '1'){?><small class="text-danger">Selected record deleted<?php } ?></small></h1>
		</div>
		<div class="col-sm-2 text-right">
			<!-- <h1 class="page-header"><a href="<?=$baseURLAdmin;?>/user-manage/" class="btn btn-info">Add a Customer</a></h1> -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<form role="form" action="<?=$baseURLAdmin;?>/user-add/" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Customer Summary
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<?php
							$mySQL = "";
							$mySQL = "SELECT iduser, fullname, email, mobile, usertype";
							$mySQL .= ", (CASE WHEN isactive = '1' THEN 'Active' ELSE 'Inactive' END) AS isactive FROM";
							$mySQL1 = " `vgetuser` WHERE `usertype` = 'CUSTOMER' ORDER BY fullname";
							$mySQL2 = " LIMIT " . $start . "," . $limit;
							$mySQLQry = $mySQL . $mySQL1 . $mySQL2;
							// echo $mySQLQry .'<hr>';
							$rsTemp = $dbAccess->selectData($mySQLQry);
							?>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th class="text-center">#</th>
										<th>Name</th>
										<th class="text-center">Phone</th>
										<th class="text-left">Email</th>
										<th class="text-center">Status</th>
										<th class="text-center">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									if (empty($rsTemp)){
									?>
										<tr>
											<td class="text-center" colspan="6">Record Not Found</td>
										</tr>
									<?php
									} else {
										foreach($rsTemp AS $rsTempVal){
									?>
											<tr>
												<td class="text-center"><?=$sno;?></td>
												<td><?=$rsTempVal['fullname'];?></td>
												<td class="text-center"><?=$rsTempVal['mobile'];?></td>
												<td class="text-left"><?=$rsTempVal['email'];?></td>
												<td class="text-center"><?=$rsTempVal['isactive'];?></td>
												<td class="text-center">
												<a href="<?=$baseURLAdmin;?>/user-manage/?iduser=<?=$rsTempVal['iduser'];?>">
												<button type="button" class="btn btn-outline btn-success btn-circle1 btn-sm"><i class="fa fa-edit"></i></button></a>
												<a href="javascript:void(0);" onclick="deleteRec(<?=$rsTempVal['iduser'];?>)">
												<button type="button" class="btn btn-outline btn-danger btn-circle1 btn-sm"><i class="fa fa-trash-o"></i></button></a>
												</td>
											</tr>
									<?php
											$sno++;
										}
									}
									?>
								</tbody>
							</table>
							<div><?php echo $dbAccess->pagination($mySQL1, $limit, $page, $targetpage);?></div>
						</div>
						<!-- /.table-responsive -->
					</div>
					<!-- /.panel-body -->
				</div>
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</form>
	<!-- /.row -->
</div>
<script>
function deleteRec(id){
	isDeleted = confirm("Are you sure delete this record.");
	if(isDeleted == true){
		top.document.location.href = "<?=$baseURLAdmin;?>/user-list?action=delete&id="+id;
	} else {
		return (false);
	}
}
</script>
<?php
include 'include/_footer_.php';
?>
