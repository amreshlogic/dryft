<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="<?=$baseURLAdmin;?>/dashboard">Admin</a>
	</div>
	<!-- /.navbar-header -->
	<ul class="nav navbar-top-links navbar-right">
		
		<!-- /.dropdown -->
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<!--<li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a></li>
				<li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a></li>
				<li class="divider"></li>-->
				<li><a href="<?=$baseURLAdmin;?>/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
			</ul>
			<!-- /.dropdown-user -->
		</li>
		<!-- /.dropdown -->
	</ul>
	<!-- /.navbar-top-links -->

	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav in" id="side-menu">
				<li>
					<a href="<?=$baseURLAdmin;?>/dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
				</li>
				<li>
					<a href="javascript:void(0)"><i class="fa fa-shopping-cart fa-fw"></i> Order<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level collapse in" aria-expanded="true" style="">
						<li>
							<a href="<?=$baseURLAdmin;?>/all-jobs-list">All Jobs</a>
						</li>
						<li>
							<a href="<?=$baseURLAdmin;?>/new-jobs-list">New Jobs</a>
						</li>
						<li>
							<a href="<?=$baseURLAdmin;?>/driver-accepted-list">Accecpted By Driver</a>
						</li>
						<li>
							<a href="<?=$baseURLAdmin;?>/driver-on-the-way-list">Driver on The Way</a>
						</li>
						<li>
							<a href="<?=$baseURLAdmin;?>/job-started-list">Started Jobs</a>
						</li>
						<li>
							<a href="<?=$baseURLAdmin;?>/completed-job-list">Completed Jobs</a>
						</li>
						<li>
							<a href="<?=$baseURLAdmin;?>/payment-completed-job-list">Payment Done</a>
						</li>
					</ul>
					<!-- /.nav-second-level -->
				</li>
				<?php
				if ($_SESSION['xlaANM_lvl'] == '2'){
				?>
					<li>
						<a href="javascript:void(0)"><i class="fa fa-users fa-fw"></i> Driver<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse in" aria-expanded="true" style="">
							<li>
								<a href="<?=$baseURLAdmin;?>/driver-manage">Manage Driver</a>
							</li>
							<li>
								<a href="<?=$baseURLAdmin;?>/driver-list">Driver List</a>
							</li>
						</ul>
						<!-- /.nav-second-level -->
					</li>
					<li>
						<a href="javascript:void(0)"><i class="fa fa-user fa-fw"></i> Customer<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse in" aria-expanded="true" style="">
							<li>
								<a href="<?=$baseURLAdmin;?>/user-list">Customer List</a>
							</li>
						</ul>
						<!-- /.nav-second-level -->
					</li>
					<li>
						<a href="javascript:void(0)"><i class="fa fa-money fa-fw"></i> Promo Code<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse in" aria-expanded="true" style="">
							<li>
								<a href="<?=$baseURLAdmin;?>/promo-manage">Manage Promo Code</a>
							</li>
							<li>
								<a href="<?=$baseURLAdmin;?>/promo-list">Promo Code List</a>
							</li>
						</ul>
						<!-- /.nav-second-level -->
					</li>
					<li>
						<a href="javascript:void(0)"><i class="fa fa-file fa-fw"></i> Content Page<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse in" aria-expanded="true" style="">
							<li>
								<a href="<?=$baseURLAdmin;?>/content-manage">Add New Content</a>
							</li>
							<li>
								<a href="<?=$baseURLAdmin;?>/content-list">Content List</a>
							</li>
						</ul>
						<!-- /.nav-second-level -->
					</li>
				<?php
				}
				?>
			</ul>
		</div>
		<!-- /.sidebar-collapse -->
	</div>
	<!-- /.navbar-static-side -->
</nav>