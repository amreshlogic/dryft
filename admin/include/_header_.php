<?php
include '../lib/_baseURL_.php';
include '../lib/_appFunction_.php';
include 'include/_chksession_.php';
include '../lib/Connection.php';
include '../lib/DBQuery.php';
include '../lib/Helpers.php';
include '../lib/ReturnMsg.php';

$connection = new Connection();
$helper = new Helpers();
$dbAccess = new DBQuery();
$LoadMsg = new ReturnMsg();
$appFunction = new appFunction();

$errMsg = '';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrator</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?=$baseURL;?>/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?=$baseURL;?>/admin/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=$baseURL;?>/admin/css/sb-admin-2.css" rel="stylesheet">

	<!-- Morris Charts CSS -->
    <link href="<?=$baseURL;?>/admin/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?=$baseURL;?>/admin/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.0.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<script src="<?=$baseURL;?>/admin/datepicker/jquery.timepicker.js"></script>
	<link href="<?=$baseURL;?>/admin/datepicker/jquery.timepicker.css" rel="stylesheet" />

    <script src="<?=$baseURL;?>/js/_Application_.js"></script>
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
		<?php include "include/_leftmenu_.php"; ?>