<?php
include 'include/_header_.php';

error_reporting(-1);

$lvl = $appFunction->validate('0', $baseURL);

$errorMsg = '';
$idpost = '';
$idcategory = '';
$idstate = '';
$headline = '';
$postdate = '';
$posttime = '';
$shortdescription = '';
$fulldescription = '';
$posttype = '';
$isshowinslider = '';
$showalltime = '';

$friendlyurl = '';
$metatitle = '';
$metakeyword = '';
$metadescription = '';
$isactive = '';
$uploadImage_Old = '';

if (!empty($_POST['submit'])){
	$idpost = trim($appFunction->validHTML($_POST['idpost']));
	$headline = trim($appFunction->validHTML($_POST['headline']));
	$postdate = $appFunction->validHTML($_POST['postdate']);
	$posttime = $appFunction->validHTML($_POST['posttime']);
	$shortdescription = $appFunction->validHTML($_POST['shortdescription']);
	//$fulldescription = $appFunction->validHTML($_POST['fulldescription']);
	$fulldescription = ($_POST['fulldescription']);
	$posttype = $appFunction->validHTML($_POST['posttype']);
	
	if(!empty($_POST['isshowinslider'])){
		$isshowinslider = $appFunction->validHTML($_POST['isshowinslider']);
	} else {
		$isshowinslider = '';
	}
	if(!empty($_POST['showalltime'])){
		$showalltime = $appFunction->validHTML($_POST['showalltime']);
	} else {
		$showalltime = '';
	}
	$friendlyurl = $appFunction->validHTML($_POST['friendlyurl']);
	$metatitle = $appFunction->validHTML($_POST['metatitle']);
	$metakeyword = $appFunction->validHTML($_POST['metakeyword']);
	$metadescription = $appFunction->validHTML($_POST['metadescription']);
	$isactive = $appFunction->validHTML($_POST['isactive']);
	
	$uploadImage_Old = $appFunction->validHTML($_POST['uploadImage_Old']);
	
	$idcategory = $appFunction->validHTML($_POST['idcategory']);
	$idstate = $appFunction->validHTML($_POST['idstate']);
	$iduser = $appFunction->validHTML($_SESSION['xlaANM_usr']);
		
	// CHECK IMAGE EXT
	$uploadImageName = basename($_FILES['uploadImage']['name']);
	$uploadImageSize = basename($_FILES['uploadImage']['size']);
	$uploadImageExt = pathinfo($uploadImageName,PATHINFO_EXTENSION);
	
	if(empty($friendlyurl)){
		$friendlyurl = strtolower($categoryname);
		$friendlyurl = $appFunction->friendlyURL($friendlyurl);
	} else {
		$friendlyurl = strtolower($friendlyurl);
		$friendlyurl = $appFunction->friendlyURL($friendlyurl);
	}
	
	echo '<div style="padding-left:300px;">';
	//echo 'uploadImageSize = ' . $uploadImageSize .'<br>';
	//echo $uploadImageExt .'<br>';
	
	if(empty($headline)){
		$errorMsg = 'Please enter headline';
	} else if(empty($postdate)){
		$errorMsg = 'Please enter event date';
	} else if(empty($posttime)){
		$errorMsg = 'Please enter event start time';
	} else if(!empty($uploadImageName) AND strtolower($uploadImageExt) != "jpg" AND strtolower($uploadImageExt) != "jpeg"){
		$errorMsg = 'Please upload imgage in JPG & JPEG format';
	} else if(!empty($uploadImageName) AND ($uploadImageSize == 0 OR $uploadImageSize > 500000)){
		$errorMsg = 'Please upload image size less than 500KB';
	} else if(empty($idcategory)){
		$errorMsg = 'Please select any one category';
	}
	
	$fileDir = '../postimage/';
	
	if (!file_exists($fileDir)) {
		mkdir($fileDir, 0777, true);
	}
	
	//echo $fileDir;
	if(empty($errorMsg)){
		$postdate = date("Y-m-d", strtotime($postdate));
		
		if($uploadImageName != '') {
			$uploadImageName = time() . '_' . str_replace('.php', '', $uploadImageName);
			move_uploaded_file($_FILES['uploadImage']['tmp_name'], $fileDir.$uploadImageName);
		}else{
			$uploadImageName = $uploadImage_Old;
		}
		
		if(empty($idpost)){
			$mySQL = "";
			$mySQL .= "INSERT INTO post (";
			$mySQL .= " iduser";
			$mySQL .= ", headline";
			$mySQL .= ", postdate";
			$mySQL .= ", posttime";
			$mySQL .= ", shortdescription";
			$mySQL .= ", fulldescription";
			$mySQL .= ", image";
			$mySQL .= ", posttype";
			$mySQL .= ", isshowinslider";
			$mySQL .= ", showalltime";
			$mySQL .= ", metatitle";
			$mySQL .= ", metakeyword";
			$mySQL .= ", metadescription";
			$mySQL .= ", isactive";
			$mySQL .= ") VALUES (";
			$mySQL .= "'". $appFunction->validSQL($iduser,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($headline,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($postdate,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($posttime,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($shortdescription,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($fulldescription,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($uploadImageName,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($posttype,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($isshowinslider,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($showalltime,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($metatitle,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($metakeyword,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($metadescription,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($isactive,"")."'";
			$mySQL .= ")";
			//echo $mySQL .'<br>';			
			//exit;
			$dbAccess->queryExec($mySQL);
			$idpost = $dbAccess->LastId();
			
			$mySQL = "";
			$mySQL .= "UPDATE post SET";
			$mySQL .= " friendlyurl = '". $appFunction->validSQL($idpost . '-' . $friendlyurl,"") ."'";
			$mySQL .= " WHERE idpost = '".$appFunction->validSQL($idpost,"")."'";
			//echo $mySQL .'<br>';			
			//exit;
			$dbAccess->queryExec($mySQL);
		} else {
			$mySQL = "";
			$mySQL .= "UPDATE post SET";
			$mySQL .= "  headline = '". $appFunction->validSQL($headline,"")."'";
			$mySQL .= ", friendlyurl = '". $appFunction->validSQL($idpost . '-' . $friendlyurl,"") ."'";
			$mySQL .= ", postdate = '". $appFunction->validSQL($postdate,"")."'";
			$mySQL .= ", posttime = '". $appFunction->validSQL($posttime,"")."'";
			$mySQL .= ", shortdescription = '". $appFunction->validSQL($shortdescription,"")."'";
			$mySQL .= ", fulldescription = '". $appFunction->validSQL($fulldescription,"")."'";
			$mySQL .= ", image = '". $appFunction->validSQL($uploadImageName,"")."'";
			$mySQL .= ", posttype = '". $appFunction->validSQL($posttype,"")."'";
			$mySQL .= ", isshowinslider = '". $appFunction->validSQL($isshowinslider,"")."'";
			$mySQL .= ", showalltime = '". $appFunction->validSQL($showalltime,"")."'";
			$mySQL .= ", metatitle = '". $appFunction->validSQL($metatitle,"")."'";
			$mySQL .= ", metakeyword = '". $appFunction->validSQL($metakeyword,"")."'";
			$mySQL .= ", metadescription = '". $appFunction->validSQL($metadescription,"")."'";
			$mySQL .= ", isactive = '". $appFunction->validSQL($isactive,"")."'";
			$mySQL .= " WHERE idpost = '".$appFunction->validSQL($idpost,"")."'";
			//echo $mySQL .'<br>';			
			//exit;
			$dbAccess->queryExec($mySQL);
		}
		
		//echo COUNT($idcategory);
		
		if (!empty($idcategory)){
			$mySQL = "";
			$mySQL = "DELETE FROM postcategory WHERE idpost = '".$appFunction->validSQL($idpost,"")."'";
			//echo $mySQL .'<br>';
			$dbAccess->queryExec($mySQL);
			
			for ($i=0; $i<COUNT($idcategory); $i++){
				//if (!empty(trim($idcategory[$i]))){
					$mySQL = "";
					$mySQL .= "INSERT INTO postcategory(";
					$mySQL .= "idpost";
					$mySQL .= ", idcategory";
					$mySQL .= ") VALUES (";
					$mySQL .= "'".$appFunction->validSQL($idpost,"")."'";
					$mySQL .= ", '".$appFunction->validSQL(trim($idcategory[$i]),"")."'";
					$mySQL .= ")";
					//echo $mySQL .'<br>';
					$dbAccess->queryExec($mySQL);
				//} 
			} 
		}
		if (!empty($idstate)){
			$mySQL = "";
			$mySQL = "DELETE FROM poststate WHERE idpost = '".$appFunction->validSQL($idpost,"")."'";
			$dbAccess->queryExec($mySQL);
			
			for ($i=0; $i < COUNT($idstate); $i++){
				//if (!empty(trim($idstate[$i]))){
					$mySQL = "";
					$mySQL .= "INSERT INTO poststate(";
					$mySQL .= "idpost";
					$mySQL .= ", idstate";
					$mySQL .= ") VALUES (";
					$mySQL .= "'".$appFunction->validSQL($idpost,"")."'";
					$mySQL .= ", '".$appFunction->validSQL(trim($idstate[$i]),"")."'";
					$mySQL .= ")";
					//echo $mySQL .'<br>';
					$dbAccess->queryExec($mySQL);
				//}
			}
		}
		
		header("location:".$baseURL."/admin/post-list");
		exit;
	}
	echo '</div>';
}

if(!empty($_GET['idpost'])){
	$idpost = trim($appFunction->validHTML($_GET['idpost']));
	$mySQL = "";
	$mySQL = "SELECT idpost";
	$mySQL .= ", headline";
	$mySQL .= ", postdate";
	$mySQL .= ", posttime";
	$mySQL .= ", shortdescription";
	$mySQL .= ", fulldescription";
	$mySQL .= ", posttype";
	$mySQL .= ", isshowinslider";
	$mySQL .= ", showalltime";
	$mySQL .= ", friendlyurl";
	$mySQL .= ", metatitle";
	$mySQL .= ", metakeyword";
	$mySQL .= ", metadescription";
	$mySQL .= ", image";
	$mySQL .= ", CONCAT('../postimage/', image) AS imagepath";
	$mySQL .= ", isactive";
	$mySQL .= ", (SELECT GROUP_CONCAT(idcategory) FROM postcategory WHERE idpost = post.idpost) AS idcategory";
	$mySQL .= ", (SELECT GROUP_CONCAT(idstate) FROM poststate WHERE idpost = post.idpost) AS idstate";
	$mySQL .= " FROM post";
	$mySQL .= " WHERE idpost = '".$appFunction->validSQL($idpost,"")."'";
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	
	$idpost = $rsTemp['idpost'];
	$headline = $rsTemp['headline'];
	$postdate = $rsTemp['postdate'];
	$posttime = $rsTemp['posttime'];
	$shortdescription = $rsTemp['shortdescription'];
	$fulldescription = $rsTemp['fulldescription'];
	$posttype = strtoupper($rsTemp['posttype']);
	$isshowinslider = strtoupper($rsTemp['isshowinslider']);
	$showalltime = strtoupper($rsTemp['showalltime']);
	
	$friendlyurl = str_replace($idpost .'-', '', $rsTemp['friendlyurl']);
	$metatitle = $rsTemp['metatitle'];
	$metakeyword = $rsTemp['metakeyword'];
	$metadescription = $rsTemp['metadescription'];
	$uploadImage_Old = $rsTemp['image'];
	$imagepath = $rsTemp['imagepath'];
	
	$isactive = $rsTemp['isactive'];
	$idcategory = explode(',',$rsTemp['idcategory']);
	$idstate = explode(',',$rsTemp['idstate']);
}
?>
<script>
$( function() {
	$("#postdate").datepicker({ 
		format: 'yyyy-mm-dd',
		minDate: 0, 
		maxDate: "+36M", 
		changeMonth: true,
		changeYear: true 
	});

	$("#posttime").timepicker({
		//timeFormat: 'h:mm p',
		//interval: 10,
		//dropdown: true,
		//scrollbar: true
		
		timeFormat: 'h:mm p',
		interval: 1,
		minTime: '00',
		maxTime: '11:59pm',
		//defaultTime: '07',
		startTime: '00:00',
		dynamic: false,
		dropdown: true,
		scrollbar: true
	});
} );
</script>
 
<?php include 'fckeditor/fckeditor.php';?>
		
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">News / Post Information <small><?=$errorMsg;?></small></h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<form role="form" action="<?=$baseURL;?>/admin/post-manage/" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<label>Headline</label>
								<input type="text" name="headline" id="headline" value="<?=$headline;?>" maxlength="250" autocomplete="off" class="form-control" placeholder="Headline" autofocus required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<label>Date of Post</label>
								<input type="postdate" name="postdate" id="postdate" value="<?=$postdate;?>" maxlength="10" autocomplete="off" class="form-control" placeholder="Post Date" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<label>Post Time</label>
								<div><small>e.g.: 11:30 PM</small><input type="text" name="posttime" id="posttime" value="<?=$posttime;?>" maxlength="8" autocomplete="off" class="form-control" placeholder="Post Time" required /></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label>Intro</label>
								<textarea name="shortdescription" id="shortdescription" class="form-control"><?=$shortdescription;?></textarea>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label>Content</label>
								<!--<textarea name="fulldescription" id="fulldescription" rows="6" cols="60"><?//=$fulldescription;?></textarea>-->
								
								<?php
								$oFCKeditor = new FCKeditor('fulldescription');
								$oFCKeditor->BasePath = $baseURL . "/admin/fckeditor/";
								$oFCKeditor->ToolbarSet = 'MyToolbar';
								$oFCKeditor->Value = $fulldescription;
								$oFCKeditor->Create();
								$oFCKeditor->Height = 900;
								$oFCKeditor->Width = 900;
								?>
							</div>
						</div>
					</div>	
					<!-- /.panel-body -->	
					
					<div class="panel-heading">
						SEO Detail
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<label>English URL</label>
								<input type="text" placeholder="Enter English URL" id="friendlyurl" name="friendlyurl" value="<?=$friendlyurl;?>" class="form-control" maxlength="250" required  />
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<label>Meta Title</label>
								<input type="text" placeholder="Meta title" id="metatitle" name="metatitle" value="<?=$metatitle;?>" class="form-control" maxlength="250"  />
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<label>Meta Keyword</label>
								<input type="text" name="metakeyword" id="metakeyword" value="<?=$metakeyword;?>" maxlength="255" class="form-control" placeholder="Meta Keyword" autocomplete="off" />
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<label>Meta Description</label>
								<input type="text" name="metadescription" id="metadescription" value="<?=$metadescription;?>" maxlength="255" class="form-control" placeholder="Meta Description" autocomplete="off" />
							</div>
						</div>
					</div>	
					<!-- /.panel-body -->	
					
					<div class="panel-heading">
						More Info
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-3">
								<label>Add An Image (Image Dimensions - 743 x 448)</label>
								<input type="file" name="uploadImage" id="uploadImage" class="form-control" />
								<input type="hidden" name="uploadImage_Old" id="uploadImage_Old" value="<?=$uploadImage_Old;?>" />
								<?php
								if(!empty($uploadImage_Old)) {
								?>
									<div style="margin-top:10px;"><img src="<?=$imagepath;?>" width="200px" style="auto;" /></div>
								<?php
								}
								?>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>Category</label>
								<?php
								$mySQL = "";
								$mySQL = "SELECT idcategory, categoryname FROM category WHERE isActive = 'Active'"; 
								$mySQL .= " ORDER BY categoryname";
								//echo $mySQL;
								$rsTemp = $dbAccess->selectData($mySQL);
								
								?>
								<select name="idcategory[]" multiple="multiple" class="form-control" size="5" required>
									<option value="">Select</option>
									<?php
									if (!empty($rsTemp)){
										foreach($rsTemp AS $rsTempVal){
											if(in_array($rsTempVal['idcategory'], $idcategory)){
												$selected = 'SELECTED';
											} else {
												$selected = '';
											}
									?>
											<option value="<?=$rsTempVal['idcategory'];?>" <?=$selected;?>><?=$rsTempVal['categoryname'];?></option>
									<?php
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>State</label>
								<?php
								$mySQL = "";
								$mySQL = "SELECT idstate, statename FROM state WHERE isActive = 'Active'"; 
								$mySQL .= " ORDER BY statename";
								//echo $mySQL;
								$rsTemp = $dbAccess->selectData($mySQL);
								
								?>
								<select name="idstate[]" multiple="multiple" class="form-control" size="5" required>
									<option value="">Select</option>
									<?php
									if (!empty($rsTemp)){
										foreach($rsTemp AS $rsTempVal){
											if(in_array($rsTempVal['idstate'], $idstate)){
												$selected = 'SELECTED';
											} else {
												$selected = '';
											}
									?>
											<option value="<?=$rsTempVal['idstate'];?>" <?=$selected;?>><?=$rsTempVal['statename'];?></option>
									<?php
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>Post Type</label>
								<select name="posttype" class="form-control">
									<option value="">Select</option>
									<option value="BREAKING" <?php if ($posttype == 'BREAKING'){ echo 'selected';}?>>BREAKING</option>
									<option value="EXCLUSIVE" <?php if ($posttype == 'EXCLUSIVE'){ echo 'selected';}?>>EXCLUSIVE</option>
									<option value="FLASH" <?php if ($posttype == 'FLASH'){ echo 'selected';}?>>FLASH</option>
									<option value="STING OPERATION" <?php if ($posttype == 'STING OPERATION'){ echo 'selected';}?>>STING OPERATION</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>Status</label>
								<select name="isactive" class="form-control">
									<option value="Active" <?php if ($isactive == 'Active'){ echo 'selected';}?>>Active</option>
									<option value="Inactive" <?php if ($isactive == 'Inactive'){ echo 'selected';}?>>Inactive</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<label>Show in Slider?</label>
								<div class="checkbox">
									<label><input type="checkbox" name="isshowinslider" id="isshowinslider" value="CHECKED" <?=$isshowinslider;?> /> Check to Show in Slider</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<label>Show All Time?</label>
								<div class="checkbox">
									<label><input type="checkbox" name="showalltime" id="showalltime" value="CHECKED" <?=$showalltime;?> /> Check to Show All Time</label>
								</div>
							</div>
						</div>
					</div>	
					<!-- /.panel-body -->	
					
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<input type="hidden" name="idpost" id="idpost" value="<?=$idpost;?>" />
									<input type="submit" name="submit" value="Save Post" class="btn btn-primary" />
								</div>
								<!-- /.col-lg-6 (nested) -->
							</div>
						</div>
					</div>
					<!-- /.row (nested) -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</form>
</div>
<?php
include 'include/_footer_.php';
?>