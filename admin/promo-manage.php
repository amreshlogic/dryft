<?php
include 'include/_header_.php';

error_reporting(-1);

$lvl = $appFunction->validate('0', $baseURL);

$errorMsg = '';
$idpromo = '';
$promocode = '';
$promovalue = '';
$promotype = '';
$starttime = '';
$endtime = '';
$isactive = '';

if (!empty($_POST['submit'])){
	$idpromo = trim($appFunction->validHTML($_POST['idpromo']));
	$promocode = trim($appFunction->validHTML($_POST['promocode']));
	$promovalue = trim($appFunction->validHTML($_POST['promovalue']));
	$promotype = $appFunction->validHTML($_POST['promotype']);
	$starttime = $appFunction->validHTML($_POST['starttime']);
	$endtime = $appFunction->validHTML($_POST['endtime']);
	$isactive = $_POST['isactive'];
	
	echo '<div style="padding-left:300px;">';
	
	if(empty($promocode)){
		$errorMsg = 'Please enter promocode';
	} else if(empty($promovalue)){
		$errorMsg = 'Please enter promo value';
	} else if(empty($promotype)){
		$errorMsg = 'Please select promo type';
	} else if(empty($starttime)){
		$errorMsg = 'Please enter start date';
	} else if(empty($endtime)){
		$errorMsg = 'Please enter end date';
	}
	
	if(empty($errorMsg)){
		
		if(empty($idpromo)){
			$mySQL = "";
			$mySQL .= "INSERT INTO promos (";
			$mySQL .= "  promocode";
			$mySQL .= ", promovalue";
			$mySQL .= ", promotype";
			$mySQL .= ", starttime";
			$mySQL .= ", endtime";
			$mySQL .= ", isactive";
			$mySQL .= ") VALUES (";
			$mySQL .= " '". $appFunction->validSQL($promocode,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($promovalue,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($promotype,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($starttime,"")."'";
			$mySQL .= ",'". $appFunction->validSQL($endtime,"")."'";
			$mySQL .= ",'". $isactive."'";
			$mySQL .= ")";
			// echo $mySQL .'<br>';			
			//exit;
			$dbAccess->queryExec($mySQL);
		} else {
			$mySQL = "";
			$mySQL .= "UPDATE promos SET";
			$mySQL .= "  promocode = '". $appFunction->validSQL($promocode,"")."'";
			$mySQL .= ", promovalue = '". $appFunction->validSQL($promovalue,"") ."'";
			$mySQL .= ", promotype = '". $appFunction->validSQL($promotype,"")."'";
			$mySQL .= ", starttime = '". $appFunction->validSQL($starttime,"")."'";
			$mySQL .= ", endtime = '". $appFunction->validSQL($endtime,"")."'";
			$mySQL .= ", isactive = '". $isactive ."'";
			$mySQL .= " WHERE idpromo = '".$appFunction->validSQL($idpromo,"")."'";
			// echo $mySQL .'<br>';			
			//exit;
			$dbAccess->queryExec($mySQL);
		}
		
		header("location:".$baseURLAdmin."/promo-list");
		exit;
	}
	echo '</div>';
}

if(!empty($_GET['idpromo'])){
	$idpromo = trim($appFunction->validHTML($_GET['idpromo']));
	$mySQL = "";
	$mySQL = "SELECT idpromo";
	$mySQL .= ", promocode";
	$mySQL .= ", promovalue";
	$mySQL .= ", promotype";
	$mySQL .= ", starttime";
	$mySQL .= ", endtime";
	$mySQL .= ", isactive";
	$mySQL .= " FROM promos";
	$mySQL .= " WHERE idpromo = '".$appFunction->validSQL($idpromo,"")."'";
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	
	$idpromo = $rsTemp['idpromo'];
	$promocode = $rsTemp['promocode'];
	$promovalue = $rsTemp['promovalue'];
	$promotype = $rsTemp['promotype'];
	$starttime = $rsTemp['starttime'];
	$endtime = $rsTemp['endtime'];
	$isactive = $rsTemp['isactive'];
}
?>
<script>
$( function() {
	$("#starttime1").datepicker({ 
		dateFormat: 'yy-mm-dd',
		minDate: 0, 
		maxDate: "+36M", 
		changeMonth: true,
		changeYear: true 
	});

} );
</script>
<script>
$( function() {
    var dateFormat = "yy-mm-dd",
		from = $( "#starttime" )
			.datepicker({
				dateFormat: 'yy-mm-dd',
				minDate: 0, 
				maxDate: "+36M",
				changeMonth: true,
				numberOfMonths: 1
			})
			.on( "change", function() {
				to.datepicker( "option", "minDate", getDate( this ) );
			}),
		to = $( "#endtime" ).datepicker({
			dateFormat: 'yy-mm-dd',
			minDate: 0, 
			maxDate: "+36M",
			changeMonth: true,
			numberOfMonths: 1
		})
		.on( "change", function() {
			from.datepicker( "option", "maxDate", getDate( this ) );
		});
 
		function getDate( element ) {
			var date;
			try {
				date = $.datepicker.parseDate( dateFormat, element.value );
			} catch( error ) {
				date = null;
			}
 			return date;
		}
  } );
</script> 
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Promo <small><?=$errorMsg;?></small></h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<form role="form" action="<?=$baseURLAdmin;?>/promo-manage/" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<label>Promo Code</label>
								<input type="text" name="promocode" id="promocode" value="<?=$promocode;?>" maxlength="250" autocomplete="off" class="form-control" placeholder="Promo Code" autofocus required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>Promo Value</label>
								<input type="text" name="promovalue" id="promovalue" value="<?=$promovalue;?>" maxlength="5" autocomplete="off" class="form-control" placeholder="Promo Value" autofocus required />
							</div>
							<div class="col-md-3">
								<label>Promo Type</label>
								<select name="promotype" class="form-control">
									<option value="USD">Amount</option>
									<option value="Percent">Percentage</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>Start Date</label>
								<input type="starttime" name="starttime" id="starttime" value="<?=$starttime;?>" maxlength="10" autocomplete="off" class="form-control" placeholder="Start Date" required />
							</div>
							<div class="col-md-3">
								<label>End Date</label>
								<input type="endtime" name="endtime" id="endtime" value="<?=$endtime;?>" maxlength="10" autocomplete="off" class="form-control" placeholder="End Date" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label>Status</label>
								<select name="isactive" class="form-control">
									<option value="1" <?php if ($isactive == '1'){ echo 'selected';}?>>Active</option>
									<option value="0" <?php if ($isactive == '0'){ echo 'selected';}?>>Inactive</option>
								</select>
							</div>
						</div>
					</div>	
					<!-- /.panel-body -->	
					
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<input type="hidden" name="idpromo" id="idpromo" value="<?=$idpromo;?>" />
									<input type="submit" name="submit" value="Save Post" class="btn btn-primary" />
								</div>
								<!-- /.col-lg-6 (nested) -->
							</div>
						</div>
					</div>
					<!-- /.row (nested) -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</form>
</div>
<?php
include 'include/_footer_.php';
?>