<?php
include 'include/_header_.php';

$lvl = $appFunction->validate('0', $baseURLAdmin);

if(!empty($_GET['action']) == 'deleted'){
	if(!empty($_GET['id'])){
		$iduser = $appFunction->validHTML($_GET['id']);
		$mySQL = "";
		$mySQL = "DELETE FROM user WHERE usertype = 'DRIVER' AND iduser = '".$appFunction->validSQL($iduser,"")."'";
		//echo $mySQL .'<br>';
		$dbAccess->queryExec($mySQL);
		header("location:".$baseURLAdmin."/driver-list?msg=2");
		exit;
	}		
}

$qryString='';
// PAGINATION
$targetpage = 'driver-list?'.$qryString; //your file name
$limit = 25; //how many items to show per page
$page=0;
if(!empty($_GET['page']))
{
	$page = $_GET['page'];
}
// echo 'Page = ' . $page . '<br>';
if($page){ 
	$start = ($page - 1) * $limit; //first item to display on this page
	$sno = $start+1;
} else {
	$start = 0;
	$sno = 1;
}
/* Setup page vars for display. */
if ($page == 0) $page = 1; //if no page var is given, default to 1.	
// PAGINATION
?>

<div id="page-wrapper">
	<div class="row">
		<div class="col-sm-10">
			<h1 class="page-header">Driver List 
				<small class="text-danger">
				<?php 
				if (!empty($_GET['msg'])  AND $_GET['msg'] == '1') { 
					echo 'Driver detail saved';
				} else if (!empty($_GET['msg']) AND $_GET['msg'] == '2') { 
					echo 'Selected record deleted';
				}
				?>
				</small>
			</h1>
		</div>
		<div class="col-sm-2 text-right">
			<h1 class="page-header"><a href="<?=$baseURLAdmin;?>/driver-manage" class="btn btn-info">Add a Driver</a></h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<form role="form" action="<?=$baseURLAdmin;?>/driver-manage/" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Driver Summary
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<?php
							$mySQL = "";
							$mySQL = "SELECT iduser, fullname, email, mobile, usertype";
							$mySQL .= ", (CASE WHEN isactive = '1' THEN 'Active' ELSE 'Inactive' END) AS isactive FROM";
							$mySQL1 = " `vgetuser` WHERE `usertype` = 'DRIVER' ORDER BY fullname";
							$mySQL2 = " LIMIT " . $start . "," . $limit;
							$mySQLQry = $mySQL . $mySQL1 . $mySQL2;
							// echo $mySQLQry .'<hr>';
							$rsTemp = $dbAccess->selectData($mySQLQry);
							?>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th class="text-center">#</th>
										<th>Name</th>
										<th class="text-center">Phone</th>
										<th class="text-left">Email</th>
										<th class="text-center">Status</th>
										<th class="text-center">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									if (empty($rsTemp)){
									?>
										<tr>
											<td class="text-center" colspan="6">Record Not Found</td>
										</tr>
									<?php
									} else {
										foreach($rsTemp AS $rsTempVal){
									?>
											<tr>
												<td class="text-center"><?=$sno;?></td>
												<td><?=$rsTempVal['fullname'];?></td>
												<td class="text-center"><?=$rsTempVal['mobile'];?></td>
												<td class="text-left"><?=$rsTempVal['email'];?></td>
												<td class="text-center">
													<select namw="isactive" class="form-control" onchange="setStatus('<?=$rsTempVal['iduser'];?>', this.value)">
														<option value="1" <?php if ($rsTempVal['isactive'] == 'Active') { echo 'Selected';} ?>>Active</option>
														<option value="0" <?php if ($rsTempVal['isactive'] == 'Inactive') { echo 'Selected';} ?>>Inactive</option>
													</select>
												</td>
												<td class="text-center">
												<a href="<?=$baseURLAdmin;?>/driver-manage/?iduser=<?=$rsTempVal['iduser'];?>">
												<button type="button" class="btn btn-outline btn-success btn-circle1 btn-sm"><i class="fa fa-edit"></i></button></a>
												<a href="javascript:void(0);" onclick="deleteRec(<?=$rsTempVal['iduser'];?>)">
												<button type="button" class="btn btn-outline btn-danger btn-circle1 btn-sm"><i class="fa fa-trash-o"></i></button></a>
												</td>
											</tr>
									<?php
											$sno++;
										}
									}
									?>
								</tbody>
							</table>
							<div><?php echo $dbAccess->pagination($mySQL1, $limit, $page, $targetpage);?></div>
						</div>
						<!-- /.table-responsive -->
					</div>
					<!-- /.panel-body -->
				</div>
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</form>
	<!-- /.row -->
</div>
<div id="msg" class="msg"></div>

<script src="<?=$baseURLAdmin;?>/jquery/jquery.min.js"></script>
<script>
function setStatus(id, isactive){
	var postData = {
		id: id,
		isactive: isactive,
	};
	var targetURL = "<?=$baseURLAdmin;?>/ajax-set-status.php";
	// alert(targetURL);
	// alert(id);
	
	$.ajax({
		type: "POST",
		url: targetURL,
		async: false,
		data: JSON.stringify(postData),
		success: function(data){
			// alert(data);
			if(data == '1'){
				alert('Status saved successfully');
				// msg = 'Status saved successfully';
				// $('#msg').show().slideDown(5000).html(msg);
				// setTimeout(function(){ $('#msg').hide(); }, 5000);
			}
		},
		complete: function() {},
		error: function(xhr, textStatus, errorThrown) {
			console.log('ajax loading error...');
			return false;
		}
	});
}
function deleteRec(id){
	isDeleted = confirm("Are you sure delete this record.");
	if(isDeleted == true){
		top.document.location.href = "<?=$baseURLAdmin;?>/driver-list?action=delete&id="+id;
	} else {
		return (false);
	}
}
</script>
<?php
include 'include/_footer_.php';
?>
