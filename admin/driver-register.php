<?php
include 'include/_header_.php';

$lvl = $appFunction->validate('0', $baseURLAdmin);

$errorMsg = array();

$iduser = '';
$fullname = '';
$profileimage = '';
$profileimage_OLD = '';
$address = '';
$city = '';
$state = '';
$zipcode = '';
$phone = '';
$email = '';

//SELECT `idusertruck`, `iduser`, `idplowtype`, `plowtype`, `truckname`, `vehiclenumber`, `registrationcertificateimage`, `insuranceimage`, `truckimage`, `plowsize`, `rubberblade`, `snowblower`, `saltspreader`, `bobcat`, `shovellers`, `misc`, `isactive`, `createdon` FROM `usertruck` WHERE 1
$experience = '';
$areaofwork = '';
$drivinglicense = '';
$drivinglicenseimage = '';
$drivinglicenseimage_OLD = '';
$driverinsuranceno = '';
$driverinsurancenoimage = '';
$driverinsurancenoimage_OLD = '';
$ssnumber = '';
$ssnumberimage = '';
$ssnumberimage_OLD = '';

// TRUCK DETAIL
$truckname = '';
$vehiclenumber = '';
$registrationcertificateimage = '';
$registrationcertificateimage_OLD = '';
$truckinsuranceno = '';
$truckinsuranceimage = '';
$truckinsuranceimage_OLD = '';
$truckimage = '';
$truckimage_OLD = '';

$idplowtype = '';
$idplowsize = '';

$saltspreader = '';
$rubberblade = '';
$bobcat = '';
$snowblower = '';
$shovellers = '';
$driveragreement = '';

if (!empty($_POST['submit'])){
	// echo '<div style="padding:2% 20%;">';
	
	$usertype = 'DRIVER';
	$password = randomPassword();
	
	$iduser = trim($appFunction->validHTML($_POST['iduser']));
	$fullname = trim($appFunction->validHTML($_POST['fullname']));
	$address = trim($appFunction->validHTML($_POST['address']));
	$city = trim($appFunction->validHTML($_POST['city']));
	$state = trim($appFunction->validHTML($_POST['state']));
	$zipcode = trim($appFunction->validHTML($_POST['zipcode']));
	$phone = trim($appFunction->validHTML($_POST['phone']));
	$email = trim($appFunction->validHTML($_POST['email']));
	
	$experience = trim($appFunction->validHTML($_POST['experience']));
	$areaofwork = trim($appFunction->validHTML($_POST['areaofwork']));
	$drivinglicense = trim($appFunction->validHTML($_POST['drivinglicense']));
	$driverinsuranceno = trim($appFunction->validHTML($_POST['driverinsuranceno']));
	$ssnumber = trim($appFunction->validHTML($_POST['ssnumber']));
	
	$truckname = trim($appFunction->validHTML($_POST['truckname']));
	$vehiclenumber = trim($appFunction->validHTML($_POST['vehiclenumber']));
	$truckinsuranceno = trim($appFunction->validHTML($_POST['truckinsuranceno']));
	
	$idplowtype = trim($appFunction->validHTML($_POST['idplowtype']));
	$idplowsize = trim($appFunction->validHTML($_POST['idplowsize']));
	$isactive = trim($appFunction->validHTML($_POST['isactive']));
	
	if(!empty($_POST['saltspreader'])){
		$saltspreader = $appFunction->validHTML($_POST['saltspreader']);
	}
	if(!empty($_POST['rubberblade'])){
		$rubberblade = $appFunction->validHTML($_POST['rubberblade']);
	}
	if(!empty($_POST['bobcat'])){
		$bobcat = $appFunction->validHTML($_POST['bobcat']);
	}
	if(!empty($_POST['snowblower'])){
		$snowblower = $appFunction->validHTML($_POST['snowblower']);
	}
	if(!empty($_POST['shovellers'])){
		$shovellers = $appFunction->validHTML($_POST['shovellers']);
	}
	
	/* if(!empty($_POST['driveragreement'])){
		$driveragreement = $_POST['driveragreement'];
	} */
	
	// CHECK IMAGE EXT
	$uploadImagePIName = basename($_FILES['profileimage']['name']);
	$uploadImagePISize = basename($_FILES['profileimage']['size']);
	$uploadImagePIExt = pathinfo($uploadImagePIName,PATHINFO_EXTENSION);
	
	$uploadImageDLName = basename($_FILES['drivinglicenseimage']['name']);
	$uploadImageDLSize = basename($_FILES['drivinglicenseimage']['size']);
	$uploadImageDLExt = pathinfo($uploadImageDLName,PATHINFO_EXTENSION);
	
	$uploadImageDIName = basename($_FILES['driverinsurancenoimage']['name']);
	$uploadImageDISize = basename($_FILES['driverinsurancenoimage']['size']);
	$uploadImageDIExt = pathinfo($uploadImageDIName,PATHINFO_EXTENSION);
	
	$uploadImageSSNName = basename($_FILES['ssnumberimage']['name']);
	$uploadImageSSNSize = basename($_FILES['ssnumberimage']['size']);
	$uploadImageSSNExt = pathinfo($uploadImageSSNName,PATHINFO_EXTENSION);
	
	$uploadImageTIName = basename($_FILES['truckimage']['name']);
	$uploadImageTISize = basename($_FILES['truckimage']['size']);
	$uploadImageTIExt = pathinfo($uploadImageTIName,PATHINFO_EXTENSION);
	
	$uploadImageRCName = basename($_FILES['registrationcertificateimage']['name']);
	$uploadImageRCSize = basename($_FILES['registrationcertificateimage']['size']);
	$uploadImageRCExt = pathinfo($uploadImageRCName,PATHINFO_EXTENSION);
	
	$uploadImageTINName = basename($_FILES['truckinsuranceimage']['name']);
	$uploadImageTINSize = basename($_FILES['truckinsuranceimage']['size']);
	$uploadImageTINExt = pathinfo($uploadImageTINName,PATHINFO_EXTENSION);
	
	$profileimage_OLD = trim($appFunction->validHTML($_POST['profileimage_OLD']));
	$drivinglicenseimage_OLD = trim($appFunction->validHTML($_POST['drivinglicenseimage_OLD']));
	$driverinsurancenoimage_OLD = trim($appFunction->validHTML($_POST['driverinsurancenoimage_OLD']));
	$ssnumberimage_OLD = trim($appFunction->validHTML($_POST['ssnumberimage_OLD']));
	$registrationcertificateimage_OLD = trim($appFunction->validHTML($_POST['registrationcertificateimage_OLD']));
	$truckinsuranceimage_OLD = trim($appFunction->validHTML($_POST['truckinsuranceimage_OLD']));
	$truckimage_OLD = trim($appFunction->validHTML($_POST['truckimage_OLD']));

	$friendlyURL = strtolower($fullname);
	$friendlyURL = $appFunction->friendlyURL($friendlyURL);
	
	// echo 'profileimage_OLD = ' . $profileimage_OLD .'<br>';
	// echo 'drivinglicenseimage_OLD = ' . $drivinglicenseimage_OLD .'<br>';
	// echo 'driverinsurancenoimage_OLD = ' . $driverinsurancenoimage_OLD .'<br>';
	// echo 'ssnumberimage_OLD = ' . $ssnumberimage_OLD .'<br>';
	// echo 'registrationcertificateimage_OLD = ' . $registrationcertificateimage_OLD .'<br>';
	// echo 'truckinsuranceimage_OLD = ' . $truckinsuranceimage_OLD .'<br>';
	// echo 'truckimage_OLD = ' . $truckimage_OLD .'<br>';
	// echo $uploadImageExt .'<br>';
	
	if(empty($fullname)){
		$errorMsg[] = 'FULLNAME EMPTY';
	}
	if(empty($profileimage_OLD) AND empty($uploadImagePIName)){
		$errorMsg[] = 'PROFILE IMAGE EMPTY';
	}
	if(empty($address)){
		$errorMsg[] = 'ADDRESS EMPTY';
	}
	if(empty($city)){
		$errorMsg[] = 'CITY EMPTY';
	}
	if(empty($state)){
		$errorMsg[] = 'STATE EMPTY';
	}
	if(empty($zipcode)){
		$errorMsg[] = 'ZIPCODE EMPTY';
	}
	if(empty($phone)){
		$errorMsg[] = 'PHONE EMPTY';
	}
	if(empty($email)){
		$errorMsg[] = 'EMAIL EMPTY';
	}
	if(empty($experience)){
		$errorMsg[] = 'EXPERIENCE EMPTY';
	}
	if(empty($areaofwork)){
		$errorMsg[] = 'AREA OF WORK EMPTY';
	}
	if(empty($drivinglicense)){
		$errorMsg[] = 'DL EMPTY';
	}
	if(empty($driverinsuranceno)){
		$errorMsg[] = 'DRIVER INSURANCE EMPTY';
	}
	if(empty($ssnumber)){
		$errorMsg[] = 'SSN EMPTY';
	}
	if(empty($truckname)){
		$errorMsg[] = 'TRUCK NAME EMPTY';
	}
	if(empty($vehiclenumber)){
		$errorMsg[] = 'VEHICLE NO EMPTY';
	}
	if(empty($truckinsuranceno)){
		$errorMsg[] = 'TRUCK INSURANCE EMPTY';
	}
	if(empty($idplowtype)){
		$errorMsg[] = 'PLOW TYPE EMPTY';
	}
	if(empty($idplowsize)){
		$errorMsg[] = 'PLOW SIZE EMPTY';
	}
	/* if(empty($driveragreement)){
		$errorMsg[] = 'ACCEPT AGREEMENT';
	} */
	if(empty($saltspreader) AND empty($rubberblade) AND empty($bobcat) AND empty($snowblower) AND empty($shovellers)){
		$errorMsg[] = 'ANY ONE SELECT';
	}
	if(!empty($uploadImagePIName) AND strtolower($uploadImagePIExt) != "jpg" AND strtolower($uploadImagePIExt) != "jpeg" AND strtolower($uploadImagePIExt) != "png"){
		//$errorMsg = 'Please upload driving lincense imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'PROFILE IMAGE FORMAT';
	} else if(!empty($uploadImagePIName) AND ($uploadImagePISize == 0 OR $uploadImagePISize > 10000000)){
		//$errorMsg = 'Please upload driving lincense image size less than 500KB';
		$errorMsg[] = 'PROFILE IMAGE SIZE';
	}
	if(empty($drivinglicenseimage_OLD) AND empty($uploadImageDLName)){
		$errorMsg[] = 'DL IMAGE EMPTY';
	}
	if(!empty($uploadImageDLName) AND strtolower($uploadImageDLExt) != "jpg" AND strtolower($uploadImageDLExt) != "jpeg" AND strtolower($uploadImageDLExt) != "png"){
		//$errorMsg = 'Please upload driving lincense imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'DL IMAGE FORMAT';
	} else if(!empty($uploadImageDLName) AND ($uploadImageDLSize == 0 OR $uploadImageDLSize > 10000000)){
		//$errorMsg = 'Please upload driving lincense image size less than 500KB';
		$errorMsg[] = 'DL IMAGE SIZE';
	} 
	if(empty($driverinsurancenoimage_OLD) AND empty($uploadImageDIName)){
		$errorMsg[] = 'DI IMAGE EMPTY';
	}
	if(!empty($uploadImageDIName) AND strtolower($uploadImageDIExt) != "jpg" AND strtolower($uploadImageDIExt) != "jpeg" AND strtolower($uploadImageDIExt) != "png"){
		//$errorMsg = 'Please upload driver insurance imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'DI IMAGE FORMAT';
	} else if(!empty($uploadImageDIName) AND ($uploadImageDISize == 0 OR $uploadImageDISize > 10000000)){
		//$errorMsg = 'Please upload driver insurance image size less than 500KB';
		$errorMsg[] = 'DI IMAGE SIZE';
	} 
	if(empty($ssnumberimage_OLD) AND empty($uploadImageSSNName)){
		$errorMsg[] = 'SSN IMAGE EMPTY';
	}
	if(!empty($uploadImageSSNName) AND strtolower($uploadImageSSNExt) != "jpg" AND strtolower($uploadImageSSNExt) != "jpeg" AND strtolower($uploadImageSSNExt) != "png"){
		//$errorMsg = 'Please upload imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'SSN IMAGE FORMAT';
	} else if(!empty($uploadImageSSNName) AND ($uploadImageSSNSize == 0 OR $uploadImageSSNSize > 10000000)){
		//$errorMsg = 'Please upload image size less than 500KB';
		$errorMsg[] = 'SSN IMAGE SIZE';
	}
	if(empty($truckimage_OLD) AND empty($uploadImageTIName)){
		$errorMsg[] = 'TRUCK IMAGE EMPTY';
	}
	if(!empty($uploadImageTIName) AND strtolower($uploadImageTIExt) != "jpg" AND strtolower($uploadImageTIExt) != "jpeg" AND strtolower($uploadImageTIExt) != "png"){
		//$errorMsg = 'Please upload imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'TRUCK IMAGE FORMAT';
	} else if(!empty($uploadImageTIName) AND ($uploadImageTISize == 0 OR $uploadImageTISize > 10000000)){
		//$errorMsg = 'Please upload image size less than 500KB';
		$errorMsg[] = 'TRUCK IMAGE SIZE';
	}
	if(empty($registrationcertificateimage_OLD) AND empty($uploadImageRCName)){
		$errorMsg[] = 'REGISTRATION IMAGE EMPTY';
	}
	if(!empty($uploadImageRCName) AND strtolower($uploadImageRCExt) != "jpg" AND strtolower($uploadImageRCExt) != "jpeg" AND strtolower($uploadImageRCExt) != "png"){
		//$errorMsg = 'Please upload imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'REGISTRATION IMAGE FORMAT';
	} else if(!empty($uploadImageRCName) AND ($uploadImageRCSize == 0 OR $uploadImageRCSize > 10000000)){
		//$errorMsg = 'Please upload image size less than 500KB';
		$errorMsg[] = 'REGISTRATION IMAGE SIZE';
	}
	if(empty($truckinsuranceimage_OLD) AND empty($uploadImageTINName)){
		$errorMsg[] = 'TRUCK INSURANCE IMAGE EMPTY';
	}
	if(!empty($uploadImageTINName) AND strtolower($uploadImageTINExt) != "jpg" AND strtolower($uploadImageTINExt) != "jpeg" AND strtolower($uploadImageTINExt) != "png"){
		//$errorMsg = 'Please upload imgage in JPG, JPEG & PNG format';
		$errorMsg[] = 'TRUCK INSURANCE IMAGE FORMAT';
	} else if(!empty($uploadImageTINName) AND ($uploadImageTINSize == 0 OR $uploadImageTINSize > 10000000)){
		//$errorMsg = 'Please upload image size less than 500KB';
		$errorMsg[] = 'TRUCK INSURANCE IMAGE SIZE';
	}
	//var_dump($errorMsg);
	
	$mySQL = "";
	$mySQL = "SELECT `email`, `mobile` FROM `user` WHERE `usertype` = 'DRIVER' AND (`email` = '".$appFunction->validSQL($email,"")."' OR `mobile` = '".$appFunction->validSQL($phone,"")."')";
	if (!empty($iduser)){
		$mySQL .= " AND `iduser` <> '". $appFunction->validSQL($iduser,"")."'";
	}
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	if(!empty($rsTemp)){
		if($rsTemp['email'] == $email AND $rsTemp['mobile'] == $phone){
			$errorMsg[] = 'DRIVER EMAIL & PHONE EXIST';
		} else if($rsTemp['email'] == $email){
			$errorMsg[] = 'DRIVER EMAIL EXIST';
		} else if($rsTemp['mobile'] == $phone){
			$errorMsg[] = 'DRIVER PHONE EXIST';
		}
	}

	// echo COUNT($errorMsg) .'<br>';
	// echo '<pre>';
	// var_dump($errorMsg);
	// echo '<br>';
	// exit;

	$fileDir = '../driverimage/';
	if(empty($errorMsg)){
		// PROFILE IMAGE
		if(!empty($uploadImagePIName)) {
			$uploadImagePIName = time() . '_' . str_replace('.php', '', $uploadImagePIName);
			move_uploaded_file($_FILES['profileimage']['tmp_name'], $fileDir.$uploadImagePIName);
			//copy($_FILES['profileimage']['tmp_name'], $fileDir.$uploadImagePIName);
		}else{
			$uploadImagePIName = $profileimage_OLD;
		}
		
		// DL IMAGE
		if(!empty($uploadImageDLName)) {
			$uploadImageDLName = time() . '_' . str_replace('.php', '', $uploadImageDLName);
			move_uploaded_file($_FILES['drivinglicenseimage']['tmp_name'], $fileDir.$uploadImageDLName);
			//copy($_FILES['drivinglicenseimage']['tmp_name'], $fileDir.$uploadImageDLName);
		}else{
			$uploadImageDLName = $drivinglicenseimage_OLD;
		}
		
		// DI IMAGE
		if(!empty($uploadImageDIName)) {
			$uploadImageDIName = time() . '_' . str_replace('.php', '', $uploadImageDIName);
			move_uploaded_file($_FILES['driverinsurancenoimage']['tmp_name'], $fileDir.$uploadImageDIName);
			//copy($_FILES['driverinsurancenoimage']['tmp_name'], $fileDir.$uploadImageDIName);
		}else{
			$uploadImageDIName = $driverinsurancenoimage_OLD;
		}
		
		// DI IMAGE
		if(!empty($uploadImageSSNName)) {
			$uploadImageSSNName = time() . '_' . str_replace('.php', '', $uploadImageSSNName);
			move_uploaded_file($_FILES['ssnumberimage']['tmp_name'], $fileDir.$uploadImageSSNName);
			//copy($_FILES['ssnumberimage']['tmp_name'], $fileDir.$uploadImageSSNName);
		}else{
			$uploadImageSSNName = $ssnumberimage_OLD;
		}
		
		// RC IMAGE
		if(!empty($uploadImageRCName)) {
			$uploadImageRCName = time() . '_' . str_replace('.php', '', $uploadImageRCName);
			move_uploaded_file($_FILES['registrationcertificateimage']['tmp_name'], $fileDir.$uploadImageRCName);
			//copy($_FILES['registrationcertificateimage']['tmp_name'], $fileDir.$uploadImageRCName);
		}else{
			$uploadImageRCName = $registrationcertificateimage_OLD;
		}
		
		// TI IMAGE
		if(!empty($uploadImageTINName)) {
			$uploadImageTINName = time() . '_' . str_replace('.php', '', $uploadImageTINName);
			move_uploaded_file($_FILES['truckinsuranceimage']['tmp_name'], $fileDir.$uploadImageTINName);
			//copy($_FILES['truckinsuranceimage']['tmp_name'], $fileDir.$uploadImageTINName);
		}else{
			$uploadImageTINName = $truckinsuranceimage_OLD;
		}
		
		// TRUCK IMAGE
		if(!empty($uploadImageTIName)) {
			$uploadImageTIName = time() . '_' . str_replace('.php', '', $uploadImageTIName);
			move_uploaded_file($_FILES['truckimage']['tmp_name'], $fileDir.$uploadImageTIName);
			//copy($_FILES['truckimage']['tmp_name'], $fileDir.$uploadImageTIName);
		}else{
			$uploadImageTIName = $truckimage_OLD;
		}
		
		if(empty($iduser)){
			$mySQL = "";
			$mySQL .= "INSERT INTO `user` SET";
			$mySQL .= "  fullname = '". $appFunction->validSQL($fullname,"")."'";
			$mySQL .= ", usertype = '". $appFunction->validSQL($usertype,"")."'";
			$mySQL .= ", password = '". $appFunction->validSQL($password,"")."'";
			$mySQL .= ", address = '". $appFunction->validSQL($address,"")."'";
			$mySQL .= ", city = '". $appFunction->validSQL($city,"")."'";
			$mySQL .= ", state = '". $appFunction->validSQL($state,"")."'";
			$mySQL .= ", zipcode = '". $appFunction->validSQL($zipcode,"")."'";
			$mySQL .= ", mobile = '". $appFunction->validSQL($phone,"")."'";
			$mySQL .= ", email = '". $appFunction->validSQL($email,"")."'";
			$mySQL .= ", experience = '". $appFunction->validSQL($experience,"")."'";
			$mySQL .= ", areaofwork = '". $appFunction->validSQL($areaofwork,"")."'";
			$mySQL .= ", drivinglicense = '". $appFunction->validSQL($drivinglicense,"")."'";
			$mySQL .= ", drivinglicenseimage = '". $appFunction->validSQL($uploadImageDLName,"")."'";
			$mySQL .= ", driverinsuranceno = '". $appFunction->validSQL($driverinsuranceno,"")."'";
			$mySQL .= ", driverinsurancenoimage = '". $appFunction->validSQL($uploadImageDIName,"")."'";
			$mySQL .= ", ssnumber = '". $appFunction->validSQL($ssnumber,"")."'";
			$mySQL .= ", ssnumberimage = '". $appFunction->validSQL($uploadImageSSNName,"")."'";
			$mySQL .= ", profileimage = '". $appFunction->validSQL($uploadImagePIName,"")."'";
			$mySQL .= ", profileprivacy = '1'";
			$mySQL .= ", isactive = '". $isactive ."'";
			// echo $mySQL .'<br>';			
			//exit;
			$dbAccess->queryExec($mySQL);
			$iduser = $dbAccess->LastId();
		} else {
			$mySQL = "";
			$mySQL .= "UPDATE `user` SET";
			$mySQL .= "  fullname = '". $appFunction->validSQL($fullname,"")."'";
			$mySQL .= ", usertype = '". $appFunction->validSQL($usertype,"")."'";
			$mySQL .= ", password = '". $appFunction->validSQL(SHA1($password),"")."'";
			$mySQL .= ", securitycode = '". $appFunction->validSQL($password,"")."'";
			$mySQL .= ", address = '". $appFunction->validSQL($address,"")."'";
			$mySQL .= ", city = '". $appFunction->validSQL($city,"")."'";
			$mySQL .= ", state = '". $appFunction->validSQL($state,"")."'";
			$mySQL .= ", zipcode = '". $appFunction->validSQL($zipcode,"")."'";
			$mySQL .= ", mobile = '". $appFunction->validSQL($phone,"")."'";
			$mySQL .= ", email = '". $appFunction->validSQL($email,"")."'";
			$mySQL .= ", experience = '". $appFunction->validSQL($experience,"")."'";
			$mySQL .= ", areaofwork = '". $appFunction->validSQL($areaofwork,"")."'";
			$mySQL .= ", drivinglicense = '". $appFunction->validSQL($drivinglicense,"")."'";
			$mySQL .= ", drivinglicenseimage = '". $appFunction->validSQL($uploadImageDLName,"")."'";
			$mySQL .= ", driverinsuranceno = '". $appFunction->validSQL($driverinsuranceno,"")."'";
			$mySQL .= ", driverinsurancenoimage = '". $appFunction->validSQL($uploadImageDIName,"")."'";
			$mySQL .= ", ssnumber = '". $appFunction->validSQL($ssnumber,"")."'";
			$mySQL .= ", ssnumberimage = '". $appFunction->validSQL($uploadImageSSNName,"")."'";
			$mySQL .= ", profileimage = '". $appFunction->validSQL($uploadImagePIName,"")."'";
			$mySQL .= ", profileprivacy = '1'";
			$mySQL .= ", isactive = '". $isactive ."'";
			$mySQL .= " WHERE `iduser` = '". $appFunction->validSQL($iduser,"")."'";
			// echo $mySQL .'<hr>';			
			// exit;
			$dbAccess->queryExec($mySQL);
		}

		$mySQL = "";
		$mySQL .= "UPDATE `user` SET";
		$mySQL .= " friendlyURL = '". $appFunction->validSQL($iduser . '-' . trim($friendlyURL),"") ."'";
		$mySQL .= " WHERE `iduser` = '".$appFunction->validSQL($iduser,"")."';";
		// echo $mySQL .'<br>';			
		// exit;
		$dbAccess->queryExec($mySQL);
		
		$plowtype = $dbAccess->getDetail('plowtype','plowtype','idplowtype',$idplowtype);
		$plowsize = $dbAccess->getDetail('plowsize','plowsize','idplowsize',$idplowsize);
		
		$mySQL = "";
		$mySQL = "DELETE FROM `usertruck`";
		$mySQL .= " WHERE `iduser` = '".$appFunction->validSQL($iduser,"")."';";
		$dbAccess->queryExec($mySQL);

		$mySQL = "";
		$mySQL = "INSERT INTO `usertruck` SET";
		$mySQL .= " `iduser` = '".$appFunction->validSQL($iduser,"")."'";
		$mySQL .= ", `idplowtype` = '".$appFunction->validSQL($idplowtype,"")."'";
		$mySQL .= ", `plowtype` = '".$appFunction->validSQL($plowtype,"")."'";
		$mySQL .= ", `truckname` = '".$appFunction->validSQL($truckname,"")."'";
		$mySQL .= ", `vehiclenumber` = '".$appFunction->validSQL($vehiclenumber,"")."'";
		$mySQL .= ", `registrationcertificateimage` = '".$appFunction->validSQL($uploadImageRCName,"")."'";
		$mySQL .= ", `truckinsuranceno` = '".$appFunction->validSQL($truckinsuranceno,"")."'";
		$mySQL .= ", `truckinsuranceimage` = '".$appFunction->validSQL($uploadImageTINName,"")."'";
		$mySQL .= ", `truckimage` = '".$appFunction->validSQL($uploadImageTIName,"")."'";
		$mySQL .= ", `plowsize` = '".$appFunction->validSQL($plowsize,"")."'";
		$mySQL .= ", `rubberblade` = '".$appFunction->validSQL($rubberblade,"")."'";
		$mySQL .= ", `snowblower` = '".$appFunction->validSQL($snowblower,"")."'";
		$mySQL .= ", `saltspreader` = '".$appFunction->validSQL($saltspreader,"")."'";
		$mySQL .= ", `bobcat` = '".$appFunction->validSQL($bobcat,"")."'";
		$mySQL .= ", `shovellers` = '".$appFunction->validSQL($shovellers,"")."'";
		//$mySQL .= ", `misc` = '".$appFunction->validSQL($misc,"")."'";
		//$mySQL .= ", `isactive` = '".$appFunction->validSQL($iduser,"")."'";
		//$mySQL .= ", `createdon` = '".$appFunction->validSQL($iduser,"")."'";
		// echo $mySQL .'<br>';			
		// exit;
		$dbAccess->queryExec($mySQL);
		
		header("location:".$baseURLAdmin."/driver-list?msg=1");
		exit;
	}
	// echo '</div>';
}

if(!empty($_GET['iduser'])){
	$iduser = $_GET['iduser'];
	
	$mySQL = "";
	$mySQL .= "SELECT";
	$mySQL .= "  fullname";
	$mySQL .= ", usertype";
	$mySQL .= ", password";
	$mySQL .= ", address";
	$mySQL .= ", city";
	$mySQL .= ", state";
	$mySQL .= ", zipcode";
	$mySQL .= ", mobile";
	$mySQL .= ", email";
	$mySQL .= ", experience";
	$mySQL .= ", areaofwork";
	$mySQL .= ", drivinglicense";
	$mySQL .= ", drivinglicenseimage";
	$mySQL .= ", driverinsuranceno";
	$mySQL .= ", driverinsurancenoimage";
	$mySQL .= ", ssnumber";
	$mySQL .= ", ssnumberimage";
	$mySQL .= ", profileimage";
	$mySQL .= ", isactive";
	$mySQL .= " FROM `user`";
	$mySQL .= " WHERE iduser = '".$appFunction->validSQL($iduser,"")."'";
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	
	$fullname = $rsTemp['fullname'];
	$usertype = $rsTemp['usertype'];
	$password = $rsTemp['password'];
	$address = $rsTemp['address'];
	$city = $rsTemp['city'];
	$state = $rsTemp['state'];
	$zipcode = $rsTemp['zipcode'];
	$phone = $rsTemp['mobile'];
	$email = $rsTemp['email'];
	$experience = $rsTemp['experience'];
	$areaofwork = $rsTemp['areaofwork'];
	$drivinglicense = $rsTemp['drivinglicense'];
	$drivinglicenseimage_OLD = $rsTemp['drivinglicenseimage'];
	$driverinsuranceno = $rsTemp['driverinsuranceno'];
	$driverinsurancenoimage_OLD = $rsTemp['driverinsurancenoimage'];
	$ssnumber = $rsTemp['ssnumber'];
	$ssnumberimage_OLD = $rsTemp['ssnumberimage'];
	$profileimage_OLD = $rsTemp['profileimage'];
	$isactive = $rsTemp['isactive'];
	
	$mySQL = "";
	$mySQL = "SELECT";
	$mySQL .= " `iduser`";
	$mySQL .= ", `idplowtype`";
	$mySQL .= ", `plowtype`";
	$mySQL .= ", `truckname`";
	$mySQL .= ", `vehiclenumber`";
	$mySQL .= ", `registrationcertificateimage`";
	$mySQL .= ", `truckinsuranceno`";
	$mySQL .= ", `truckinsuranceimage`";
	$mySQL .= ", `truckimage`";
	$mySQL .= ", (SELECT `idplowsize` FROM `plowsize` WHERE `plowsize` = `usertruck`.`plowsize` LIMIT 1) AS idplowsize";
	$mySQL .= ", `plowsize`";
	$mySQL .= ", `rubberblade`";
	$mySQL .= ", `snowblower`";
	$mySQL .= ", `saltspreader`";
	$mySQL .= ", `bobcat`";
	$mySQL .= ", `shovellers`";
	$mySQL .= " FROM `usertruck`";
	$mySQL .= " WHERE iduser = '".$appFunction->validSQL($iduser,"")."'";
	// echo $mySQL;
	$rsTemp = $dbAccess->selectSingleStmt($mySQL);
	
	$idplowtype = $rsTemp['idplowtype'];
	$plowtype = $rsTemp['plowtype'];
	$idplowsize = $rsTemp['idplowsize'];
	$truckname = $rsTemp['truckname'];
	$vehiclenumber = $rsTemp['vehiclenumber'];
	$registrationcertificateimage_OLD = $rsTemp['registrationcertificateimage'];
	$truckinsuranceno = $rsTemp['truckinsuranceno'];
	$truckinsuranceimage_OLD = $rsTemp['truckinsuranceimage'];
	$truckimage_OLD = $rsTemp['truckimage'];
	$plowsize = $rsTemp['plowsize'];
	$rubberblade = $rsTemp['rubberblade'];
	$snowblower = $rsTemp['snowblower'];
	$saltspreader = $rsTemp['saltspreader'];
	$bobcat = $rsTemp['bobcat'];
	$shovellers = $rsTemp['shovellers'];
}
?>	
<div id="page-wrapper">
	<div class="row">
		<div class="col-md-12">
			<h2 align="center">Driver’s Registration</h2>
			<h3 align="center">Dryft Workers</h3>
			<div class="text-center text-danger">
			<?php 
			if (!empty($_GET['result']) AND $_GET['result'] == '1') { 
				echo 'Driver detail saved successfully';
			}

			if (in_array('DRIVER EMAIL & PHONE EXIST', $errorMsg)){
				echo 'Driver email & phone already register with us.';
			} else if(in_array('DRIVER EMAIL EXIST', $errorMsg)){
				echo 'Driver email already register with us.';
			} else if(in_array('DRIVER PHONE EXIST', $errorMsg)){
				echo 'Driver phone already register with us.';
			} else if(in_array('ANY ONE SELECT', $errorMsg)){
				echo 'Please select any one option in following (Salt Spreader, Bobcat, Shovellers, Rubber Blade, Snow Blower)';
			} 
			?>
			</div>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<form role="form" action="" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Personal Information
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<label>Full Name</label>
								<input type="text" name="fullname" id="fullname" value="<?=$fullname;?>" maxlength="250" autocomplete="off" class="form-control" placeholder="Full Name" autofocus onkeypress="return isAlpha(event)" required />
							</div>
							<div class="col-md-6">
								<label>Driver Photo</label>
								<input type="file" name="profileimage" id="profileimage" class="form-control" placeholder="Driving Photo" />

								<?php
								if(!empty($profileimage_OLD)){
									$profileimagePath = $baseURL . '/driverimage/' . $profileimage_OLD;
									// echo $profileimagePath;
								?>
									<a href="<?=$profileimagePath;?>" download><img src="<?=$profileimagePath;?>" width="50px" /></a>
								<?php
								}
								?>
								<input type="hidden" name="profileimage_OLD" id="profileimage_OLD" value="<?=$profileimage_OLD;?>" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Address</label>
								<input type="text" name="address" id="address" value="<?=$address;?>" maxlength="250" autocomplete="off" class="form-control" placeholder="Address" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>City</label>
								<input type="text" name="city" id="city" value="<?=$city;?>" maxlength="100" autocomplete="off" class="form-control" placeholder="City" onkeypress="return isAlpha(event)" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>State</label>
								<input type="text" name="state" id="state" value="<?=$state;?>" maxlength="100" autocomplete="off" class="form-control" placeholder="State" onkeypress="return isAlpha(event)" required />
							</div>
							<div class="col-md-6">
								<label>Zipcode</label>
								<input type="text" name="zipcode" id="zipcode" value="<?=$zipcode;?>" maxlength="10" autocomplete="off" class="form-control" placeholder="Zipcode" onkeypress="return isNumber(event)" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Phone</label>
								<input type="text" name="phone" id="phone" value="<?=$phone;?>" maxlength="10" autocomplete="off" class="form-control" placeholder="Phone" required onkeyup="removeErrClass('mobile');removeHide('mobile', 'errMobile');" onkeypress="return isNumber(event)" pattern="\d{10}" />
							</div>
							<div class="col-md-6">
								<label>Email</label>
								<input type="text" name="email" id="email" value="<?=$email;?>" maxlength="150" autocomplete="off" class="form-control" placeholder="Email" required onkeyup="removeErrClass('email');removeHide('email', 'errEmail');" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" />
							</div>
						</div>
					</div>	
					<!-- /.panel-body -->	
					
					<div class="panel-heading">
						Other Information
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<label>Experience</label>
								<select name="experience" id="experience" class="form-control" autocomplete="off" placeholder="Driving Experience" required />
									<option value="">Select Experience</option>
									<?php
									for($i=1;$i<70;$i++){
										if($experience == $i){
											$selected = 'SELECTED';
										} else {
											$selected = '';
										}
									?>
										<option value="<?=$i;?>" <?=$selected;?>><?=$i;?></option>
									<?php
									}
									?>
								</select>
							</div>
							<div class="col-md-6">
								<label>Area of Work</label>
								<input type="text" name="areaofwork" id="areaofwork" value="<?=$areaofwork;?>" maxlength="255" class="form-control" placeholder="Area of Wwork" autocomplete="off" onkeypress="return isAlpha(event)" required />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Driving License No.</label>
								<input type="text" name="drivinglicense" id="drivinglicense" value="<?=$drivinglicense;?>" maxlength="50" class="form-control" placeholder="Driving Experience" autocomplete="off" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>Driving License Image</label>
								<input type="file" name="drivinglicenseimage" id="drivinglicenseimage" class="form-control" placeholder="Driving License" />
								
								<?php
								if(!empty($drivinglicenseimage_OLD)){
									$drivinglicenseimagePath = $baseURL . '/driverimage/' . $drivinglicenseimage_OLD;
									// echo $drivinglicenseimagePath;
								?>
									<a href="<?=$drivinglicenseimagePath;?>" download><img src="<?=$drivinglicenseimagePath;?>" width="50px" /></a>
								<?php
								}
								?>
								<input type="hidden" name="drivinglicenseimage_OLD" id="drivinglicenseimage_OLD" value="<?=$drivinglicenseimage_OLD;?>" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Insurance No.</label>
								<input type="text" name="driverinsuranceno" id="driverinsuranceno" value="<?=$driverinsuranceno;?>" maxlength="50" class="form-control" placeholder="Insurance No." autocomplete="off" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>Insurance Image</label>
								<input type="file" name="driverinsurancenoimage" id="driverinsurancenoimage" class="form-control" placeholder="Driver Personal Insurance" />
								
								<?php
								if(!empty($driverinsurancenoimage_OLD)){
									$driverinsurancenoimagePath = $baseURL . '/driverimage/' . $driverinsurancenoimage_OLD;
									// echo $driverinsurancenoimagePath;
								?>
									<a href="<?=$driverinsurancenoimagePath;?>" download><img src="<?=$driverinsurancenoimagePath;?>" width="50px" /></a>
								<?php
								}
								?>
								<input type="hidden" name="driverinsurancenoimage_OLD" id="driverinsurancenoimage_OLD" value="<?=$driverinsurancenoimage_OLD;?>" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Social Security No.</label>
								<input type="text" name="ssnumber" id="ssnumber" value="<?=$ssnumber;?>" maxlength="50" class="form-control" placeholder="Social Security No." autocomplete="off" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>SSN Image</label>
								<input type="file" name="ssnumberimage" id="ssnumberimage" class="form-control" placeholder="Social Security No." autocomplete="off" />
								
								<?php
								if(!empty($ssnumberimage_OLD)){
									$ssnumberimagePath = $baseURL . '/driverimage/' . $ssnumberimage_OLD;
									// echo $driverinsurancenoimagePath;
								?>
									<a href="<?=$ssnumberimagePath;?>" download><img src="<?=$ssnumberimagePath;?>" width="50px" /></a>
								<?php
								}
								?>
								<input type="hidden" name="ssnumberimage_OLD" id="ssnumberimage_OLD" value="<?=$ssnumberimage_OLD;?>" />
							</div>
						</div>
					</div>	
					<!-- /.panel-body -->	
					
					<div class="panel-heading">
						Truck / Equipment
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<label>Truck Type</label>
								<input type="text" name="truckname" id="truckname" value="<?=$truckname;?>" maxlength="255" class="form-control" placeholder="Truck Type" autocomplete="off" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>Truck Image</label>
								<input type="file" name="truckimage" id="truckimage" class="form-control" placeholder="Truck Image" autocomplete="off" />
							
								<?php
								if(!empty($truckimage_OLD)){
									$truckimagePath = $baseURL . '/driverimage/' . $truckimage_OLD;
									// echo $truckimagePath;
								?>
									<a href="<?=$truckimagePath;?>" download><img src="<?=$truckimagePath;?>" width="50px" /></a>
								<?php
								}
								?>
								<input type="hidden" name="truckimage_OLD" id="truckimage_OLD" value="<?=$truckimage_OLD;?>" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Vehicle Number</label>
								<input type="text" name="vehiclenumber" id="vehiclenumber" value="<?=$vehiclenumber;?>" maxlength="25" class="form-control" placeholder="Vehicle Number" autocomplete="off" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>Registration Certificate</label>
								<input type="file" name="registrationcertificateimage" id="registrationcertificateimage" class="form-control" placeholder="Registration Certificate" />
							
								<?php
								if(!empty($registrationcertificateimage_OLD)){
									$registrationcertificateimagePath = $baseURL . '/driverimage/' . $registrationcertificateimage_OLD;
									// echo $registrationcertificateimagePath;
								?>
									<a href="<?=$registrationcertificateimagePath;?>" download><img src="<?=$registrationcertificateimagePath;?>" width="50px" /></a>
								<?php
								}
								?>
								<input type="hidden" name="registrationcertificateimage_OLD" id="registrationcertificateimage_OLD" value="<?=$registrationcertificateimage_OLD;?>" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Insurance Number</label>
								<input type="text" name="truckinsuranceno" id="truckinsuranceno" value="<?=$truckinsuranceno;?>" maxlength="25" class="form-control" placeholder="Truck Insurance No." autocomplete="off" onkeypress="return isAlphaNumeric(event)" required />
							</div>
							<div class="col-md-6">
								<label>Truck Insurance</label>
								<input type="file" name="truckinsuranceimage" id="truckinsuranceimage" class="form-control" placeholder="Truck Insurance" autocomplete="off" />
							
								<?php
								if(!empty($truckinsuranceimage_OLD)){
									$truckinsuranceimagePath = $baseURL . '/driverimage/' . $truckinsuranceimage_OLD;
									// echo $truckinsuranceimagePath;
								?>
									<a href="<?=$truckinsuranceimagePath;?>" download><img src="<?=$truckinsuranceimagePath;?>" width="50px" /></a>
								<?php
								}
								?>
								<input type="hidden" name="truckinsuranceimage_OLD" id="truckinsuranceimage_OLD" value="<?=$truckinsuranceimage_OLD;?>" />
							</div>
						</div>
						
					</div>
					<div class="panel-heading">
						Truck Other Detail
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<label>Plow Type</label>
								<?php
								$mySQL = "";
								$mySQL = "SELECT `idplowtype`, `plowtype` FROM `plowtype` WHERE 1=1 AND  isactive = '1'"; 
								$mySQL .= " ORDER BY plowtype";
								//echo $mySQL;
								$rsTemp = $dbAccess->selectData($mySQL);
								
								$selected = '';
								?>
								<select name="idplowtype" id="idplowtype" class="form-control" required>
									<option value="">Select</option>
									<?php
									if (!empty($rsTemp)){
										foreach($rsTemp AS $rsTempVal){
											if(!empty($idplowtype)){
												if($rsTempVal['idplowtype'] == $idplowtype){
													$selected = 'SELECTED';
												} else {
													$selected = '';
												}
											}
									?>
											<option value="<?=$rsTempVal['idplowtype'];?>" <?=$selected;?>><?=$rsTempVal['plowtype'];?></option>
									<?php
										}
									}
									?>
								</select>
							</div>
							<div class="col-md-6">
								<label>Plow Size</label>
								<?php
								$mySQL = "";
								$mySQL = "SELECT `idplowsize`, `plowsize` FROM `plowsize` WHERE 1=1 AND  isactive = '1'"; 
								$mySQL .= " ORDER BY plowsize";
								//echo $mySQL;
								$rsTemp = $dbAccess->selectData($mySQL);
								
								$selected = '';
								?>
								<select name="idplowsize" id="idplowsize" class="form-control" required>
									<option value="">Select</option>
									<?php
									if (!empty($rsTemp)){
										foreach($rsTemp AS $rsTempVal){
											if(!empty($idplowsize)){
												if($rsTempVal['idplowsize'] == $idplowsize){
													$selected = 'SELECTED';
												} else {
													$selected = '';
												}
											}
									?>
											<option value="<?=$rsTempVal['idplowsize'];?>" <?=$selected;?>><?=$rsTempVal['plowsize'];?></option>
									<?php
										}
									}
									?>
								</select>								
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="checkbox">
									<label><input type="checkbox" name="saltspreader" id="saltspreader" value="CHECKED" <?=$saltspreader;?> /> Salt Spreader</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" name="rubberblade" id="rubberblade" value="CHECKED" <?=$rubberblade;?> /> Rubber Blade</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="checkbox">
									<label><input type="checkbox" name="bobcat" id="bobcat" value="CHECKED" <?=$bobcat;?> /> Bobcat</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" name="snowblower" id="snowblower" value="CHECKED" <?=$snowblower;?> /> Snow Blower</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="checkbox">
									<label><input type="checkbox" name="shovellers" id="shovellers" value="CHECKED" <?=$shovellers;?> /> Shovellers</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Status</label>
								<select name="isactive" id="isactive" class="form-control" required>
									<option value="1" <?php if ($isactive == '1'){ echo 'selected'; }?>>Active</option>									
									<option value="0" <?php if ($isactive == '0'){ echo 'selected'; }?>>Inactive</option>
								</select>
							</div>
						</div>
					</div>
					<!-- /.panel-body -->	
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group text-center">
								<input type="hidden" name="iduser" id="iduser" value="<?=$iduser;?>" />
									<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
								</div>
								<!-- /.col-lg-6 (nested) -->
							</div>
						</div>
					</div>
					<!-- /.row (nested) -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</form>
</div>
<?php
include 'include/_footer_.php';
?>